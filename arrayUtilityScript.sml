open HolKernel boolLib bossLib Parse wordsLib;

val _ = new_theory "arrayUtility";

(* ======================================================Generalized Definitions=============================================== *)
val GENARRAY_def = Define`
    GENARRAY (f:'a->'b) =
      let ayName = f
      in ayName
`;

val AYMAP_def = Define
    `AYMAP f b =
	(\x. f (b x))
    `;

(* val array_fold_inner_def = tDefine  *)
(*     "array_fold_inner"  *)
(*     `array_fold_inner (f:('a#'b)-> ('a)) (i:'a) (b:bool['c]->'b) (idx:num) = *)
(* 	 if (idx >= dimword(:'c)) then f (i, b(n2w(idx):bool['c])) *)
(* 	 else array_fold_inner f (f(i, b(n2w(idx):bool['c]))) b (idx+1) *)
(*      ` *)
(*     (WF_REL_TAC `measure ( *)
(* 		  \ (f,i,b,idx). (dimword(:'c) - idx) *)
(* 		  ) *)
(*     `); *)





val AYFIND_def = tDefine
"AYFIND"
    `AYFIND (f:'a -> bool) (b:bool['b]->'a) (idx:num) =
	 if (idx >= dimword(:'b)) then n2w(dimword(:'b)):bool['b]
	 else if ((idx < dimword(:'b)) /\ (f (b(n2w(idx):bool['b])))) then  n2w(idx):bool['b]
	 else AYFIND f b (idx+1)
     `
    (WF_REL_TAC `measure (
		  \ (f,b,idx). (dimword(:'b) - idx)
		  )
    `);
val AYEXIST_def = tDefine
"AYEXIST"
    `AYEXIST (f:'a -> bool) (b:bool['b]->'a) (idx:num) =
	 if (idx >= dimword(:'b)) then F
	 else if ((idx < dimword(:'b)) /\ (f (b(n2w(idx):bool['b])))) then  T
	 else AYEXIST f b (idx+1)
     `
    (WF_REL_TAC `measure (
		  \ (f,b,idx). (dimword(:'b) - idx)
		  )
    `);
val AYINSERT_def = Define`
    AYINSERT (a:bool['a]->'b) ((idx:bool['a]),(value:'b)) =
        (\x. if (x<>idx) then a(x) else value)
`;




val array_fold_inner_def = tDefine
    "array_fold_inner"
    `array_fold_inner (f:'a->'b->'a) (i:'a) (b:bool['c]->'b) (idx:num) =
	 if (idx >= dimword(:'c)) then (f i (b(n2w(idx):bool['c])))
	 else array_fold_inner f (f i (b(n2w(idx):bool['c]))) b (idx+1)
     `
    (WF_REL_TAC `measure (
		  \ (f,i,b,idx). (dimword(:'c) - idx)
		  )
    `);
    
val AYFOLDL_def = Define
`AYFOLDL (f:'a->'b->'a) (i:'a) (b:bool['c]->'b) =
     array_fold_inner f i b 0
`;

val _ = export_theory ();

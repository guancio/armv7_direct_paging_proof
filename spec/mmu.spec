---------------------------------------------
-- Types
---------------------------------------------

type wordT = bits(32)
type byteT  = bits(8)
type addrT = wordT
type memT  = addrT -> byteT


register l1FaultT :: bits(32)
{
  1-0:		typ
}

register l1PTT :: bits(32)
{
  31-10:	addr
  8-5:		dom
  4:		sbz
  3:		ns
  2:		pxn
  1-0:		typ
}
register l1SecT :: bits(32)
{
  31-20:	addr
  19:		ns
  18,1:		typ
  17:		ng
  16:		s
  15,11-10:	ap
  14-12:	tex
  8-5:		dom
  4:		xn
  3:		c
  2:		b
  0:		pxn
}
register l1SupSecT :: bits(32)
{
  31-24:	addr
  23-20:	exaddr
  19:		ns
  18,1:		typ
  17:		ng
  16:		s
  15,11-10:	ap
  14-12:	tex
  8-5:		dom
  4:		xn
  3:		c
  2:		b
  0:		pxn
}

register l2LargeT :: bits(32)
{
  31-16:	addr
  15:		xn
  14-12:	tex
  11:		ng
  10:		s
  9,5-4:	ap
  3:		c
  2:		b
}
register l2SmallT :: bits(32)
{
  31-12:	addr
  11:		ng
  10:		s
  9,5-4:	ap
  8-6:		tex
  3:		c
  2:		b
  0:		xn
}

register sctlrT :: bits(32)
{
  0:		M
}




addrT mmu_tbl_base_addr (TTBR0::wordT) = {
    (TTBR0 && 0xFFFFC000)
}

wordT mmu_tbl_index(add::addrT) = {
    (add >>+ 20)
}

wordT mmu_l1_decode_type (desc::wordT) = {
    (desc && 0b11)
}

bits(2) mmu_domain_status (c3::wordT, domain::bits(4)) = {
    dom_s::wordT = ZeroExtend(domain);
    dom_s = dom_s * 2;
    (c3 >>+ dom_s)<1:0>
}

(bool*bool) mmu_decode_l1_ap (ap::bits(3), PL1::bool) = {
    if      ap == 0b000 then (false,false)
    else if ap == 0b001 then (PL1,PL1)
    else if ap == 0b010 then (true,PL1)
    else if ap == 0b011 then (true,true)
    else if ap == 0b101 then (PL1,false)
    else if ap == 0b110 then (true,false)
    else if ap == 0b111 then (true,false)
    else                     (false,false)
}

(bool*bool*bool*bool) mmu_check_access (ap::bits(3), xn::bool, domain_status::bits(2), PL1::bool) = {
  -- disabled 
   if domain_status == 0b00 then
     (true, false, false, false)
  -- manager 
   else if domain_status == 0b11 then
     (true, true, true, true)
  -- reserved 
   else if domain_status == 0b10 then
     (false, true, true, true)
  -- reserved 
  else if ap == 0b100 then 
     (false, true, true, true)
  else {
     (rd,wt) = mmu_decode_l1_ap(ap, PL1);
     (true, rd, wt, rd and (!xn))
  }
}

(bool*bool*bool*bool*addrT) mmu_byte_section (
    l1_desc::l1SecT,
    c3::wordT,
    PL1::bool,
    add::addrT) = {
    l1_sec_add::wordT = ZeroExtend(l1_desc.addr);
    l1_sec_add = l1_sec_add << 20;
    sec_index = (add && 0xFFFFF);
    domain_status = mmu_domain_status(c3,l1_desc.dom);
    ph_add = l1_sec_add || sec_index;
    (sup, rd, wt, ex) = mmu_check_access(l1_desc.ap, l1_desc.xn, domain_status, PL1);
    (sup, rd, wt, ex, ph_add)
}

(bool*bool*bool*bool*addrT) mmu_byte_supsection (
    l1_desc::l1SupSecT,
    c3::wordT,
    PL1::bool,
    add::addrT) = {
    l1_supsec_add::wordT = ZeroExtend(l1_desc.addr);
    l1_supsec_add = l1_supsec_add << 24;
    supsec_index = (add && 0xFFFFFF);
    domain_status = mmu_domain_status(c3,l1_desc.dom);
    ph_add = l1_supsec_add || supsec_index;
    (sup, rd, wt, ex) = mmu_check_access(l1_desc.ap, l1_desc.xn, domain_status, PL1);
    (sup, rd, wt, ex, ph_add)
}

(bool*bool*bool*bool*addrT) mmu_byte_large_page (l2_desc::l2LargeT, domain_status::bits(2), PL1::bool, add::addrT) = {
  l2_large_page_add::addrT = ZeroExtend(l2_desc.addr);
  l2_large_page_add = l2_large_page_add << 16;
  large_page_index = (add && 0xFFFF);
  ph_add = l2_large_page_add || large_page_index;
  (sup, rd, wt, ex) = mmu_check_access(l2_desc.ap, l2_desc.xn, domain_status, PL1);
  (sup, rd, wt, ex, ph_add)
}

(bool*bool*bool*bool*addrT) mmu_byte_small_page (l2_desc::l2SmallT, domain_status::bits(2), PL1::bool, add::addrT) = {
  l2_small_page_add::addrT = ZeroExtend(l2_desc.addr);
  l2_small_page_add = l2_small_page_add << 12;
  small_page_index = (add && 0xFFF);
  ph_add = l2_small_page_add || small_page_index;
  (sup, rd, wt, ex) = mmu_check_access(l2_desc.ap, l2_desc.xn, domain_status, PL1);
  (sup, rd, wt, ex, ph_add)
}


(bool*bool*bool*bool*addrT) mmu_byte_l2 (tbl_base::addrT, domain_status::bits(2), mem::memT, PL1::bool, add::addrT) = {
  tbl_index = (add && 0xff000) >>+ 12;
  desc_add = (tbl_base || (tbl_index << 2));
  l2_desc = read_mem32(desc_add, mem);
  l2_type = (l2_desc && 0b11);
  -- Fault
  if l2_type == 0b00 then (true, false, false, false, add)
  -- Large Page 
  else if l2_type == 0b01 then
     mmu_byte_large_page(l2LargeT(l2_desc), domain_status, PL1, add)
  else 
     mmu_byte_small_page(l2SmallT(l2_desc), domain_status, PL1, add)
}


(bool*bool*bool*bool*addrT) mmu_byte_pt (l1_desc::l1PTT, c3::wordT, mem::memT,
	 PL1::bool,
	 add::addrT) = {
  l2_pt_base_add::addrT = ZeroExtend(l1_desc.addr);
  l2_pt_base_add = l2_pt_base_add << 10;
  domain_status = mmu_domain_status(c3,l1_desc.dom);
  if l1_desc.pxn then (false, true, true, true, add)
  else mmu_byte_l2(l2_pt_base_add, domain_status, mem, PL1, add)
}

(bool*bool*bool*bool*addrT) mmu_byte (
	 c1::sctlrT,
	 c2::wordT,
	 c3::wordT,
	 mem::memT,
	 PL1::bool,
	 add::addrT) = {
 if !c1.M then
   (true,true,true,true,add)
 else {
   tbl_base = mmu_tbl_base_addr(c2);
   tbl_index = mmu_tbl_index(add);
   desc_add = (tbl_base || (tbl_index << 2));
   l1_desc = read_mem32(desc_add, mem);
   l1_type = mmu_l1_decode_type(l1_desc);
   -- Unmapped
   if l1_type == 0b00 then
      (true, false, false, false, add)
   -- Page fault if no large phisical address extension
   else if l1_type == 0b11 then
      (true, false, false, false, add)
   else {
      l1_bit_18 = ((l1_desc && 0x40000) >>+ 18);
      -- Section
      if ((l1_type == 0b10) and (l1_bit_18 == 0)) then
         mmu_byte_section(l1SecT(l1_desc),c3,PL1,add)
      -- super-Section
      else if ((l1_type == 0b10) and (l1_bit_18 == 1)) then
	 mmu_byte_supsection(l1SupSecT(l1_desc),c3,PL1,add)
      -- L2 Page table, l1_type = 0b10
      else
         mmu_byte_pt(l1PTT(l1_desc),c3,mem,PL1,add)
   }
 }
}


structure guest_write_accessTheory :> guest_write_accessTheory =
struct
  val _ = if !Globals.print_thy_loads then print "Loading guest_write_accessTheory ... " else ()
  open Type Term Thm
  infixr -->

  fun C s t ty = mk_thy_const{Name=s,Thy=t,Ty=ty}
  fun T s t A = mk_thy_type{Tyop=s, Thy=t,Args=A}
  fun V s q = mk_var(s,q)
  val U     = mk_vartype
  (*  Parents *)
  local open guest_write_access_utilsTheory
  in end;
  val _ = Theory.link_parents
          ("guest_write_access",
          Arbnum.fromString "1387541286",
          Arbnum.fromString "522149")
          [("guest_write_access_utils",
           Arbnum.fromString "1387541258",
           Arbnum.fromString "319939")];
  val _ = Theory.incorporate_types "guest_write_access" [];

  val idvector = 
    let fun ID(thy,oth) = {Thy = thy, Other = oth}
    in Vector.fromList
  [ID("tiny", "sctlrT"), ID("fcp", "cart"), ID("fcp", "bit0"),
   ID("one", "one"), ID("min", "bool"), ID("min", "fun"),
   ID("fcp", "bit1"), ID("bool", "!"), ID("num", "0"), ID("num", "num"),
   ID("min", "="), ID("min", "==>"), ID("arithmetic", "BIT1"),
   ID("arithmetic", "BIT2"), ID("bool", "F"), ID("bool", "LET"),
   ID("pair", "prod"), ID("arithmetic", "NUMERAL"), ID("pair", "UNCURRY"),
   ID("arithmetic", "ZERO"), ID("hypervisor_model", "invariant_ref_cnt"),
   ID("mmu_properties", "mmu_phi_byte"), ID("words", "n2w"),
   ID("tiny", "rec'sctlrT"), ID("words", "w2w"), ID("words", "word_lsr"),
   ID("bool", "~")]
  end;
  local open SharingTables
  in
  val tyvector = build_type_vector idvector
  [TYOP [0], TYOP [3], TYOP [2, 1], TYOP [2, 2], TYOP [2, 3], TYOP [2, 4],
   TYOP [2, 5], TYOP [4], TYOP [1, 7, 6], TYOP [1, 7, 4], TYOP [5, 8, 9],
   TYOP [6, 1], TYOP [6, 11], TYOP [6, 12], TYOP [2, 13], TYOP [1, 7, 14],
   TYOP [6, 2], TYOP [2, 16], TYOP [2, 17], TYOP [1, 7, 18],
   TYOP [5, 19, 15], TYOP [1, 7, 2], TYOP [5, 19, 21], TYOP [5, 8, 7],
   TYOP [5, 23, 7], TYOP [5, 19, 7], TYOP [5, 25, 7], TYOP [5, 0, 7],
   TYOP [5, 27, 7], TYOP [9], TYOP [5, 19, 25], TYOP [5, 21, 7],
   TYOP [5, 21, 31], TYOP [5, 0, 27], TYOP [5, 7, 7], TYOP [5, 7, 34],
   TYOP [5, 29, 29], TYOP [16, 7, 7], TYOP [16, 7, 37], TYOP [16, 7, 38],
   TYOP [5, 39, 7], TYOP [5, 40, 40], TYOP [5, 37, 7], TYOP [5, 35, 42],
   TYOP [5, 38, 7], TYOP [5, 7, 42], TYOP [5, 45, 44], TYOP [5, 7, 44],
   TYOP [5, 47, 40], TYOP [5, 20, 7], TYOP [5, 22, 49], TYOP [5, 10, 50],
   TYOP [5, 8, 51], TYOP [5, 8, 52], TYOP [5, 0, 53], TYOP [5, 8, 39],
   TYOP [5, 7, 55], TYOP [5, 10, 56], TYOP [5, 8, 57], TYOP [5, 8, 58],
   TYOP [5, 0, 59], TYOP [5, 29, 8], TYOP [5, 29, 21], TYOP [5, 8, 0],
   TYOP [5, 8, 19], TYOP [5, 8, 61]]
  end
  val _ = Theory.incorporate_consts "guest_write_access" tyvector [];

  local open SharingTables
  in
  val tmvector = build_term_vector idvector tyvector
  [TMV("c1", 0), TMV("c2", 8), TMV("c3", 8), TMV("ex", 7), TMV("mem", 10),
   TMV("pgrefs", 20), TMV("pgtype", 22), TMV("ph_add", 8),
   TMV("ph_frame", 19), TMV("rd", 7), TMV("und", 7), TMV("wt", 7),
   TMC(7, 24), TMC(7, 26), TMC(7, 28), TMC(8, 29), TMC(10, 30),
   TMC(10, 32), TMC(10, 33), TMC(11, 35), TMC(12, 36), TMC(13, 36),
   TMC(14, 7), TMC(15, 41), TMC(17, 36), TMC(18, 43), TMC(18, 46),
   TMC(18, 48), TMC(19, 29), TMC(20, 54), TMC(21, 60), TMC(22, 61),
   TMC(22, 62), TMC(23, 63), TMC(24, 64), TMC(25, 65), TMC(26, 34)]
  end
  local
  val DT = Thm.disk_thm val read = Term.read_raw tmvector
  in
  val op guest_write_access_thm =
    DT(["DISK_THM"],
       [read"(%14 (|%0. (%12 (|%7. (%13 (|%8. ((%19 ((%18 $2) (%33 (%31 (%24 (%20 %28)))))) ((%19 ((%16 $0) (%34 ((%35 $1) (%24 (%21 (%20 (%21 %28)))))))) ((%19 (%36 ((%17 (%6 $0)) (%32 %15)))) ((%19 ((((((%29 $2) %1) %2) %4) %6) %5)) ((%23 (%27 (|%10. (%26 (|%9. (%25 (|%11. (|%3. (%36 $1))))))))) ((((((%30 $2) %1) %2) %4) %22) $1))))))))))))"])
  end
  val _ = DB.bindl "guest_write_access"
  [("guest_write_access_thm",guest_write_access_thm,DB.Thm)]

  local open Portable GrammarSpecials Parse
    fun UTOFF f = Feedback.trace("Parse.unicode_trace_off_complaints",0)f
  in
  val _ = mk_local_grms [("guest_write_access_utilsTheory.guest_write_access_utils_grammars",
                          guest_write_access_utilsTheory.guest_write_access_utils_grammars)]
  val _ = List.app (update_grms reveal) []

  val guest_write_access_grammars = Parse.current_lgrms()
  end

val _ = if !Globals.print_thy_loads then print "done\n" else ()
val _ = Theory.load_complete "guest_write_access"
end

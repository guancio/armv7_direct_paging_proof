open HolKernel boolLib bossLib Parse wordsLib;
open hypervisor_modelTheory;
open mmu_propertiesTheory;
open tinyTheory;
open helperTheory;
open guest_write_access_utilsTheory;

load "mmu_utilsLib";
open mmu_utilsLib;

load "guest_write_accessLib";
open guest_write_accessLib;

val _ = new_theory "hypSwitch";

val hyperSwitch_goal = ``
  let c1 = rec'sctlrT 1w in
      (invariant_ref_cnt c1 c2 c3 mem pgtype pgrefs) ==>  
  let (c1',c2',c3',mem',pgtype', pgrefs') = 
      (hyper_switch (c1, c2, c3, mem, pgtype, pgrefs) param_c2) in 
      (invariant_ref_cnt c1' c2' c3' mem' pgtype' pgrefs') 
``;

val hyperSwitch_thm = prove(
  ``^hyperSwitch_goal``,
  (RW_TAC (srw_ss()) [])
  THEN (FULL_SIMP_TAC (srw_ss()) [hyper_switch_def])
  THEN (Cases_on `param_c2 ≠ (0xFFFFC000w && param_c2)`)
  THENL [
  (* The address is not 16k aligned *)
          (FULL_SIMP_TAC (srw_ss()) [])
     THEN (RW_TAC (srw_ss()) []),
	  (FULL_SIMP_TAC (srw_ss()) [])
     THEN (Cases_on `pgtype (0xFFFFFw && w2w (param_c2 ⋙ 12)) ≠ 1w`)
     THENL [
  (* the address is not marked as an l1 page *)
          (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (RW_TAC (srw_ss()) []),
  (* the hypercall successes *)
	  (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (FULL_SIMP_TAC (srw_ss()) [invariant_ref_cnt_def])
	  THEN (RW_TAC (srw_ss()) [])
	  THEN (SIMP_TAC (srw_ss()) [invariant_page_type_def])
	  (* First we prove that the mmu is active *)
	  THEN (FULL_SIMP_TAC (srw_ss()) [rec'sctlrT_def, Abbr `c1`])
	  THEN INSTANTIATE_INVARIANT_CURRENT_L1_TAC
	  (* This check the domains, which have not been changed *)
	  THEN (FULL_SIMP_TAC (srw_ss()) [])
	  (* Now the invariant on page contents *)
	  THEN (SIMP_TAC (srw_ss()) [LET_DEF])
	  THEN (SIMP_TAC (srw_ss()) [mmu_tbl_base_addr_def])
	  (* Different byte expressions on c2' *)
	  THEN (REWRITE_TAC [ UNDISCH_ALL (
	   blastLib.BBLAST_PROVE ``
            (c2' = 0xFFFFC000w && c2') ==> (
            (w2w ((0xFFFFCw :word32) && (c2' :word32) ⋙ (12 :num)) :word20) =
            ((0xFFFFFw :word20) && (w2w ((c2' :word32) ⋙ (12 :num)) :word20))
            )``)
          ])
	  THEN (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def])
	  THEN (LET_HYP_ELIM_TAC [])
	  THEN (RW_TAC (srw_ss()) [])
     ]
  ]
);

val _ = export_theory ();

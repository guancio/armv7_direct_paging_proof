(* 
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/spec/" :: !loadPath;
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/" ::  !loadPath;
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/proof/" :: !loadPath;
load "mmu_utilsLib";
load "hypervisor_modelTheory";
load "mmu_propertiesTheory";
load "helperTheory";
*)



open HolKernel boolLib bossLib Parse wordsLib;
open hypervisor_modelTheory;
open mmu_propertiesTheory;
open tinyTheory;
open helperTheory;

open sum_numTheory;
open wordsTheory;

load "mmu_utilsLib";
open mmu_utilsLib;
load "helperLib";
open helperLib;
open numSyntax;

val _ = new_theory "L1create";

(* start *)




val l1_create_exception_case_tac = 
TRY (SYM_ASSUMPTION_TAC ``w = create_l1_pt x y z``)
THEN THM_KEEP_TAC ``create_l1_pt x y z = w`` (
  (FULL_SIMP_TAC (srw_ss()) [create_l1_pt_def])
  THEN (Cases_on ` pa ≠ (0xFFFFC000w && pa)`)
  THENL [
     (FULL_SIMP_TAC (srw_ss()) []),
     (FULL_SIMP_TAC (srw_ss()) [])
     THEN (Q.ABBREV_TAC `pointed_page:bool[20] = w2w (pa ⋙ 12)`)
     THEN (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF])
     THEN (Q.ABBREV_TAC `already_l1 =
        (pgtype (pointed_page) = 1w) ∧
        (pgtype (pointed_page + 1w) = 1w) ∧
        (pgtype (pointed_page + 2w) = 1w) ∧
        (pgtype (pointed_page + 3w) = 1w)`)
     THEN (Cases_on `already_l1`)
     THENL [
       (FULL_SIMP_TAC (srw_ss()) []),
       (FULL_SIMP_TAC (srw_ss()) [])
       THEN (Q.ABBREV_TAC `some_l2 =
        (pgtype (pointed_page) = 2w) ∨
        (pgtype (pointed_page + 1w) = 2w) ∨
        (pgtype (pointed_page + 2w) = 2w) ∨
        (pgtype (pointed_page + 3w) = 2w)`)
       THEN (Cases_on `some_l2`)
       THENL [
         (FULL_SIMP_TAC (srw_ss()) []),
	 (FULL_SIMP_TAC (srw_ss()) [])
	 THEN (Q.ABBREV_TAC `some_used_data =
           ((pgtype (pointed_page) <> 0w) \/
            pgrefs (pointed_page) <> 0w) ∨
           ((pgtype (pointed_page + 1w) <> 0w) \/
            pgrefs (pointed_page + 1w) <> 0w) ∨
           ((pgtype (pointed_page + 2w) <> 0w) \/
            pgrefs (pointed_page + 2w) <> 0w) ∨
           ((pgtype (pointed_page + 3w) <> 0w) \/
            pgrefs (pointed_page + 3w) <> 0w)`)
	 THEN (Cases_on `some_used_data`)
	 THENL [
	   (FULL_SIMP_TAC (srw_ss()) []),
	   (FULL_SIMP_TAC (srw_ss()) [])
	   THEN (Q.ABBREV_TAC `l1_safe =
	   ∀l1Idx.
           l1Idx <₊ 4096w ⇒
           (let l1_desc_add = 0xFFFFC000w && pa ‖ l1Idx ≪ 2 in
            let l1_desc = read_mem32 (l1_desc_add,mem) in
            let l1_type = mmu_l1_decode_type l1_desc
            in
              (l1_type = 0w) ∨
              (l1_type = 1w) ∧
              (let l1_pg_desc = rec'l1PTT l1_desc
               in
                 (pgtype (w2w (l1_pg_desc.addr ⋙ 2)) = 2w) ∧
                 pgrefs (w2w (l1_pg_desc.addr ⋙ 2)) <₊ 0x3FFFEFFFw ∧
                 ¬l1_pg_desc.pxn) ∨
              (l1_type = 2w) ∧
              (let l1_sec_desc = rec'l1SecT l1_desc
               in
                 (l1_sec_desc.typ = 1w) ∧
                 ((l1_sec_desc.ap = 2w) ∨ (l1_sec_desc.ap = 3w)) ∧
                 ((l1_sec_desc.ap = 3w) ⇒
                  (∀secIdx:bool[8].
                     (pgtype (w2w secIdx ‖ w2w l1_sec_desc.addr ≪ 8) =
                      0w) ∧
                     (0xFFFFCw &&
                      (w2w secIdx ‖ w2w l1_sec_desc.addr ≪ 8)) ≠
                     pointed_page /\
                    ¬(pgrefs (w2w secIdx ‖ w2w l1_sec_desc.addr ≪ 8) ≥₊
                       0x3FFFEFFFw)
		  ) ∧
                  (0xFFF00000w && pa = w2w l1_sec_desc.addr ≪ 20))))
	  `)
	   THEN (Cases_on `~l1_safe`)
	   THENL [
	     (FULL_SIMP_TAC (srw_ss()) []),
	     (FULL_SIMP_TAC (srw_ss()) [])
	   ]
	 ]
       ]
     ]
  ]);





val pgtype_update1_thm = prove(``
   !ph_page ph_page'.
   (pgtype':bool[20]->bool[2] =
   (ph_page + 3w =+ 1w)
              ((ph_page + 2w =+ 1w)
                 ((ph_page + 1w =+ 1w)
                    ((ph_page =+ 1w) pgtype)))) ==>
   (pgtype ph_page <> 2w) ==>
   (pgtype (ph_page+1w) <> 2w) ==>
   (pgtype (ph_page+2w) <> 2w) ==>
   (pgtype (ph_page+3w) <> 2w) ==>
   ((pgtype' ph_page' = 2w) ==> (pgtype ph_page' = 2w))
``,
   (RW_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM])
);

val pgtype_update2_thm = prove(``
   !ph_page ph_page'.
   (pgtype':bool[20]->bool[2] =
   (ph_page + 3w =+ 1w)
              ((ph_page + 2w =+ 1w)
                 ((ph_page + 1w =+ 1w)
                    ((ph_page =+ 1w) pgtype)))) ==>
   (pgtype ph_page <> 2w) ==>
   (pgtype (ph_page+1w) <> 2w) ==>
   (pgtype (ph_page+2w) <> 2w) ==>
   (pgtype (ph_page+3w) <> 2w) ==>
   (pgtype ph_page' = 2w) ==>
   (pgtype' ph_page' = 2w)
``,
   (RW_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM])
);

val pgtype_update3_thm = prove(``
   !ph_page ph_page'.
   (pgtype':bool[20]->bool[2] =
   (ph_page + 3w =+ 1w)
              ((ph_page + 2w =+ 1w)
                 ((ph_page + 1w =+ 1w)
                    ((ph_page =+ 1w) pgtype)))) ==>
   (pgtype ph_page' <> 0w) ==>
   (pgtype' ph_page' <> 0w)
``,
   (RW_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM])
);

val pgtype_update31_thm = prove(``
   !ph_page ph_page'.
   ((ph_page + 3w =+ 1w)
              ((ph_page + 2w =+ 1w)
                 ((ph_page + 1w =+ 1w)
                    ((ph_page =+ 1w) pgtype))) =pgtype':bool[20]->bool[2]
   ) ==>
   (pgtype ph_page' <> 2w) ==>
   (pgtype' ph_page' <> 2w)
``,
   (RW_TAC (srw_ss()) [])
   THEN (RW_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM, Once COND_RAND, Once COND_RATOR])
);

val pgtype_update_l1_1thm = prove(``
   !ph_page ph_page'.
   (pgtype':bool[20]->bool[2] =
   (ph_page + 3w =+ 1w)
              ((ph_page + 2w =+ 1w)
                 ((ph_page + 1w =+ 1w)
                    ((ph_page =+ 1w) pgtype)))) ==>
   (ph_page <> ph_page') ==>
   (0xFFFFCw && ph_page = ph_page) ==>
   (0xFFFFCw && ph_page' = ph_page') ==>
   (pgtype' ph_page' = 1w) ==>
   (pgtype ph_page' = 1w)
``,
   REPEAT STRIP_TAC
   THEN (ASSUME_TAC (UNDISCH_ALL (blastLib.BBLAST_PROVE ``
(0xFFFFCw && ph_page = ph_page:bool[20]) ==>
(0xFFFFCw && ph_page' = ph_page') ==>
(ph_page <> ph_page') ==>
((ph_page+1w <> ph_page') /\ (ph_page+2w <> ph_page') /\ (ph_page+3w <> ph_page'))
``)))
   THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype':bool[20]->bool[2] = f``)
   THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM])
   THEN (REPEAT (
	 (FULL_SIMP_TAC (srw_ss()) [Once COND_RAND])
         THEN (FULL_SIMP_TAC (srw_ss()) [Once COND_RATOR])))
);

val pgtype_update_l1_2_thm = prove(``
   !ph_page ph_page'.
   ((ph_page + 3w =+ 1w)
              ((ph_page + 2w =+ 1w)
                 ((ph_page + 1w =+ 1w)
                    ((ph_page =+ 1w) pgtype)))
    = pgtype':bool[20]->bool[2]
   ) ==>
   (ph_page <> ph_page') ==>
   (0xFFFFCw && ph_page = ph_page) ==>
   (0xFFFFCw && ph_page' = ph_page') ==>
   (pgtype (ph_page' !! 1w) = 1w) ==>
   (pgtype (ph_page' !! 2w) = 1w) ==>
   (pgtype (ph_page' !! 3w) = 1w) ==>
   ((pgtype' (ph_page' !! 1w) = 1w) /\
   (pgtype' (ph_page' !! 2w) = 1w) /\
   (pgtype' (ph_page' !! 3w) = 1w))
``,
   REPEAT GEN_TAC
   THEN (REPEAT DISCH_TAC)
   THEN (ASSUME_TAC (UNDISCH_ALL (blastLib.BBLAST_PROVE ``
(0xFFFFCw && ph_page = ph_page:bool[20]) ==>
(0xFFFFCw && ph_page' = ph_page') ==>
(ph_page <> ph_page') ==>
((ph_page+1w <> (ph_page'!!1w)) /\ (ph_page+2w <> (ph_page'!!1w)) /\ (ph_page+3w <> (ph_page'!!1w)) /\
 (ph_page+1w <> (ph_page'!!2w)) /\ (ph_page+2w <> (ph_page'!!2w)) /\ (ph_page+3w <> (ph_page'!!2w)) /\
 (ph_page+1w <> (ph_page'!!3w)) /\ (ph_page+2w <> (ph_page'!!3w)) /\ (ph_page+3w <> (ph_page'!!3w))
)
``)))
   THEN (SYM_ASSUMPTION_TAC ``pgtype':bool[20]->bool[2] = f``)
   THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM])
);

val pgtype_update_l1_3_thm = prove(``
   !ph_page.
   ((ph_page + 3w =+ 1w)
              ((ph_page + 2w =+ 1w)
                 ((ph_page + 1w =+ 1w)
                    ((ph_page =+ 1w) pgtype)))
    = pgtype':bool[20]->bool[2]
   ) ==>
   (0xFFFFCw && ph_page = ph_page) ==>
   ((pgtype' (ph_page !! 1w) = 1w) /\
   (pgtype' (ph_page !! 2w) = 1w) /\
   (pgtype' (ph_page !! 3w) = 1w))
``,
   GEN_TAC
   THEN (REPEAT DISCH_TAC)
   THEN (ASSUME_TAC (UNDISCH_ALL (blastLib.BBLAST_PROVE ``
(0xFFFFCw && ph_page = ph_page:bool[20]) ==>
((ph_page+1w = (ph_page!!1w)) /\ (ph_page+2w = (ph_page!!2w)) /\ (ph_page+3w = (ph_page!!3w)))
``)))
   THEN (SYM_ASSUMPTION_TAC ``pgtype':bool[20]->bool[2] = f``)
   THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM])
);

val pgtype_update_l1_4_thm = prove(``
   !ph_page ph_page'.
   ((ph_page + 3w =+ 1w)
              ((ph_page + 2w =+ 1w)
                 ((ph_page + 1w =+ 1w)
                    ((ph_page =+ 1w) pgtype)))
    = pgtype':bool[20]->bool[2]
   ) ==>
   (0xFFFFCw && ph_page = ph_page) ==>
   ((0xFFFFCw && ph_page') ≠ ph_page) ==>
   (pgtype (ph_page') = 0w) ==>
   (pgtype' (ph_page') = 0w)
``,
   REPEAT GEN_TAC
   THEN (REPEAT DISCH_TAC)
   THEN (ASSUME_TAC (UNDISCH_ALL (blastLib.BBLAST_PROVE ``
(0xFFFFCw && ph_page = ph_page:bool[20]) ==>
((0xFFFFCw && ph_page') <> ph_page:bool[20]) ==>
((ph_page <> ph_page') /\ (ph_page+1w <> ph_page') /\ (ph_page+2w <> ph_page') /\ (ph_page+3w <> ph_page'))
``)))
   THEN (SYM_ASSUMPTION_TAC ``pgtype':bool[20]->bool[2] = f``)
   THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM])
);

val pgtype_update_l1_5_thm = prove(``
   !ph_page ph_page'.
   ((ph_page + 3w =+ 1w)
              ((ph_page + 2w =+ 1w)
                 ((ph_page + 1w =+ 1w)
                    ((ph_page =+ 1w) pgtype)))
    = pgtype':bool[20]->bool[2]
   ) ==>
   (pgtype (ph_page') = 1w) ==>
   (pgtype' (ph_page') = 1w)
``,
   REPEAT GEN_TAC
   THEN (REPEAT DISCH_TAC)
   THEN (SYM_ASSUMPTION_TAC ``pgtype':bool[20]->bool[2] = f``)
   THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM])
);

val pgtype_update_thm = prove(``
   !ph_page''.
   (pgtype':bool[20]->bool[2] =
   ((pointed_page + 3w =+ 1w)
        ((pointed_page + 2w =+ 1w)
           ((pointed_page + 1w =+ 1w)
              ((pointed_page =+ 1w) pgtype))))) ==>
   (pgtype' ph_page'' ≠ pgtype ph_page'') ==>
   ((ph_page'' = pointed_page) \/
    (ph_page'' = pointed_page + 1w) \/
    (ph_page'' = pointed_page + 2w) \/
    (ph_page'' = pointed_page + 3w))
``,
   REPEAT STRIP_TAC
   THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype':bool[20]->bool[2] = f``)
   THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM])
   THEN (METIS_TAC [])
);


val l1_map_invariant_page_type_l2_tac = 
    (`
         (∀ph_page''.
          pgtype' ph_page'' ≠ pgtype ph_page'' ⇒
          (pgrefs ph_page'' = 0w))` by (
	  REPEAT STRIP_TAC
          THEN (SYM_ASSUMPTION_TAC ``a = pgtype':bool[20]->bool[2]``)
          THEN (ASSUME_TAC (UNDISCH_ALL pgtype_update_thm))
	  THEN (METIS_TAC [])
     ))
     THEN (ASSUME_TAC invariant_page_type_l2_ok_changed_types_thm)
     THEN STRIP_TAC
     THEN (SYM_ASSUMPTION_TAC ``pgtype':bool[20]->bool[2] = f``)
     THEN (ASSUME_TAC (UNDISCH ( 
	   SPECL [``pointed_page:bool[20]``, ``ph_page:bool[20]``] pgtype_update1_thm
	  )))
     THEN (`pgtype pointed_page ≠ 2w` by METIS_TAC [])
     THEN (`pgtype (pointed_page+1w) ≠ 2w` by METIS_TAC [])
     THEN (`pgtype (pointed_page+2w) ≠ 2w` by METIS_TAC [])
     THEN (`pgtype (pointed_page+3w) ≠ 2w` by METIS_TAC [])
     THEN (SYM_ASSUMPTION_TAC ``pgtype':bool[20]->bool[2] = f``)
     THEN (FULL_SIMP_TAC (srw_ss()) [])
     THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype' ph_page = 2w``)
     (* ok, we know that the page was an l2 *)
     THEN (SPEC_ASSUMPTION_TAC ``∀ph_page:bool[20].pgtype ph_page ≠ 3w ∧ p`` ``ph_page:bool[20]``)
     THEN (FULL_SIMP_TAC (srw_ss()) [])
     THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype ph_page = 2w``)
     THEN (SPEC_ASSUMPTION_TAC ``∀pgtype:bool[20]->bool[2].p`` ``pgtype:bool[20]->bool[2]``)
     THEN (SPEC_ASSUMPTION_TAC ``∀ph_page:bool[20].p`` ``ph_page:bool[20]``)
     THEN (FULL_SIMP_TAC (srw_ss()) [inv_counter_part_def, sound_type_change_def])
     THEN (METIS_TAC [ref_inv_l2_thm])
;

val l1_map_invariant_page_type_l1_tac = 
    REPEAT DISCH_TAC
    (* first we show that the pointed page is aligned *)
    THEN (Q.UNABBREV_TAC `pointed_page`)
    THEN (ASSUME_TAC (UNDISCH (blastLib.BBLAST_PROVE (
    ``(pa = 0xFFFFC000w && pa:word32) ==>
      (0xFFFFCw && (w2w (pa ⋙ 12):word20)  = (w2w (pa ⋙ 12):word20))
    ``))))
    THEN (Q.ABBREV_TAC `pointed_page:word20 = (w2w (pa ⋙ 12):word20)`)
    THEN (Cases_on `ph_page <> pointed_page`)
    THENL [
         (* non-updated L1: first we show that the priginal page type was 1 *)
         (SYM_ASSUMPTION_TAC ``pgtype':bool[20]->bool[2] = f``)
	 THEN (ASSUME_TAC (UNDISCH ( 
	   SPECL [``pointed_page:bool[20]``, ``ph_page:bool[20]``] pgtype_update_l1_1thm)))
	 THEN (SYM_ASSUMPTION_TAC ``pgtype':bool[20]->bool[2] = f``)
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``ph_page ≠ pointed_page``)
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``ph_page = 0xFFFFCw && ph_page``)
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC `` 0xFFFFCw && pointed_page = pointed_page``)
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(pgtype' ph_page = 1w)``)
	 (* we instantiate the invariant for the old settings *)
	 THEN (SPEC_ASSUMPTION_TAC ``!ph_page:word20.p`` ``ph_page:word20``)
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(pgtype ph_page = 1w)``)
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``ph_page = 0xFFFFCw && ph_page``)
	 THEN (ASSUME_TAC (UNDISCH ( 
	   SPECL [``pointed_page:bool[20]``, ``ph_page:bool[20]``] pgtype_update_l1_2_thm)))
	 THEN (SYM_ASSUMPTION_TAC ``pgtype':bool[20]->bool[2] = f``)
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
	 (* enable the hypotesys of the right page type change *)
	 THEN (`
           (∀ph_page''.
          pgtype' ph_page'' ≠ pgtype ph_page'' ⇒
          (pgrefs ph_page'' = 0w))` by (
	  REPEAT STRIP_TAC
          THEN (SYM_ASSUMPTION_TAC ``a = pgtype':bool[20]->bool[2]``)
          THEN (ASSUME_TAC (UNDISCH_ALL pgtype_update_thm))
	  THEN (METIS_TAC [])
	 ))
	 THEN (ASSUME_TAC invariant_page_type_l1_ok_changed_types_thm)
	 THEN (SPEC_ASSUMPTION_TAC ``∀pgtype:bool[20]->bool[2].p`` ``pgtype:bool[20]->bool[2]``)
	 THEN (SPEC_ASSUMPTION_TAC ``∀ph_page:bool[20].p`` ``ph_page:bool[20]``)
	 THEN (METIS_TAC [ref_inv_l1_thm]),
	 (* The update l1 *)
	 (FULL_SIMP_TAC (srw_ss()) [])
	 THEN (ASSUME_TAC (UNDISCH ( 
	   SPEC ``pointed_page:bool[20]`` pgtype_update_l1_3_thm)))
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC `` 0xFFFFCw && pointed_page = pointed_page``)
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
	 THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_l1_def])
	 THEN GEN_TAC
	 THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
	 THEN (REABBREV_TAC)
	 THEN (SPEC_ASSUMPTION_TAC ``∀l1Idx:word32.p`` ``(w2w (pg_idx:bool[12])):word32``)
	 THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``(w2w (pg_idx:bool[12])):word32 <+ 4096w``))
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``w2w pg_idx <₊ 4096w``)
	 THEN (SYM_ASSUMPTION_TAC ``pa = 0xFFFFC000w && pa``)
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``0xFFFFC000w && pa = pa``)
	 THEN (Q.UNABBREV_TAC `pointed_page`)
	 THEN (ASSUME_TAC (UNDISCH (blastLib.BBLAST_PROVE ``
	  (0xFFFFC000w && pa = pa) ==>
          ((w2w ((w2w (pa ⋙ 12)):word20) ≪ 12) = pa:word32)
         ``)))
	 THEN (REWRITE_TAC [UNDISCH (blastLib.BBLAST_PROVE ``
	  (0xFFFFC000w && pa = pa) ==>
          ((w2w ((w2w (pa ⋙ 12)):word20) ≪ 12) = pa:word32)
         ``)])
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``mem=mem':word32->word8``)
	 THEN (Cases_on `(mmu_l1_decode_type (read_mem32 (pa ‖ w2w pg_idx ≪ 2,mem')) = 0w)`)
	 THENL [
	    (RW_TAC (srw_ss()) []),
	    (SYM_ASSUMPTION_TAC ``ph_page = w2w (pa ⋙ 12)``)
	    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``w2w (pa ⋙ 12) = ph_page``)
	    THEN (Q.ABBREV_TAC `no_split = pgtype ph_page ≠ 1w ∨ pgtype (ph_page + 1w) ≠ 1w ∨
                      pgtype (ph_page + 2w) ≠ 1w ∨ pgtype (ph_page + 3w) ≠ 1w`)
	    THEN (Cases_on `(mmu_l1_decode_type (read_mem32 (pa ‖ w2w pg_idx ≪ 2,mem')) = 1w)`)
            THENL [
  	       DISJ2_TAC THEN DISJ2_TAC
	       THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``mmu_l1_decode_type (read_mem32 (pa ‖ w2w pg_idx ≪ 2,mem')) = 1w``)
	       THEN (Q.ABBREV_TAC `pointed_l2:word20 = (w2w ((rec'l1PTT (read_mem32 (pa ‖ w2w pg_idx ≪ 2,mem'))).addr ⋙2))`)
	       THEN (SYM_ASSUMPTION_TAC ``a = b:bool[20]->bool[2]``)
	       THEN (ASSUME_TAC (UNDISCH 
                 (SPECL [``ph_page:bool[20]``, ``pointed_l2:bool[20]``] pgtype_update2_thm)))
	       THEN (SYM_ASSUMPTION_TAC ``a = b:bool[20]->bool[2]``)
	       THEN (FULL_SIMP_TAC (srw_ss()) []),
	       (* we are on a section entry *)
	       (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``mmu_l1_decode_type (read_mem32 (pa ‖ w2w pg_idx ≪ 2,mem')) <> 0w``)
	       THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``mmu_l1_decode_type (read_mem32 (pa ‖ w2w pg_idx ≪ 2,mem')) <> 1w``)
	       THEN DISJ2_TAC THEN DISJ1_TAC
	       THEN (Q.ABBREV_TAC `l1_desc = (read_mem32 (pa ‖ w2w pg_idx ≪ 2,mem'))`)
	       THEN (Q.ABBREV_TAC `no_split2 = (((rec'l1SecT l1_desc).ap = 2w) ∨ ((rec'l1SecT l1_desc).ap = 3w))`)
	       THEN (FULL_SIMP_TAC (srw_ss()) [])
	       THEN (FULL_SIMP_TAC (srw_ss()) [rec'l1SecT_def])

	       THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``
((((((18 :num) >< (18 :num)) (l1_desc :word32) :word1) @@ (((1 :num) >< (1 :num)) l1_desc :word1)) :word2) =
  (1w:word2)) ==>
 ((1w :word32) && (l1_desc :word32) ⋙ (18 :num) = (0w :word32))
	       ``))
	       THEN (FULL_SIMP_TAC (srw_ss()) [])
	       THEN GEN_TAC
	       THEN (Q.ABBREV_TAC `ap:bool[3] = (((((15 :num) >< (15 :num)) l1_desc :word1) @@
   (((11 :num) >< (10 :num)) l1_desc :word2)) :word3)`)
	       THEN (Cases_on `ap = 2w`)
	       THENL [
	         (* it is read only, ok *)
	         (FULL_SIMP_TAC (srw_ss()) []),
		 (FULL_SIMP_TAC (srw_ss()) [Abbr `no_split2`])
  	         THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``ap = 3w``)
		 THEN (FULL_SIMP_TAC (srw_ss()) [])
		 THEN (SPEC_ASSUMPTION_TAC ``!secIdx:word8.p`` ``w2w(va:word32 ⋙ 12):word8``)
		 THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``
(w2w   ((255w :word32) && (va :word32) ⋙ (12 :num) ‖
      (w2w (((31 :num) >< (20 :num)) (l1_desc :word32) :word12) :
         word32) ≪ (20 :num) ⋙ (12 :num)) :word20) = 
((w2w (w2w ((va :word32) ⋙ (12 :num)) :word8) :word20) ‖
           (w2w (((31 :num) >< (20 :num)) (l1_desc :word32) :word12) :
              word20) ≪ (8 :num))
				   ``))
		 THEN (Q.ABBREV_TAC `pointed_page' =
((w2w (w2w ((va :word32) ⋙ (12 :num)) :word8) :word20) ‖
           (w2w (((31 :num) >< (20 :num)) (l1_desc :word32) :word12) :
              word20) ≪ (8 :num))`)
		 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``
w2w (255w && va ⋙ 12 ‖ w2w ((31 >< 20) l1_desc) ≪ 20 ⋙ 12) = pointed_page'``)
		 THEN (FULL_SIMP_TAC (srw_ss()) [])
		 THEN (ASSUME_TAC (UNDISCH ( 
			   SPECL [``ph_page:bool[20]``, ``pointed_page':bool[20]``] pgtype_update_l1_4_thm)))
		 THEN (METIS_TAC [])
	       ]
	    ]
	 ]
    ]
;

val l1_map_invariant_page_type_tac = 
    (* we are not interested in the prefs here *)
    (FULL_SIMP_TAC (srw_ss()) [ LET_DEF])
    THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def])
    THEN (REPEAT CONJ_TAC)
    (* Three sub goals *)
    THENL [
    (* The MMU is still enabled *)
      (SYM_ASSUMPTION_TAC ``rec'sctlrT 1w = c1'``)
      THEN (FULL_SIMP_TAC (srw_ss()) [rec'sctlrT_def]),
    (* The Domain are still well setupped, keep the invariant for further use *)
      (FULL_SIMP_TAC (srw_ss()) [invariant_ref_cnt_def])
      THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def]),
    (* The real part of the invariant *)
      (FULL_SIMP_TAC (srw_ss()) [ LET_DEF])
      THEN (FULL_SIMP_TAC (srw_ss()) [invariant_ref_cnt_def])
      THEN (THM_KEEP_TAC ``invariant_page_type c1' c2' c3' mem pgtype pgrefs``
			 (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def]))
    (* First we check that the main L1 is still 16kb aligned *)
      THEN (LET_HYP_ELIM_TAC [])
      THEN (FULL_SIMP_TAC (srw_ss()) [])
      THEN (STRIP_TAC) 
      THENL [
	(* The active page table is still marked as L1 *)
        (SYM_ASSUMPTION_TAC ``f = pgtype':bool[20]->bool[2]``)
        THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM]),
	(* The real about the page tables *)
	(GEN_TAC)
	THEN (REPEAT (CONJ_TAC)) 
	THENL [
	  (* no page is marked with type 3 *)
   	  (RW_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM]),
	  (* ALL_TAC *)
	  l1_map_invariant_page_type_l2_tac ,
	  (* ALL_TAC *)
	  l1_map_invariant_page_type_l1_tac
	]
      ]
    ]
;

val l1_count_add_helper_thm = Q.store_thm("l1_count_add_helper_thm", `
(x < 4096) ==>
(0xFFFFC000w && pa = pa)  ==> (
((((w2w (w2w (pa ⋙ 12) :word20) :word32) ≪ 12) !! (w2w (n2w x:word12) ≪ 2)) = 
 (pa + n2w x ≪ 2)) /\
 (pa !! n2w x ≪ 2 = pa + n2w x ≪ 2)
)
`,
  REPEAT STRIP_TAC
  THEN (ASSUME_TAC (UNDISCH (blastLib.BBLAST_PROVE (``
(0xFFFFC000w && pa = pa) ==>
(((w2w (w2w ((pa :word32) ⋙ (12 :num)) :word20) :word32) ≪ (12 :
        num)) = pa)
``))))
  THEN (FULL_SIMP_TAC (srw_ss()) [])
  THEN (FULL_SIMP_TAC (srw_ss()) [w2w_n2w])
  THEN (`BITS 11 0 x = x` by (
	FULL_SIMP_TAC (arith_ss) [bitTheory.BITS_THM]
       ))
  THEN (FULL_SIMP_TAC (srw_ss()) [])
  THEN (ASSUME_TAC (
	SPECL [``x:num``, ``4096:num``]
	      (Thm.INST_TYPE[alpha |-> ``:32``] (GSYM word_lo_n2w))))
  THEN (FULL_SIMP_TAC (srw_ss()) [])
  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x<4096:num``)
  THEN (FULL_SIMP_TAC (arith_ss) [])
  THEN (ASSUME_TAC (SPEC ``(n2w x):bool[32]`` (blastLib.BBLAST_PROVE ``
!y.
(pa = 0xFFFFC000w && pa) ==>
(y <+ 4096w) ==>
(
((pa:bool[32]) ‖ y ≪ 2) = (pa + y ≪ 2))
``)))
  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(0xFFFFC000w && pa = pa)``)
  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``n2w x <₊ 4096w``)
  THEN (FULL_SIMP_TAC (srw_ss()) [])
);


val helper_overflow_1_thm = Q.store_thm("l1_count_add_helper_thm", `
  !a . (x < 4096) ==>
  ((a <₊ n2w x + 0x3FFFEFFFw:word30) \/ (a <₊ 0x3FFFEFFFw:word30)) ==>
  ((a <₊ n2w (SUC x) + 0x3FFFEFFFw:word30) /\  (a + 1w <₊ n2w x + 0x3FFFF000w:word30))
`,
  (REPEAT GEN_TAC)
  THEN (REPEAT DISCH_TAC)
  THEN (FULL_SIMP_TAC (srw_ss()) [arithmeticTheory.ADD1])
  THEN (FULL_SIMP_TAC (srw_ss()) [GSYM word_add_n2w])
  THEN (ASSUME_TAC (SPECL [``n2w x:word30``, ``a:word30``] (blastLib.BBLAST_PROVE ``!x y.
(x <₊ 4096w:bool[30]) ==>
((y <₊ x + 0x3FFFEFFFw) \/ (y <₊ 0x3FFFEFFFw)) ==>
((y <₊ x + 0x3FFFF000w) /\ (y+1w <₊ x + 0x3FFFF000w))
	  ``)))
  THEN (ASSUME_TAC (
	SPECL [``x:num``, ``4096:num``]
	      (Thm.INST_TYPE[alpha |-> ``:30``] (GSYM word_lo_n2w))))
  THEN (FULL_SIMP_TAC (srw_ss()) [])
  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x<4096:num``)
  THEN (FULL_SIMP_TAC (arith_ss) [])
);

val l1_create_external_for_unmapped_tac =
	  (FULL_SIMP_TAC (srw_ss()) [])
          THEN (SPEC_ASSUMPTION_TAC ``∀x'. ref'' x' = ref' x'`` ``ph_page:bool[20]``)
	  THEN (FULL_SIMP_TAC (srw_ss()) [l1_count_def])
	  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
	  THEN (FULL_SIMP_TAC (srw_ss()) [mmu_l1_decode_type_def])
	  THEN (SYM_ASSUMPTION_TAC ``pa = 0xFFFFC000w && pa``)
	  THEN (`x < 4096` by FULL_SIMP_TAC (arith_ss) [])
	  THEN (FULL_SIMP_TAC (srw_ss()) [(UNDISCH_ALL l1_count_add_helper_thm)])
	  (* overflow *)
	  THEN (METIS_TAC [helper_overflow_1_thm])
;


val l1_create_external_for_pt_tac =
	  (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (FULL_SIMP_TAC (srw_ss()) [l1_count_def])
	  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
	  THEN (SYM_ASSUMPTION_TAC ``pa = 0xFFFFC000w && pa``)
	  THEN (`x < 4096` by FULL_SIMP_TAC (arith_ss) [])
	  THEN (FULL_SIMP_TAC (srw_ss()) [(UNDISCH_ALL l1_count_add_helper_thm)])
	  THEN (SYM_ASSUMPTION_TAC ``0xFFFFC000w && pa = pa``)
	  THEN (FULL_SIMP_TAC (srw_ss()) [mmu_l1_decode_type_def])
	  THEN (Cases_on `w2w ((rec'l1PTT (read_mem32 (pa + n2w x ≪ 2,mem'))).addr ⋙ 2) <> ph_page`)
	  THENL [
	    (FULL_SIMP_TAC (srw_ss()) [])
	    THEN (THM_KEEP_TAC ``∀x'. ref'' x' = (ref':bool[20]->bool[30]) x'`` (SPEC_ASSUMPTION_TAC ``∀x'. ref'' x' = (ref':bool[20]->bool[30]) x'`` ``ph_page:bool[20]``))
	    THEN (PAT_ASSUM ``ref'' x' = ref':bool[20]->bool[30] x'`` (fn thm =>
              ASSUME_TAC (SIMP_RULE (srw_ss()) [combinTheory.APPLY_UPDATE_THM] thm)
            ))
	    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``w2w ((rec'l1PTT (read_mem32 (pa + n2w x ≪ 2,mem'))).addr ⋙ 2) <> ph_page``)
	    THEN (FULL_SIMP_TAC (srw_ss()) [])
	    (* overflow *)
	    THEN (METIS_TAC [helper_overflow_1_thm])
	  ,
	    (FULL_SIMP_TAC (srw_ss()) [])
	    THEN (THM_KEEP_TAC ``∀x'. ref'' x' = (ref':bool[20]->bool[30]) x'`` (SPEC_ASSUMPTION_TAC ``∀x'. ref'' x' = (ref':bool[20]->bool[30]) x'`` ``ph_page:bool[20]``))
	    THEN (PAT_ASSUM ``ref'' x' = ref':bool[20]->bool[30] x'`` (fn thm =>
              ASSUME_TAC (SIMP_RULE (srw_ss()) [combinTheory.APPLY_UPDATE_THM] thm)
            ))
	    (* We have to show that the pgtype' ph_page = 2w *)
	    (* first we show that this hold for the previous pgtype *)
	    THEN (PAT_ASSUM ``Abbrev (!idx:bool[32].p)`` (fn thm =>
              ASSUME_TAC (SIMP_RULE bool_ss [FUN_EQ_THM, markerTheory.Abbrev_def] thm)))
	    THEN (SPEC_ASSUMPTION_TAC ``!idx:bool[32].p`` ``n2w(x):bool[32]``)
	    THEN (SYM_ASSUMPTION_TAC ``pa = 0xFFFFC000w && pa``)
	    THEN (FULL_SIMP_TAC (srw_ss()) [(UNDISCH_ALL l1_count_add_helper_thm)])
	    THEN (ASSUME_TAC (
		  SPECL [``x:num``, ``4096:num``]
			(Thm.INST_TYPE[alpha |-> ``:32``] (GSYM word_lo_n2w))))
	    THEN (FULL_SIMP_TAC (srw_ss()) [])
	    THEN (`x MOD 4294967296 < 4096` by (
		  (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x<4096:num``)
		  THEN (FULL_SIMP_TAC (arith_ss) [])))
	    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x MOD 4294967296 < 4096``)
	    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``n2w x <+ 4096w``)
	    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``3w && read_mem32 (pa + n2w x ≪ 2,mem') = 1w``)
	    THEN (FULL_SIMP_TAC (srw_ss()) [])
	    THEN (Q.ABBREV_TAC `l1_desc = read_mem32 (pa + n2w x ≪ 2,mem')`)
	    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``w2w ((rec'l1PTT l1_desc).addr ⋙ 2) = ph_page``)
	    (* now we have to show that also the new pgtype is 2 *)
	    THEN (SYM_ASSUMPTION_TAC ``a = b:bool[20]->bool[2]``)
	    THEN (ASSUME_TAC (UNDISCH (UNDISCH (
	    SPECL [``w2w (pa:bool[32] ⋙ 12):bool[20]``, ``ph_page:bool[20]``] pgtype_update2_thm
	    ))))
	    THEN (PAT_ASSUM ``Abbrev (pgtype (w2w (pa ⋙ 12) + 1w) ≠ 2w ∧ p)`` (fn thm =>
              ASSUME_TAC (SIMP_RULE bool_ss [FUN_EQ_THM, markerTheory.Abbrev_def] thm)))
	    THEN (FULL_SIMP_TAC (srw_ss()) [])
	    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype ph_page = 2w``)
	    (* now we have to show that there is no overflow *)
	    THEN (SYM_ASSUMPTION_TAC ``ref'' ph_page + 1w = ref' ph_page``)
	    THEN (FULL_SIMP_TAC (srw_ss()) [])
	    THEN (ASSUME_TAC (SPECL [``ref'':bool[20]->bool[30] ph_page``] non_max_no_overflow_thm))
	    THEN (`ref'' ph_page ≠ 0x3FFFFFFFw` by (
                 (Cases_on `ref'' ph_page = pgrefs ph_page`)
		 THENL [
		    (FULL_SIMP_TAC (srw_ss()) [])
		    THEN (METIS_TAC [blastLib.BBLAST_PROVE ``
                      (pgrefs:bool[20]->bool[30] ph_page <₊ 0x3FFFEFFFw) ==>
                      (pgrefs:bool[20]->bool[30] ph_page <>  0x3FFFFFFFw)
			  ``]),
		    (FULL_SIMP_TAC (srw_ss()) [])
                    THEN (ASSUME_TAC (SPECL [``(n2w x):bool[30]``, ``ref'':bool[20]->bool[30] ph_page``] (blastLib.BBLAST_PROVE ``!x y.
(x <₊ 4096w:bool[30]) ==>
(y <₊ x + 0x3FFFEFFFw) ==>
(y <> 0x3FFFFFFFw)
	  ``)))
		    THEN (ASSUME_TAC (
			  SPECL [``x:num``, ``4096:num``]
				(Thm.INST_TYPE[alpha |-> ``:30``] (GSYM word_lo_n2w))))
		    THEN (FULL_SIMP_TAC (srw_ss()) [])
		    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x<4096:num``)
		    THEN (FULL_SIMP_TAC (arith_ss) [])
		    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``ref'' ph_page <₊ n2w x + 0x3FFFEFFFw``)
		 ]
  	    ))
	    THEN (FULL_SIMP_TAC (srw_ss()) [])
	    THEN (STRIP_TAC)
	    THEN (FULL_SIMP_TAC (srw_ss()) [arithmeticTheory.ADD1])
	    THEN (FULL_SIMP_TAC (srw_ss()) [GSYM word_add_n2w])
	    THEN (Cases_on `ref'' ph_page = pgrefs ph_page`)
	    THENL [
	      (FULL_SIMP_TAC (srw_ss()) [])
	      THEN (ASSUME_TAC (SPECL [``(n2w x):bool[30]``, ``pgrefs:bool[20]->bool[30] ph_page``] (blastLib.BBLAST_PROVE ``!x y.
(x <₊ 4096w:bool[30]) ==>
(y <₊  0x3FFFEFFFw) ==>
(y +1w <+ x + 0x3FFFF000w)
	  ``)))
	      THEN (ASSUME_TAC (
			  SPECL [``x:num``, ``4096:num``]
				(Thm.INST_TYPE[alpha |-> ``:30``] (GSYM word_lo_n2w))))
	      THEN (FULL_SIMP_TAC (srw_ss()) [])
	      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x<4096:num``)
	      THEN (FULL_SIMP_TAC (arith_ss) []),
	      (FULL_SIMP_TAC (srw_ss()) [])
	      THEN (ASSUME_TAC (SPECL [``(n2w x):bool[30]``, ``ref'':bool[20]->bool[30] ph_page``] (blastLib.BBLAST_PROVE ``!x y.
(x <₊ 4096w:bool[30]) ==>
(y <₊  x + 0x3FFFEFFFw) ==>
(y +1w <+ x + 0x3FFFF000w)
	  ``)))
	      THEN (ASSUME_TAC (
			  SPECL [``x:num``, ``4096:num``]
				(Thm.INST_TYPE[alpha |-> ``:30``] (GSYM word_lo_n2w))))
	      THEN (FULL_SIMP_TAC (srw_ss()) [])
	      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x<4096:num``)
	      THEN (FULL_SIMP_TAC (arith_ss) [])
	    ]
	  ]
;



val l1_create_nested_for_section_non_updated_tac =
      	 (Q.ABBREV_TAC `prev_ref_inner = (for y 256 ref'' f')`)
	 THEN (PAT_ASSUM `` Abbrev (f':num -> (word20->word30) -> (word20->word30) =g) `` (fn thm =>
        ASSUME_TAC thm
	THEN (FULL_SIMP_TAC (srw_ss()) [SIMP_RULE bool_ss [FUN_EQ_THM, markerTheory.Abbrev_def] thm]
	   )))
	 THEN (Q.ABBREV_TAC `sec_entry_add = (rec'l1SecT (read_mem32  (pa + n2w x ≪ 2,mem'))).addr`)
	 THEN (FULL_SIMP_TAC (srw_ss()) [w2w_id])
	 THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM])
	 THEN (`y < 256:num` by (FULL_SIMP_TAC (arith_ss) []))
	 THEN (ASSUME_TAC (
		SPECL [``y:num``, ``256:num``]
		(Thm.INST_TYPE[alpha |-> ``:20``] (GSYM word_lo_n2w))))
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``y<256:num``)
	 THEN (FULL_SIMP_TAC (arith_ss) [])
	 THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``
	 ((n2w y)<+256w:bool[20]) ==>
	 ((255w && ph_page:bool[20]) ≠ (n2w y)) ==>
	 (~(((n2w y) ‖ (w2w (sec_entry_add:bool[12])) ≪ 8) = ph_page))
	 ``))
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``n2w y<+ 256w``)
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(255w && ph_page) ≠ n2w y``)
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(n2w y ‖ w2w sec_entry_add ≪ 8) ≠ ph_page``)
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
	 (* we are sure that we are not updating the reference *)
	 THEN (FULL_SIMP_TAC (srw_ss()) [arithmeticTheory.ADD1])
	 THEN (FULL_SIMP_TAC (srw_ss()) [GSYM word_add_n2w])
	 THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``
	 ((n2w y)<+256w:bool[20]) ==>
	 ((255w && ph_page:bool[20]) ≠ (n2w y)) ==>
	 (((255w && ph_page) <₊ n2w y + 1w) =
	  ((255w && ph_page) <₊ n2w y)
	 )
	 ``))
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``n2w y<+ 256w``)
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(255w && ph_page) ≠ n2w y``)
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
;

val l1_create_nested_for_section_non_pointed_page =
	   (FULL_SIMP_TAC (srw_ss()) [])
 	   THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``~(n2w y <₊ n2w y:bool[20])``))
	   THEN (FULL_SIMP_TAC (srw_ss()) [])
	   THEN (SPEC_ASSUMPTION_TAC ``!ref_inner':word20->word30.p`` ``ref_inner:word20->word30``)
	   THEN (FULL_SIMP_TAC (srw_ss()) [])
	   THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``ref_inner ph_page = ref'' ph_page``)
	   THEN (FULL_SIMP_TAC (srw_ss()) [])
	   THEN (`x < 4096` by FULL_SIMP_TAC (arith_ss) [])
	   THEN (ASSUME_TAC (
		SPECL [``x:num``, ``4096:num``]
		(Thm.INST_TYPE[alpha |-> ``:30``] (GSYM word_lo_n2w))))
	   THEN (FULL_SIMP_TAC (srw_ss()) [])
	   THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x<4096:num``)
	   THEN (FULL_SIMP_TAC (arith_ss) [])
	   THEN (ASSUME_TAC (SPECL [``(n2w x):bool[30]``, ``ref'':bool[20]->bool[30] ph_page``] (blastLib.BBLAST_PROVE ``!x y.
(x <₊ 4096w:bool[30]) ==>
(y <₊ x + 0x3FFFEFFFw) ==>
(y <₊ x + 0x3FFFF000w)
	  ``)))
	   THEN (FULL_SIMP_TAC (srw_ss()) [])
	   (* we must werify that the count is 0 *)
	   THEN (SIMP_TAC (srw_ss()) [l1_count_def])
	   THEN (SIMP_TAC (srw_ss()) [LET_DEF])
	   THEN (REABBREV_TAC)
	   THEN (SYM_ASSUMPTION_TAC ``pa = 0xFFFFC000w && pa``)
	   THEN (FULL_SIMP_TAC (srw_ss()) [(UNDISCH_ALL l1_count_add_helper_thm)])
	  THEN (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (Q.ABBREV_TAC `sec_desc = (read_mem32 (pa + n2w x ≪ 2,mem'))`)
	  THEN (FULL_SIMP_TAC (srw_ss()) [mmu_l1_decode_type_def])
	  THEN (Q.ABBREV_TAC `sec_pointed_add = (rec'l1SecT sec_desc).addr`)
	  THEN (Q.ABBREV_TAC `sec_idx:bool[20] = n2w y`)
	  THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``
	  (sec_idx <+ 256w) ==>
	  (255w && ph_page = sec_idx) ==>
	  ((sec_idx ‖ w2w (sec_pointed_add:bool[12]) ≪ 8) ≠ ph_page:bool[20]) ==>
	  (sec_pointed_add <> w2w (ph_page ⋙ 8))
	  ``))
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(sec_idx <+ 256w)``)
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(255w && ph_page = sec_idx)``)
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``((sec_idx ‖ w2w (sec_pointed_add:bool[12]) ≪ 8) ≠ ph_page:bool[20])``)
	  THEN (FULL_SIMP_TAC (srw_ss()) [])
;

val l1_create_nested_for_section_pointed_page =
	  (FULL_SIMP_TAC (srw_ss()) [])
 	  THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``~(n2w y <₊ n2w y:bool[20])``))
	  THEN (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (SPEC_ASSUMPTION_TAC ``!ref_inner':word20->word30.p`` ``prev_ref_inner:word20->word30``)
	  THEN (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``prev_ref_inner ph_page = ref'' (ph_page:bool[20])``)
	  THEN (SYM_ASSUMPTION_TAC ``ref'' ph_page + 1w = ref_inner ph_page``)
	  THEN (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (PAT_ASSUM ``Abbrev (!idx:bool[32].p)`` (fn thm =>
              ASSUME_TAC (SIMP_RULE bool_ss [FUN_EQ_THM, markerTheory.Abbrev_def] thm)))
	  THEN (SPEC_ASSUMPTION_TAC ``!idx:bool[32].p`` ``n2w(x):bool[32]``)
	  THEN (ASSUME_TAC (
		SPECL [``x:num``, ``4096:num``]
		(Thm.INST_TYPE[alpha |-> ``:32``] (GSYM word_lo_n2w))))
	  THEN (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (SYM_ASSUMPTION_TAC ``pa = (0xFFFFC000w && pa)``)
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(0xFFFFC000w && pa) = pa``)
	  THEN (`x < 4096` by FULL_SIMP_TAC (arith_ss) [])
	  THEN (`x MOD 4294967296 < 4096` by FULL_SIMP_TAC (arith_ss) [])
	  THEN (SIMP_BY_ASSUMPTION_TAC ``x MOD 4294967296 < 4096``)
	  THEN (ASSUME_TAC (SPEC ``(n2w x):bool[32]`` (blastLib.BBLAST_PROVE ``
!y.
(0xFFFFC000w && pa = pa) ==>
(y <+ 4096w) ==>
(
((pa:bool[32]) ‖ y ≪ 2) = (pa + y ≪ 2))
``)))
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(0xFFFFC000w && pa) = pa``)
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``n2w x <₊ 4096w``)
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pa ‖ n2w x ≪ 2 = pa + n2w x ≪ 2``)
	  THEN (Q.ABBREV_TAC `l1_desc =  read_mem32 (pa + n2w x ≪ 2,mem')`)
	  THEN (`mmu_l1_decode_type l1_desc = 2w` by (
		SIMP_TAC (srw_ss()) [mmu_l1_decode_type_def]
		THEN (METIS_TAC [])
	       ))
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``mmu_l1_decode_type l1_desc = 2w``)
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(rec'l1SecT l1_desc).ap = 3w``)
	  THEN (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (SPEC_ASSUMPTION_TAC ``∀secIdx:bool[8].p`` ``n2w y:word8``)
	  THEN (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (Q.ABBREV_TAC `sec_pointed_add = (rec'l1SecT l1_desc).addr`)
	  THEN (Q.ABBREV_TAC `sec_idx:bool[20] = n2w y`)
	  THEN (FULL_SIMP_TAC (srw_ss()) [w2w_n2w])
	  THEN (`BITS 7 0 y = y` by (
		FULL_SIMP_TAC (arith_ss) [bitTheory.BITS_THM]
	       ))
	  THEN (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``sec_idx ‖ w2w sec_pointed_add ≪ 8 = ph_page``)
	  THEN (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE ``
		(¬(pgrefs ph_page ≥₊ 0x3FFFEFFFw:bool[30])) = 
		(pgrefs ph_page <+ 0x3FFFEFFFw:bool[30])
		``])
	  THEN (`ref'' ph_page <₊ 0x3FFFFFFFw` by (
		(Cases_on `ref'' ph_page ≠ pgrefs ph_page`)
		THENL [
		   (FULL_SIMP_TAC (srw_ss()) [])
		   THEN (`x < 4096` by FULL_SIMP_TAC (arith_ss) [])
		   THEN (ASSUME_TAC (
			 SPECL [``x:num``, ``4096:num``]
			       (Thm.INST_TYPE[alpha |-> ``:20``] (GSYM word_lo_n2w))))
		   THEN (FULL_SIMP_TAC (srw_ss()) [])
		   THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x<4096:num``)
		   THEN (FULL_SIMP_TAC (arith_ss) [])
		   THEN (METIS_TAC [blastLib.BBLAST_PROVE ``
			 (n2w x <+ 4096w:word20) ==>
			 (ref'' ph_page <₊ n2w x + 0x3FFFEFFFw:word30) ==>
			 (ref'' ph_page <₊ 0x3FFFFFFFw:word30)
			 ``])		 
		 ,
		 (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE ``
		  (pgrefs ph_page <₊ 0x3FFFEFFFw:word30) ==>
		  (pgrefs ph_page <₊ 0x3FFFFFFFw:word30)
		 ``])
		]
	  ))
	  THEN (ASSUME_TAC (SPEC ``ref'' (ph_page:word20):word30`` non_max_no_overflow_thm))
	  THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``
		(ref'' (ph_page:word20) <₊ 0x3FFFFFFFw:word30) ==>
		(ref'' ph_page <> 0x3FFFFFFFw:word30)
		``))
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``ref'' ph_page <₊ 0x3FFFFFFFw:word30``)
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``ref'' ph_page <> 0x3FFFFFFFw:word30``)
	  THEN (FULL_SIMP_TAC (srw_ss()) [])
	  THEN STRIP_TAC
	  THENL [
	    (* counter *)
	    (FULL_SIMP_TAC (srw_ss()) [l1_count_def])
            THEN (SIMP_TAC (srw_ss()) [LET_DEF])
	    THEN (FULL_SIMP_TAC (srw_ss()) [(UNDISCH_ALL l1_count_add_helper_thm)])
	    THEN (REABBREV_TAC)
	    THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``
           (sec_idx <₊ 256w) ==>
           (sec_idx ‖ (w2w (sec_pointed_add:word12) ≪ 8) = ph_page:bool[20]) ==>
           (sec_pointed_add = w2w (ph_page ⋙ 8))
		  ``))
	    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(sec_idx <₊ 256w)``)
	    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(sec_idx ‖ (w2w (sec_pointed_add:word12) ≪ 8) = ph_page:bool[20])``)
	    THEN (FULL_SIMP_TAC (srw_ss()) [])
(* 	    (* At this point we have to show that the new pgtype is still 0 *) *)
	    THEN (ASSUME_TAC (UNDISCH (
	      SPECL [``(w2w (pa:word32 ⋙ 12)):word20``, ``ph_page:word20``] pgtype_update_l1_4_thm)))
	    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(0xFFFFCw && ph_page) ≠ w2w (pa ⋙ 12)``)
	    THEN (ASSUME_TAC (UNDISCH (blastLib.BBLAST_PROVE ``
          (0xFFFFC000w && pa = pa:word32) ==>
          (0xFFFFCw:word20 && w2w (pa ⋙ 12) = w2w (pa ⋙ 12))
	  ``)))
	    THEN (FULL_SIMP_TAC (srw_ss()) []),
	    (* overflow *)
	    (METIS_TAC [helper_overflow_1_thm])
	  ]
;

val l1_create_nested_for_section_tac =
    (Induct_on `y`)
    THENL [
      (* base case *)
      REPEAT (GEN_TAC)
      THEN (FULL_SIMP_TAC (srw_ss()) [Once for_def_def])
      THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``~((255w && ph_page) <₊ 0w:word20)``))
      THEN (FULL_SIMP_TAC (srw_ss()) [])
      THEN (METIS_TAC []),
      (* inductive case *)
      REPEAT (GEN_TAC)
      THEN (REPEAT DISCH_TAC)
      (* TO enable the inductive hypotesis *)
      THEN (`y <= 256` by FULL_SIMP_TAC (arith_ss) [])
      THEN (FULL_SIMP_TAC (arith_ss) [])
      THEN (PAT_ASSUM ``for (SUC y) 256 ref'' f' ph_page = ref_inner ph_page`` (fn thm =>
       ASSUME_TAC (SIMP_RULE (srw_ss()) [Once for_def_def] thm)
	   ))
      THEN (`~(SUC(y) > 256:num)` by (FULL_SIMP_TAC (arith_ss) []))
      THEN (FULL_SIMP_TAC (arith_ss) [])
      THEN (Cases_on `((255w && ph_page) <> n2w (y))`)
      THENL [
         (* Th entry has not been updated *)
        (* ALL_TAC *)
      l1_create_nested_for_section_non_updated_tac
      ,
         (* Th entry has been updated *)
	 (FULL_SIMP_TAC (srw_ss()) [])
      	 THEN (Q.ABBREV_TAC `prev_ref_inner = (for y 256 ref'' f')`)
	 THEN (PAT_ASSUM `` Abbrev (f':num -> (word20->word30) -> (word20->word30) =g) `` (fn thm =>
        ASSUME_TAC thm
	THEN (FULL_SIMP_TAC (srw_ss()) [SIMP_RULE bool_ss [FUN_EQ_THM, markerTheory.Abbrev_def] thm]
	   )))
	 THEN (FULL_SIMP_TAC (srw_ss()) [w2w_id])
	 THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM])
	 THEN (FULL_SIMP_TAC (srw_ss()) [arithmeticTheory.ADD1])
	 THEN (FULL_SIMP_TAC (srw_ss()) [GSYM word_add_n2w])
	 THEN (`y < 256:num` by (FULL_SIMP_TAC (arith_ss) []))
	 THEN (ASSUME_TAC (
		SPECL [``y:num``, ``256:num``]
		(Thm.INST_TYPE[alpha |-> ``:20``] (GSYM word_lo_n2w))))
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``y<256:num``)
	 THEN (FULL_SIMP_TAC (arith_ss) [])
	 THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``
	 ((n2w y) <+ 256w:bool[20]) ==>
	 (n2w y <₊ n2w y + 1w:bool[20])
	 ``))
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``n2w y<+ 256w``)
	 THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``n2w y <₊ n2w y + 1w:bool[20]``)
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
	 THEN (Cases_on `(n2w y ‖ w2w (rec'l1SecT (read_mem32 (pa + n2w x ≪ 2,mem'))).addr ≪ 8) <> ph_page`)
	 THENL [
	   (* ph_page is not pointed by the section *)
	 (* ALL_TAC *)
	 l1_create_nested_for_section_non_pointed_page
         ,
	 (* ph_page is pointed by the section *)
         (* ALL_TAC *)
        l1_create_nested_for_section_pointed_page
	 ]
      ]
    ]
;



val l1_create_external_for_section_tac =
    (FULL_SIMP_TAC (srw_ss()) [])
    THEN (Cases_on `(rec'l1SecT (read_mem32 (pa + n2w x ≪ 2,mem'))).ap <> 3w`)
    THENL [
      (* if the entry is read only *)
      (FULL_SIMP_TAC (srw_ss()) [])
      THEN l1_create_external_for_unmapped_tac,
      (* if the entry is writable *)
      (FULL_SIMP_TAC (srw_ss()) [])
      THEN (THM_KEEP_TAC ``∀x'. ref'' x' = (ref':bool[20]->bool[30]) x'`` (SPEC_ASSUMPTION_TAC ``∀x'. ref'' x' = (ref':bool[20]->bool[30]) x'`` ``ph_page:bool[20]``))
      THEN (Q.ABBREV_TAC `f' =(λ(y :num) (inPgrefs :word20 -> word30).
            ((w2w
                ((n2w y :word20) ‖
                 (w2w
                    (rec'l1SecT
                       (read_mem32
                          ((pa :word32) +
                           (n2w (x :num) :word32) ≪ (2 :num),
                           (mem' :word32 -> word8)))).addr :word20 << 8)) :
                word20) =+
             inPgrefs
               (w2w
                  ((n2w y :word20) ‖
                   (w2w
                      (rec'l1SecT
                         (read_mem32
                            (pa + (n2w x :word32) ≪ (2 :num),mem'))).
                      addr :word20 << 8)) :word20) + (1w :word30)) inPgrefs)`)
      THEN 
      (SUBGOAL_THEN ``
!y ref_inner:word20 -> word30 diff . (y <= 256) ==>
 (diff = if ((0xffw && ph_page) <+ n2w y) then 1 else 0) ==>
(
  (for y 256 ref'' f' ph_page = ref_inner ph_page) ==>
  (w2n (ref_inner ph_page) = w2n (ref'' ph_page) +
     (diff * l1_count x mem' pgtype' ph_page (w2w (pa:word32 ⋙ 12)))) ∧
(ref_inner ph_page ≠ pgrefs ph_page ⇒
 ref_inner ph_page <₊ n2w (x + diff) + 0x3FFFEFFFw)
)
``   (fn thm =>
	 ASSUME_TAC (thm)
         THEN (SPEC_ASSUMPTION_TAC ``!y:num.p`` ``256:num``)
         THEN (SPEC_ASSUMPTION_TAC ``!ref_inner:word20 -> word30.p`` ``ref':word20 -> word30``)
         THEN (SPEC_ASSUMPTION_TAC ``!diff:num.p`` ``1:num``)
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
         THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``(255w && ph_page) <₊ 256w:word20``))
         THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(255w && ph_page) <₊ 256w:word20``)
	 THEN (THM_KEEP_TAC ``∀x'. ref'' x' = (ref':bool[20]->bool[30]) x'`` (SPEC_ASSUMPTION_TAC ``∀x'. ref'' x' = (ref':bool[20]->bool[30]) x'`` ``ph_page:bool[20]``))
         THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``for 256 256 ref'' f' ph_page = ref' ph_page``)
         THEN (METIS_TAC [arithmeticTheory.ADD1])
      ))
      THEN l1_create_nested_for_section_tac
    ]
;



val l1_create_external_for_tac =
    (Induct_on `x`)
    THENL [
      (FULL_SIMP_TAC (srw_ss()) [Once for_def_def, LET_DEF])
      THEN (FULL_SIMP_TAC (srw_ss()) [SUM, GSUM_def]),
      REPEAT STRIP_TAC
      (* TO enable the inductive hypotesis *)
      THEN (`x <= 4096` by FULL_SIMP_TAC (arith_ss) [])
      THEN (FULL_SIMP_TAC (arith_ss) [])
      (* To avoid to expand the same application used three times in the goal *)
      THEN (Q.ABBREV_TAC `ref' = for (SUC x) 4096 pgrefs f`)
      THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
      THEN (PAT_ASSUM ``Abbrev (ref' = for (SUC x) 4096 pgrefs f)`` (fn thm =>
       ASSUME_TAC (GSYM (SIMP_RULE bool_ss [FUN_EQ_THM, markerTheory.Abbrev_def] thm))
	   ))
      THEN (PAT_ASSUM ``!x':bool[20].for (SUC x) 4096 pgrefs f x' = ref' x'`` (fn thm =>
       ASSUME_TAC (SIMP_RULE (srw_ss()) [Once for_def_def] thm)
	   ))
      THEN (`~(SUC(x) > 4096:num)` by (FULL_SIMP_TAC (arith_ss) []))
      THEN (FULL_SIMP_TAC (arith_ss) [])
      (* we opened the for definition, now we manipulate the sum *)
      THEN (FULL_SIMP_TAC (arith_ss) [SUM, GSUM_def])
      THEN (Q.ABBREV_TAC `ref''=(for x 4096 pgrefs f)`)
      (* now we use the inductive hypotesys *)
      THEN (SYM_ASSUMPTION_TAC ``w2n (ref'' ph_page) = a``)
      THEN (SIMP_TAC (srw_ss()) [Once arithmeticTheory.ADD_ASSOC])
      THEN (FULL_SIMP_TAC (arith_ss) [])
      (* ok, now the goal is new_ref = prev_ref (previous loop) + ccount_pages(x) *)
      (* we open the function definition *)
      THEN (PAT_ASSUM `` Abbrev (f:num -> (word20->word30) -> (word20->word30) =g) `` (fn thm =>
        ASSUME_TAC thm
	THEN (FULL_SIMP_TAC (srw_ss()) [SIMP_RULE bool_ss [FUN_EQ_THM, markerTheory.Abbrev_def] thm]
	   )))
      THEN (Cases_on `(3w && read_mem32 (pa + n2w x ≪ 2,mem')) <> 2w`)
      THENL [
        (FULL_SIMP_TAC (srw_ss()) [])
	THEN (Cases_on `(3w && read_mem32 (pa + n2w x ≪ 2,mem')) <> 1w`)
	THENL [
	  (* The entry is unmapped *)
	  (* ALL_TAC *)
	  l1_create_external_for_unmapped_tac,
	  (* The entry is a pointer to an L2 *)
	  (* ALL_TAC *)
	  l1_create_external_for_pt_tac
	],
      (* The entry is a section *)
	l1_create_external_for_section_tac
      ]
    ]
;


val l1_create_counter_updated_entry_tac = 
    (Q.UNABBREV_TAC `f`)
    THEN (Q.UNABBREV_TAC `f'`)
    THEN (computeLib.RESTR_EVAL_TAC [``count_pages_for_pt``, ``count_pages`` ])
    THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``mem=mem':bool[32]->bool[8]``)
    THEN (SIMP_TAC (srw_ss()) [count_pages_for_pt_def])
    THEN (FULL_SIMP_TAC (srw_ss()) [GSYM w2w_def])
    THEN (`pgtype pointed_page ≠ 1w` by METIS_TAC [])
    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype pointed_page ≠ 1w``)
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THEN (`pgtype pointed_page ≠ 2w` by METIS_TAC [])
    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype pointed_page ≠ 2w``)
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THEN (SYM_ASSUMPTION_TAC ``a = pgtype':bool[20]->bool[2]``)
    THEN (`pgtype' pointed_page = 1w` by METIS_TAC [combinTheory.APPLY_UPDATE_THM])
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THEN (ASSUME_TAC (blastLib.BBLAST_PROVE (``
      (pointed_page:bool[20] = w2w (pa:bool[32] ⋙ 12)) ==>
      (pa = 0xFFFFC000w && pa) ==>
      (pointed_page = (0xFFFFCw && pointed_page))
   ``)))
    THEN (Q.UNABBREV_TAC `pointed_page`)
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC `` pa = 0xFFFFC000w && pa``)
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THEN (PAT_ASSUM ``for x y z f = a`` (fn thm =>
      let val (h,c) = dest_thm thm
	  val (a,b) = dest_eq c
	  val (f,arg1) = dest_comb a
      in  
	  ASSUME_TAC thm
	  THEN (Q.ABBREV_TAC `f=^arg1`)
      end
	 ))
    THEN (SYM_ASSUMPTION_TAC ``a=a':bool[20]->bool[2]``)
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THEN (SYM_ASSUMPTION_TAC ``a=a':bool[20]->bool[30]``)
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THEN 
(SUBGOAL_THEN ``
!x.(x<=4096) ==> (
let ref' = for x 4096 (pgrefs:bool[20]->bool[30]) f  in
(w2n(ref' ph_page) = SUM x (λit. l1_count it mem' pgtype' ph_page (w2w ((pa:bool[32]) ⋙ 12))) +
 count_pages mem' pgtype ph_page
) /\
(
 (ref' ph_page <> pgrefs ph_page) ==>
 ((ref' ph_page) <+ (0x3fffffffw - 4096w + n2w(x)))
))
``   (fn thm =>
       ASSUME_TAC (thm)
       THEN (SPEC_ASSUMPTION_TAC ``!x:num.p`` ``4096:num``)
       THEN (FULL_SIMP_TAC (srw_ss()) [])
       THEN (Q.ABBREV_TAC `pgrefs' = for 4096 4096 pgrefs f`)
       THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
   ))
   THEN l1_create_external_for_tac
;



val l1_create_counter_non_updated_entry_tac = 
    (FULL_SIMP_TAC (srw_ss()) [Abbr `f`, Abbr `f'`])
    THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
    THEN (Cases_on `pgtype (n2w x) = 2w`)
    THENL [
      (* l2 pages *)
      (SYM_ASSUMPTION_TAC ``a = b:bool[20]->bool[2]``)
      THEN (ASSUME_TAC (UNDISCH 
         (SPECL [``pointed_page:bool[20]``, ``(n2w x):bool[20]``] pgtype_update2_thm)))
      THEN (SYM_ASSUMPTION_TAC ``a = b:bool[20]->bool[2]``)
      THEN (PAT_ASSUM ``Abbrev (pgtype (pointed_page) ≠ 2w ∧ p)`` (fn thm =>
              ASSUME_TAC (SIMP_RULE bool_ss [FUN_EQ_THM, markerTheory.Abbrev_def] thm)))
      THEN (FULL_SIMP_TAC (srw_ss()) [])
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype (n2w x) = 2w``)
      THEN (`
         (∀ph_page''.
          pgtype' ph_page'' ≠ pgtype ph_page'' ⇒
          (pgrefs ph_page'' = 0w))` by (
	  REPEAT STRIP_TAC
          THEN (SYM_ASSUMPTION_TAC ``a = pgtype':bool[20]->bool[2]``)
          THEN (ASSUME_TAC (UNDISCH_ALL pgtype_update_thm))
	  THEN (METIS_TAC [])
     ))
      THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def])
      THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
      THEN (SPEC_ASSUMPTION_TAC ``∀ph_page':word20.pgtype ph_page' ≠ 3w /\ p`` ``(n2w x):word20``)
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype (n2w x) = 2w``)
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``mem = mem':word32->word8``)
      THEN (ASSUME_TAC 
		(UNDISCH (UNDISCH (UNDISCH (UNDISCH (UNDISCH
		(SPECL [``(n2w x):word20``, ``mem':word32->word8``,
			``pgtype:word20->word2``, ``pgtype':word20->word2``,
			``ph_page:word20``
		       ]
		       ref_cnt_equality_ok_changed_types_thm)))))))
      THEN METIS_TAC [],
      (Cases_on `pgtype (n2w x) = 1w`)
      THENL [
        (* it is L1 page table *)
        (FULL_SIMP_TAC (srw_ss()) [count_pages_for_pt_def])
	THEN (ASSUME_TAC (UNDISCH 
         (SPECL [``pointed_page:bool[20]``, ``(n2w x):bool[20]``] pgtype_update_l1_5_thm)))
	THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype (n2w x) = 1w``)
	THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype' (n2w x) = 1w``)
	THEN (FULL_SIMP_TAC (srw_ss()) [])
	THEN (Cases_on `n2w x ≠ (0xFFFFCw:word20 && n2w x)`)
	THENL [
	  FULL_SIMP_TAC (srw_ss()) [],
	  FULL_SIMP_TAC (srw_ss()) []
  	  THEN (`
         (∀ph_page''.
          pgtype' ph_page'' ≠ pgtype ph_page'' ⇒
          (pgrefs ph_page'' = 0w))` by (
	  REPEAT STRIP_TAC
          THEN (SYM_ASSUMPTION_TAC ``a = pgtype':bool[20]->bool[2]``)
          THEN (ASSUME_TAC (UNDISCH_ALL pgtype_update_thm))
	  THEN (METIS_TAC [])
	     ))
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``mem = mem':word32->word8``)
	  THEN (ASSUME_TAC 
		(SPECL [``(n2w x):word20``, ``mem':word32->word8``,
			``pgtype:word20->word2``, ``pgtype':word20->word2``,
			``ph_page:word20``
		       ]
		       ref_cnt_l1_equality_ok_changed_types_thm))
	  THEN RES_TAC
	  THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def])
	  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
	  THEN (SPEC_ASSUMPTION_TAC ``∀ph_page':word20.pgtype ph_page' ≠ 3w /\ p`` ``(n2w x):word20``)
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype (n2w x) = 1w``)
	  THEN RES_TAC
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``invariant_page_type_l1 mem' pgtype pgrefs (n2w x)``)
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``!ph_page:word20.p``)
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``!ph_page:word20. a = b:num``)
	  THEN (FULL_SIMP_TAC (srw_ss()) [count_pages_for_pt_def])
	  THEN (METIS_TAC [])
	],
	(FULL_SIMP_TAC (srw_ss()) [count_pages_for_pt_def])
	THEN (ASSUME_TAC (UNDISCH 
         (SPECL [``pointed_page:bool[20]``, ``(n2w x):bool[20]``] pgtype_update31_thm)))
	THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype (n2w x) ≠ 2w``)
	THEN (FULL_SIMP_TAC (srw_ss()) [])
	THEN (SYM_ASSUMPTION_TAC ``pgtype':bool[20]->bool[2] = h``)
	THEN (ASSUME_TAC (UNDISCH 
         (SPECL [``pointed_page:bool[20]``, ``(n2w x):bool[20]``] pgtype_update_l1_1thm)))
	THEN (SYM_ASSUMPTION_TAC ``pgtype':bool[20]->bool[2] = h``)
	THEN (Cases_on `(0xFFFFCw && n2w x) <> n2w x:word20`)
	THENL [
	  (FULL_SIMP_TAC (srw_ss()) []),
	  (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (Q.UNABBREV_TAC `pointed_page`)
	  THEN (Q.ABBREV_TAC `l2_base_add = pa`)
	  THEN (ASSUME_TAC (SPEC ``x:num`` thm_tem_yyy_thm))
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x < 1048576:num``)
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x ≠ w2n (l2_base_add ⋙ 12)``)
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``n2w x ≠ w2w (l2_base_add ⋙ 12)``)
	  THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``
			   (l2_base_add:word32 = 0xFFFFC000w && l2_base_add) ==>
		           (0xFFFFCw && w2w (l2_base_add ⋙ 12) = w2w (l2_base_add ⋙ 12):word20)
          ``))
	  THEN (METIS_TAC [])
	]
      ]
    ]
;


val l1_create_counter_tac = 
    (FULL_SIMP_TAC (srw_ss()) [invariant_ref_cnt_def])
    THEN (THM_KEEP_TAC ``!x:word20. X`` (SPEC_ASSUMPTION_TAC ``!x:word20. X`` ``(ph_page):word20``))
(* We trasform the goal into the goal *)
(* pgrefs'  + count_pages = pgrefs  + count_pages' *)
    THEN (SUBGOAL_THEN ``
(w2n ((pgrefs' :word20 -> word30) (ph_page :word20)) +
 count_pages (mem :word32 -> word8) (pgtype :word20 -> word2) ph_page) =
(w2n ((pgrefs :word20 -> word30) (ph_page :word20)) +
count_pages (mem' :word32 -> word8) (pgtype' :word20 -> word2) ph_page)
``
 (fn thm =>
  ASSUME_TAC thm
  (* THEN (SIMP_BY_ASSUMPTION_TAC ``pgtype:bool[20]->bool[2] = pgtype'``) *)
  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``w2n (pgrefs ph_page) = count_pages mem pgtype' ph_page``)
  THEN (FULL_SIMP_TAC (arith_ss) [arithmeticTheory.EQ_ADD_RCANCEL])
))
    THEN (SIMP_TAC (srw_ss()) [count_pages_def])
    THEN (Q.ABBREV_TAC `f = (λit.
           (let global_page:bool[20] = n2w it in
           count_pages_for_pt mem pgtype ph_page global_page))`)
    THEN (Q.ABBREV_TAC `f' = (λit.
           (let global_page:bool[20] = n2w it in
           count_pages_for_pt mem' pgtype' ph_page global_page))`)
    THEN (SIMP_TAC (srw_ss()) [SUM])   
    THEN (SPLITTER_SUM_TAC
		   ``w2n (pa:bool[32] ⋙ 12)``
		   ``1048576:num``
		   ``f:num->num``
		   ``f':num->num``
		   ``w2n ((pgrefs':bool[20]->bool[30]) ph_page)``
		   ``w2n ((pgrefs:bool[20]->bool[30]) ph_page)``)
    THENL [
       (* for the uppdate L1 *)
       (* ALL_TAC *)
       l1_create_counter_updated_entry_tac
     ,
       (* for the non updated pages *)
       (* ALL_TAC *)
       l1_create_counter_non_updated_entry_tac
     ,
       (* limit *)
       (Q.ABBREV_TAC `l2_base_add = pa`)
       THEN (ASSUME_TAC thm_tem_2_thm)
       THEN FULL_SIMP_TAC (srw_ss()) []
    ]

;





(* MAIN THEOREM *)
val l1_create_goal = ``
  ! c1 va:word32. 
    (c1 = rec'sctlrT 1w ) ==>
    ((invariant_ref_cnt c1 c2 c3 mem pgtype pgrefs) ==>
       (let (c1',c2',c3',mem',pgtype', pgrefs') = 
       (create_l1_pt (rec'sctlrT 1w, c2, c3, mem, pgtype, pgrefs)  pa attrs)
	in
       (invariant_ref_cnt c1' c2' c3' mem' pgtype' pgrefs')))
``;

val let_ss = simpLib.mk_simpset [boolSimps.LET_ss] ;


val l1_create_thm = Q.store_thm("l1_create_thm",
  `^l1_create_goal`,
  (RW_TAC (srw_ss()) [])
(* Before opening the invariant to check, we handle the failures *)
  THEN l1_create_exception_case_tac
  THEN (RW_TAC (srw_ss()) [invariant_ref_cnt_def])
  THENL [
(* Split for the two main part of the invariant *)
    l1_map_invariant_page_type_tac
    (* ALL_TAC *)
    (* cheat *)
  ,
    l1_create_counter_tac
    (* ALL_TAC *)
  ]
);

val _ = export_theory ();

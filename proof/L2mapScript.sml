(* 
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/spec/" :: !loadPath;
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/" ::  !loadPath;
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/proof/" :: !loadPath;
load "mmu_utilsLib";
load "hypervisor_modelTheory";
load "mmu_propertiesTheory";
load "helperTheory";
*)



open HolKernel boolLib bossLib Parse wordsLib;
open hypervisor_modelTheory;
open mmu_propertiesTheory;
open tinyTheory;
open helperTheory;

open sum_numTheory;
open wordsTheory;

load "mmu_utilsLib";
open mmu_utilsLib;
load "helperLib";
open helperLib;
open numSyntax;

val _ = new_theory "L2map";


val sum_if_thm = Q.store_thm("sum_if_thm", 
`
!c f.
  (if c then (GSUM (0,1024) f) else 0) =
  (GSUM (0,1024) \x.if c then f(x) else 0)
`,
  REPEAT (STRIP_TAC)
  THEN (Cases_on `c`)
  THENL [
    (FULL_SIMP_TAC (srw_ss()) [])
    THEN METIS_TAC [],
    (FULL_SIMP_TAC (srw_ss()) [])
    THEN (`(∀m. 0 ≤ m ∧ m < 1024 ⇒ ((\x.0) m = 0))` by (METIS_TAC []))
    THEN METIS_TAC [
      SPECL [``0:num``, ``1024:num``, ``(\x.0):num->num``] GSUM_ZERO
    ]
  ]
);

val l2_map_exception_case_tac = 
TRY (SYM_ASSUMPTION_TAC ``w = hyper_l2_map_pagetable x y z z1 z2``)
THEN THM_KEEP_TAC ``hyper_l2_map_pagetable x y z z1 z2 = w`` (
    (FULL_SIMP_TAC (srw_ss()) [hyper_l2_map_pagetable_def])
    THEN (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF])
    THEN (Q.ABBREV_TAC `ap = 4w && attrs ≫ 7 ‖ 3w && attrs ≫ 4`)
    THEN (Q.ABBREV_TAC `ph_page:word20 = (w2w (0xFFFFFw && (phadd ⋙ 12)))`)
    THEN (Q.ABBREV_TAC `cnd1 = ap ≠ 1w /\ ap ≠ 2w /\ ap ≠ 3w`)
    THEN (Q.ABBREV_TAC `cnd2 = (ap = 3w) ∧ (pgtype ph_page ≠ 0w ∨ (pgrefs ph_page = 0x3FFFFFFFw))`)
    THEN (Cases_on `cnd1`)
    THENL [
      (FULL_SIMP_TAC (srw_ss()) []),
      (FULL_SIMP_TAC (srw_ss()) [])
      THEN (Cases_on `cnd2`)
      THENL [
       (FULL_SIMP_TAC (srw_ss()) []),
       (FULL_SIMP_TAC (srw_ss()) [])
       THEN (Cases_on `pgtype (w2w (l2_base_add ⋙ 12)) ≠ 2w`)
       THENL [
         (FULL_SIMP_TAC (srw_ss()) []),
	 (FULL_SIMP_TAC (srw_ss()) [])
	 THEN (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF])
	 THEN (Q.ABBREV_TAC `entryAddrL2:word32 = 0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2`)
	 THEN (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF])
	 THEN (Q.ABBREV_TAC `page_desc:word32 = read_mem32 (entryAddrL2,mem)`)
	 THEN (Cases_on `((3w && page_desc) <> 0w)`)
	 (* Second failure: the entry was unmapped, nothing change in the system *)
	 THENL [
           (FULL_SIMP_TAC (srw_ss()) []),
	   (FULL_SIMP_TAC (srw_ss()) [])
	 ]
       ]
      ]
    ]
  );


val l2_map_unchanged_mem_thm = Q.store_thm("l2_map_unchanged_mem_thm", 
  `!ptb:word20 (l2_base_add:bool[32]).
   ((c1',c2':bool[32],c3':bool[32],mem',pgtype', pgrefs') = 
       (hyper_l2_map_pagetable (rec'sctlrT 1w, c2, c3, mem, pgtype, pgrefs) 
	     (l2_base_add:bool[32]) (l2_index:bool[32]) phadd attrs)) ==>
	        (
		 (ptb <> w2w(l2_base_add >>> 12):word20) ==>
                 (ph_page_unchanged ptb mem mem')
               )
`,
  (FULL_SIMP_TAC (srw_ss()) [ph_page_unchanged_def])
  THEN (REPEAT STRIP_TAC)
  THEN l2_map_exception_case_tac
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN Q.ABBREV_TAC `page_desc'=4092w && attrs ‖ 0xFFFFF000w && phadd ‖ 2w`
  THEN (SYM_ASSUMPTION_TAC ``write_mem32 (entryAddrL2,mem,page_desc') = mem'``)
  THEN FULL_SIMP_TAC (srw_ss()) [Abbr `entryAddrL2`]
  THEN (
      ASSUME_TAC (
       WRITE_READ_UNCH_RULE 
	   ``((w2w ((a :word32) >>> 12) :word20) <>  (w2w ((l2_base_add
  :word32) >>> 12) :word20)) /\ (a = 0xFFFFFFFCw && a)``
           ``0xFFFFF000w && l2_base_add:bool[32] !! 4092w && l2_index << 2``
	   ``a:bool[32]``
           ``page_desc':bool[32]``
	   ``mem:bool[32]->bool[8]``
	   ``mem':bool[32]->bool[8]``
	 ))
  THEN METIS_TAC []
);

val l2_map_unchanged_pagetype_thm = Q.store_thm("l2_map_unchanged_pagetype_thm", 
  `!(l2_base_add:bool[32]).
   ((c1',c2':bool[32],c3':bool[32],mem',pgtype', pgrefs') = 
       (hyper_l2_map_pagetable (rec'sctlrT 1w, c2, c3, mem, pgtype, pgrefs) 
	     (l2_base_add:bool[32]) (l2_index:bool[32]) phadd attrs)) ==>
	     ( pgtype' = pgtype)
`,
  FULL_SIMP_TAC (srw_ss()) [hyper_l2_map_pagetable_def]
  THEN RW_TAC (srw_ss()) [LET_DEF]
);


val l2_map_unchanged_l1_mem_thm = Q.store_thm("l2_map_unchanged_l1_mem_thm", 
  `!ptb:word20 (l2_base_add:bool[32]).
   (invariant_page_type (rec'sctlrT 1w) c2 c3 mem pgtype pgrefs) ==>
   ((c1',c2':bool[32],c3':bool[32],mem',pgtype', pgrefs') = 
       (hyper_l2_map_pagetable (rec'sctlrT 1w, c2, c3, mem, pgtype, pgrefs) 
	     (l2_base_add:bool[32]) (l2_index:bool[32]) phadd attrs)) ==>
             (ph_l1_unchanged ptb pgtype mem mem')
`,
  (FULL_SIMP_TAC (srw_ss()) [ph_l1_unchanged_def])
  THEN STRIP_TAC
  THEN (Q.ABBREV_TAC `goal =   ph_page_unchanged ptb mem mem' /\
                               ph_page_unchanged (ptb + 1w) mem mem' /\
                               ph_page_unchanged (ptb + 2w) mem mem' /\
                               ph_page_unchanged (ptb + 3w) mem mem'`)
  THEN (REPEAT STRIP_TAC)
(* check that the four pages has not been changed *)
  THEN l2_map_exception_case_tac
  THENL [
     FULL_SIMP_TAC (srw_ss()) [Abbr `goal`]
     THEN METIS_TAC [ph_page_unchanged_def],
     FULL_SIMP_TAC (srw_ss()) [Abbr `goal`]
     THEN METIS_TAC [ph_page_unchanged_def],
     FULL_SIMP_TAC (srw_ss()) [Abbr `goal`]
     THEN METIS_TAC [ph_page_unchanged_def],
     FULL_SIMP_TAC (srw_ss()) [Abbr `goal`]
     THEN METIS_TAC [ph_page_unchanged_def],
     (* REAL GOAL *)
     (SYM_ASSUMPTION_TAC ``hyper_l2_map_pagetable x y z z1 z2= w``)
     THEN ASSUME_TAC (UNDISCH (
      SPECL [``ptb:bool[20]``, ``l2_base_add:bool[32]``]
      l2_map_unchanged_mem_thm))
     THEN ASSUME_TAC (UNDISCH (
      SPECL [``ptb+1w:bool[20]``, ``l2_base_add:bool[32]``]
      l2_map_unchanged_mem_thm))
     THEN ASSUME_TAC (UNDISCH (
      SPECL [``ptb+2w:bool[20]``, ``l2_base_add:bool[32]``]
      l2_map_unchanged_mem_thm))
     THEN ASSUME_TAC (UNDISCH (
      SPECL [``ptb+3w:bool[20]``, ``l2_base_add:bool[32]``]
      l2_map_unchanged_mem_thm))
     THEN ASSUME_TAC (
     GSYM (UNDISCH ( UNDISCH (
    SPECL [``pgtype:bool[20]->bool[2]``,
	   ``(w2w (l2_base_add:word32 >>> 12)):word20``,
           ``ptb:bool[20]``]
    diff_pg_type_diff_pages_thm
    ))))
     (* instantiate the invariant to check the type of the next pages *)
     THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def])
     THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
    (* use the invariant to guarantee that the ph_page is a valid l1 *)
     THEN (SPEC_ASSUMPTION_TAC ``∀ph_page:bool[20].p``
			       ``ptb:bool[20]``)
     THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(pgtype ptb = 1w)``)
     THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``0xFFFFCw && ptb = ptb``)
     (* blast stuff *)
    THEN (FULL_SIMP_TAC (srw_ss()) [UNDISCH (
        blastLib.BBLAST_PROVE ``
          (0xFFFFCw && ptb:bool[20] = ptb) ==>
          ((ptb ‖ 1w) = (ptb + 1w))
	``)])
    THEN (FULL_SIMP_TAC (srw_ss()) [UNDISCH (
        blastLib.BBLAST_PROVE ``
          (0xFFFFCw && ptb:bool[20] = ptb) ==>
          ((ptb ‖ 2w) = (ptb + 2w))
	``)])
    THEN (FULL_SIMP_TAC (srw_ss()) [UNDISCH (
        blastLib.BBLAST_PROVE ``
          (0xFFFFCw && ptb:bool[20] = ptb) ==>
          ((ptb ‖ 3w) = (ptb + 3w))
	``)])
    THEN ASSUME_TAC (
    GSYM (UNDISCH ( UNDISCH (
    SPECL [``pgtype:bool[20]->bool[2]``,
	   ``(w2w (l2_base_add:word32 >>> 12)):word20``,
           ``ptb+1w:bool[20]``]
    diff_pg_type_diff_pages_thm
    ))))
    THEN ASSUME_TAC (
    GSYM (UNDISCH ( UNDISCH (
    SPECL [``pgtype:bool[20]->bool[2]``,
	   ``(w2w (l2_base_add:word32 >>> 12)):word20``,
           ``ptb+2w:bool[20]``]
    diff_pg_type_diff_pages_thm
    ))))
    THEN ASSUME_TAC (
    GSYM (UNDISCH ( UNDISCH (
    SPECL [``pgtype:bool[20]->bool[2]``,
	   ``(w2w (l2_base_add:word32 >>> 12)):word20``,
           ``ptb+3w:bool[20]``]
    diff_pg_type_diff_pages_thm
    ))))
     THEN RES_TAC
     THEN METIS_TAC []
  ]
);




val l2_map_unchanged_l2_mem_thm = Q.store_thm("l2_map_unchanged_l2_mem_thm", 
     `
!l2_idx:word10 c1 pgtype'.
      ((c1,c2':bool[32],c3':bool[32],mem',pgtype',pgrefs') = 
            (hyper_l2_map_pagetable (rec'sctlrT 1w, c2, c3, mem, pgtype, pgrefs) 
	     (l2_base_add:bool[32]) (l2_index:bool[32]) phadd attrs)) ==>
       ((l2_idx) <> (w2w (l2_index  && 0x000003FFw)):word10) ==>
       (l2_entry_unchanged l2_base_add l2_idx mem mem')
`,
      (REPEAT STRIP_TAC)
  THEN (FULL_SIMP_TAC (srw_ss()) [l2_entry_unchanged_def])
  THEN l2_map_exception_case_tac
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `entryAddrL2`])
  THEN (METIS_TAC [
       WRITE_READ_UNCH_RULE 
	   ``((l2_idx) <> (w2w ( 1023w && l2_index:bool[32])):word10)``
           ``0xFFFFF000w && l2_base_add:bool[32] !! 4092w && l2_index << 2``
	   ``(0xFFFFF000w && l2_base_add ) !! (w2w (l2_idx:bool[10])):word32 << 2``
           ``4092w && attrs ‖ 0xFFFFF000w && phadd ‖ 2w:bool[32]``
	   ``mem:bool[32]->bool[8]``
	   ``mem':bool[32]->bool[8]``])
);









val l2_map_invariant_page_type_l2_tac =
(* We have to check that all l2 are still valid *)
(* remember that one entry of the l2s is the updated one *)
    Cases_on `ph_page' <> (w2w (l2_base_add ⋙ 12))`
    THENL [
    (* the page we are checking is different from the page updated *)
    (* we know that all words in the phisical page are unchanged *)
      ASSUME_TAC (
        SPECL [``ph_page':bool[20]``, ``l2_base_add:bool[32]``]
	      l2_map_unchanged_mem_thm)
      THEN (SYM_ASSUMPTION_TAC ``hyper_l2_map_pagetable x y z z1 z2 = w``)
      THEN (SIMP_BY_ASSUMPTION_TAC ``a = hyper_l2_map_pagetable x y z z1 z2``)
      THEN (SIMP_BY_ASSUMPTION_TAC ``ph_page' ≠ w2w (l2_base_add ⋙ 12)``)
    (* we instantiate the invariant for the phisical page and we show
    that the invariant hold for the new memory
     trick: we have two invariants
     *)
      THEN (SPEC_ASSUMPTION_TAC ``!ph_page:bool[20].p`` ``ph_page':bool[20]``)
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype' ph_page' = 2w``)
      THEN (ASSUME_TAC  (
           SPECL [``pgtype':bool[20]->bool[2]``, ``ph_page':word20``]
	   invariant_page_type_l2_unchanged_mem_thm
      ))
      THEN (SIMP_BY_ASSUMPTION_TAC ``pgtype' ph_page' = 2w``)
      THEN (SIMP_BY_ASSUMPTION_TAC ``invariant_page_type_l2 mem pgtype' pgrefs ph_page'``)
      THEN RES_TAC
      THEN FULL_SIMP_TAC (srw_ss()) []
    (* Finally we show that the invariant hold independently
     from the updated ref_cnt
     *)
      THEN (ASSUME_TAC (SPECL [
			``mem':bool[32]->bool[8]``, 
			``pgtype':bool[20]->bool[2]``,
			``ph_page':word20``]
		       ref_inv_l2_thm))
      THEN (METIS_TAC []),
    (* Second case: we are checking the updated page *)
      FULL_SIMP_TAC (srw_ss()) []
      THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_l2_def])
      THEN (STRIP_TAC)
      THEN BasicProvers.LET_ELIM_TAC
      THEN (Cases_on `1023w && l2_index = (w2w l2_idx)`)
      THENL [
      (* First case: we check the updated entry *)
      (* first we show that the address where we read is
	 the same written *)
      (* TODO THIS is really dirty, mainlty to prevent splits *)
	 (Q.ABBREV_TAC `prevent_split =
(3w && l2_desc = 0w) ∨
((3w && l2_desc = 2w) ∨ (3w && l2_desc = 3w)) ∧
if pgtype' (rec'l2SmallT l2_desc).addr ≠ 0w then
  ((rec'l2SmallT l2_desc).ap = 1w) \/
  ((rec'l2SmallT l2_desc).ap = 2w)
else   ((rec'l2SmallT l2_desc).ap = 1w) \/ ((rec'l2SmallT l2_desc).ap = 2w) ∨ ((rec'l2SmallT l2_desc).ap =
3w)
`)
	 THEN (Q.ABBREV_TAC `other_split = (ap ≠ 3w ∨ (pgtype ph_page = 0w) ∧ pgrefs ph_page ≠ 0x3FFFFFFFw)`)
	 THEN (Q.ABBREV_TAC `other_split2 = (ap = 1w) ∨ (ap = 2w) ∨ (ap = 3w)`)
         THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `entryAddrL2`])
         THEN ( ASSUME_TAC (UNDISCH(
	 blastLib.BBLAST_PROVE ``
( 1023w && l2_index = w2w l2_idx) ==>
(
(w2w (w2w ((l2_base_add :word32) ⋙ (12 :num)) :word20) : word32) ≪ (12 :num) ‖
(w2w (l2_idx :word10) :word32) ≪ (2 :num) =
(0xFFFFF000w :word32) && (l2_base_add :word32) ‖ (4092w :word32)
 &&(l2_index :word32) ≪ (2 :num)
)
``
	 )))
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
	 THEN (Q.ABBREV_TAC `entryAddrL2:word32 =
             0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2`)
      (* we show that what we read is what has been written *)
	 THEN (SYM_ASSUMPTION_TAC ``write_mem32 a = mem'``)
	 THEN (ASSUME_TAC (
	     UNDISCH (
	     SPECL [``entryAddrL2:bool[32]``,
		    ``4092w && attrs ‖ 0xFFFFF000w && phadd ‖ 2w:bool[32]``,
		    ``mem:bool[32]->bool[8]``,
		    ``mem':bool[32]->bool[8]``]
	     write_read_thm)))
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
	 THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `l2_desc`])
	 (* check that the page has been mapped as small page *)
	 THEN (FULL_SIMP_TAC (srw_ss()) [(blastLib.BBLAST_PROVE ``
	   3w && (4092w && attrs ‖ 0xFFFFF000w && phadd ‖ 2w):bool[32] = 2w
         ``)])
	 THEN (FULL_SIMP_TAC (srw_ss()) [rec'l2SmallT_def])
	 THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `ph_page`])
	 THEN (FULL_SIMP_TAC (srw_ss()) [(blastLib.BBLAST_PROVE ``
(
 ((31 >< 12) (4092w && attrs ‖ 0xFFFFF000w && phadd ‖ 2w:bool[32])) =
 (w2w (0xFFFFFw && phadd ⋙ 12)):bool[20]
)
         ``)])
	 THEN (Q.ABBREV_TAC `ph_page:bool[20] = (w2w (0xFFFFFw && phadd ⋙ 12))`)
	 THEN (Cases_on `pgtype' ph_page ≠ 0w`)
	 THENL [
	   (FULL_SIMP_TAC (srw_ss()) [])
	   THEN (SIMP_BY_ASSUMPTION_TAC ``pgtype = pgtype':bool[20]->bool[2]``)
	   THEN (SIMP_BY_ASSUMPTION_TAC ``pgtype' ph_page ≠ 0w``)
	   THEN (Q.UNABBREV_TAC  `other_split`)
	   THEN (Q.UNABBREV_TAC  `other_split2`)
	   THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``
	     (ap ≠ 3w:bool[32]) ==>
	     ((ap = 1w) ∨ (ap = 2w) ∨ (ap = 3w)) ==>
	     (ap = 4w && attrs ≫ 7 ‖ 3w && attrs ≫ 4) ==>
	     (
((((((9 :num) >< (9 :num))((4092w :word32) && (attrs :word32) ‖(0xFFFFF000w :word32) && (phadd :word32) ‖ (2w:word32)) :word1) @@(((5 :num) >< (4 :num))((4092w :word32) && attrs ‖(0xFFFFF000w :word32) && phadd ‖ (2w :word32)) :word2)):word3) =
           (2w:word3)) \/
((((((9 :num) >< (9 :num))((4092w :word32) && (attrs :word32) ‖(0xFFFFF000w :word32) && (phadd :word32) ‖ (2w:word32)) :word1) @@(((5 :num) >< (4 :num))((4092w :word32) && attrs ‖(0xFFFFF000w :word32) && phadd ‖ (2w :word32)) :word2)):word3) =
           (1w:word3))
)
	   ``))
	   THEN METIS_TAC [],
	   (FULL_SIMP_TAC (srw_ss()) [])
	   THEN (Q.UNABBREV_TAC  `other_split`)
	   THEN (Q.UNABBREV_TAC  `other_split2`)
	   THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``
	     ((ap = 1w) ∨ (ap = 2w) ∨ (ap = 3w)) ==>
	     (ap = 4w && attrs ≫ 7 ‖ 3w && attrs ≫ 4) ==>
	     (
((((((9 :num) >< (9 :num))((4092w :word32) && (attrs :word32) ‖(0xFFFFF000w :word32) && (phadd :word32) ‖ (2w:word32)) :word1) @@(((5 :num) >< (4 :num))((4092w :word32) && attrs ‖(0xFFFFF000w :word32) && phadd ‖ (2w :word32)) :word2)):word3) =
           (2w:word3)) \/
((((((9 :num) >< (9 :num))((4092w :word32) && (attrs :word32) ‖(0xFFFFF000w :word32) && (phadd :word32) ‖ (2w:word32)) :word1) @@(((5 :num) >< (4 :num))((4092w :word32) && attrs ‖(0xFFFFF000w :word32) && phadd ‖ (2w :word32)) :word2)):word3) =
           (1w:word3)) \/
((((((9 :num) >< (9 :num))((4092w :word32) && (attrs :word32) ‖(0xFFFFF000w :word32) && (phadd :word32) ‖ (2w:word32)) :word1) @@(((5 :num) >< (4 :num))((4092w :word32) && attrs ‖(0xFFFFF000w :word32) && phadd ‖ (2w :word32)) :word2)):word3) =
           (3w:word3))
)
	   ``))
	   THEN METIS_TAC []
	 ],
      (* Second case, the we read from a different position *)
	 (Q.ABBREV_TAC `prevent_split =
(3w && l2_desc = 0w) ∨
((3w && l2_desc = 2w) ∨ (3w && l2_desc = 3w)) ∧
if pgtype' (rec'l2SmallT l2_desc).addr ≠ 0w then
  ((rec'l2SmallT l2_desc).ap = 1w) \/
  ((rec'l2SmallT l2_desc).ap = 2w)
else   ((rec'l2SmallT l2_desc).ap = 1w) \/ ((rec'l2SmallT l2_desc).ap = 2w) ∨ ((rec'l2SmallT l2_desc).ap =
3w)
`)
	 THEN (Q.ABBREV_TAC `other_split = (ap ≠ 3w ∨ (pgtype ph_page = 0w) ∧ pgrefs ph_page ≠ 0x3FFFFFFFw)`)
	 THEN (Q.ABBREV_TAC `other_split2 = (ap = 1w) ∨ (ap = 2w) ∨ (ap = 3w)`)
	 THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `entryAddrL2`])
	 THEN (ASSUME_TAC (
	 WRITE_READ_UNCH_RULE
	   ``(1023w && l2_index:bool[32]) ≠ w2w (l2_idx:bool[10])``
	   ``0xFFFFF000w:bool[32] && l2_base_add ‖ 4092w && l2_index ≪	2``
	   ``(w2w (w2w ((l2_base_add :word32) ⋙ (12 :num)) :word20) :
	     word32) ≪ (12 :num) ‖ (w2w (l2_idx :word10) :word32) ≪ (2 :num)``
	   ``4092w && attrs ‖ 0xFFFFF000w && phadd ‖ 2w:bool[32]``
	   ``mem:bool[32]->bool[8]``
	   ``mem':bool[32]->bool[8]``))
	 THEN (SIMP_BY_ASSUMPTION_TAC ``(1023w && l2_index) ≠ w2w l2_idx``)
	 THEN (SYM_ASSUMPTION_TAC ``write_mem32 a = mem'``)
	 THEN (SIMP_BY_ASSUMPTION_TAC ``mem' = a``)
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
	 (* now we know that the memory did not changed, we need to use  *)
	 (* the invarnant to verify its property *)
	 THEN (SPEC_ASSUMPTION_TAC ``!ph_page:bool[20].p``
		``(w2w (l2_base_add:bool[32] ⋙ 12) :bool[20])``)
	 THEN (SIMP_BY_ASSUMPTION_TAC ``pgtype' (w2w (l2_base_add ⋙ 12)) = 2w``)
	 THEN (SPEC_ASSUMPTION_TAC ``!l2_idx:bool[10].p`` ``l2_idx:word10``)
	 THEN (LET_HYP_ELIM_TAC [])
	 THEN REABBREV_TAC
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
      ]
    ]
;


val l2_map_invariant_page_type_l1_tac =
    (* if we did non change the 4 pages, than the invariant hold *)
    ASSUME_TAC (
    UNDISCH (UNDISCH (
    SPECL [``pgtype':bool[20]->bool[2]``, ``ph_page':bool[20]``]
    invariant_page_type_l1_unchanged_mem_thm
    )))
    THEN (ASSUME_TAC (
      SPECL [``ph_page':bool[20]``, ``l2_base_add:bool[32]``]
	  l2_map_unchanged_l1_mem_thm
    ))
    THEN (SYM_ASSUMPTION_TAC ``hyper_l2_map_pagetable x y z z1 z2 = w``)
    THEN (FULL_SIMP_TAC (srw_ss()) [
	  UNDISCH (
	  SPEC ``l2_base_add:bool[32]``
	  l2_map_unchanged_pagetype_thm)
	 ])
    THEN (`ph_l1_unchanged ph_page' pgtype mem mem'` by METIS_TAC [])
    (* Enstablish that the l1 invariant hold for the original ph_page *)
    THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def])
    THEN (SPEC_ASSUMPTION_TAC ``!ph_page:bool[20].p`` ``ph_page':bool[20]``)
    THEN (`invariant_page_type_l1 mem pgtype pgrefs ph_page'` by METIS_TAC [])
    THEN (FULL_SIMP_TAC (srw_ss()) [ph_l1_unchanged_def])
    THEN (METIS_TAC [ref_inv_l1_thm])
;



val l2_map_invariant_page_type_tac =
    (FULL_SIMP_TAC (srw_ss()) [ LET_DEF])
    THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def])
    THEN (REPEAT CONJ_TAC)
    (* Three sub goals *)
    THENL [
    (* The MMU is still enabled *)
      (SYM_ASSUMPTION_TAC ``rec'sctlrT 1w = c1'``)
      THEN (FULL_SIMP_TAC (srw_ss()) [rec'sctlrT_def]),
    (* The Domain are still well setupped, keep the invariant for further use *)
      (FULL_SIMP_TAC (srw_ss()) [invariant_ref_cnt_def])
      THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def]),
    (* The real part of the invariant *)
      (FULL_SIMP_TAC (srw_ss()) [ LET_DEF])
      THEN (FULL_SIMP_TAC (srw_ss()) [invariant_ref_cnt_def])
      THEN (THM_KEEP_TAC ``invariant_page_type c1' c2' c3' mem pgtype' pgrefs``
			 (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def]))
    (* First we check that the main L1 is still 16kb aligned *)
      THEN (LET_HYP_ELIM_TAC [])
      THEN (FULL_SIMP_TAC (srw_ss()) [])
      THEN (REPEAT STRIP_TAC)
    (* Two goals: one for the L1 pages, one for the L2 pages *)
      THENL [
        (* check l2 pages *)
        l2_map_invariant_page_type_l2_tac,
        (* check l1 pages *)
	l2_map_invariant_page_type_l1_tac
      ]
    ]
;



val add_thm = (blastLib.BBLAST_PROVE ``
((w2w (w2w (l2_base_add ⋙ 12) :word20) :word32) ≪ 12 ‖
(w2w (w2w ((1023w :word32) && l2_index) :word10) :word32) ≪ 2) =
(0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2)
	``);
val sum_l2_base_chaged_pt_change_entry_tac =
    (FULL_SIMP_TAC (srw_ss()) [])
    THEN (SIMP_TAC(srw_ss())[l2_count_def])
    THEN (SIMP_TAC (srw_ss()) [LET_DEF, GSYM w2w_def, w2w_id])
    THEN (REWRITE_TAC [add_thm])
    THEN (REABBREV_TAC)
    THEN (Q.ABBREV_TAC `page_desc' = (read_mem32 (entryAddrL2,mem'))`)
    THEN (Q.ABBREV_TAC `no_spit = ap ≠ 3w ∨ (pgtype ph_page = 0w) ∧ pgrefs ph_page ≠ 0x3FFFFFFFw`)
    THEN (Q.ABBREV_TAC `no_spit2 = (ap = 1w) ∨ (ap = 2w) ∨ (ap = 3w)`)
    (* check what we write: *)
    THEN (`page_desc' = 4092w && attrs ‖ 0xFFFFF000w && phadd ‖ 2w` by
           (
	    (SYM_ASSUMPTION_TAC ``write_mem32 a = mem'``)
  	    THEN (ASSUME_TAC (
	     UNDISCH (
	     SPECL [``entryAddrL2:bool[32]``,
		    ``4092w && attrs ‖ 0xFFFFF000w && phadd ‖ 2w:bool[32]``,
		    ``mem:bool[32]->bool[8]``,
		    ``mem':bool[32]->bool[8]``]
	     write_read_thm)))
	    THEN METIS_TAC []
	 ))
    (* first we show that the intial counter for the old page_desc is zero since it was unmapped *)
    THEN (`~(((3w && page_desc = 2w) ∨ (3w && page_desc = 3w)) ∧ ((rec'l2SmallT page_desc).ap = 3w))` by (
	     (FULL_SIMP_TAC (srw_ss()) [])
       ))
    THEN (Q.ABBREV_TAC `no_split4 = (((3w && page_desc = 2w) ∨ (3w && page_desc = 3w)) ∧ ((rec'l2SmallT page_desc).ap = 3w))`)
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THEN (Q.ABBREV_TAC `written_entry = 4092w && attrs ‖ 0xFFFFF000w && phadd ‖ 2w`)
    THEN (Cases_on `(rec'l2SmallT written_entry).addr <> ph_page'`)
    THENL [
      (* first case: entry we are counting is not pointed by the updated entry *)
      (FULL_SIMP_TAC(srw_ss())[])
      THEN (SUBGOAL_THEN 
		``(pgrefs':bool[20]->bool[30])(ph_page') = (pgrefs:bool[20]->bool[30])(ph_page')``
		(fn thm => ASSUME_TAC thm
                             THEN  METIS_TAC []
		))
      THEN (SYM_ASSUMPTION_TAC ``f = pgrefs':bool[20]->bool[30]``)
      THEN (FULL_SIMP_TAC (srw_ss()) [])
      THEN (FULL_SIMP_TAC (srw_ss()) [COND_RATOR])
      THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM])
      THEN (SUBGOAL_THEN 
		``ph_page:bool[20] <> ph_page':bool[20]``
		(fn thm => ASSUME_TAC thm
                             THEN  METIS_TAC []
		))
      THEN (Q.UNABBREV_TAC `ph_page`)
      THEN (FULL_SIMP_TAC (srw_ss()) [rec'l2SmallT_def])
      THEN (METIS_TAC [(blastLib.BBLAST_PROVE ``
(written_entry:bool[32] = 4092w && attrs ‖ 0xFFFFF000w && phadd ‖ 2w) ==>
((31 >< 12) written_entry ≠ ph_page':bool[20]) ==>
(w2w (0xFFFFFw:bool[32] && phadd ⋙ 12) ≠ ph_page')
      ``)]),
       (* second case: the new mapped entry *)
       (FULL_SIMP_TAC (srw_ss()) [])
       THEN (`ph_page' = ph_page` by (
	     (Q.UNABBREV_TAC `ph_page`)
	     THEN (FULL_SIMP_TAC (srw_ss()) [rec'l2SmallT_def])
	     THEN (METIS_TAC [(blastLib.BBLAST_PROVE ``
(written_entry:bool[32] = 4092w && attrs ‖ 0xFFFFF000w && phadd ‖ 2w) ==>
((31 >< 12) written_entry ≠ ph_page':bool[20]) ==>
(w2w (0xFFFFFw:bool[32] && phadd ⋙ 12) ≠ ph_page')
      ``)])
       ))
       THEN (FULL_SIMP_TAC (srw_ss()) [])
       THEN (Cases_on `pgtype ph_page <> 0w`)
       THENL [
	  (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (SUBGOAL_THEN 
		``(pgrefs':bool[20]->bool[30])(ph_page) = (pgrefs:bool[20]->bool[30])(ph_page)``
		(fn thm => ASSUME_TAC thm
                             THEN  METIS_TAC []
		))
	  THEN (SYM_ASSUMPTION_TAC ``f = pgrefs':bool[20]->bool[30]``)
	  THEN (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (FULL_SIMP_TAC (srw_ss()) [COND_RATOR])
	  THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM])
	  THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `no_spit`]),
	  (* other case, we are pointing the phisical data page *)
	  (FULL_SIMP_TAC (srw_ss()) [])
          THEN (Cases_on `ap <> 3w`)
	  THENL [
	     (* the updated entry does not change the counter *)
	     (FULL_SIMP_TAC (srw_ss()) [])
             THEN (`(rec'l2SmallT written_entry).ap <> 3w` by (
		   (FULL_SIMP_TAC (srw_ss()) [rec'l2SmallT_def])
		   THEN (Q.UNABBREV_TAC `written_entry`)
		   THEN (Q.UNABBREV_TAC `ap`)
		   THEN (METIS_TAC [blastLib.BBLAST_PROVE ``
((4w && attrs ≫ 7 ‖ 3w && attrs ≫ 4) ≠ 3w:bool[32]) ==>
((((((9 :num) >< (9 :num))
     ((4092w :word32) && (attrs :word32) ‖
      (0xFFFFF000w :word32) && (phadd :word32) ‖ (2w :word32)) :
     word1) @@
  (((5 :num) >< (4 :num))
     ((4092w :word32) && attrs ‖ (0xFFFFF000w :word32) && phadd ‖ (2w
        :word32)) :word2))
   :word3) ≠ (3w :word3))
		   ``])
		  ))
	     THEN (FULL_SIMP_TAC (srw_ss()) [])
	     THEN (SUBGOAL_THEN 
		``(pgrefs':bool[20]->bool[30])(ph_page) = (pgrefs:bool[20]->bool[30])(ph_page)``
		(fn thm => ASSUME_TAC thm
                             THEN  METIS_TAC []
		))
	     THEN (SYM_ASSUMPTION_TAC ``f = pgrefs':bool[20]->bool[30]``)
	     THEN (FULL_SIMP_TAC (srw_ss()) []),
	     (* the updated entry really changes the counter with +1*)
	     (FULL_SIMP_TAC (srw_ss()) [])
	     THEN (`(rec'l2SmallT written_entry).ap = 3w` by (
		   (FULL_SIMP_TAC (srw_ss()) [rec'l2SmallT_def])
		   THEN (Q.UNABBREV_TAC `written_entry`)
		   THEN (METIS_TAC [blastLib.BBLAST_PROVE ``
(3w = (4w && attrs ≫ 7 ‖ 3w && attrs ≫ 4)) ==>
((((((9 :num) >< (9 :num))
     ((4092w :word32) && (attrs :word32) ‖
      (0xFFFFF000w :word32) && (phadd :word32) ‖ (2w :word32)) :
     word1) @@
  (((5 :num) >< (4 :num))
     ((4092w :word32) && attrs ‖ (0xFFFFF000w :word32) && phadd ‖ (2w
        :word32)) :word2))
   :word3) = (3w :word3))
		   ``])
		  ))
	     THEN (FULL_SIMP_TAC (srw_ss()) [])
	     THEN (Q.UNABBREV_TAC `written_entry`)
	     THEN (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE ``(3w && (4092w && attrs ‖ 0xFFFFF000w && phadd ‖ 2w) = 2w:bool[32])``])
	     THEN (SUBGOAL_THEN 
		``(pgrefs':bool[20]->bool[30])(ph_page') = (pgrefs:bool[20]->bool[30])(ph_page') + 1w``
		(fn thm => ASSUME_TAC thm
                     THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``ph_page':bool[20] = ph_page``)
                     THEN (FULL_SIMP_TAC (srw_ss()) [])
                     THEN (SYM_ASSUMPTION_TAC ``w2n (pgrefs ph_page) = count_pages mem pgtype ph_page``)
                     THEN (FULL_SIMP_TAC (srw_ss()) [])
		     THEN (FULL_SIMP_TAC (arith_ss) [non_max_no_overflow_thm])
		))
	     THEN (SYM_ASSUMPTION_TAC ``f = pgrefs':bool[20]->bool[30]``)
	     THEN (FULL_SIMP_TAC (srw_ss()) [])
	     THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM])
	  ]
       ]
    ] ;


val sum_l2_base_chaged_pt_unchanged_entry_tac =
    FULL_SIMP_TAC (arith_ss) []
    THEN (Cases_on `pgtype ph_page' <> 0w`)
    THENL [
      FULL_SIMP_TAC (arith_ss) [],
      FULL_SIMP_TAC (arith_ss) []
      THEN (ASSUME_TAC (SPECL [
		      ``l2_base_add:bool[32]``,
		      ``(n2w x):word10``, ``mem:word32->word8``,  ``mem':word32->word8``,
		      ``pgtype:bool[20]->bool[2]``,
		      ``ph_page':bool[20]``
      ] ref_cnt_equality_l2_thm))
      THEN (SIMP_BY_ASSUMPTION_TAC ``pgtype (w2w (l2_base_add ⋙ 12)) = 2w``)
      THEN (ASSUME_TAC (SPECL [``(n2w x):word10``,
			     ``c1':sctlrT``,
			     ``pgtype:bool[20]->bool[2]``
			    ]  l2_map_unchanged_l2_mem_thm))
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x =  hyper_l2_map_pagetable y l2_base_add l2_index z1 z2``)
      THEN (ASSUME_TAC (SPECL [``x:num``, `` w2n (1023w && l2_index:bool[32])``]
			    (Thm.INST_TYPE[alpha |-> ``:10``] (GSYM wordsTheory.n2w_11))))
      THEN (FULL_SIMP_TAC (srw_ss()) [])
      THEN (ASSUME_TAC (SPEC ``x:num`` thm_tem_xxx_thm))
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x:num < 1024``)
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x:num ≠ w2n (1023w && l2_index)``)
      THEN (RES_TAC)
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``l2_entry_unchanged l2_base_add (n2w x) mem mem'``)
      THEN (FULL_SIMP_TAC (srw_ss()) [])
    ];


val sum_l2_base_chaged_pt_tac =
    (Q.UNABBREV_TAC `f`)
    THEN (Q.UNABBREV_TAC `f'`)
    THEN (computeLib.RESTR_EVAL_TAC [``count_pages_for_pt``, ``count_pages`` ])
    THEN (SYM_ASSUMPTION_TAC ``hyper_l2_map_pagetable a b c z1 z2 = x``)
    (* we show that we do not change the page type *)
    THEN (FULL_SIMP_TAC (srw_ss()) [
	  UNDISCH (
	  SPEC ``l2_base_add:bool[32]``
	  l2_map_unchanged_pagetype_thm)
	 ])
    THEN (SIMP_TAC (srw_ss()) [count_pages_for_pt_def])
    (* we know that the page is a valid l2 *)
    THEN (FULL_SIMP_TAC (srw_ss()) [GSYM w2w_def])
    THEN (FULL_SIMP_TAC (srw_ss()) [SUM])
    THEN (FULL_SIMP_TAC (srw_ss()) [
      SPECL [``pgtype (ph_page':bool[20]) = 0w:bool[2]``,
	     ``(λit.l2_count it mem pgtype ph_page' (w2w (l2_base_add ⋙ 12)))``
	    ] sum_if_thm
	 ])
    THEN (FULL_SIMP_TAC (srw_ss()) [
      SPECL [``pgtype (ph_page':bool[20]) = 0w:bool[2]``,
	     ``(λit.l2_count it mem' pgtype ph_page' (w2w (l2_base_add ⋙ 12)))``
	    ] sum_if_thm
	 ])
     THEN (SPLITTER_SUM_TAC
	      ``w2n (l2_index:bool[32] && 1023w)``
	      ``1024:num``
	      ``(λx.if pgtype ph_page' = 0w then l2_count x mem pgtype ph_page' (w2w ((l2_base_add:bool[32]) ⋙ 12)) else 0)``
	      ``(λx.if pgtype ph_page' = 0w then l2_count x mem' pgtype ph_page' (w2w ((l2_base_add:bool[32]) ⋙ 12)) else 0)``
	      ``w2n ((pgrefs':bool[20]->bool[30]) ph_page')``
	      ``count_pages mem pgtype ph_page'``)
      THENL [
        sum_l2_base_chaged_pt_change_entry_tac,
        sum_l2_base_chaged_pt_unchanged_entry_tac,
        REWRITE_TAC [blastLib.BBLAST_PROVE (``1023w && l2_index:bool[32] = l2_index:bool[32] && 1023w``)]
            THEN (ASSUME_TAC thm_tem_l2_thm)
	    THEN FULL_SIMP_TAC (srw_ss()) []
      ]
   ; 


val sum_unchanged_pts_tac =
    FULL_SIMP_TAC (arith_ss) []
    THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `f`, Abbr `f'`])
    THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
    THEN (ASSUME_TAC (SPECL [
      ``(n2w x):word20``, ``mem:word32->word8``,  ``mem':word32->word8``, ``ph_page':bool[20]``
      ] ref_cnt_equality_thm))
    THEN (SYM_ASSUMPTION_TAC ``map_L2_pageTable_entry a b c z1 z2= x``)
    THEN (Cases_on `pgtype (n2w x) = 0b10w`)
    THENL [
      (* L2 page table *)
      (FULL_SIMP_TAC (srw_ss()) [])
      THEN (ASSUME_TAC (
	    SPECL [``(n2w x):word20``, ``l2_base_add:word32``]
		  l2_map_unchanged_mem_thm))
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC 
		``x = map_L2_pageTable_entry a b c z1 z2``)
      THEN (ASSUME_TAC (SPECL [``x:num``, `` w2n ((l2_base_add:bool[32]) ⋙ 12)``]
			    (Thm.INST_TYPE[alpha |-> ``:20``] (GSYM wordsTheory.n2w_11))))
      THEN (FULL_SIMP_TAC (srw_ss()) [])
      THEN (ASSUME_TAC (SPEC ``x:num`` thm_tem_yyy_thm))
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x:num < 1048576``)
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x:num ≠ w2n (l2_base_add ⋙ 12)``)
      THEN (RES_TAC)
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``ph_page_unchanged (n2w x) mem mem'``)
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype=pgtype':bool[20]->bool[2]``)
      THEN (FULL_SIMP_TAC (srw_ss()) []),
      (* L1 PAGES *)
      (FULL_SIMP_TAC (srw_ss()) [])
      THEN (Cases_on `(pgtype (n2w x) = 1w) ∧ (0xFFFFCw && n2w x = ((n2w x):bool[20]))`)
      THENL [
          (ASSUME_TAC (
	    SPECL [``(n2w x):bool[20]``, ``l2_base_add:bool[32]``]
		  l2_map_unchanged_l1_mem_thm
	   ))
	   THEN (FULL_SIMP_TAC (srw_ss()) [
		 UNDISCH (
		 SPEC ``l2_base_add:bool[32]``
		      l2_map_unchanged_pagetype_thm)
		])
	   THEN (`ph_l1_unchanged (n2w x) pgtype mem mem'` by METIS_TAC [])
	   THEN (FULL_SIMP_TAC (srw_ss()) [ph_l1_unchanged_def]),
	  (FULL_SIMP_TAC (srw_ss()) [count_pages_for_pt_def])
	  THENL [
	  METIS_TAC [],
	  METIS_TAC []
	  ]
      ]
    ];




(* REF COUNTER *)
val l2_map_invariant_ref_cnt_tac =
    (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
    THEN (FULL_SIMP_TAC (srw_ss()) [invariant_ref_cnt_def])
    THEN (SPEC_ASSUMPTION_TAC ``!x:word20. X`` ``(ph_page'):word20``)
(* We trasform the goal into the goal *)
(* pgrefs'  + count_pages = pgrefs  + count_pages' *)
    THEN (SUBGOAL_THEN ``
(w2n ((pgrefs' :word20 -> word30) (ph_page' :word20)) +
 count_pages (mem :word32 -> word8) (pgtype :word20 -> word2) ph_page') =
(w2n ((pgrefs :word20 -> word30) (ph_page' :word20)) +
count_pages (mem' :word32 -> word8) (pgtype' :word20 -> word2) ph_page')
``
 (fn thm =>
  ASSUME_TAC thm
  THEN (SIMP_BY_ASSUMPTION_TAC ``pgtype:bool[20]->bool[2] = pgtype'``)
  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``w2n (pgrefs ph_page) = count_pages mem pgtype' ph_page``)
  THEN (FULL_SIMP_TAC (arith_ss) [arithmeticTheory.EQ_ADD_RCANCEL])
))
    (* we restructure the sums to split in several intervals *)
    THEN (SIMP_TAC (srw_ss()) [count_pages_def])
    THEN (Q.ABBREV_TAC `f = (λit.
           (let global_page:bool[20] = n2w it in
           count_pages_for_pt mem pgtype ph_page' global_page))`)
    THEN (Q.ABBREV_TAC `f' = (λit.
           (let global_page:bool[20] = n2w it in
           count_pages_for_pt mem' pgtype' ph_page' global_page))`)
    THEN (SIMP_TAC (srw_ss()) [SUM])   
    THEN (SPLITTER_SUM_TAC
		   ``w2n (l2_base_add:bool[32] ⋙ 12)``
		   ``1048576:num``
		   ``f:num->num``
		   ``f':num->num``
		   ``w2n ((pgrefs':bool[20]->bool[30]) ph_page')``
		   ``w2n ((pgrefs:bool[20]->bool[30]) ph_page')``)
    THENL [
       (* for the uppdate l2 *)
       sum_l2_base_chaged_pt_tac,
       (* for the non updated pages *)
       sum_unchanged_pts_tac,
       (* limit *)
       (ASSUME_TAC thm_tem_2_thm)
       THEN FULL_SIMP_TAC (srw_ss()) []
    ];
    





(* MAIN THEOREM *)
val l2_map_goal = ``
  ! c1 va:word32. 
    (c1 = rec'sctlrT 1w ) ==>
    ((invariant_ref_cnt c1 c2 c3 mem pgtype pgrefs) ==>
       (let (c1',c2',c3',mem',pgtype', pgrefs') = 
       (hyper_l2_map_pagetable (rec'sctlrT 1w, c2, c3, mem, pgtype, pgrefs)  (l2_base_add:bool[32]) (l2_index:bool[32]) phadd attrs)
	in
       (invariant_ref_cnt c1' c2' c3' mem' pgtype' pgrefs')))
``;

val let_ss = simpLib.mk_simpset [boolSimps.LET_ss] ;


val l2_map_thm = Q.store_thm("l2_map_thm",
  `^l2_map_goal`,
  (RW_TAC (srw_ss()) [])
(* Before opening the invariant to check, we handle the failures *)
  THEN l2_map_exception_case_tac
  THEN (RW_TAC (srw_ss()) [invariant_ref_cnt_def])
  THENL [
(* Split for the two main part of the invariant *)
    l2_map_invariant_page_type_tac,
    l2_map_invariant_ref_cnt_tac
  ]
);

val _ = export_theory ();

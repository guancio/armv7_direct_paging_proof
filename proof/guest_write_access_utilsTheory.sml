structure guest_write_access_utilsTheory :> guest_write_access_utilsTheory =
struct
  val _ = if !Globals.print_thy_loads then print "Loading guest_write_access_utilsTheory ... " else ()
  open Type Term Thm
  infixr -->

  fun C s t ty = mk_thy_const{Name=s,Thy=t,Ty=ty}
  fun T s t A = mk_thy_type{Tyop=s, Thy=t,Args=A}
  fun V s q = mk_var(s,q)
  val U     = mk_vartype
  (*  Parents *)
  local open helperTheory
  in end;
  val _ = Theory.link_parents
          ("guest_write_access_utils",
          Arbnum.fromString "1387541258",
          Arbnum.fromString "319939")
          [("helper",
           Arbnum.fromString "1387541074",
           Arbnum.fromString "913350")];
  val _ = Theory.incorporate_types "guest_write_access_utils" [];

  val idvector = 
    let fun ID(thy,oth) = {Thy = thy, Other = oth}
    in Vector.fromList
  [ID("fcp", "cart"), ID("fcp", "bit0"), ID("one", "one"),
   ID("min", "bool"), ID("fcp", "bit1"), ID("min", "fun"), ID("bool", "!"),
   ID("pair", ","), ID("pair", "prod"), ID("bool", "/\\"), ID("num", "0"),
   ID("num", "num"), ID("min", "="), ID("min", "==>"),
   ID("arithmetic", "BIT1"), ID("arithmetic", "BIT2"),
   ID("arithmetic", "NUMERAL"), ID("bool", "T"), ID("arithmetic", "ZERO"),
   ID("bool", "\\/"), ID("hypervisor_model", "invariant_page_type"),
   ID("tiny", "sctlrT"), ID("hypervisor_model", "invariant_page_type_l1"),
   ID("hypervisor_model", "invariant_page_type_l2"),
   ID("tiny", "mmu_domain_status"), ID("tiny", "mmu_tbl_base_addr"),
   ID("tiny", "mmu_tbl_index"), ID("words", "n2w"), ID("words", "w2w"),
   ID("words", "word_and"), ID("words", "word_lsl"),
   ID("words", "word_lsr"), ID("words", "word_or")]
  end;
  local open SharingTables
  in
  val tyvector = build_type_vector idvector
  [TYOP [2], TYOP [1, 0], TYOP [1, 1], TYOP [1, 2], TYOP [1, 3],
   TYOP [1, 4], TYOP [3], TYOP [0, 6, 5], TYOP [0, 6, 2], TYOP [4, 1],
   TYOP [4, 9], TYOP [1, 10], TYOP [0, 6, 11], TYOP [0, 6, 3],
   TYOP [5, 7, 13], TYOP [4, 0], TYOP [4, 15], TYOP [4, 16], TYOP [1, 17],
   TYOP [0, 6, 18], TYOP [1, 9], TYOP [1, 20], TYOP [0, 6, 21],
   TYOP [5, 22, 19], TYOP [0, 6, 1], TYOP [5, 22, 24], TYOP [5, 7, 6],
   TYOP [5, 26, 6], TYOP [5, 22, 6], TYOP [5, 28, 6], TYOP [5, 8, 6],
   TYOP [5, 30, 6], TYOP [8, 7, 8], TYOP [5, 8, 32], TYOP [5, 7, 33],
   TYOP [5, 6, 6], TYOP [5, 6, 35], TYOP [11], TYOP [5, 7, 26],
   TYOP [5, 24, 6], TYOP [5, 24, 39], TYOP [5, 37, 37], TYOP [5, 23, 6],
   TYOP [5, 25, 42], TYOP [5, 14, 43], TYOP [5, 7, 44], TYOP [5, 7, 45],
   TYOP [21], TYOP [5, 47, 46], TYOP [5, 23, 28], TYOP [5, 25, 49],
   TYOP [5, 14, 50], TYOP [5, 32, 24], TYOP [5, 7, 7], TYOP [5, 37, 7],
   TYOP [5, 37, 22], TYOP [5, 37, 12], TYOP [5, 37, 24], TYOP [4, 17],
   TYOP [0, 6, 58], TYOP [5, 37, 59], TYOP [5, 6, 47], TYOP [5, 59, 61],
   TYOP [5, 7, 22], TYOP [1, 15], TYOP [1, 64], TYOP [0, 6, 65],
   TYOP [5, 7, 66], TYOP [0, 6, 20], TYOP [5, 7, 68], TYOP [5, 22, 7],
   TYOP [5, 66, 7], TYOP [5, 68, 7], TYOP [5, 12, 7], TYOP [5, 12, 22],
   TYOP [5, 12, 68], TYOP [5, 7, 53], TYOP [5, 22, 22], TYOP [5, 22, 77],
   TYOP [5, 12, 12], TYOP [5, 12, 79], TYOP [5, 7, 54], TYOP [5, 12, 56],
   TYOP [5, 68, 68], TYOP [5, 68, 83]]
  end
  val _ = Theory.incorporate_consts "guest_write_access_utils" tyvector [];

  local open SharingTables
  in
  val tmvector = build_term_vector idvector tyvector
  [TMV("add'", 7), TMV("c2", 7), TMV("c3", 7), TMV("dom", 8),
   TMV("l2_base", 12), TMV("mem", 14), TMV("pgrefs", 23),
   TMV("pgtype", 25), TMV("ph_page", 22), TMC(6, 27), TMC(6, 29),
   TMC(6, 31), TMC(7, 34), TMC(9, 36), TMC(10, 37), TMC(12, 38),
   TMC(12, 40), TMC(13, 36), TMC(14, 41), TMC(15, 41), TMC(16, 41),
   TMC(17, 6), TMC(18, 37), TMC(19, 36), TMC(20, 48), TMC(22, 51),
   TMC(23, 51), TMC(24, 52), TMC(25, 53), TMC(26, 53), TMC(27, 54),
   TMC(27, 55), TMC(27, 56), TMC(27, 57), TMC(27, 60), TMC(21, 62),
   TMC(28, 63), TMC(28, 67), TMC(28, 69), TMC(28, 70), TMC(28, 71),
   TMC(28, 72), TMC(28, 73), TMC(28, 74), TMC(28, 75), TMC(29, 76),
   TMC(29, 78), TMC(29, 80), TMC(30, 81), TMC(30, 82), TMC(31, 81),
   TMC(31, 82), TMC(32, 76), TMC(32, 78), TMC(32, 84)]
  end
  local
  val DT = Thm.disk_thm val read = Term.read_raw tmvector
  in
  val op currentl1_dom_thm =
    DT(["DISK_THM"],
       [read"((%17 ((((((%24 ((%35 (%34 %14)) %21)) %1) %2) %5) %7) %6)) (%11 (|%3. ((%23 ((%16 (%27 ((%12 %2) $0))) (%33 %14))) ((%16 (%27 ((%12 %2) $0))) (%33 (%20 (%18 %22))))))))"])
  val op currentl1_tbl_thm =
    DT(["DISK_THM"],
       [read"((%17 ((((((%24 ((%35 (%34 %14)) %21)) %1) %2) %5) %7) %6)) ((%13 ((%15 (%28 %1)) ((%45 (%28 %1)) (%30 (%20 (%19 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 %22)))))))))))))))))))))))))))))))))))) ((%13 ((%16 (%7 (%36 ((%50 (%28 %1)) (%20 (%19 (%18 (%19 %22)))))))) (%33 (%20 (%18 %22))))) ((%13 ((%16 (%7 ((%53 (%36 ((%50 (%28 %1)) (%20 (%19 (%18 (%19 %22))))))) (%31 (%20 (%18 %22)))))) (%33 (%20 (%18 %22))))) ((%13 ((%16 (%7 ((%53 (%36 ((%50 (%28 %1)) (%20 (%19 (%18 (%19 %22))))))) (%31 (%20 (%19 %22)))))) (%33 (%20 (%18 %22))))) ((%13 ((%16 (%7 ((%53 (%36 ((%50 (%28 %1)) (%20 (%19 (%18 (%19 %22))))))) (%31 (%20 (%18 (%18 %22))))))) (%33 (%20 (%18 %22))))) ((((%25 %5) %7) %6) (%36 ((%50 (%28 %1)) (%20 (%19 (%18 (%19 %22)))))))))))))"])
  val op l2_tbl_thm =
    DT(["DISK_THM"],
       [read"(%10 (|%8. ((%17 ((((((%24 ((%35 (%34 %14)) %21)) %1) %2) %5) %7) %6)) ((%17 ((%16 (%7 $0)) (%33 (%20 (%19 %22))))) ((((%26 %5) %7) %6) $0)))))"])
  val op l1_desc_idx_thm =
    DT(["DISK_THM"],
       [read"(%9 (|%0. ((%15 (%40 (%37 (%29 $0)))) (%29 $0))))"])
  val op l1_page_add_thm =
    DT(["DISK_THM"],
       [read"((%15 ((%48 (%39 ((%46 (%31 (%20 (%19 (%18 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 %22)))))))))))))))))))))) (%36 ((%50 (%28 %1)) (%20 (%19 (%18 (%19 %22))))))))) (%20 (%19 (%18 (%19 %22)))))) (%28 %1))"])
  val op l2_desc_idx_thm =
    DT(["DISK_THM"],
       [read"((%15 ((%52 ((%48 (%39 (%43 ((%51 %4) (%20 (%19 %22)))))) (%20 (%19 (%18 (%19 %22)))))) ((%48 (%41 ((%54 (%44 ((%49 ((%47 %4) (%32 (%20 (%18 (%18 %22)))))) (%20 (%19 (%18 (%18 %22))))))) (%38 ((%50 ((%45 %0) (%30 (%20 (%19 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%19 (%19 (%19 (%19 (%19 (%19 (%19 %22))))))))))))))))))))))) (%20 (%19 (%18 (%19 %22))))))))) (%20 (%19 %22))))) ((%52 ((%48 (%42 %4)) (%20 (%19 (%19 (%18 %22)))))) ((%45 (%30 (%20 (%19 (%18 (%19 (%19 (%19 (%19 (%19 (%19 (%19 %22)))))))))))) ((%48 ((%50 %0) (%20 (%19 (%18 (%19 %22)))))) (%20 (%19 %22))))))"])
  end
  val _ = DB.bindl "guest_write_access_utils"
  [("currentl1_dom_thm",currentl1_dom_thm,DB.Thm),
   ("currentl1_tbl_thm",currentl1_tbl_thm,DB.Thm),
   ("l2_tbl_thm",l2_tbl_thm,DB.Thm),
   ("l1_desc_idx_thm",l1_desc_idx_thm,DB.Thm),
   ("l1_page_add_thm",l1_page_add_thm,DB.Thm),
   ("l2_desc_idx_thm",l2_desc_idx_thm,DB.Thm)]

  local open Portable GrammarSpecials Parse
    fun UTOFF f = Feedback.trace("Parse.unicode_trace_off_complaints",0)f
  in
  val _ = mk_local_grms [("helperTheory.helper_grammars",
                          helperTheory.helper_grammars)]
  val _ = List.app (update_grms reveal) []

  val guest_write_access_utils_grammars = Parse.current_lgrms()
  end

val _ = if !Globals.print_thy_loads then print "done\n" else ()
val _ = Theory.load_complete "guest_write_access_utils"
end

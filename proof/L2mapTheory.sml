structure L2mapTheory :> L2mapTheory =
struct
  val _ = if !Globals.print_thy_loads then print "Loading L2mapTheory ... " else ()
  open Type Term Thm
  infixr -->

  fun C s t ty = mk_thy_const{Name=s,Thy=t,Ty=ty}
  fun T s t A = mk_thy_type{Tyop=s, Thy=t,Args=A}
  fun V s q = mk_var(s,q)
  val U     = mk_vartype
  (*  Parents *)
  local open helperTheory
  in end;
  val _ = Theory.link_parents
          ("L2map",
          Arbnum.fromString "1387541183",
          Arbnum.fromString "12683")
          [("helper",
           Arbnum.fromString "1387541074",
           Arbnum.fromString "913350")];
  val _ = Theory.incorporate_types "L2map" [];

  val idvector = 
    let fun ID(thy,oth) = {Thy = thy, Other = oth}
    in Vector.fromList
  [ID("fcp", "cart"), ID("fcp", "bit0"), ID("one", "one"),
   ID("min", "bool"), ID("tiny", "sctlrT"), ID("min", "fun"),
   ID("num", "num"), ID("fcp", "bit1"), ID("bool", "!"), ID("pair", ","),
   ID("pair", "prod"), ID("num", "0"), ID("min", "="), ID("min", "==>"),
   ID("arithmetic", "BIT1"), ID("arithmetic", "BIT2"), ID("bool", "COND"),
   ID("sum_num", "GSUM"), ID("bool", "LET"), ID("arithmetic", "NUMERAL"),
   ID("pair", "UNCURRY"), ID("arithmetic", "ZERO"),
   ID("hypervisor_model", "hyper_l2_map_pagetable"),
   ID("hypervisor_model", "invariant_page_type"),
   ID("hypervisor_model", "invariant_ref_cnt"),
   ID("helper", "l2_entry_unchanged"), ID("words", "n2w"),
   ID("helper", "ph_l1_unchanged"), ID("helper", "ph_page_unchanged"),
   ID("tiny", "rec'sctlrT"), ID("words", "w2w"), ID("words", "word_and"),
   ID("words", "word_lsr"), ID("bool", "~")]
  end;
  local open SharingTables
  in
  val tyvector = build_type_vector idvector
  [TYOP [2], TYOP [1, 0], TYOP [1, 1], TYOP [1, 2], TYOP [1, 3],
   TYOP [1, 4], TYOP [3], TYOP [0, 6, 5], TYOP [4], TYOP [6],
   TYOP [5, 9, 9], TYOP [7, 1], TYOP [1, 11], TYOP [0, 6, 12],
   TYOP [0, 6, 3], TYOP [5, 7, 14], TYOP [7, 0], TYOP [7, 16],
   TYOP [7, 17], TYOP [1, 18], TYOP [0, 6, 19], TYOP [1, 12],
   TYOP [0, 6, 21], TYOP [5, 22, 20], TYOP [0, 6, 1], TYOP [5, 22, 24],
   TYOP [5, 6, 6], TYOP [5, 26, 6], TYOP [5, 7, 6], TYOP [5, 28, 6],
   TYOP [5, 22, 6], TYOP [5, 30, 6], TYOP [5, 13, 6], TYOP [5, 32, 6],
   TYOP [5, 25, 6], TYOP [5, 34, 6], TYOP [5, 10, 6], TYOP [5, 36, 6],
   TYOP [5, 8, 6], TYOP [5, 38, 6], TYOP [10, 25, 23], TYOP [10, 15, 40],
   TYOP [10, 7, 41], TYOP [10, 7, 42], TYOP [5, 42, 43], TYOP [5, 7, 44],
   TYOP [5, 41, 42], TYOP [5, 7, 46], TYOP [5, 40, 41], TYOP [5, 15, 48],
   TYOP [5, 23, 40], TYOP [5, 25, 50], TYOP [10, 9, 9], TYOP [5, 9, 52],
   TYOP [5, 9, 53], TYOP [10, 8, 43], TYOP [5, 43, 55], TYOP [5, 8, 56],
   TYOP [5, 22, 30], TYOP [5, 13, 32], TYOP [5, 25, 34], TYOP [5, 9, 6],
   TYOP [5, 9, 61], TYOP [5, 55, 6], TYOP [5, 55, 63], TYOP [5, 8, 38],
   TYOP [5, 6, 26], TYOP [5, 9, 10], TYOP [5, 6, 67], TYOP [5, 10, 9],
   TYOP [5, 52, 69], TYOP [5, 63, 63], TYOP [5, 43, 6], TYOP [5, 42, 6],
   TYOP [5, 7, 73], TYOP [5, 74, 72], TYOP [5, 41, 6], TYOP [5, 7, 76],
   TYOP [5, 77, 73], TYOP [5, 40, 6], TYOP [5, 15, 79], TYOP [5, 80, 76],
   TYOP [5, 23, 6], TYOP [5, 25, 82], TYOP [5, 83, 79], TYOP [5, 8, 72],
   TYOP [5, 85, 63], TYOP [5, 7, 55], TYOP [5, 7, 87], TYOP [5, 7, 88],
   TYOP [5, 7, 89], TYOP [5, 55, 90], TYOP [5, 15, 83], TYOP [5, 7, 92],
   TYOP [5, 7, 93], TYOP [5, 8, 94], TYOP [5, 15, 6], TYOP [5, 15, 96],
   TYOP [5, 13, 97], TYOP [5, 7, 98], TYOP [5, 9, 7], TYOP [5, 25, 97],
   TYOP [5, 22, 101], TYOP [5, 22, 97], TYOP [5, 7, 8], TYOP [5, 7, 22],
   TYOP [5, 7, 13], TYOP [5, 7, 7], TYOP [5, 7, 107], TYOP [5, 7, 100]]
  end
  val _ = Theory.incorporate_consts "L2map" tyvector [];

  local open SharingTables
  in
  val tmvector = build_term_vector idvector tyvector
  [TMV("attrs", 7), TMV("c", 6), TMV("c1", 8), TMV("c1'", 8), TMV("c2", 7),
   TMV("c2'", 7), TMV("c3", 7), TMV("c3'", 7), TMV("f", 10),
   TMV("l2_base_add", 7), TMV("l2_idx", 13), TMV("l2_index", 7),
   TMV("mem", 15), TMV("mem'", 15), TMV("pgrefs", 23), TMV("pgrefs'", 23),
   TMV("pgtype", 25), TMV("pgtype'", 25), TMV("phadd", 7), TMV("ptb", 22),
   TMV("va", 7), TMV("x", 9), TMC(8, 27), TMC(8, 29), TMC(8, 31),
   TMC(8, 33), TMC(8, 35), TMC(8, 37), TMC(8, 39), TMC(9, 45), TMC(9, 47),
   TMC(9, 49), TMC(9, 51), TMC(9, 54), TMC(9, 57), TMC(11, 9), TMC(12, 58),
   TMC(12, 59), TMC(12, 60), TMC(12, 62), TMC(12, 64), TMC(12, 65),
   TMC(13, 66), TMC(14, 10), TMC(15, 10), TMC(16, 68), TMC(17, 70),
   TMC(18, 71), TMC(19, 10), TMC(20, 75), TMC(20, 78), TMC(20, 81),
   TMC(20, 84), TMC(20, 86), TMC(21, 9), TMC(22, 91), TMC(23, 95),
   TMC(24, 95), TMC(25, 99), TMC(26, 100), TMC(27, 102), TMC(28, 103),
   TMC(29, 104), TMC(30, 105), TMC(30, 106), TMC(31, 108), TMC(32, 109),
   TMC(33, 26)]
  end
  local
  val DT = Thm.disk_thm val read = Term.read_raw tmvector
  in
  val op sum_if_thm =
    DT(["DISK_THM"],
       [read"(%22 (|%1. (%27 (|%8. ((%39 (((%45 $1) ((%46 ((%33 %35) (%48 (%44 (%43 (%43 (%43 (%43 (%43 (%43 (%43 (%43 (%43 %54))))))))))))) $0)) %35)) ((%46 ((%33 %35) (%48 (%44 (%43 (%43 (%43 (%43 (%43 (%43 (%43 (%43 (%43 %54))))))))))))) (|%21. (((%45 $2) ($1 $0)) %35))))))))"])
  val op l2_map_unchanged_mem_thm =
    DT(["DISK_THM"],
       [read"(%24 (|%19. (%23 (|%9. ((%42 ((%40 ((%34 %3) ((%29 %5) ((%30 %7) ((%31 %13) ((%32 %17) %15)))))) (((((%55 ((%34 (%62 (%59 (%48 (%43 %54))))) ((%29 %4) ((%30 %6) ((%31 %12) ((%32 %16) %14)))))) $0) %11) %18) %0))) ((%42 (%67 ((%36 $1) (%63 ((%66 $0) (%48 (%44 (%43 (%44 %54))))))))) (((%61 $1) %12) %13)))))))"])
  val op l2_map_unchanged_pagetype_thm =
    DT(["DISK_THM"],
       [read"(%23 (|%9. ((%42 ((%40 ((%34 %3) ((%29 %5) ((%30 %7) ((%31 %13) ((%32 %17) %15)))))) (((((%55 ((%34 (%62 (%59 (%48 (%43 %54))))) ((%29 %4) ((%30 %6) ((%31 %12) ((%32 %16) %14)))))) $0) %11) %18) %0))) ((%38 %17) %16))))"])
  val op l2_map_unchanged_l1_mem_thm =
    DT(["DISK_THM"],
       [read"(%24 (|%19. (%23 (|%9. ((%42 ((((((%56 (%62 (%59 (%48 (%43 %54))))) %4) %6) %12) %16) %14)) ((%42 ((%40 ((%34 %3) ((%29 %5) ((%30 %7) ((%31 %13) ((%32 %17) %15)))))) (((((%55 ((%34 (%62 (%59 (%48 (%43 %54))))) ((%29 %4) ((%30 %6) ((%31 %12) ((%32 %16) %14)))))) $0) %11) %18) %0))) ((((%60 $1) %16) %12) %13)))))))"])
  val op l2_map_unchanged_l2_mem_thm =
    DT(["DISK_THM"],
       [read"(%25 (|%10. (%28 (|%2. (%26 (|%17. ((%42 ((%40 ((%34 $1) ((%29 %5) ((%30 %7) ((%31 %13) ((%32 $0) %15)))))) (((((%55 ((%34 (%62 (%59 (%48 (%43 %54))))) ((%29 %4) ((%30 %6) ((%31 %12) ((%32 %16) %14)))))) %9) %11) %18) %0))) ((%42 (%67 ((%37 $2) (%64 ((%65 %11) (%59 (%48 (%43 (%43 (%43 (%43 (%43 (%43 (%43 (%43 (%43 (%43 %54))))))))))))))))) ((((%58 %9) $2) %12) %13)))))))))"])
  val op l2_map_thm =
    DT(["DISK_THM"],
       [read"(%28 (|%2. (%23 (|%20. ((%42 ((%41 $1) (%62 (%59 (%48 (%43 %54)))))) ((%42 ((((((%57 $1) %4) %6) %12) %16) %14)) ((%47 (%53 (|%3. (%49 (|%5. (%50 (|%7. (%51 (|%13. (%52 (|%17. (|%15. ((((((%57 $5) $4) $3) $2) $1) $0))))))))))))) (((((%55 ((%34 (%62 (%59 (%48 (%43 %54))))) ((%29 %4) ((%30 %6) ((%31 %12) ((%32 %16) %14)))))) %9) %11) %18) %0))))))))"])
  end
  val _ = DB.bindl "L2map"
  [("sum_if_thm",sum_if_thm,DB.Thm),
   ("l2_map_unchanged_mem_thm",l2_map_unchanged_mem_thm,DB.Thm),
   ("l2_map_unchanged_pagetype_thm",l2_map_unchanged_pagetype_thm,DB.Thm),
   ("l2_map_unchanged_l1_mem_thm",l2_map_unchanged_l1_mem_thm,DB.Thm),
   ("l2_map_unchanged_l2_mem_thm",l2_map_unchanged_l2_mem_thm,DB.Thm),
   ("l2_map_thm",l2_map_thm,DB.Thm)]

  local open Portable GrammarSpecials Parse
    fun UTOFF f = Feedback.trace("Parse.unicode_trace_off_complaints",0)f
  in
  val _ = mk_local_grms [("helperTheory.helper_grammars",
                          helperTheory.helper_grammars)]
  val _ = List.app (update_grms reveal) []

  val L2map_grammars = Parse.current_lgrms()
  end

val _ = if !Globals.print_thy_loads then print "done\n" else ()
val _ = Theory.load_complete "L2map"
end

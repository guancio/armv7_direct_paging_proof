structure L1createTheory :> L1createTheory =
struct
  val _ = if !Globals.print_thy_loads then print "Loading L1createTheory ... " else ()
  open Type Term Thm
  infixr -->

  fun C s t ty = mk_thy_const{Name=s,Thy=t,Ty=ty}
  fun T s t A = mk_thy_type{Tyop=s, Thy=t,Args=A}
  fun V s q = mk_var(s,q)
  val U     = mk_vartype
  (*  Parents *)
  local open helperTheory
  in end;
  val _ = Theory.link_parents
          ("L1create",
          Arbnum.fromString "1387552166",
          Arbnum.fromString "253139")
          [("helper",
           Arbnum.fromString "1387541074",
           Arbnum.fromString "913350")];
  val _ = Theory.incorporate_types "L1create" [];

  val idvector = 
    let fun ID(thy,oth) = {Thy = thy, Other = oth}
    in Vector.fromList
  [ID("tiny", "sctlrT"), ID("fcp", "cart"), ID("fcp", "bit0"),
   ID("one", "one"), ID("min", "bool"), ID("min", "fun"),
   ID("fcp", "bit1"), ID("num", "num"), ID("bool", "!"), ID("pair", ","),
   ID("pair", "prod"), ID("bool", "/\\"), ID("prim_rec", "<"),
   ID("min", "="), ID("min", "==>"), ID("arithmetic", "BIT1"),
   ID("arithmetic", "BIT2"), ID("bool", "LET"),
   ID("arithmetic", "NUMERAL"), ID("pair", "UNCURRY"),
   ID("arithmetic", "ZERO"), ID("hypervisor_model", "create_l1_pt"),
   ID("hypervisor_model", "invariant_ref_cnt"), ID("words", "n2w"),
   ID("tiny", "rec'sctlrT"), ID("words", "w2w"), ID("words", "word_add"),
   ID("words", "word_and"), ID("words", "word_lsl"),
   ID("words", "word_lsr"), ID("words", "word_or")]
  end;
  local open SharingTables
  in
  val tyvector = build_type_vector idvector
  [TYV "'a", TYOP [0], TYOP [3], TYOP [2, 2], TYOP [2, 3], TYOP [2, 4],
   TYOP [2, 5], TYOP [2, 6], TYOP [4], TYOP [1, 8, 7], TYOP [1, 8, 5],
   TYOP [5, 9, 10], TYOP [6, 2], TYOP [6, 12], TYOP [6, 13], TYOP [2, 14],
   TYOP [1, 8, 15], TYOP [6, 3], TYOP [2, 17], TYOP [2, 18],
   TYOP [1, 8, 19], TYOP [5, 20, 16], TYOP [1, 8, 3], TYOP [5, 20, 22],
   TYOP [7], TYOP [5, 9, 8], TYOP [5, 25, 8], TYOP [5, 1, 8],
   TYOP [5, 27, 8], TYOP [10, 23, 21], TYOP [10, 11, 29], TYOP [10, 9, 30],
   TYOP [10, 9, 31], TYOP [5, 31, 32], TYOP [5, 9, 33], TYOP [5, 30, 31],
   TYOP [5, 9, 35], TYOP [5, 29, 30], TYOP [5, 11, 37], TYOP [5, 21, 29],
   TYOP [5, 23, 39], TYOP [10, 1, 32], TYOP [5, 32, 41], TYOP [5, 1, 42],
   TYOP [5, 8, 8], TYOP [5, 8, 44], TYOP [5, 24, 8], TYOP [5, 24, 46],
   TYOP [5, 9, 25], TYOP [5, 1, 27], TYOP [5, 24, 24], TYOP [5, 41, 8],
   TYOP [5, 51, 51], TYOP [5, 32, 8], TYOP [5, 31, 8], TYOP [5, 9, 54],
   TYOP [5, 55, 53], TYOP [5, 30, 8], TYOP [5, 9, 57], TYOP [5, 58, 54],
   TYOP [5, 29, 8], TYOP [5, 11, 60], TYOP [5, 61, 57], TYOP [5, 21, 8],
   TYOP [5, 23, 63], TYOP [5, 64, 60], TYOP [5, 1, 53], TYOP [5, 66, 51],
   TYOP [5, 0, 41], TYOP [5, 9, 68], TYOP [5, 41, 69], TYOP [5, 11, 64],
   TYOP [5, 9, 71], TYOP [5, 9, 72], TYOP [5, 1, 73], TYOP [5, 24, 9],
   TYOP [2, 12], TYOP [2, 76], TYOP [1, 8, 77], TYOP [5, 24, 78],
   TYOP [5, 9, 1], TYOP [5, 9, 20], TYOP [5, 20, 9], TYOP [5, 78, 9],
   TYOP [5, 9, 9], TYOP [5, 9, 84], TYOP [5, 9, 75]]
  end
  val _ = Theory.incorporate_consts "L1create" tyvector [];

  local open SharingTables
  in
  val tmvector = build_term_vector idvector tyvector
  [TMV("attrs", 0), TMV("c1", 1), TMV("c1'", 1), TMV("c2", 9),
   TMV("c2'", 9), TMV("c3", 9), TMV("c3'", 9), TMV("mem", 11),
   TMV("mem'", 11), TMV("pa", 9), TMV("pgrefs", 21), TMV("pgrefs'", 21),
   TMV("pgtype", 23), TMV("pgtype'", 23), TMV("va", 9), TMV("x", 24),
   TMC(8, 26), TMC(8, 28), TMC(9, 34), TMC(9, 36), TMC(9, 38), TMC(9, 40),
   TMC(9, 43), TMC(11, 45), TMC(12, 47), TMC(13, 48), TMC(13, 49),
   TMC(14, 45), TMC(15, 50), TMC(16, 50), TMC(17, 52), TMC(18, 50),
   TMC(19, 56), TMC(19, 59), TMC(19, 62), TMC(19, 65), TMC(19, 67),
   TMC(20, 24), TMC(21, 70), TMC(22, 74), TMC(23, 75), TMC(23, 79),
   TMC(24, 80), TMC(25, 81), TMC(25, 82), TMC(25, 83), TMC(26, 85),
   TMC(27, 85), TMC(28, 86), TMC(29, 86), TMC(30, 85)]
  end
  local
  val DT = Thm.disk_thm val read = Term.read_raw tmvector
  in
  val op l1_count_add_helper_thm =
    DT(["DISK_THM"],
       [read"((%27 ((%24 %15) (%31 (%29 (%28 (%28 (%28 (%28 (%28 (%28 (%28 (%28 (%28 (%28 (%28 %37))))))))))))))) ((%27 ((%25 ((%47 (%40 (%31 (%29 (%28 (%28 (%28 (%28 (%28 (%28 (%28 (%28 (%28 (%28 (%28 (%28 (%28 (%29 (%29 (%29 (%29 (%29 (%29 (%29 (%29 (%29 (%29 (%29 (%29 (%29 (%29 (%29 (%29 (%29 %37)))))))))))))))))))))))))))))))))) %9)) %9)) ((%23 ((%25 ((%50 ((%48 (%44 (%43 ((%49 %9) (%31 (%29 (%28 (%29 %37)))))))) (%31 (%29 (%28 (%29 %37)))))) ((%48 (%45 (%41 %15))) (%31 (%29 %37))))) ((%46 %9) ((%48 (%40 %15)) (%31 (%29 %37)))))) ((%25 ((%50 %9) ((%48 (%40 %15)) (%31 (%29 %37))))) ((%46 %9) ((%48 (%40 %15)) (%31 (%29 %37))))))))"])
  val op l1_create_thm =
    DT(["DISK_THM"],
       [read"(%17 (|%1. (%16 (|%14. ((%27 ((%26 $1) (%42 (%40 (%31 (%28 %37)))))) ((%27 ((((((%39 $1) %3) %5) %7) %12) %10)) ((%30 (%36 (|%2. (%32 (|%4. (%33 (|%6. (%34 (|%8. (%35 (|%13. (|%11. ((((((%39 $5) $4) $3) $2) $1) $0))))))))))))) (((%38 ((%22 (%42 (%40 (%31 (%28 %37))))) ((%18 %3) ((%19 %5) ((%20 %7) ((%21 %12) %10)))))) %9) %0))))))))"])
  end
  val _ = DB.bindl "L1create"
  [("l1_count_add_helper_thm",l1_count_add_helper_thm,DB.Thm),
   ("l1_create_thm",l1_create_thm,DB.Thm)]

  local open Portable GrammarSpecials Parse
    fun UTOFF f = Feedback.trace("Parse.unicode_trace_off_complaints",0)f
  in
  val _ = mk_local_grms [("helperTheory.helper_grammars",
                          helperTheory.helper_grammars)]
  val _ = List.app (update_grms reveal) []

  val L1create_grammars = Parse.current_lgrms()
  end

val _ = if !Globals.print_thy_loads then print "done\n" else ()
val _ = Theory.load_complete "L1create"
end

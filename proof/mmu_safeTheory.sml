structure mmu_safeTheory :> mmu_safeTheory =
struct
  val _ = if !Globals.print_thy_loads then print "Loading mmu_safeTheory ... " else ()
  open Type Term Thm
  infixr -->

  fun C s t ty = mk_thy_const{Name=s,Thy=t,Ty=ty}
  fun T s t A = mk_thy_type{Tyop=s, Thy=t,Args=A}
  fun V s q = mk_var(s,q)
  val U     = mk_vartype
  (*  Parents *)
  local open guest_write_accessTheory
  in end;
  val _ = Theory.link_parents
          ("mmu_safe",
          Arbnum.fromString "1387541343",
          Arbnum.fromString "508258")
          [("guest_write_access",
           Arbnum.fromString "1387541286",
           Arbnum.fromString "522149")];
  val _ = Theory.incorporate_types "mmu_safe" [];

  val idvector = 
    let fun ID(thy,oth) = {Thy = thy, Other = oth}
    in Vector.fromList
  [ID("fcp", "cart"), ID("fcp", "bit0"), ID("one", "one"),
   ID("min", "bool"), ID("tiny", "sctlrT"), ID("fcp", "bit1"),
   ID("min", "fun"), ID("bool", "!"), ID("min", "="), ID("min", "==>"),
   ID("arithmetic", "BIT1"), ID("num", "num"), ID("arithmetic", "BIT2"),
   ID("bool", "F"), ID("arithmetic", "NUMERAL"), ID("arithmetic", "ZERO"),
   ID("hypervisor_model", "invariant_ref_cnt"),
   ID("mmu_properties", "mmu_safe"), ID("words", "n2w"),
   ID("tiny", "rec'sctlrT"), ID("words", "w2w"), ID("words", "word_add"),
   ID("words", "word_and"), ID("words", "word_lsl"),
   ID("words", "word_lsr"), ID("words", "word_or")]
  end;
  local open SharingTables
  in
  val tyvector = build_type_vector idvector
  [TYOP [2], TYOP [1, 0], TYOP [3], TYOP [0, 2, 1], TYOP [4], TYOP [1, 1],
   TYOP [1, 5], TYOP [1, 6], TYOP [1, 7], TYOP [0, 2, 8], TYOP [5, 1],
   TYOP [5, 10], TYOP [1, 11], TYOP [0, 2, 12], TYOP [0, 2, 6],
   TYOP [6, 9, 14], TYOP [5, 0], TYOP [5, 16], TYOP [5, 17], TYOP [1, 18],
   TYOP [0, 2, 19], TYOP [1, 10], TYOP [1, 21], TYOP [0, 2, 22],
   TYOP [6, 23, 20], TYOP [6, 23, 3], TYOP [6, 9, 2], TYOP [6, 26, 2],
   TYOP [6, 13, 2], TYOP [6, 28, 2], TYOP [6, 3, 2], TYOP [6, 30, 2],
   TYOP [6, 4, 2], TYOP [6, 32, 2], TYOP [6, 9, 26], TYOP [6, 3, 30],
   TYOP [6, 4, 32], TYOP [6, 2, 2], TYOP [6, 2, 37], TYOP [11],
   TYOP [6, 39, 39], TYOP [6, 24, 2], TYOP [6, 25, 41], TYOP [6, 15, 42],
   TYOP [6, 9, 43], TYOP [6, 9, 44], TYOP [6, 4, 45], TYOP [6, 15, 37],
   TYOP [6, 9, 47], TYOP [6, 9, 48], TYOP [6, 4, 49], TYOP [6, 39, 9],
   TYOP [6, 39, 23], TYOP [6, 39, 3], TYOP [6, 9, 4], TYOP [6, 9, 23],
   TYOP [6, 13, 9], TYOP [6, 13, 23], TYOP [6, 3, 9], TYOP [6, 9, 9],
   TYOP [6, 9, 59], TYOP [6, 9, 51], TYOP [6, 39, 13], TYOP [6, 13, 62],
   TYOP [6, 23, 23], TYOP [6, 23, 64]]
  end
  val _ = Theory.incorporate_consts "mmu_safe" tyvector [];

  local open SharingTables
  in
  val tmvector = build_term_vector idvector tyvector
  [TMV("byte_idx", 3), TMV("c1", 4), TMV("c2", 9), TMV("c3", 9),
   TMV("l2_base", 13), TMV("mem", 15), TMV("pgrefs", 24),
   TMV("pgtype", 25), TMV("tbl_base", 9), TMV("va", 9), TMC(7, 27),
   TMC(7, 29), TMC(7, 31), TMC(7, 33), TMC(8, 34), TMC(8, 35), TMC(8, 36),
   TMC(9, 38), TMC(10, 40), TMC(12, 40), TMC(13, 2), TMC(14, 40),
   TMC(15, 39), TMC(16, 46), TMC(17, 50), TMC(18, 51), TMC(18, 52),
   TMC(18, 53), TMC(19, 54), TMC(20, 55), TMC(20, 56), TMC(20, 57),
   TMC(20, 58), TMC(21, 60), TMC(22, 60), TMC(23, 61), TMC(24, 61),
   TMC(24, 63), TMC(25, 60), TMC(25, 65)]
  end
  local
  val DT = Thm.disk_thm val read = Term.read_raw tmvector
  in
  val op all_bytes_in_l1_type_1_thm =
    DT(["DISK_THM"],
       [read"(%10 (|%8. ((%17 ((%14 $0) ((%34 $0) (%25 (%21 (%19 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%18 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 (%19 %22)))))))))))))))))))))))))))))))))))) ((%17 ((%15 (%7 (%29 ((%36 $0) (%21 (%19 (%18 (%19 %22)))))))) (%27 (%21 (%18 %22))))) ((%17 ((%15 (%7 ((%39 (%29 ((%36 $0) (%21 (%19 (%18 (%19 %22))))))) (%26 (%21 (%18 %22)))))) (%27 (%21 (%18 %22))))) ((%17 ((%15 (%7 ((%39 (%29 ((%36 $0) (%21 (%19 (%18 (%19 %22))))))) (%26 (%21 (%19 %22)))))) (%27 (%21 (%18 %22))))) ((%17 ((%15 (%7 ((%39 (%29 ((%36 $0) (%21 (%19 (%18 (%19 %22))))))) (%26 (%21 (%18 (%18 %22))))))) (%27 (%21 (%18 %22))))) (%10 (|%9. (%12 (|%0. ((%15 (%7 (%29 ((%36 ((%33 ((%38 $2) ((%35 ((%36 $1) (%21 (%19 (%18 (%19 (%18 %22))))))) (%21 (%19 %22))))) (%32 $0))) (%21 (%19 (%18 (%19 %22)))))))) (%27 (%21 (%18 %22)))))))))))))))"])
  val op all_bytes_in_l2_type_2_thm =
    DT(["DISK_THM"],
       [read"(%11 (|%4. ((%17 ((%15 (%7 (%31 ((%37 $0) (%21 (%19 %22)))))) (%27 (%21 (%19 %22))))) (%10 (|%9. (%12 (|%0. ((%15 (%7 (%29 ((%36 ((%33 ((%38 ((%35 (%30 $2)) (%21 (%19 (%19 (%18 %22)))))) ((%34 (%25 (%21 (%19 (%18 (%19 (%19 (%19 (%19 (%19 (%19 (%19 %22)))))))))))) ((%35 ((%36 $1) (%21 (%19 (%18 (%19 %22)))))) (%21 (%19 %22)))))) (%32 $0))) (%21 (%19 (%18 (%19 %22)))))))) (%27 (%21 (%19 %22)))))))))))"])
  val op mmu_safe_thm =
    DT(["DISK_THM"],
       [read"(%13 (|%1. ((%17 ((%16 $0) (%28 (%25 (%21 (%18 %22)))))) ((%17 ((((((%23 $0) %2) %3) %5) %7) %6)) (((((%24 $0) %2) %3) %5) %20)))))"])
  end
  val _ = DB.bindl "mmu_safe"
  [("all_bytes_in_l1_type_1_thm",all_bytes_in_l1_type_1_thm,DB.Thm),
   ("all_bytes_in_l2_type_2_thm",all_bytes_in_l2_type_2_thm,DB.Thm),
   ("mmu_safe_thm",mmu_safe_thm,DB.Thm)]

  local open Portable GrammarSpecials Parse
    fun UTOFF f = Feedback.trace("Parse.unicode_trace_off_complaints",0)f
  in
  val _ = mk_local_grms [("guest_write_accessTheory.guest_write_access_grammars",
                          guest_write_accessTheory.guest_write_access_grammars)]
  val _ = List.app (update_grms reveal) []

  val mmu_safe_grammars = Parse.current_lgrms()
  end

val _ = if !Globals.print_thy_loads then print "done\n" else ()
val _ = Theory.load_complete "mmu_safe"
end

open HolKernel boolLib bossLib Parse;
open tinyTheory;
open hypervisor_modelTheory;
open mmu_propertiesTheory;
open wordsLib;

load "mmu_utilsLib";
open mmu_utilsLib;
open numSyntax;
open sum_numTheory;
open wordsTheory;


val _ = new_theory "helper";

(* read_mem32 related *)
val read_mem_eq_thm = Q.store_thm("read_mem_eq_thm", `
  !a.
  (mem(a) = mem'(a)) ==>
  (mem(a+1w) = mem'(a+1w)) ==>
  (mem(a+2w) = mem'(a+2w)) ==>
  (mem(a+3w) = mem'(a+3w)) ==>
  (read_mem32(a,mem) = read_mem32(a,mem'))
`, FULL_SIMP_TAC (srw_ss()) [read_mem32_def]);

val write_read_thm = Q.store_thm("write_read_thm", `
!a v m m'. (m' = write_mem32(a,m,v)) ==> 
      (read_mem32(a,m') = v)
`,
  (REPEAT GEN_TAC)
THEN EVAL_TAC
THEN (RW_TAC (srw_ss()) [read_mem32_def])
THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.UPDATE_def])
THEN (RW_TAC (srw_ss()) [])
THENL [
 (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE
  ``~(((a:bool[32]) + 3w) = a)``]),
 (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE
  ``~(((a:bool[32]) + 2w) = a)``]),
 (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE
  ``~(((a:bool[32]) + 1w) = a)``]),
 (fn (asl, w) =>
 let val t = blastLib.BBLAST_PROVE w in
 blastLib.BBLAST_TAC (asl, w)
 end)
]);

val write_read_unch_thm = Q.store_thm("write_read_unch_thm", `
!a a' v m m'. 
  (a'+3w <+ a /\ a'+3w >=+ 3w /\ a+3w >=+ 3w) \/
  (a' >+ a+3w /\ a'+3w >=+ 3w /\ a+3w >=+ 3w) ==>
  (m' = write_mem32(a,m,v)) ==> 
  (read_mem32(a',m') = read_mem32(a',m))
`,
  (REPEAT GEN_TAC)
THEN EVAL_TAC
THEN (RW_TAC (srw_ss()) [read_mem32_def])
THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.UPDATE_def])
THENL [
   (MAP_EVERY (fn (tm1, tm2) =>
   (ASSUME_TAC (UNDISCH (UNDISCH (
   (blastLib.BBLAST_PROVE ``
   (((a':bool[32]) + 3w) <₊ a) ==>
   (((a':bool[32]) + 3w) ≥₊ 3w) ==>
   (((a:bool[32]) + 3w) ≥₊ 3w) ==>
   (^tm1 <> ^tm2)
   ``))))))
   (List.concat (
     List.map (fn tm1 =>
       List.map (fn tm2 =>
        (tm1,tm2)
       )
       [``a':bool[32]``, ``a'+1w:bool[32]``, ``a'+2w:bool[32]``, ``a'+3w:bool[32]``]
     )
     [``a:bool[32]``, ``a+1w:bool[32]``, ``a+2w:bool[32]``, ``a+3w:bool[32]``]
   )))
   THEN (FULL_SIMP_TAC (srw_ss()) [])
,
   (MAP_EVERY (fn (tm1, tm2) =>
   (ASSUME_TAC (UNDISCH (UNDISCH (
   (blastLib.BBLAST_PROVE ``
   (((a':bool[32])) >+ a+3w) ==>
   (((a':bool[32]) + 3w) ≥₊ 3w) ==>
   (((a:bool[32]) + 3w) ≥₊ 3w) ==>
   (^tm1 <> ^tm2)
   ``))))))
   (List.concat (
     List.map (fn tm1 =>
       List.map (fn tm2 =>
        (tm1,tm2)
       )
       [``a':bool[32]``, ``a'+1w:bool[32]``, ``a'+2w:bool[32]``, ``a'+3w:bool[32]``]
     )
     [``a:bool[32]``, ``a+1w:bool[32]``, ``a+2w:bool[32]``, ``a+3w:bool[32]``]
   )))
   THEN (FULL_SIMP_TAC (srw_ss()) [])
]);




(* mmu related *)

(* oldname: thm_mmu_l1_decode_type_values *)
val mmu_l1_decode_type_values_thm = Q.store_thm(
    "mmu_l1_decode_type_values_thm",
    `!a.(mmu_l1_decode_type a = 0w) \/ 
        (mmu_l1_decode_type a = 1w) \/ 
	(mmu_l1_decode_type a = 2w) \/ 
	(mmu_l1_decode_type a = 3w)`,
    (FULL_SIMP_TAC (srw_ss()) [mmu_l1_decode_type_def])
    THEN GEN_TAC
    THEN (ASSUME_TAC 
    (blastLib.BBLAST_PROVE 
      (``(3w && a:bool[32] = 0w) ∨ 
	 (3w && a = 1w) ∨ 
	 (3w && a = 2w) ∨ 
	 (3w && a = 3w)``)))
    THEN (FULL_SIMP_TAC (srw_ss()) []));

val mmu_l1_18bit_values_thm = Q.store_thm(
  "mmu_l1_18bit_values_thm",
  `!a:bool[32].((1w && a) = 0w) \/ ((1w && a) = 1w)`,
  FULL_SIMP_TAC (srw_ss()) [
    blastLib.BBLAST_PROVE (``!a:bool[32].((1w && a) = 0w) \/ ((1w && a) = 1w)``)
  ]);



(* ******************** *)
(* ******************** *)
(* ******************** *)
(*  NUMBER     HELPERS  *)
(* ******************** *)
(* ******************** *)
(* ******************** *)
val Bless_word_thm = Q.store_thm("Bless_word_thm",
		`!a b. (b < dimword (:'a)) ==> (a < dimword (:'a)) ==> ((a < b) =  (((n2w a):'a word) <+ ((n2w b):'a word)))`,
		     RW_TAC (srw_ss()) [word_lo_n2w]);
val Bless_eq_word_thm = Q.store_thm("Bless_eq_word_thm",
		`!a b. (b < dimword (:'a)) /\ (a < dimword (:'a)) ==> ((a <= b) = (((n2w a):'a word) <=+ ((n2w b):'a word)))`,
		RW_TAC (srw_ss()) [word_ls_n2w]);


fun ASSUME_SPEC_X_32_TAC thm = 
    ASSUME_TAC (SPEC ``x:bool[32]`` thm);

val thm_max_20 = blastLib.BBLAST_PROVE(``!x. (x:bool[32] ⋙ 12) <+ 1048576w``);
val thm_max_30 = blastLib.BBLAST_PROVE(``!x.(x:bool[32] ⋙ 12) <=+ 1048575w``);

val thm_temo_1_thm = Q.store_thm("thm_temo_1_thm",
`!x.w2n (x:bool[32] ⋙ 12) + (1048576 − w2n (x ⋙ 12)) = 1048576`,
		           STRIP_TAC
		      THEN (ASSUME_SPEC_X_32_TAC thm_max_20)
		      THEN (ASSUME_TAC (SPECL [``(x:bool[32] ⋙ 12)``, ``1048576w:bool[32]``]
		      			      (Thm.INST_TYPE[alpha |-> ``:32``] 
							    wordsTheory.WORD_LO)))
		      THEN (FULL_SIMP_TAC (srw_ss()) [])
		      THEN (FULL_SIMP_TAC (arith_ss) []));

val thm_temo_3_thm = Q.store_thm("thm_temo_3_thm",
           `!x.1 + (1048575 − w2n (x ⋙ 12)) = 1048576 − w2n (x:bool[32] ⋙ 12)`,
		           STRIP_TAC
		      THEN (ASSUME_SPEC_X_32_TAC thm_max_30)
		      THEN (ASSUME_TAC (SPECL [``(x:bool[32] ⋙ 12)``, ``1048575w:bool[32]``]
					      (Thm.INST_TYPE[alpha |-> ``:32``] wordsTheory.WORD_LS)))
		      THEN (FULL_SIMP_TAC (srw_ss()) [])
		      THEN (FULL_SIMP_TAC (arith_ss) []));

val thm_tem_1_thm = Q.store_thm("thm_tem_1_thm",
                 `!x. x < (w2n (l2_base_add:bool[32] ⋙ 12)) ==>  (x < dimword(:20))`,
		      STRIP_TAC
		      THEN (ASSUME_TAC (SPEC ``l2_base_add:bool[32]`` thm_max_20))
		      THEN (ASSUME_TAC (SPECL [``(l2_base_add:bool[32] ⋙ 12)``, ``1048576w:bool[32]``]
					      (Thm.INST_TYPE[alpha |-> ``:32``] wordsTheory.WORD_LO)))
		      THEN (FULL_SIMP_TAC (srw_ss()) [])
		      THEN (FULL_SIMP_TAC (arith_ss) []));

val thm_tem_2_thm = Q.store_thm("thm_tem_2_thm",
		` (w2n (l2_base_add:bool[32] ⋙ 12)) < dimword(:20)`,
		      (ASSUME_TAC (SPEC ``l2_base_add:bool[32]`` thm_max_20))
	              THEN (`(w2n (0x100000w :word32)) = dimword(:20)` by blastLib.BBLAST_TAC)
		      THEN (ASSUME_TAC (SPECL [``(l2_base_add:bool[32] ⋙ 12)``, ``1048576w:bool[32]``]
					      (Thm.INST_TYPE[alpha |-> ``:32``] wordsTheory.WORD_LO)))
		      THEN  (FULL_SIMP_TAC (srw_ss()) []));



val non_zero_no_overflow_thm = Q.store_thm("non_zero_no_overflow_thm",
 `
!x:bool[30].
(w2n (x) > 0) ==>
(w2n (x - 1w) = w2n (x) - 1)
`,
  REPEAT (STRIP_TAC)
  THEN (ASSUME_TAC (
  SPECL [``x:bool[30]``, ``1w:bool[30]``]
	(Thm.INST_TYPE[alpha |-> ``:30``] word_sub_w2n)))
  THEN (ASSUME_TAC (
	SPECL [``1:num``, ``w2n(x:bool[30])``]
	(Thm.INST_TYPE[alpha |-> ``:30``] (GSYM word_ls_n2w))))
  THEN (FULL_SIMP_TAC (srw_ss()) [])
  THEN ASSUME_TAC (SPEC ``x:bool[30]`` (
			INST_TYPE [alpha |-> ``:30``] w2n_lt))
  THEN (FULL_SIMP_TAC (srw_ss()) [])
  THEN (`0 < w2n x ` by FULL_SIMP_TAC (arith_ss) [])
  THEN (`1 <= w2n x` by FULL_SIMP_TAC (arith_ss) [])
  THEN (METIS_TAC [])
);

val thm_max_l20 = blastLib.BBLAST_PROVE(``!x.(x:word32 && 1023w) <+ 1024w``);
val thm_max_l30 = blastLib.BBLAST_PROVE(``!x.(x:word32 && 1023w) <=+ 1023w``);

val thm_temo_l2_thm = Q.store_thm("thm_temo_l2_thm",
                  `!x. w2n (x:word32 && 1023w) + (1024 − w2n (x:word32 && 1023w)) = 1024`,
		           STRIP_TAC
		      THEN (ASSUME_SPEC_X_32_TAC thm_max_l20)
		      THEN (ASSUME_TAC (SPECL [``(x:word32 && 1023w)``, ``1024w:bool[32]``]
		      			      (Thm.INST_TYPE[alpha |-> ``:32``] 
							    wordsTheory.WORD_LO)))
		      THEN (FULL_SIMP_TAC (srw_ss()) [])
		      THEN (FULL_SIMP_TAC (arith_ss) []));
val thm_temo_l3_thm = Q.store_thm("thm_temo_l3_thm",
               `!x.1 + (1023 − w2n(x:word32 && 1023w)) = 1024 − w2n(x:word32 && 1023w)`,
		           STRIP_TAC
		      THEN (ASSUME_SPEC_X_32_TAC thm_max_l30)
		      THEN (ASSUME_TAC (SPECL [``(x:word32 && 1023w)``, ``1023w:bool[32]``]
					      (Thm.INST_TYPE[alpha |-> ``:32``] wordsTheory.WORD_LS)))
		      THEN (FULL_SIMP_TAC (srw_ss()) [])
		      THEN (FULL_SIMP_TAC (arith_ss) []));

val thm_tem_l1_thm = Q.store_thm("thm_tem_l1_thm",
                 `!x. x < (w2n (l2_index:word32 && 1023w)) ==>  (x < dimword(:10))`,
		      STRIP_TAC
		      THEN (ASSUME_TAC (SPEC ``l2_index:bool[32]`` thm_max_l20))
		      THEN (ASSUME_TAC (SPECL [``l2_index:word32 && 1023w``, ``1024w:bool[32]``]
					      (Thm.INST_TYPE[alpha |-> ``:32``] wordsTheory.WORD_LO)))
		      THEN (FULL_SIMP_TAC (srw_ss()) [])
		      THEN (FULL_SIMP_TAC (arith_ss) []));

val thm_tem_l2_thm = Q.store_thm("thm_tem_l2_thm",
		` (w2n (l2_index:word32 && 1023w)) < dimword(:10)`,
		      (ASSUME_TAC (SPEC ``l2_index:word32:bool[32]`` thm_max_l20))
	              THEN (`(w2n (1024w :word32)) = dimword(:10)` by blastLib.BBLAST_TAC)
		      THEN (ASSUME_TAC (SPECL [``(l2_index:word32 && 1023w)``, ``1024w:bool[32]``]
					      (Thm.INST_TYPE[alpha |-> ``:32``] wordsTheory.WORD_LO)))
		      THEN  (FULL_SIMP_TAC (srw_ss()) []));


val mod_less_thm = Q.store_thm("mod_less_thm", `!x:num y. (x < y)  ==> (x MOD y = x)`,
		      (FULL_SIMP_TAC (arith_ss) [])
);

val thm_tem_xxx_thm = Q.store_thm("thm_tem_xxx_thm",
              `!x. (x < 1024) ==> (x <> (w2n (l2_index:word32 && 1023w))) ==>  ((n2w x) <> (w2w (l2_index:word32 && 1023w)):bool[10])`,
		      STRIP_TAC THEN STRIP_TAC THEN STRIP_TAC
		      THEN (ASSUME_TAC (SPECL [``x:num``, ``w2n (l2_index:word32 && 1023w)``]
					      (Thm.INST_TYPE[alpha |-> ``:10``] (GSYM wordsTheory.n2w_11))))
		      THEN (FULL_SIMP_TAC (bool_ss) [GSYM w2w_def])
		      THEN (FULL_SIMP_TAC (srw_ss()) [])
		      THEN (ASSUME_TAC (SPEC ``l2_index:bool[32]`` thm_max_l20))
		      THEN (ASSUME_TAC (SPECL [``l2_index:word32 && 1023w``, ``1024w:bool[32]``]
					      (Thm.INST_TYPE[alpha |-> ``:32``] wordsTheory.WORD_LO)))
		      THEN (FULL_SIMP_TAC (srw_ss()) [])
		      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x:num < 1024``)
		      THEN (METIS_TAC []));

val thm_tem_yyy_thm = Q.store_thm("thm_tem_yyy_thm",
                  `!x. (x < 1048576) ==> (x <> (w2n ((l2_base_add:bool[32]) ⋙ 12))) ==>  ((n2w x) <> (w2w ((l2_base_add:bool[32]) ⋙ 12)):bool[20])`,
		      STRIP_TAC THEN STRIP_TAC THEN STRIP_TAC
		      THEN (ASSUME_TAC (SPECL [``x:num``, ``w2n ((l2_base_add:bool[32]) ⋙ 12)``]
					      (Thm.INST_TYPE[alpha |-> ``:20``] (GSYM wordsTheory.n2w_11))))
		      THEN (FULL_SIMP_TAC (bool_ss) [GSYM w2w_def])
		      THEN (FULL_SIMP_TAC (srw_ss()) [])
		      THEN (ASSUME_TAC (SPEC ``(l2_base_add:bool[32])`` thm_max_20))
		      THEN (ASSUME_TAC (SPECL [``(l2_base_add:bool[32]) ⋙ 12``, ``1048576w:bool[32]``]
					      (Thm.INST_TYPE[alpha |-> ``:32``] wordsTheory.WORD_LO)))
		      THEN (FULL_SIMP_TAC (srw_ss()) [])
		      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x:num < 1048576``)
		      THEN (METIS_TAC []));


val tmp_thm1 = blastLib.BBLAST_PROVE ``((x:bool[30]) <> 0x3fffffffw) ==>
			((x:bool[30]) <+ 0x3fffffffw)``;

val non_max_no_overflow_thm = Q.store_thm("non_max_no_overflow_thm",
 `
!x:bool[30].
(x <> 0x3FFFFFFFw) ==>
(w2n (x + 1w) = w2n (x) + 1)
`,
  REPEAT (STRIP_TAC)
  THEN (RW_TAC (srw_ss()) [wordsTheory.word_add_def])
  THEN (ASSUME_TAC (UNDISCH tmp_thm1))
  THEN (ASSUME_TAC (
       SPECL [``w2n(x:bool[30])``, ``dimword (:30) - 1``] (INST_TYPE [alpha |-> ``:30``] word_lo_n2w)
	     ))
  THEN (FULL_SIMP_TAC (srw_ss()) [])
  THEN (ASSUME_TAC (
       SPECL [``x:bool[30]``] (INST_TYPE [alpha |-> ``:30``, beta |-> ``:30``] w2w_lt)
	     ))
  THEN (FULL_SIMP_TAC (srw_ss()) [w2w_def])
  THEN (FULL_SIMP_TAC (arith_ss) [])
);


(* ******************** *)
(* ******************** *)
(* ******************** *)
(*  HYPERVISOR HELPERS  *)
(* ******************** *)
(* ******************** *)
(* ******************** *)

(* The invariant page types do not depend on the reference counter *)
val ref_inv_l2_thm =Q.store_thm("ref_inv_l2_thm", 
     `! mem pgtype ph_page. 
       ((invariant_page_type_l2 mem pgtype pgrefs ph_page) ==>
       (invariant_page_type_l2 mem pgtype pgrefs' ph_page))
       `,
    (FULL_SIMP_TAC (srw_ss()) [LET_DEF,invariant_page_type_l2_def]));

val ref_inv_l1_thm =Q.store_thm("ref_inv_l1_thm", 
     `! mem pgtype ph_page. 
       ((invariant_page_type_l1 mem pgtype pgrefs ph_page) ==>
       (invariant_page_type_l1 mem pgtype pgrefs' ph_page))
       `,
    (FULL_SIMP_TAC (srw_ss()) [LET_DEF,invariant_page_type_l1_def]));





(* define when a complete phisical block is unchanged *)
val ph_page_unchanged_def = Define `
  ph_page_unchanged ph_page mem mem' =
    !a:bool[32].
   (((ph_page = ((w2w (a >>> 12)):word20)) /\ (a = a && 0xFFFFFFFCw )) ==>
   (read_mem32(a,mem) = read_mem32(a,mem')))
`;

val ph_l1_unchanged_def = Define `
  ph_l1_unchanged ph_page pgtype mem mem' =
    (((pgtype ph_page = 0b01w:bool[2]) /\ (ph_page && 0xffffcw = ph_page)) ==> (
          (ph_page_unchanged ph_page mem mem') /\
          (ph_page_unchanged (ph_page+1w) mem mem') /\
          (ph_page_unchanged (ph_page+2w) mem mem') /\
          (ph_page_unchanged (ph_page+3w) mem mem')
        ))
`;


val diff_pg_type_diff_pages_thm = Q.store_thm("diff_pg_type_diff_pages_thm", 
`
 !pgtype:bool[20]->bool[2] ph_page:bool[20] ph_page'.
   (pgtype(ph_page) = 2w)==>
   (pgtype(ph_page') = 1w)==>
   (ph_page <> ph_page')
`,
   REPEAT (STRIP_TAC)
   THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``2w:bool[2] <> 1w``))
   THEN METIS_TAC []
);


(* if you do not change the memory of a page and it is a valid L2 *)
(* then it is still a valid l2 *)
val invariant_page_type_l2_unchanged_mem_thm = Q.store_thm("invariant_page_type_l2_unchanged_mem_thm", 
`
  !pgtype ph_page.
  (pgtype ph_page = 2w) ==>
  (invariant_page_type_l2 mem pgtype pgrefs ph_page) ==>
  (ph_page_unchanged ph_page mem mem') ==>
  (invariant_page_type_l2 mem' pgtype pgrefs ph_page)
`,
     (FULL_SIMP_TAC (srw_ss()) [ph_page_unchanged_def])
     THEN REPEAT (STRIP_TAC)
     THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_l2_def])
     THEN STRIP_TAC
     THEN (SPEC_ASSUMPTION_TAC ``!a:bool[32].p``
			       ``w2w (ph_page:bool[20]):bool[32] ≪ 12 ‖
                                 w2w (l2_idx:bool[10]):bool[32] ≪ 2``)
     THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``
        ((ph_page :word20) =  (w2w (((w2w ph_page) ≪ 12 ‖
            (w2w (l2_idx :word10) :word32) ≪ (2 :num)) ⋙ (12 :num)) :
          word20))
	   ``))
     THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``
      ((w2w (ph_page :word20) :word32) ≪ (12 :num) ‖
       (w2w (l2_idx :word10) :word32) ≪ (2 :num) =
       (0xFFFFFFFCw :word32) &&
       ((w2w ph_page :word32) ≪ (12 :num) ‖
        (w2w l2_idx :word32) ≪ (2 :num)))
	   ``))
     THEN (FULL_SIMP_TAC (srw_ss()) [])
     THEN (SPEC_ASSUMPTION_TAC ``!l2_idx:bool[10].p`` ``l2_idx:bool[10]``)
     THEN (METIS_TAC [])
);

val checked_add = ``
	  (w2w ((0xFFFFCw :word20) && (ph_page :word20)) :word32) ≪
              (12 :num) ‖ (w2w (pg_idx :word12) :word32) ≪ (2 :num)``;
val checked_add1 = ``(w2w ((w2w ((0xFFFFCw :word20) && ph_page) :word32) ≪ (12 :num) ⋙
           (12 :num) ‖
           (w2w (pg_idx :word12) :word32) ≪ (2 :num) ⋙ (12 :num)) :
          word20)``;
val possible_ph_page_of_l1_thm = blastLib.BBLAST_PROVE (``
(ph_page = 0xFFFFCw && ph_page) ==>
(((ph_page) = ^checked_add1) \/
((ph_page+1w) = ^checked_add1) \/
((ph_page+2w) = ^checked_add1) \/
((ph_page+3w) = ^checked_add1))
``);

(* if you do not change the memory of the 4 pages and it is a valid L1 *)
(* then it is still a valid l1 *)
val invariant_page_type_l1_unchanged_mem_thm = Q.store_thm("invariant_page_type_l1_unchanged_mem_thm", 
`
  !pgtype ph_page.
  (pgtype ph_page = 1w) ==>
  (ph_page = 0xFFFFCw && ph_page) ==>
  (invariant_page_type_l1 mem pgtype pgrefs ph_page) ==>
  (ph_page_unchanged ph_page mem mem') ==>
  (ph_page_unchanged (ph_page+1w) mem mem') ==>
  (ph_page_unchanged (ph_page+2w) mem mem') ==>
  (ph_page_unchanged (ph_page+3w) mem mem') ==>
  (invariant_page_type_l1 mem' pgtype pgrefs ph_page)
`,
     REPEAT (STRIP_TAC)
     THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_l1_def])
     THEN (REPEAT (STRIP_TAC))
     THEN (SPEC_ASSUMPTION_TAC ``∀pg_idx:bool[12].p`` ``pg_idx:bool[12]``)
     THEN (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF])
     THEN (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF])
     THEN (FULL_SIMP_TAC (srw_ss()) [ph_page_unchanged_def])
     THEN (REPEAT (
	   (SPEC_ASSUMPTION_TAC ``!a:bool[32].p`` checked_add)))
     THEN (FULL_SIMP_TAC (srw_ss()) [
       blastLib.BBLAST_PROVE ``^checked_add = 0xFFFFFFFCw && ^checked_add``
     ])
     THEN (Q.ABBREV_TAC `l1_add = ^checked_add`)
     THEN (ASSUME_TAC possible_ph_page_of_l1_thm)
     THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(ph_page = 0xFFFFCw && ph_page)``)
     THEN (RW_TAC (srw_ss()) [])
     THENL [
       METIS_TAC [],
       METIS_TAC [],
       METIS_TAC [],
       METIS_TAC []
     ]
);

(* there is a copy in helperLib *)
fun SUM_BY_EQ_FUN_TAC (asl, w) =
    let val (a,b) = dest_eq w
	val (f,arg1) = dest_comb a
	val (f1,arg2) = dest_comb b
	val (f,a) = dest_comb f
	val (a1,a2) = pairLib.dest_pair a
	val tmp_thm = SPECL [a1, a2, arg1, arg2]  GSUM_FUN_EQUAL
	val (t_hyp, t_concl) = dest_imp (concl tmp_thm)
    in
	(SUBGOAL_THEN t_hyp (fn thm =>
      ASSUME_TAC thm
      THEN (ACCEPT_TAC (UNDISCH tmp_thm))
   )) (asl, w)
    end;


val ref_cnt_equality_thm = Q.store_thm("ref_cnt_equality_thm",
   `!(ph_page:word20) (mem:word32->word8) mem' dest_page.
       (
        ((pgtype ph_page = 0b10w) ==> 
          (ph_page_unchanged ph_page mem mem'))
        /\
	(((pgtype ph_page = 0b01w) /\ (ph_page && 0xffffcw = ph_page)) ==> (
          (ph_page_unchanged ph_page mem mem') /\
          (ph_page_unchanged (ph_page+1w) mem mem') /\
          (ph_page_unchanged (ph_page+2w) mem mem') /\
          (ph_page_unchanged (ph_page+3w) mem mem')
        ))
      )	  ==>
	  (
	   (count_pages_for_pt mem pgtype  dest_page ph_page ) =
	   (count_pages_for_pt mem' pgtype dest_page ph_page )
	  )
`,
  RW_TAC (srw_ss()) []
  THEN FULL_SIMP_TAC(srw_ss()) [count_pages_for_pt_def]
  THEN RW_TAC (srw_ss()) []
  THENL
  [ (* case data: trivial *)
    FULL_SIMP_TAC(srw_ss()) [],
    (* case L1 *)
    FULL_SIMP_TAC(srw_ss()) [SUM]
    THEN SUM_BY_EQ_FUN_TAC
    THEN RW_TAC (srw_ss()) []
    THEN FULL_SIMP_TAC(srw_ss()) [l1_count_def]
    THEN RW_TAC (bool_ss) []
    THEN (FULL_SIMP_TAC (srw_ss()) [ph_page_unchanged_def])
    THEN (REPEAT ((SPEC_ASSUMPTION_TAC ``!a:bool[32].p`` checked_add)))
    THEN (FULL_SIMP_TAC (srw_ss()) [
       blastLib.BBLAST_PROVE ``^checked_add = 0xFFFFFFFCw && ^checked_add``
     ])
    THEN (Q.ABBREV_TAC `l1_add = ^checked_add`)
    THEN (ASSUME_TAC possible_ph_page_of_l1_thm)
    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(ph_page = 0xFFFFCw && ph_page)``)
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THENL [
       METIS_TAC [],
       METIS_TAC [],
       METIS_TAC [],
       METIS_TAC []
    ]    ,
   (* case L2 *)
   FULL_SIMP_TAC(srw_ss()) [SUM]
   THEN SUM_BY_EQ_FUN_TAC
   THEN RW_TAC (srw_ss()) []
   THEN FULL_SIMP_TAC(srw_ss()) [l2_count_def]
   THEN RW_TAC (bool_ss) []
   THEN (FULL_SIMP_TAC(srw_ss()) [ph_page_unchanged_def])
   THEN (SPEC_ASSUMPTION_TAC ``!a.p`` ``w2w (ph_page:word20) ≪ 12 ‖ w2w (pt_idx:word10):word32 ≪ 2``)
   THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``
((ph_page :word20) = (w2w (((w2w ph_page :word32) ≪ (12 :num) ‖
  (w2w (pt_idx :word10) :word32) ≪ (2 :num)) ⋙ (12 :num)) : word20)) ∧
((w2w ph_page :word32) ≪ (12 :num) ‖ (w2w pt_idx :word32) ≪ (2 :num) =
  (0xFFFFFFFCw :word32) && ((w2w ph_page :word32) ≪ (12 :num) ‖
  (w2w pt_idx :word32) ≪ (2 :num)))
``))
   THEN METIS_TAC []
  ]
)
;

val l2_entry_unchanged_def = Define `
  l2_entry_unchanged l2_base_add (l2_idx:bool[10]) mem mem' =
         (
       	  read_mem32 ((l2_base_add && 0xFFFFF000w) !! (w2w l2_idx):word32 << 2,mem) =
       	  read_mem32 ((l2_base_add && 0xFFFFF000w) !! (w2w l2_idx):word32 << 2,mem')
       	 )
`;


val ref_cnt_equality_l2_thm = Q.store_thm("ref_cnt_equality_l2_thm", 
   `!l2_base_add:word32 idx:word10 (mem:word32->word8) mem' pgtype:bool[20]->bool[2] ph_page.
      (pgtype (w2w (l2_base_add ⋙ 12)) = 2w) ==>
      (l2_entry_unchanged l2_base_add idx mem mem')
	==> 
       (l2_count (w2n idx) mem pgtype ph_page (w2w (l2_base_add ⋙ 12)) =
       l2_count (w2n idx) mem' pgtype ph_page (w2w (l2_base_add ⋙ 12)))
      `,
  RW_TAC (srw_ss()) []
  THEN FULL_SIMP_TAC(srw_ss()) [l2_count_def]
  THEN RW_TAC (srw_ss()) []
  THEN `desc_add = desc_add'` by METIS_TAC[]
  THEN FULL_SIMP_TAC(srw_ss()) [l2_entry_unchanged_def]
  THEN (UNABBREV_ALL_TAC)
  THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``
((w2w (w2w (l2_base_add ⋙ (12 :num)) :word20) :word32) ≪ (12 :
         num) ‖ (w2w idx :word32) ≪ (2 :num)) =
((0xFFFFF000w :word32) && (l2_base_add :word32) ‖
         (w2w (idx :word10) :word32) ≪ (2 :num))
``))
  (* to finish the proof *)
  THEN METIS_TAC []);

(* counter 0 related *)
val inv_counter_part_def = Define `
   inv_counter_part (pgrefs:word20->word30) mem pgtype =
   (!ph_page':bool[20].
   (w2n(pgrefs ph_page') = (count_pages mem pgtype ph_page' ))
  )
`;
val sound_type_change_def = Define `
   sound_type_change (pgtype:word20->word2) pgtype' (pgrefs :word20->word30) =
   (!ph_page'':bool[20].
   (pgtype' ph_page'' <> pgtype ph_page'') ==>
   (pgrefs ph_page'' = 0w)
  )
`;


val invariant_page_type_l2_ok_changed_types_thm = Q.store_thm("invariant_page_type_l2_ok_changed_types_thm", 
`
  !pgtype ph_page.
  (pgtype ph_page = 2w) ==>
  (invariant_page_type_l2 mem pgtype pgrefs ph_page) ==>
  (inv_counter_part pgrefs mem pgtype) ==>
  (sound_type_change pgtype pgtype' pgrefs) ==>
  (invariant_page_type_l2 mem pgtype' pgrefs ph_page)
`,
  REPEAT (STRIP_TAC)
  THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_l2_def,
				  inv_counter_part_def, sound_type_change_def])
  THEN REPEAT (STRIP_TAC)
  THEN (SPEC_ASSUMPTION_TAC ``!l2_idx:bool[10].p`` ``l2_idx:bool[10]``)
  THEN (Q.ABBREV_TAC `l2_desc_add:bool[32] = w2w ph_page ≪ 12 ‖ w2w l2_idx ≪ 2`)
  THEN (Q.ABBREV_TAC `l2_desc:bool[32] = (read_mem32 (l2_desc_add,mem))`)
  THEN (SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (LET_HYP_ELIM_TAC [])
  THEN (Cases_on `pgtype' (rec'l2SmallT l2_desc).addr = pgtype (rec'l2SmallT l2_desc).addr`)
  THENL [
    (* the entry has unchanged pg_type *)
    (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF]),
    (* the entry has changed pg_type *)
    (* we know that its ref_counter is 0 *)
    (* then we know that the current pt is not pointing, writing to the entry *)
    (Q.ABBREV_TAC `prevent_split = ((3w && l2_desc = 2w) ∨ (3w && l2_desc = 3w))`)
    THEN (FULL_SIMP_TAC (srw_ss()) [sound_type_change_def])
    THEN (SPEC_ASSUMPTION_TAC ``∀ph_page'':bool[20]. x ==> y`` ``(rec'l2SmallT l2_desc).addr``)
    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``
       pgtype' (rec'l2SmallT l2_desc).addr <> pgtype (rec'l2SmallT l2_desc).addr
    ``)
    THEN (FULL_SIMP_TAC (srw_ss()) [inv_counter_part_def])
    THEN (SPEC_ASSUMPTION_TAC ``∀ph_page'':bool[20]. p`` ``(rec'l2SmallT l2_desc).addr``)
    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``
       pgrefs (rec'l2SmallT l2_desc).addr = 0w
    ``)
    THEN (FULL_SIMP_TAC (srw_ss()) [count_pages_def, SUM])
    THEN (FULL_SIMP_TAC (srw_ss()) [GSYM GSUM_ZERO])
    THEN (SPEC_ASSUMPTION_TAC ``∀it:num. p`` ``w2n (ph_page:bool[20])``)
    THEN (ASSUME_TAC (
	  (SPEC ``ph_page:bool[20]`` (
			INST_TYPE [alpha |-> ``:20``] w2n_lt))
	 ))
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
    THEN (FULL_SIMP_TAC (srw_ss()) [count_pages_for_pt_def])
    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(pgtype:bool[20]->bool[2] ph_page) = 2w``)
    THEN (Cases_on `pgtype (rec'l2SmallT l2_desc).addr <> 0w`)
    THENL [
       (* the page was pointed but not writable *)
       (FULL_SIMP_TAC (srw_ss()) []),
       (* we need to show that it was not writable *)
       (FULL_SIMP_TAC (srw_ss()) [])
       THEN (UNDISCH_TAC ``(rec'l2SmallT l2_desc).ap = 3w``)
       THEN (FULL_SIMP_TAC (srw_ss()) [])
       THEN (FULL_SIMP_TAC (srw_ss()) [SUM])
       THEN (FULL_SIMP_TAC (srw_ss()) [GSYM GSUM_ZERO])
       THEN (SPEC_ASSUMPTION_TAC ``∀it:num. p`` ``w2n (l2_idx:bool[10])``)
       THEN (ASSUME_TAC (
	  (SPEC ``l2_idx:bool[10]`` (
			INST_TYPE [alpha |-> ``:10``] w2n_lt))
	 ))
       THEN (FULL_SIMP_TAC (srw_ss()) [])
       THEN (FULL_SIMP_TAC (srw_ss()) [l2_count_def])
       THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
       THEN (REABBREV_TAC)
       THEN (FULL_SIMP_TAC (srw_ss()) [])
       THEN (FULL_SIMP_TAC (srw_ss()) [COND_RAND])
       THEN (FULL_SIMP_TAC (srw_ss()) [COND_RATOR])
       THEN (FULL_SIMP_TAC (srw_ss()) [rec'l2SmallT_def])
    ]
  ]
);


val invariant_page_type_l1_ok_changed_types_thm = Q.store_thm("invariant_page_type_l1_ok_changed_types_thm", 
`
  !pgtype ph_page.
  (pgtype ph_page = 1w) ==>
  (ph_page = 0xFFFFCw && ph_page) ==>
  (invariant_page_type_l1 mem pgtype pgrefs ph_page) ==>
  (!ph_page':bool[20].
   (w2n(pgrefs ph_page') = (count_pages mem pgtype ph_page' ))
  ) ==>
  (!ph_page'':bool[20].
   (pgtype' ph_page'' <> pgtype ph_page'') ==>
   (pgrefs ph_page'' = 0w)
  ) ==>
  (invariant_page_type_l1 mem pgtype' pgrefs ph_page)
`,
  REPEAT (STRIP_TAC)
  THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_l1_def])
  THEN REPEAT (STRIP_TAC)
  THEN (SPEC_ASSUMPTION_TAC ``∀pg_idx:bool[12].p`` ``pg_idx:bool[12]``)
  THEN (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF])
  THEN (Q.ABBREV_TAC `desc_add:bool[32] = w2w (0xFFFFCw && ph_page) ≪ 12 ‖ w2w pg_idx ≪ 2`)
  THEN (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF])
  THEN (Q.ABBREV_TAC `l1_desc = read_mem32 (desc_add,mem)`)
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THENL [
    (* each address pointed by a section is still safe *)
    REPEAT STRIP_TAC
    THEN (SPEC_ASSUMPTION_TAC ``!va:bool[32].p`` ``va:bool[32]``)
    THEN (Q.ABBREV_TAC `pointed_page:bool[20] = (w2w (255w && va ⋙ 12 ‖ w2w (rec'l1SecT l1_desc).addr ≪ 20 ⋙ 12))`)
    THEN (Cases_on `pgtype' pointed_page = pgtype pointed_page`)
    THENL [
      FULL_SIMP_TAC (srw_ss()) [],
      (* the entry has changed pg_type *)
      (* we know that its ref_counter is 0 *)
      (* then we know that the current section is not pointing, writing to the entry *)
      SPEC_ASSUMPTION_TAC ``!ph_page'':bool[20].p`` ``pointed_page:bool[20]``
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype' pointed_page <> pgtype' pointed_page``)
      THEN (SPEC_ASSUMPTION_TAC ``∀ph_page'':bool[20]. p`` ``pointed_page:bool[20]``)
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``
       pgrefs pointed_page = 0w
    ``)
      THEN (FULL_SIMP_TAC (srw_ss()) [count_pages_def, SUM])
      THEN (FULL_SIMP_TAC (srw_ss()) [GSYM GSUM_ZERO])
      THEN (SPEC_ASSUMPTION_TAC ``∀it:num. p`` ``w2n (ph_page:bool[20])``)
      THEN (ASSUME_TAC (
	  (SPEC ``ph_page:bool[20]`` (
			INST_TYPE [alpha |-> ``:20``] w2n_lt))
	 ))
      THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
      THEN (FULL_SIMP_TAC (srw_ss()) [count_pages_for_pt_def])
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(pgtype:bool[20]->bool[2] ph_page) = 1w``)
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``ph_page = 0xFFFFCw && ph_page``)
      THEN (FULL_SIMP_TAC (srw_ss()) [SUM])
      THEN (FULL_SIMP_TAC (srw_ss()) [GSYM GSUM_ZERO])
      THEN (SPEC_ASSUMPTION_TAC ``∀it:num. p`` ``w2n (pg_idx:bool[12])``)
      THEN (ASSUME_TAC (
	  (SPEC ``pg_idx:bool[12]`` (
			INST_TYPE [alpha |-> ``:12``] w2n_lt))
	 ))
      THEN (FULL_SIMP_TAC (srw_ss()) [])
      THEN (FULL_SIMP_TAC (srw_ss()) [l1_count_def])
      THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
      THEN (REABBREV_TAC)
      THEN (SYM_ASSUMPTION_TAC ``ph_page = 0xFFFFCw && ph_page``)
      THEN (FULL_SIMP_TAC (srw_ss()) [])
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``mmu_l1_decode_type l1_desc = 2w``)
      THEN (Cases_on `pgtype pointed_page ≠ 0w`)
      THENL [
         (* The page was not a data page *)
         METIS_TAC [],
	 (FULL_SIMP_TAC (srw_ss()) [])
	 THEN (FULL_SIMP_TAC (srw_ss()) [rec'l1SecT_def])
	 THEN (Q.UNABBREV_TAC `pointed_page`)
	 THEN (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE ``
(((31 :num) >< (20 :num)) l1_desc :word12) =
           (w2w
              ((w2w
                  ((255w :word32) && (va :word32) ⋙ (12 :num) ‖
                   (w2w (((31 :num) >< (20 :num)) l1_desc :word12) :
                      word32) ≪ (20 :num) ⋙ (12 :num)) :word20) ⋙ (8 :
               num)) :word12)
	 ``])
	 THEN (Q.ABBREV_TAC `ap:bool[3] = (((((15 :num) >< (15 :num)) (l1_desc :word32) :word1) @@
        (((11 :num) >< (10 :num)) l1_desc :word2))
         :word3)`)
	 THEN (FULL_SIMP_TAC (srw_ss()) [mmu_l1_decode_type_def])
	 THEN (FULL_SIMP_TAC (srw_ss()) [UNDISCH_ALL (blastLib.BBLAST_PROVE ``(1w && l1_desc ⋙ 18 = 0w) ==> 
(3w && l1_desc = 2w) ==>
(
(((((18 :num) >< (18 :num)) (l1_desc :word32) :word1) @@
        (((1 :num) >< (1 :num)) l1_desc :word1))
         :word2) = (1w :word2))``)])
	 THEN (FULL_SIMP_TAC (srw_ss()) [COND_RAND])
	 THEN (FULL_SIMP_TAC (srw_ss()) [COND_RATOR])
      ]
    ],
    (* we need to show that the pointed l2 is a valid l2 *)
    (* first we know that since we have not changed the memory, the type was 2 *)
    (* since it was a valid L1, the pointed l2 has counter bigger than 0 *)
    (Q.ABBREV_TAC `pointed_page:bool[20] = (w2w ((rec'l1PTT l1_desc).addr ⋙ 2))`)
    THEN (Cases_on `pgtype' pointed_page = pgtype pointed_page`)
    THENL [
       FULL_SIMP_TAC (srw_ss()) [],
       SPEC_ASSUMPTION_TAC ``!ph_page'':bool[20].p`` ``pointed_page:bool[20]``
       THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype' pointed_page <> pgtype' pointed_page``)
       THEN (SPEC_ASSUMPTION_TAC ``∀ph_page'':bool[20]. p`` ``pointed_page:bool[20]``)
       THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``
        pgrefs pointed_page = 0w
      ``)
       THEN (FULL_SIMP_TAC (srw_ss()) [count_pages_def, SUM])
       THEN (FULL_SIMP_TAC (srw_ss()) [GSYM GSUM_ZERO])
      THEN (SPEC_ASSUMPTION_TAC ``∀it:num. p`` ``w2n (ph_page:bool[20])``)
      THEN (ASSUME_TAC (
	  (SPEC ``ph_page:bool[20]`` (
			INST_TYPE [alpha |-> ``:20``] w2n_lt))
	 ))
      THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
      THEN (FULL_SIMP_TAC (srw_ss()) [count_pages_for_pt_def])
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(pgtype:bool[20]->bool[2] ph_page) = 1w``)
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``ph_page = 0xFFFFCw && ph_page``)
      THEN (FULL_SIMP_TAC (srw_ss()) [SUM])
      THEN (FULL_SIMP_TAC (srw_ss()) [GSYM GSUM_ZERO])
      THEN (SPEC_ASSUMPTION_TAC ``∀it:num. p`` ``w2n (pg_idx:bool[12])``)
      THEN (ASSUME_TAC (
	  (SPEC ``pg_idx:bool[12]`` (
			INST_TYPE [alpha |-> ``:12``] w2n_lt))
	 ))
      THEN (FULL_SIMP_TAC (srw_ss()) [])
      THEN (FULL_SIMP_TAC (srw_ss()) [l1_count_def])
      THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
      THEN (REABBREV_TAC)
      THEN (SYM_ASSUMPTION_TAC ``ph_page = 0xFFFFCw && ph_page``)
      THEN (FULL_SIMP_TAC (srw_ss()) [])
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype pointed_page = 2w``)
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``mmu_l1_decode_type l1_desc = 1w``)
      THEN (METIS_TAC [])
    ]
  ]
);



val ref_cnt_equality_ok_changed_types_thm = Q.store_thm("ref_cnt_equality_ok_changed_types_thm", `
!(ph_page:word20) (mem:word32->word8) pgtype pgtype' dest_page.
(pgtype ph_page = 2w:bool[2]) ==>
(pgtype' ph_page = 2w:bool[2]) ==>
(invariant_page_type_l2 mem pgtype pgrefs ph_page) ==>
(!ph_page':bool[20].
   (w2n(pgrefs ph_page') = (count_pages mem pgtype ph_page' ))
  ) ==>
(!ph_page'':bool[20].
   (pgtype' ph_page'' <> pgtype ph_page'') ==>
   (pgrefs ph_page'' = 0w)
  ) ==>
	  (
	   (count_pages_for_pt mem pgtype  dest_page ph_page ) =
	   (count_pages_for_pt mem pgtype' dest_page ph_page )
	  )
`,
   REPEAT STRIP_TAC
   THEN (Cases_on `pgtype' dest_page <> pgtype dest_page`)
   THENL [
       (SPEC_ASSUMPTION_TAC ``∀ph_page:bool[20]. p`` ``dest_page:bool[20]``)
       THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype' dest_page <> pgtype dest_page``)
       THEN (SPEC_ASSUMPTION_TAC ``∀ph_page:bool[20]. p`` ``dest_page:bool[20]``)
       THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgrefs dest_page = 0w``)
       THEN (SYM_ASSUMPTION_TAC ``0:num=a``)
       THEN (FULL_SIMP_TAC (srw_ss()) [count_pages_def, SUM])
       THEN (FULL_SIMP_TAC (srw_ss()) [GSYM GSUM_ZERO])
       THEN (SPEC_ASSUMPTION_TAC ``∀it:num. p`` ``w2n(ph_page:bool[20])``)
       THEN (ASSUME_TAC (
	  (SPEC ``ph_page:bool[20]`` (
			INST_TYPE [alpha |-> ``:20``] w2n_lt))
	 ))
       THEN (FULL_SIMP_TAC (srw_ss()) [])
       THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
       THEN (FULL_SIMP_TAC (srw_ss()) [count_pages_for_pt_def])
       THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype ph_page = 2w``)
       THEN (Cases_on `pgtype' dest_page <> 0w`)
       THENL [
         (FULL_SIMP_TAC (srw_ss()) []),
	 (* the new page type is zero. if it was zero we can use the previous ref_counter, otherwise we need the invariant *)
	 (FULL_SIMP_TAC (srw_ss()) [])
         THEN (Cases_on `pgtype dest_page = 0w`)
	 THENL [
	   (FULL_SIMP_TAC (srw_ss()) []),
	   (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_l2_def])
	   THEN (FULL_SIMP_TAC (srw_ss()) [SUM])
	   THEN (FULL_SIMP_TAC (srw_ss()) [GSYM GSUM_ZERO])
	   THEN (REPEAT STRIP_TAC)
	   THEN (SPEC_ASSUMPTION_TAC ``!l2_idx:bool[10].p`` ``(n2w it):bool[10]``)
	   THEN (FULL_SIMP_TAC (srw_ss()) [l2_count_def])
	   THEN (RW_TAC (srw_ss()) [])
	   THEN (LET_HYP_ELIM_TAC [])
	   THEN (Q.ABBREV_TAC `prevent_split = ((3w && l2_desc = 2w) ∨ (3w && l2_desc = 3w))`)
	   THEN (Cases_on `l2_pt_desc.addr <> dest_page`)
           THENL [
	     (FULL_SIMP_TAC (srw_ss()) []),
	     (REABBREV_TAC)
   	     THEN (Cases_on `(l2_type = 0w)`)
	     THENL [
	       (FULL_SIMP_TAC (srw_ss()) [Abbr `prevent_split`]),
	       (FULL_SIMP_TAC (srw_ss()) [])
	       THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype dest_page ≠ 0w``)
               THEN (ASSUME_TAC (UNDISCH (blastLib.BBLAST_PROVE ``
	           ((l2_pt_desc.ap = 1w) ∨ (l2_pt_desc.ap = 2w)) ==> (l2_pt_desc.ap <> 3w:bool[3])
		   ``)))
	       THEN (METIS_TAC [])
	     ]
	   ]
	 ]
       ],
       (* other case: the two page tapyes are equals *)
       (FULL_SIMP_TAC (srw_ss()) [])
       THEN (FULL_SIMP_TAC (srw_ss()) [count_pages_for_pt_def])
       THEN (RW_TAC (srw_ss()) [])
       THEN FULL_SIMP_TAC(srw_ss()) [SUM]
       THEN SUM_BY_EQ_FUN_TAC
       THEN RW_TAC (srw_ss()) []
       THEN FULL_SIMP_TAC(srw_ss()) [l2_count_def]
   ]
);



val ref_cnt_l1_equality_ok_changed_types_thm = Q.store_thm("ref_cnt_l1_equality_ok_changed_types_thm", `
!(ph_page:word20) (mem:word32->word8) pgtype pgtype' dest_page.
(pgtype ph_page = 1w:bool[2]) ==>
(pgtype' ph_page = 1w:bool[2]) ==>
(invariant_page_type_l1 mem pgtype pgrefs ph_page) ==>
(!ph_page':bool[20].
   (w2n(pgrefs ph_page') = (count_pages mem pgtype ph_page' ))
  ) ==>
(!ph_page'':bool[20].
   (pgtype' ph_page'' <> pgtype ph_page'') ==>
   (pgrefs ph_page'' = 0w)
  ) ==>
	  (
	   (count_pages_for_pt mem pgtype  dest_page ph_page ) =
	   (count_pages_for_pt mem pgtype' dest_page ph_page )
	  )
`,
   REPEAT STRIP_TAC
   THEN (Cases_on `ph_page ≠ (0xFFFFCw && ph_page)`)
   THENL [
      (FULL_SIMP_TAC (srw_ss()) [count_pages_for_pt_def]),
      (FULL_SIMP_TAC (srw_ss()) [count_pages_for_pt_def])
      THEN (Cases_on `pgtype' dest_page = pgtype dest_page`)
      THENL [
        FULL_SIMP_TAC(srw_ss()) [SUM]
        THEN SUM_BY_EQ_FUN_TAC
        THEN (RW_TAC (srw_ss()) [])
	THEN FULL_SIMP_TAC(srw_ss()) [l1_count_def],
	(* the page type changed *)
	(SPEC_ASSUMPTION_TAC ``∀ph_page:bool[20]. p`` ``dest_page:bool[20]``)
	THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype' dest_page <> pgtype dest_page``)
	THEN (SPEC_ASSUMPTION_TAC ``∀ph_page:bool[20]. p`` ``dest_page:bool[20]``)
	THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgrefs dest_page = 0w``)
	THEN (SYM_ASSUMPTION_TAC ``0:num=a``)
	THEN (FULL_SIMP_TAC (srw_ss()) [count_pages_def, SUM])
	THEN (FULL_SIMP_TAC (srw_ss()) [GSYM GSUM_ZERO])
	THEN (SPEC_ASSUMPTION_TAC ``∀it:num. p`` ``w2n(ph_page:bool[20])``)
	THEN (ASSUME_TAC (
	  (SPEC ``ph_page:bool[20]`` (
			INST_TYPE [alpha |-> ``:20``] w2n_lt))
	 ))
	THEN (FULL_SIMP_TAC (srw_ss()) [])
	THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
	THEN (FULL_SIMP_TAC (srw_ss()) [count_pages_for_pt_def])
	THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype ph_page = 1w``)
	THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``ph_page = 0xFFFFCw && ph_page``)
	THEN (FULL_SIMP_TAC (srw_ss()) [SUM])
	THEN (FULL_SIMP_TAC (srw_ss()) [GSYM GSUM_ZERO])
	THEN (RW_TAC (srw_ss()) [])
	THEN (SPEC_ASSUMPTION_TAC ``∀it:num. p`` ``it:num``)
	THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``it < 4096:num``)
	THEN (FULL_SIMP_TAC (srw_ss()) [l1_count_def])
	THEN (RW_TAC (srw_ss()) [])
	THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
	THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_l1_def])
	THEN (SPEC_ASSUMPTION_TAC ``!pg_idx:bool[12].p`` ``(n2w it):bool[12]``)
	THEN (REPEAT (LET_HYP_ELIM_TAC []))
	THEN (SYM_ASSUMPTION_TAC ``ph_page = 0xFFFFCw && ph_page``)
	THEN (FULL_SIMP_TAC (srw_ss()) [])
	THENL [
	  (* UNMAPPED *)
	  REABBREV_TAC
          THEN (ASSUME_TAC (UNDISCH (blastLib.BBLAST_PROVE ``(l1_type = 0w:word32) ==> (l1_type <> 1w:word32)``)))
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``l1_type <> 1w``)
	  THEN (FULL_SIMP_TAC (srw_ss()) []),
	  (* SECTION *)
	  REABBREV_TAC
	  THEN (ASSUME_TAC (UNDISCH (blastLib.BBLAST_PROVE ``(l1_type = 2w:word32) ==> (l1_type <> 1w:word32)``)))
	  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``l1_type <> 1w``)
	  THEN (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (Cases_on `l1_sec_desc.addr <> (w2w (dest_page ⋙ 8))`)
	  THENL [
	    (FULL_SIMP_TAC (srw_ss()) []),
	    (FULL_SIMP_TAC (srw_ss()) [])
            THEN (SPEC_ASSUMPTION_TAC ``!va:bool[32].p`` ``((w2w(dest_page:word20)):word32) << 12``)
	    THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
	    THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``
(w2w  ((255w :word32) &&
               (w2w (dest_page :word20) :word32) ≪ (12 :num) ⋙ (12 :
               num) ‖
               (w2w (w2w (dest_page ⋙ (8 :num)) :word12) :word32) ≪
               (20 :num) ⋙ (12 :num)) :word20) = dest_page``))
	    THEN (FULL_SIMP_TAC (srw_ss()) [])
	    THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``((l1_sec_desc:l1SecT).ap = 2w:word3) ==> (l1_sec_desc.ap <> 3w:word3)``))
	    THEN METIS_TAC []
	  ],
	  (* PT *)
	  REABBREV_TAC
	  THEN (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (Cases_on `pgtype dest_page = 2w`)
	  THENL [
	    (FULL_SIMP_TAC (srw_ss()) []),
	    (FULL_SIMP_TAC (srw_ss()) [])
	    THEN (Cases_on `w2w (l1_pt_desc.addr ⋙ 2) <> dest_page`)
	    THENL [
	      (FULL_SIMP_TAC (srw_ss()) []),
	      (FULL_SIMP_TAC (srw_ss()) [])
	    ]
	  ]
	]
      ]
   ]
);



(* ******************** *)
(* ******************** *)
(* ******************** *)
(*      SUM HELPERS     *)
(* ******************** *)
(* ******************** *)
(* ******************** *)


val sum_split_thm = Q.store_thm("sum_split_thm", 
`
!x n f.
  (x < n) ==>
  ((GSUM (0,n) f) = (GSUM (0, x) f) +  (f x) + (GSUM (x + 1, n - x - 1) f))
`,
   REPEAT (STRIP_TAC)
   THEN (ASSUME_TAC (
	 SPECL [``0:num``,
       ``x:num``,
       ``(n:num)-x``,
       ``f:num->num``
      ] GSUM_ADD))
   THEN (`x + (n − x) = n` by (FULL_SIMP_TAC (arith_ss) []))
   THEN (FULL_SIMP_TAC (arith_ss) [])
   THEN (ASSUME_TAC (
	 SPECL [``x:num``,
	  ``1:num``,
	  ``n - x - 1:num``,
	  ``f:num->num``
      ] GSUM_ADD))
   THEN (`1 + (n − x − 1) = n-x` by (FULL_SIMP_TAC (arith_ss) []))
   THEN (FULL_SIMP_TAC (srw_ss()) [GSUM_1])
   THEN (FULL_SIMP_TAC (arith_ss) [])
);

val sum_splitter_thm = Q.store_thm("sum_splitter_thm", 
`
!idx n f f' a b.
((a + f idx = f' idx + b) /\
 (!x. (x <> idx) ==> (x < n) ==> (f x = f' x)) /\
 (idx < n)
) ==>
(a + GSUM (0,n) f = GSUM (0,n) f' + b)
`,
    RW_TAC (srw_ss()) []
    THEN (PURE_ONCE_REWRITE_TAC [
	  UNDISCH (
	  SPECL [``idx:num``, ``n:num``, ``f:num->num``] sum_split_thm)
	 ])
    THEN ASSUME_TAC (
    SPECL [``0:num``, ``idx:num``, ``f:num->num``, ``f':num->num``]
       GSUM_FUN_EQUAL)
    THEN (`(!x. 0 <= x ∧ x < 0 + idx  ==> (f x = f' x))` by
	  (RW_TAC (arith_ss) [])
	 )
    THEN RES_TAC
    THEN (FULL_SIMP_TAC (arith_ss) [])
    THEN ADD_CANC_TAC
    THEN ASSUME_TAC (
    SPECL [``idx+1:num``, ``n − (idx + 1:num)``, ``f:num->num``, ``f':num->num``]
       GSUM_FUN_EQUAL)
    THEN (`(!x. idx + 1 <= x ∧ x < idx + 1 + (n − (idx + 1)) ==> (f x = f' x))` by
	  (RW_TAC (arith_ss) [])
	 )
    THEN RES_TAC
    THEN (FULL_SIMP_TAC (arith_ss) [])
);

val SUM_GT_thm = Q.store_thm("SUM_GT_thm",
`
  !f n k.
  (k < n) ==>
  (f k > 0) ==>
  (SUM n f > 0)
`,
  REPEAT STRIP_TAC
  THEN (FULL_SIMP_TAC (srw_ss()) [SUM])
  THEN (PURE_ONCE_REWRITE_TAC [
	  UNDISCH (
	  SPECL [``k:num``, ``n:num``, ``f:num->num``] sum_split_thm)
	 ])
  THEN (FULL_SIMP_TAC (arith_ss) [])
);



val _ = export_theory ();


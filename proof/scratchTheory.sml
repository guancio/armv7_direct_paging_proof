structure scratchTheory :> scratchTheory =
struct
  val _ = if !Globals.print_thy_loads then print "Loading scratchTheory ... " else ()
  open Type Term Thm
  infixr -->

  fun C s t ty = mk_thy_const{Name=s,Thy=t,Ty=ty}
  fun T s t A = mk_thy_type{Tyop=s, Thy=t,Args=A}
  fun V s q = mk_var(s,q)
  val U     = mk_vartype
  (*  Parents *)
  local open helperTheory hypervisor_modelTheory
  in end;
  val _ = Theory.link_parents
          ("scratch",
          Arbnum.fromString "1384167531",
          Arbnum.fromString "275406")
          [("hypervisor_model",
           Arbnum.fromString "1386326739",
           Arbnum.fromString "517482"),
           ("helper",
           Arbnum.fromString "1386328534",
           Arbnum.fromString "692951")];
  val _ = Theory.incorporate_types "scratch" [];

  val idvector = 
    let fun ID(thy,oth) = {Thy = thy, Other = oth}
    in Vector.fromList
  [ID("min", "fun"), ID("min", "bool"), ID("fcp", "cart"),
   ID("fcp", "bit0"), ID("one", "one"), ID("fcp", "bit1"),
   ID("num", "num"), ID("tiny", "sctlrT"), ID("bool", "!"),
   ID("arithmetic", "+"), ID("pair", ","), ID("pair", "prod"),
   ID("arithmetic", "-"), ID("bool", "/\\"), ID("num", "0"),
   ID("prim_rec", "<"), ID("min", "="), ID("min", "==>"),
   ID("arithmetic", ">"), ID("arithmetic", "BIT1"),
   ID("arithmetic", "BIT2"), ID("sum_num", "GSUM"), ID("bool", "LET"),
   ID("arithmetic", "NUMERAL"), ID("sum_num", "SUM"),
   ID("pair", "UNCURRY"), ID("arithmetic", "ZERO"),
   ID("hypervisor_model", "count_pages_for_pt"),
   ID("hypervisor_model", "invariant_page_type"),
   ID("hypervisor_model", "invariant_page_type_l1"),
   ID("hypervisor_model", "invariant_page_type_l2"),
   ID("hypervisor_model", "invariant_ref_cnt"),
   ID("hypervisor_model", "l2_count"), ID("scratch", "l2_entry_unchanged"),
   ID("words", "n2w"), ID("scratch", "ph_l1_unchanged"),
   ID("scratch", "ph_page_unchanged"), ID("tiny", "read_mem32"),
   ID("tiny", "rec'sctlrT"),
   ID("hypervisor_model", "unmap_L2_pageTable_entry"), ID("words", "w2n"),
   ID("words", "w2w"), ID("words", "word_add"), ID("words", "word_and"),
   ID("words", "word_lsl"), ID("words", "word_lsr"),
   ID("words", "word_or"), ID("words", "word_sub"), ID("bool", "~")]
  end;
  local open SharingTables
  in
  val tyvector = build_type_vector idvector
  [TYOP [1], TYOP [4], TYOP [3, 1], TYOP [3, 2], TYOP [3, 3],
   TYOP [2, 0, 4], TYOP [3, 4], TYOP [3, 6], TYOP [2, 0, 7],
   TYOP [0, 8, 5], TYOP [0, 9, 0], TYOP [0, 9, 10], TYOP [5, 2],
   TYOP [3, 12], TYOP [3, 13], TYOP [2, 0, 14], TYOP [0, 15, 11],
   TYOP [2, 0, 2], TYOP [0, 15, 17], TYOP [0, 18, 11], TYOP [0, 15, 19],
   TYOP [2, 0, 13], TYOP [0, 21, 11], TYOP [0, 8, 22], TYOP [6], TYOP [7],
   TYOP [0, 24, 24], TYOP [5, 1], TYOP [5, 27], TYOP [5, 28], TYOP [3, 29],
   TYOP [2, 0, 30], TYOP [0, 15, 31], TYOP [0, 8, 0], TYOP [0, 33, 0],
   TYOP [0, 15, 0], TYOP [0, 35, 0], TYOP [0, 21, 0], TYOP [0, 37, 0],
   TYOP [0, 31, 0], TYOP [0, 39, 0], TYOP [0, 10, 0], TYOP [0, 18, 0],
   TYOP [0, 42, 0], TYOP [0, 26, 0], TYOP [0, 44, 0], TYOP [0, 24, 0],
   TYOP [0, 46, 0], TYOP [0, 25, 0], TYOP [0, 48, 0], TYOP [0, 24, 26],
   TYOP [11, 8, 9], TYOP [0, 9, 51], TYOP [0, 8, 52], TYOP [11, 18, 32],
   TYOP [11, 9, 54], TYOP [11, 8, 55], TYOP [11, 8, 56], TYOP [0, 56, 57],
   TYOP [0, 8, 58], TYOP [0, 55, 56], TYOP [0, 8, 60], TYOP [0, 54, 55],
   TYOP [0, 9, 62], TYOP [0, 32, 54], TYOP [0, 18, 64], TYOP [11, 24, 24],
   TYOP [0, 24, 66], TYOP [0, 24, 67], TYOP [11, 25, 57], TYOP [0, 57, 69],
   TYOP [0, 25, 70], TYOP [0, 0, 0], TYOP [0, 0, 72], TYOP [0, 24, 46],
   TYOP [0, 8, 33], TYOP [0, 15, 35], TYOP [0, 21, 37], TYOP [0, 17, 0],
   TYOP [0, 17, 78], TYOP [0, 18, 42], TYOP [0, 69, 0], TYOP [0, 69, 81],
   TYOP [0, 25, 48], TYOP [0, 26, 24], TYOP [0, 66, 84], TYOP [0, 81, 81],
   TYOP [0, 24, 84], TYOP [0, 57, 0], TYOP [0, 56, 0], TYOP [0, 8, 89],
   TYOP [0, 90, 88], TYOP [0, 55, 0], TYOP [0, 8, 92], TYOP [0, 93, 89],
   TYOP [0, 54, 0], TYOP [0, 9, 95], TYOP [0, 96, 92], TYOP [0, 32, 0],
   TYOP [0, 18, 98], TYOP [0, 99, 95], TYOP [0, 25, 88], TYOP [0, 101, 81],
   TYOP [0, 15, 24], TYOP [0, 15, 103], TYOP [0, 18, 104],
   TYOP [0, 9, 105], TYOP [0, 9, 99], TYOP [0, 8, 107], TYOP [0, 8, 108],
   TYOP [0, 25, 109], TYOP [0, 32, 35], TYOP [0, 18, 111],
   TYOP [0, 9, 112], TYOP [0, 24, 106], TYOP [0, 24, 8], TYOP [0, 24, 15],
   TYOP [0, 24, 31], TYOP [0, 24, 17], TYOP [0, 51, 8], TYOP [0, 8, 25],
   TYOP [0, 8, 69], TYOP [0, 8, 121], TYOP [0, 69, 122], TYOP [0, 21, 24],
   TYOP [0, 31, 24], TYOP [0, 8, 15], TYOP [0, 8, 21], TYOP [0, 21, 8],
   TYOP [0, 15, 15], TYOP [0, 15, 129], TYOP [0, 8, 8], TYOP [0, 8, 131],
   TYOP [0, 8, 115], TYOP [0, 31, 31], TYOP [0, 31, 134]]
  end
  val _ = Theory.incorporate_consts "scratch" tyvector
     [("ph_page_unchanged", 16), ("ph_l1_unchanged", 20),
      ("l2_entry_unchanged", 23)];

  local open SharingTables
  in
  val tmvector = build_term_vector idvector tyvector
  [TMV("a", 8), TMV("a", 24), TMV("b", 24), TMV("c1", 25), TMV("c1'", 25),
   TMV("c2", 8), TMV("c2'", 8), TMV("c3", 8), TMV("c3'", 8),
   TMV("dest_page", 15), TMV("f", 26), TMV("f'", 26), TMV("idx", 21),
   TMV("idx", 24), TMV("k", 24), TMV("l2_base_add", 8), TMV("l2_idx", 21),
   TMV("l2_index", 8), TMV("mem", 9), TMV("mem'", 9), TMV("n", 24),
   TMV("pgrefs", 32), TMV("pgrefs'", 32), TMV("pgtype", 18),
   TMV("pgtype'", 18), TMV("ph_page", 15), TMV("ph_page'", 15),
   TMV("ptb", 15), TMV("va", 8), TMV("x", 31), TMV("x", 24), TMC(8, 34),
   TMC(8, 36), TMC(8, 38), TMC(8, 40), TMC(8, 41), TMC(8, 43), TMC(8, 45),
   TMC(8, 47), TMC(8, 49), TMC(9, 50), TMC(10, 53), TMC(10, 59),
   TMC(10, 61), TMC(10, 63), TMC(10, 65), TMC(10, 68), TMC(10, 71),
   TMC(12, 50), TMC(13, 73), TMC(14, 24), TMC(15, 74), TMC(16, 73),
   TMC(16, 75), TMC(16, 76), TMC(16, 77), TMC(16, 79), TMC(16, 80),
   TMC(16, 74), TMC(16, 82), TMC(16, 83), TMC(17, 73), TMC(18, 74),
   TMC(19, 26), TMC(20, 26), TMC(21, 85), TMC(22, 86), TMC(23, 26),
   TMC(24, 87), TMC(25, 91), TMC(25, 94), TMC(25, 97), TMC(25, 100),
   TMC(25, 102), TMC(26, 24), TMC(27, 106), TMC(28, 110), TMC(29, 113),
   TMC(30, 113), TMC(31, 110), TMC(32, 114), TMC(33, 23), TMC(34, 115),
   TMC(34, 116), TMC(34, 117), TMC(34, 118), TMC(35, 20), TMC(36, 16),
   TMC(37, 119), TMC(38, 120), TMC(39, 123), TMC(40, 124), TMC(40, 125),
   TMC(41, 126), TMC(41, 127), TMC(41, 128), TMC(42, 130), TMC(43, 132),
   TMC(43, 130), TMC(44, 133), TMC(45, 133), TMC(46, 132), TMC(47, 135),
   TMC(48, 72)]
  end
  local
  val DT = Thm.disk_thm val read = Term.read_raw tmvector
  in
  val op ph_page_unchanged_def =
    DT([],
       [read"(%32 (|%25. (%35 (|%18. (%35 (|%19. ((%52 (((%87 $2) $1) $0)) (%31 (|%0. ((%61 ((%49 ((%54 $3) (%93 ((%100 $0) (%67 (%64 (%63 (%64 %74)))))))) ((%53 $0) ((%97 $0) (%82 (%67 (%64 (%63 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 %74))))))))))))))))))))))))))))))))))))) ((%53 (%88 ((%41 $0) $2))) (%88 ((%41 $0) $1)))))))))))))"])
  val op ph_l1_unchanged_def =
    DT([],
       [read"(%32 (|%25. (%36 (|%23. (%35 (|%18. (%35 (|%19. ((%52 ((((%86 $3) $2) $1) $0)) ((%61 ((%49 ((%56 ($2 $3)) (%85 (%67 (%63 %74))))) ((%54 ((%98 $3) (%83 (%67 (%64 (%63 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 %74))))))))))))))))))))))) $3))) ((%49 (((%87 $3) $1) $0)) ((%49 (((%87 ((%96 $3) (%83 (%67 (%63 %74))))) $1) $0)) ((%49 (((%87 ((%96 $3) (%83 (%67 (%64 %74))))) $1) $0)) (((%87 ((%96 $3) (%83 (%67 (%63 (%63 %74)))))) $1) $0))))))))))))))"])
  val op l2_entry_unchanged_def =
    DT([],
       [read"(%31 (|%15. (%33 (|%16. (%35 (|%18. (%35 (|%19. ((%52 ((((%81 $3) $2) $1) $0)) ((%53 (%88 ((%41 ((%101 ((%97 $3) (%82 (%67 (%64 (%63 (%63 (%63 (%63 (%63 (%63 (%63 (%63 (%63 (%63 (%63 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 %74))))))))))))))))))))))))))))))))))) ((%99 (%95 $2)) (%67 (%64 %74))))) $1))) (%88 ((%41 ((%101 ((%97 $3) (%82 (%67 (%64 (%63 (%63 (%63 (%63 (%63 (%63 (%63 (%63 (%63 (%63 (%63 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 %74))))))))))))))))))))))))))))))))))) ((%99 (%95 $2)) (%67 (%64 %74))))) $0))))))))))))"])
  val op ref_inv_l2_thm =
    DT(["DISK_THM"],
       [read"(%35 (|%18. (%36 (|%23. (%32 (|%25. ((%61 ((((%78 $2) $1) %21) $0)) ((((%78 $2) $1) %22) $0))))))))"])
  val op ref_inv_l1_thm =
    DT(["DISK_THM"],
       [read"(%35 (|%18. (%36 (|%23. (%32 (|%25. ((%61 ((((%77 $2) $1) %21) $0)) ((((%77 $2) $1) %22) $0))))))))"])
  val op diff_pg_type_diff_pages_thm =
    DT(["DISK_THM"],
       [read"(%36 (|%23. (%32 (|%25. (%32 (|%26. ((%61 ((%56 ($2 $1)) (%85 (%67 (%64 %74))))) ((%61 ((%56 ($2 $0)) (%85 (%67 (%63 %74))))) (%103 ((%54 $1) $0))))))))))"])
  val op invariant_page_type_l2_unchanged_mem_thm =
    DT(["DISK_THM"],
       [read"(%36 (|%23. (%32 (|%25. ((%61 ((%56 ($1 $0)) (%85 (%67 (%64 %74))))) ((%61 ((((%78 %18) $1) %21) $0)) ((%61 (((%87 $0) %18) %19)) ((((%78 %19) $1) %21) $0))))))))"])
  val op invariant_page_type_l1_unchanged_mem_thm =
    DT(["DISK_THM"],
       [read"(%36 (|%23. ((%61 ((%56 ($0 %25)) (%85 (%67 (%63 %74))))) ((%61 ((%54 %25) ((%98 (%83 (%67 (%64 (%63 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 %74)))))))))))))))))))))) %25))) ((%61 ((((%77 %18) $0) %21) %25)) ((%61 (((%87 %25) %18) %19)) ((%61 (((%87 ((%96 %25) (%83 (%67 (%63 %74))))) %18) %19)) ((%61 (((%87 ((%96 %25) (%83 (%67 (%64 %74))))) %18) %19)) ((%61 (((%87 ((%96 %25) (%83 (%67 (%63 (%63 %74)))))) %18) %19)) ((((%77 %19) $0) %21) %25))))))))))"])
  val op sum_split_thm =
    DT(["DISK_THM"],
       [read"(%38 (|%30. (%38 (|%20. (%37 (|%10. ((%61 ((%51 $2) $1)) ((%58 ((%65 ((%46 %50) $1)) $0)) ((%40 ((%40 ((%65 ((%46 %50) $2)) $0)) ($0 $2))) ((%65 ((%46 ((%40 $2) (%67 (%63 %74)))) ((%48 ((%48 $1) $2)) (%67 (%63 %74))))) $0))))))))))"])
  val op sum_splitter_thm =
    DT(["DISK_THM"],
       [read"(%38 (|%13. (%38 (|%20. (%37 (|%10. (%37 (|%11. (%38 (|%1. (%38 (|%2. ((%61 ((%49 ((%58 ((%40 $1) ($3 $5))) ((%40 ($2 $5)) $0))) ((%49 (%38 (|%30. ((%61 (%103 ((%58 $0) $6))) ((%61 ((%51 $0) $5)) ((%58 ($4 $0)) ($3 $0))))))) ((%51 $5) $4)))) ((%58 ((%40 $1) ((%65 ((%46 %50) $4)) $3))) ((%40 ((%65 ((%46 %50) $4)) $2)) $0)))))))))))))))"])
  val op SUM_GT_thm =
    DT(["DISK_THM"],
       [read"(%37 (|%10. (%38 (|%20. (%38 (|%14. ((%61 ((%51 $0) $1)) ((%61 ((%62 ($2 $0)) %50)) ((%62 ((%68 $1) $2)) %50)))))))))"])
  val op ref_cnt_equality_thm =
    DT(["DISK_THM"],
       [read"(%32 (|%25. (%35 (|%18. (%35 (|%19. (%32 (|%9. ((%61 ((%49 ((%61 ((%56 (%23 $3)) (%85 (%67 (%64 %74))))) (((%87 $3) $2) $1))) ((%61 ((%49 ((%56 (%23 $3)) (%85 (%67 (%63 %74))))) ((%54 ((%98 $3) (%83 (%67 (%64 (%63 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 (%64 %74))))))))))))))))))))))) $3))) ((%49 (((%87 $3) $2) $1)) ((%49 (((%87 ((%96 $3) (%83 (%67 (%63 %74))))) $2) $1)) ((%49 (((%87 ((%96 $3) (%83 (%67 (%64 %74))))) $2) $1)) (((%87 ((%96 $3) (%83 (%67 (%63 (%63 %74)))))) $2) $1))))))) ((%58 ((((%75 $2) %23) $0) $3)) ((((%75 $1) %23) $0) $3)))))))))))"])
  val op ref_cnt_equality_l2_thm =
    DT(["DISK_THM"],
       [read"(%31 (|%15. (%33 (|%12. (%35 (|%18. (%35 (|%19. (%36 (|%23. (%32 (|%25. ((%61 ((%56 ($1 (%93 ((%100 $5) (%67 (%64 (%63 (%64 %74)))))))) (%85 (%67 (%64 %74))))) ((%61 ((((%81 $5) $4) $3) $2)) ((%58 (((((%80 (%91 $4)) $3) $1) $0) (%93 ((%100 $5) (%67 (%64 (%63 (%64 %74)))))))) (((((%80 (%91 $4)) $2) $1) $0) (%93 ((%100 $5) (%67 (%64 (%63 (%64 %74))))))))))))))))))))))"])
  val op l2_unmap_unchanged_pagetype_thm =
    DT(["DISK_THM"],
       [read"(%31 (|%15. ((%61 ((%59 ((%47 %4) ((%42 %6) ((%43 %8) ((%44 %19) ((%45 %24) %22)))))) (((%90 ((%47 (%89 (%82 (%67 (%63 %74))))) ((%42 %5) ((%43 %7) ((%44 %18) ((%45 %23) %21)))))) $0) %17))) ((%57 %24) %23))))"])
  val op l2_unmap_unchanged_mem_thm =
    DT(["DISK_THM"],
       [read"(%32 (|%27. (%31 (|%15. ((%61 ((%59 ((%47 %4) ((%42 %6) ((%43 %8) ((%44 %19) ((%45 %24) %22)))))) (((%90 ((%47 (%89 (%82 (%67 (%63 %74))))) ((%42 %5) ((%43 %7) ((%44 %18) ((%45 %23) %21)))))) $0) %17))) ((%61 (%103 ((%54 $1) (%93 ((%100 $0) (%67 (%64 (%63 (%64 %74))))))))) (((%87 $1) %18) %19)))))))"])
  val op l2_unmap_unchanged_l1_mem_thm =
    DT(["DISK_THM"],
       [read"(%32 (|%27. (%31 (|%15. ((%61 ((((((%76 (%89 (%82 (%67 (%63 %74))))) %5) %7) %18) %23) %21)) ((%61 ((%59 ((%47 %4) ((%42 %6) ((%43 %8) ((%44 %19) ((%45 %24) %22)))))) (((%90 ((%47 (%89 (%82 (%67 (%63 %74))))) ((%42 %5) ((%43 %7) ((%44 %18) ((%45 %23) %21)))))) $0) %17))) ((((%86 $1) %23) %18) %19)))))))"])
  val op l2_unmap_unchanged_l2_mem_thm =
    DT(["DISK_THM"],
       [read"(%33 (|%16. (%39 (|%3. (%36 (|%24. ((%61 ((%59 ((%47 $1) ((%42 %6) ((%43 %8) ((%44 %19) ((%45 $0) %22)))))) (((%90 ((%47 (%89 (%82 (%67 (%63 %74))))) ((%42 %5) ((%43 %7) ((%44 %18) ((%45 %23) %21)))))) %15) %17))) ((%61 (%103 ((%55 $2) (%94 ((%97 %17) (%82 (%67 (%63 (%63 (%63 (%63 (%63 (%63 (%63 (%63 (%63 (%63 %74))))))))))))))))) ((((%81 %15) $2) %18) %19)))))))))"])
  val op non_zero_no_overflow_thm =
    DT(["DISK_THM"],
       [read"(%34 (|%29. ((%61 ((%62 (%92 $0)) %50)) ((%58 (%92 ((%102 $0) (%84 (%67 (%63 %74)))))) ((%48 (%92 $0)) (%67 (%63 %74)))))))"])
  val op l2_unmap_thm =
    DT(["DISK_THM"],
       [read"(%39 (|%3. (%31 (|%28. ((%61 ((%60 $1) (%89 (%82 (%67 (%63 %74)))))) ((%61 ((((((%79 $1) %5) %7) %18) %23) %21)) ((%66 (%73 (|%4. (%69 (|%6. (%70 (|%8. (%71 (|%19. (%72 (|%24. (|%22. ((((((%79 $5) $4) $3) $2) $1) $0))))))))))))) (((%90 ((%47 $1) ((%42 %5) ((%43 %7) ((%44 %18) ((%45 %23) %21)))))) %15) %17))))))))"])
  end
  val _ = DB.bindl "scratch"
  [("ph_page_unchanged_def",ph_page_unchanged_def,DB.Def),
   ("ph_l1_unchanged_def",ph_l1_unchanged_def,DB.Def),
   ("l2_entry_unchanged_def",l2_entry_unchanged_def,DB.Def),
   ("ref_inv_l2_thm",ref_inv_l2_thm,DB.Thm),
   ("ref_inv_l1_thm",ref_inv_l1_thm,DB.Thm),
   ("diff_pg_type_diff_pages_thm",diff_pg_type_diff_pages_thm,DB.Thm),
   ("invariant_page_type_l2_unchanged_mem_thm",
    invariant_page_type_l2_unchanged_mem_thm,
    DB.Thm),
   ("invariant_page_type_l1_unchanged_mem_thm",
    invariant_page_type_l1_unchanged_mem_thm,
    DB.Thm), ("sum_split_thm",sum_split_thm,DB.Thm),
   ("sum_splitter_thm",sum_splitter_thm,DB.Thm),
   ("SUM_GT_thm",SUM_GT_thm,DB.Thm),
   ("ref_cnt_equality_thm",ref_cnt_equality_thm,DB.Thm),
   ("ref_cnt_equality_l2_thm",ref_cnt_equality_l2_thm,DB.Thm),
   ("l2_unmap_unchanged_pagetype_thm",
    l2_unmap_unchanged_pagetype_thm,
    DB.Thm),
   ("l2_unmap_unchanged_mem_thm",l2_unmap_unchanged_mem_thm,DB.Thm),
   ("l2_unmap_unchanged_l1_mem_thm",l2_unmap_unchanged_l1_mem_thm,DB.Thm),
   ("l2_unmap_unchanged_l2_mem_thm",l2_unmap_unchanged_l2_mem_thm,DB.Thm),
   ("non_zero_no_overflow_thm",non_zero_no_overflow_thm,DB.Thm),
   ("l2_unmap_thm",l2_unmap_thm,DB.Thm)]

  val _ = Theory.LoadableThyData.temp_encoded_update {
    thy = "scratch",
    thydataty = "compute",
    data =
        "scratch.ph_page_unchanged_def scratch.l2_entry_unchanged_def scratch.ph_l1_unchanged_def"
  }

val _ = if !Globals.print_thy_loads then print "done\n" else ()
val _ = Theory.load_complete "scratch"
end

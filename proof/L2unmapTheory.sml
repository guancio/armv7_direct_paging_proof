structure L2unmapTheory :> L2unmapTheory =
struct
  val _ = if !Globals.print_thy_loads then print "Loading L2unmapTheory ... " else ()
  open Type Term Thm
  infixr -->

  fun C s t ty = mk_thy_const{Name=s,Thy=t,Ty=ty}
  fun T s t A = mk_thy_type{Tyop=s, Thy=t,Args=A}
  fun V s q = mk_var(s,q)
  val U     = mk_vartype
  (*  Parents *)
  local open helperTheory
  in end;
  val _ = Theory.link_parents
          ("L2unmap",
          Arbnum.fromString "1387541223",
          Arbnum.fromString "465756")
          [("helper",
           Arbnum.fromString "1387541074",
           Arbnum.fromString "913350")];
  val _ = Theory.incorporate_types "L2unmap" [];

  val idvector = 
    let fun ID(thy,oth) = {Thy = thy, Other = oth}
    in Vector.fromList
  [ID("tiny", "sctlrT"), ID("fcp", "cart"), ID("fcp", "bit0"),
   ID("one", "one"), ID("min", "bool"), ID("fcp", "bit1"),
   ID("min", "fun"), ID("bool", "!"), ID("pair", ","), ID("pair", "prod"),
   ID("min", "="), ID("min", "==>"), ID("arithmetic", "BIT1"),
   ID("num", "num"), ID("arithmetic", "BIT2"), ID("bool", "LET"),
   ID("arithmetic", "NUMERAL"), ID("pair", "UNCURRY"),
   ID("arithmetic", "ZERO"), ID("hypervisor_model", "invariant_page_type"),
   ID("hypervisor_model", "invariant_ref_cnt"),
   ID("helper", "l2_entry_unchanged"), ID("words", "n2w"),
   ID("helper", "ph_l1_unchanged"), ID("helper", "ph_page_unchanged"),
   ID("tiny", "rec'sctlrT"),
   ID("hypervisor_model", "unmap_L2_pageTable_entry"), ID("words", "w2w"),
   ID("words", "word_and"), ID("words", "word_lsr"), ID("bool", "~")]
  end;
  local open SharingTables
  in
  val tyvector = build_type_vector idvector
  [TYOP [0], TYOP [3], TYOP [2, 1], TYOP [2, 2], TYOP [2, 3], TYOP [2, 4],
   TYOP [2, 5], TYOP [4], TYOP [1, 7, 6], TYOP [5, 2], TYOP [2, 9],
   TYOP [1, 7, 10], TYOP [1, 7, 4], TYOP [6, 8, 12], TYOP [5, 1],
   TYOP [5, 14], TYOP [5, 15], TYOP [2, 16], TYOP [1, 7, 17], TYOP [2, 10],
   TYOP [1, 7, 19], TYOP [6, 20, 18], TYOP [1, 7, 2], TYOP [6, 20, 22],
   TYOP [6, 8, 7], TYOP [6, 24, 7], TYOP [6, 20, 7], TYOP [6, 26, 7],
   TYOP [6, 11, 7], TYOP [6, 28, 7], TYOP [6, 23, 7], TYOP [6, 30, 7],
   TYOP [6, 0, 7], TYOP [6, 32, 7], TYOP [9, 23, 21], TYOP [9, 13, 34],
   TYOP [9, 8, 35], TYOP [9, 8, 36], TYOP [6, 36, 37], TYOP [6, 8, 38],
   TYOP [6, 35, 36], TYOP [6, 8, 40], TYOP [6, 34, 35], TYOP [6, 13, 42],
   TYOP [6, 21, 34], TYOP [6, 23, 44], TYOP [9, 0, 37], TYOP [6, 37, 46],
   TYOP [6, 0, 47], TYOP [6, 20, 26], TYOP [6, 11, 28], TYOP [6, 23, 30],
   TYOP [6, 46, 7], TYOP [6, 46, 52], TYOP [6, 0, 32], TYOP [6, 7, 7],
   TYOP [6, 7, 55], TYOP [13], TYOP [6, 57, 57], TYOP [6, 52, 52],
   TYOP [6, 37, 7], TYOP [6, 36, 7], TYOP [6, 8, 61], TYOP [6, 62, 60],
   TYOP [6, 35, 7], TYOP [6, 8, 64], TYOP [6, 65, 61], TYOP [6, 34, 7],
   TYOP [6, 13, 67], TYOP [6, 68, 64], TYOP [6, 21, 7], TYOP [6, 23, 70],
   TYOP [6, 71, 67], TYOP [6, 0, 60], TYOP [6, 73, 52], TYOP [6, 13, 71],
   TYOP [6, 8, 75], TYOP [6, 8, 76], TYOP [6, 0, 77], TYOP [6, 13, 7],
   TYOP [6, 13, 79], TYOP [6, 11, 80], TYOP [6, 8, 81], TYOP [6, 57, 8],
   TYOP [6, 23, 80], TYOP [6, 20, 84], TYOP [6, 20, 80], TYOP [6, 8, 0],
   TYOP [6, 8, 46], TYOP [6, 8, 88], TYOP [6, 46, 89], TYOP [6, 8, 20],
   TYOP [6, 8, 11], TYOP [6, 8, 8], TYOP [6, 8, 93], TYOP [6, 8, 83]]
  end
  val _ = Theory.incorporate_consts "L2unmap" tyvector [];

  local open SharingTables
  in
  val tmvector = build_term_vector idvector tyvector
  [TMV("c1", 0), TMV("c1'", 0), TMV("c2", 8), TMV("c2'", 8), TMV("c3", 8),
   TMV("c3'", 8), TMV("l2_base_add", 8), TMV("l2_idx", 11),
   TMV("l2_index", 8), TMV("mem", 13), TMV("mem'", 13), TMV("pgrefs", 21),
   TMV("pgrefs'", 21), TMV("pgtype", 23), TMV("pgtype'", 23),
   TMV("ptb", 20), TMV("va", 8), TMC(7, 25), TMC(7, 27), TMC(7, 29),
   TMC(7, 31), TMC(7, 33), TMC(8, 39), TMC(8, 41), TMC(8, 43), TMC(8, 45),
   TMC(8, 48), TMC(10, 49), TMC(10, 50), TMC(10, 51), TMC(10, 53),
   TMC(10, 54), TMC(11, 56), TMC(12, 58), TMC(14, 58), TMC(15, 59),
   TMC(16, 58), TMC(17, 63), TMC(17, 66), TMC(17, 69), TMC(17, 72),
   TMC(17, 74), TMC(18, 57), TMC(19, 78), TMC(20, 78), TMC(21, 82),
   TMC(22, 83), TMC(23, 85), TMC(24, 86), TMC(25, 87), TMC(26, 90),
   TMC(27, 91), TMC(27, 92), TMC(28, 94), TMC(29, 95), TMC(30, 55)]
  end
  local
  val DT = Thm.disk_thm val read = Term.read_raw tmvector
  in
  val op l2_unmap_unchanged_pagetype_thm =
    DT(["DISK_THM"],
       [read"(%17 (|%6. ((%32 ((%30 ((%26 %1) ((%22 %3) ((%23 %5) ((%24 %10) ((%25 %14) %12)))))) (((%50 ((%26 (%49 (%46 (%36 (%33 %42))))) ((%22 %2) ((%23 %4) ((%24 %9) ((%25 %13) %11)))))) $0) %8))) ((%29 %14) %13))))"])
  val op l2_unmap_unchanged_mem_thm =
    DT(["DISK_THM"],
       [read"(%18 (|%15. (%17 (|%6. ((%32 ((%30 ((%26 %1) ((%22 %3) ((%23 %5) ((%24 %10) ((%25 %14) %12)))))) (((%50 ((%26 (%49 (%46 (%36 (%33 %42))))) ((%22 %2) ((%23 %4) ((%24 %9) ((%25 %13) %11)))))) $0) %8))) ((%32 (%55 ((%27 $1) (%51 ((%54 $0) (%36 (%34 (%33 (%34 %42))))))))) (((%48 $1) %9) %10)))))))"])
  val op l2_unmap_unchanged_l1_mem_thm =
    DT(["DISK_THM"],
       [read"(%18 (|%15. (%17 (|%6. ((%32 ((((((%43 (%49 (%46 (%36 (%33 %42))))) %2) %4) %9) %13) %11)) ((%32 ((%30 ((%26 %1) ((%22 %3) ((%23 %5) ((%24 %10) ((%25 %14) %12)))))) (((%50 ((%26 (%49 (%46 (%36 (%33 %42))))) ((%22 %2) ((%23 %4) ((%24 %9) ((%25 %13) %11)))))) $0) %8))) ((((%47 $1) %13) %9) %10)))))))"])
  val op l2_unmap_unchanged_l2_mem_thm =
    DT(["DISK_THM"],
       [read"(%19 (|%7. (%21 (|%0. (%20 (|%14. ((%32 ((%30 ((%26 $1) ((%22 %3) ((%23 %5) ((%24 %10) ((%25 $0) %12)))))) (((%50 ((%26 (%49 (%46 (%36 (%33 %42))))) ((%22 %2) ((%23 %4) ((%24 %9) ((%25 %13) %11)))))) %6) %8))) ((%32 (%55 ((%28 $2) (%52 ((%53 %8) (%46 (%36 (%33 (%33 (%33 (%33 (%33 (%33 (%33 (%33 (%33 (%33 %42))))))))))))))))) ((((%45 %6) $2) %9) %10)))))))))"])
  val op l2_unmap_thm =
    DT(["DISK_THM"],
       [read"(%21 (|%0. (%17 (|%16. ((%32 ((%31 $1) (%49 (%46 (%36 (%33 %42)))))) ((%32 ((((((%44 $1) %2) %4) %9) %13) %11)) ((%35 (%41 (|%1. (%37 (|%3. (%38 (|%5. (%39 (|%10. (%40 (|%14. (|%12. ((((((%44 $5) $4) $3) $2) $1) $0))))))))))))) (((%50 ((%26 $1) ((%22 %2) ((%23 %4) ((%24 %9) ((%25 %13) %11)))))) %6) %8))))))))"])
  end
  val _ = DB.bindl "L2unmap"
  [("l2_unmap_unchanged_pagetype_thm",
    l2_unmap_unchanged_pagetype_thm,
    DB.Thm),
   ("l2_unmap_unchanged_mem_thm",l2_unmap_unchanged_mem_thm,DB.Thm),
   ("l2_unmap_unchanged_l1_mem_thm",l2_unmap_unchanged_l1_mem_thm,DB.Thm),
   ("l2_unmap_unchanged_l2_mem_thm",l2_unmap_unchanged_l2_mem_thm,DB.Thm),
   ("l2_unmap_thm",l2_unmap_thm,DB.Thm)]

  local open Portable GrammarSpecials Parse
    fun UTOFF f = Feedback.trace("Parse.unicode_trace_off_complaints",0)f
  in
  val _ = mk_local_grms [("helperTheory.helper_grammars",
                          helperTheory.helper_grammars)]
  val _ = List.app (update_grms reveal) []

  val L2unmap_grammars = Parse.current_lgrms()
  end

val _ = if !Globals.print_thy_loads then print "done\n" else ()
val _ = Theory.load_complete "L2unmap"
end

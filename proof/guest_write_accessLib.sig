signature guest_write_accessLib =
sig
  val INSTANTIATE_INVARIANT_CURRENT_L1_TAC  : Abbrev.tactic
  val INSTANTIATE_INVARIANT_L2_TAC : Abbrev.term -> Abbrev.tactic
  val INSTANTIATE_INVARIANT_CURRENT_L1_FOR_ADD_TAC : Abbrev.term -> Abbrev.tactic
  val INSTANTIATE_INVARIANT_L2_FOR_ADD_TAC : Abbrev.term -> Abbrev.term ->
					     Abbrev.tactic
  val INVPT_KEEP_TAC : Abbrev.tactic -> Abbrev.tactic
  val NO_INVPT_KEEP_TAC : Abbrev.tactic -> Abbrev.tactic
end

(* 
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/spec/" :: !loadPath;
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/" ::  !loadPath;
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/proof/" :: !loadPath;
load "mmu_utilsLib";
load "hypervisor_modelTheory";
load "mmu_propertiesTheory";
load "helperTheory";
*)



open HolKernel boolLib bossLib Parse wordsLib;
open hypervisor_modelTheory;
open mmu_propertiesTheory;
open tinyTheory;
open helperTheory;
open guest_write_access_utilsTheory;
open guest_write_accessTheory;

load "mmu_utilsLib";
open mmu_utilsLib;

load "guest_write_accessLib";
open guest_write_accessLib;

val _ = new_theory "mmu_safe";

val all_bytes_in_l1_type_1_goal = ``
! tbl_base:word32.
    (tbl_base = tbl_base && 0xFFFFC000w) ==>
    (pgtype ((w2w ((tbl_base ) >>> 12)):bool[20]) = 0b01w:word2) ==>
    (pgtype (((w2w ((tbl_base ) >>> 12)):bool[20]) !! (0b01w:bool[20])) = 0b01w) ==>
    (pgtype (((w2w ((tbl_base ) >>> 12)):bool[20]) !! (0b10w:bool[20])) = 0b01w) ==>
    (pgtype (((w2w ((tbl_base ) >>> 12)):bool[20]) !! (0b11w:bool[20])) = 0b01w) ==>
     (!va:bool[32].
      (!byte_idx:bool[2].
     	  ((pgtype ((w2w(((tbl_base !! ((va >>> 20) << 2)) +  (w2w byte_idx):word32) >>> 12)):bool[20])) = 0b01w)
      )
     )
``;

val tbl_entry_add = ``
   (w2w
     (((w2w (byte_idx :word2) :word32) +
       ((tbl_base :word32) ‖
        (va :word32) ⋙ (20 :num) ≪ (2 :num))) ⋙
      (12 :num)) :word20)``;

val all_bytes_in_l1_type_1_thm = Q.store_thm(
   "all_bytes_in_l1_type_1_thm",
   `^all_bytes_in_l1_type_1_goal`,
        (RW_TAC (srw_ss()) [])
   THEN (ASSUME_TAC (UNDISCH (
blastLib.BBLAST_PROVE(``
  (tbl_base = 0xFFFFC000w && tbl_base) ==> (
  (^tbl_entry_add = (w2w (tbl_base ⋙ 12))) \/
  (^tbl_entry_add = ((w2w (tbl_base ⋙ 12)) !! 1w)) \/
  (^tbl_entry_add = ((w2w (tbl_base ⋙ 12)) !! 2w)) \/
  (^tbl_entry_add = ((w2w (tbl_base ⋙ 12)) !! 3w))
)
``)
	)))
   THEN (FULL_SIMP_TAC (srw_ss()) [])
);

val all_bytes_in_l2_type_2_goal = ``
! l2_base:bool[22].
    (pgtype (w2w (l2_base ⋙ 2)) = 2w:word2) ==>
     (!va:bool[32].
      (!byte_idx:bool[2].
     	  (pgtype (
	   w2w(
	   (((w2w l2_base):bool[32] ≪ 10 ‖ 1020w && va ⋙ 12 ≪ 2) +
			 w2w byte_idx) >>> 12):bool[20]) = 2w)
      )
     )
``;

val all_bytes_in_l2_type_2_thm = Q.store_thm(
   "all_bytes_in_l2_type_2_thm",
   `^all_bytes_in_l2_type_2_goal`,
        (RW_TAC (srw_ss()) [])
   THEN (ASSUME_TAC (
blastLib.BBLAST_PROVE(``
(w2w
     (((w2w (byte_idx :word2) :word32) +
       ((w2w (l2_base :22 word) :word32) ≪ (10 :num) ‖
        (1020w :word32) && (va :word32) ⋙ (12 :num) ≪ (2 :num))) ⋙ (12 :
      num)) :word20)
=
(w2w ((l2_base :22 word) ⋙ (2 :num)) :word20)
``)
	))
   THEN (FULL_SIMP_TAC (srw_ss()) [])
);




val non_writable_mem_eq_thm = prove(``!add'.
   (mmu_is_reachable_memory (sctlrT 0w T) c2 (c3:bool[32]) mem F mem')
    ==>
    (let (und,rd,wt,ex) =
             mmu_phi_byte (sctlrT 0w T) c2 c3 mem F add'
       in
         ¬wt)
    ==>
    (mem add' = mem' add')
``,
         (REPEAT STRIP_TAC)
    THEN (FULL_SIMP_TAC (srw_ss()) [mmu_is_reachable_memory_def])
    THEN (SPEC_ASSUMPTION_TAC ``!ph_add:bool[32].p`` ``add':bool[32]``)
    THEN (Cases_on `mem add' <> mem' add'`)
    THENL [
           RES_TAC
      THEN (Cases_on `mmu_phi_byte (sctlrT 0w T) c2 c3 mem F add'`)
      THEN (Cases_on `r`)
      THEN (Cases_on `r'`)
      THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF]),
	   (FULL_SIMP_TAC (srw_ss()) [])
    ]
);




fun mmu_safe_pt () =
    (FULL_SIMP_TAC (srw_ss()) [mmu_byte_pt_def])
    THEN (Cases_on `(rec'l1PTT l1_desc).pxn`)
    THENL [
       (FULL_SIMP_TAC (srw_ss()) []),
       (FULL_SIMP_TAC (srw_ss()) [])
       THEN (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def])
(*
we need to show that the word stored at (the entry in the L2)
w2w (rec'l1PTT l1_desc).addr ≪ 10 ‖ 1020w && add' ⋙ 12 ≪ 2
has not been changed.
to do this we:
1) show that the pointed section has type 2
2) show that the four bytes of the read word have type 2
3) this means that  mmu_phi_byte (sctlrT 0w T) c2 c3 mem F of the
   four address is false
4) since  mmu_phi_byte do no allow to write, then the two memory are
   the same
5) then the memory word read is the same
*)
(* POINT 1 *)
       THEN (INSTANTIATE_INVARIANT_CURRENT_L1_FOR_ADD_TAC ``add':bool[32]``)
       THEN (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_index_def])
       THEN REABBREV_TAC
       THEN (REPEAT (LET_HYP_ELIM_TAC []))
       THEN (FULL_SIMP_BY_ASSUMPTION_TAC ``mmu_l1_decode_type l1_desc = 1w``)
(* POINT 2 *)
       THEN (ASSUME_TAC (
         SPEC ``add':bool[32]`` (
         UNDISCH_ALL (
         SPECL [``(rec'l1PTT l1_desc).addr``] all_bytes_in_l2_type_2_thm
             ))))
(* page type of each byte *)
       THEN (MAP_EVERY (fn off =>
	   THM_KEEP_TAC ``!byte_idx:bool[2].pgtype(x) = 2w``
             (SPEC_ASSUMPTION_TAC ``!byte_idx:bool[2].p`` off))
		   [``0w:bool[2]``, ``1w:bool[2]``, ``2w:bool[2]``,
		    ``3w:bool[2]``])
       THEN (SPEC_ASSUMPTION_TAC ``!byte_idx:bool[2].p`` ``3w:bool[2]``)
       THEN (Q.ABBREV_TAC `l2_add = w2w (rec'l1PTT l1_desc).addr ≪ 10 ‖ 1020w && add' ⋙ 12 ≪ 2`)
       THEN (FULL_SIMP_TAC (srw_ss()) [])
(* POINT 3 *)
(* since the page type is 2 it is not 0 *)
       THEN (MAP_EVERY (fn off =>
	`pgtype (w2w ((l2_add + ^off) ⋙ 12)) <> 0w` by 
	 FULL_SIMP_TAC (srw_ss()) [])
	    [``0w:bool[32]``, ``1w:bool[32]``, ``2w:bool[32]``,
	     ``3w:bool[32]``]
	)
       THEN (FULL_SIMP_TAC (srw_ss()) [])
       THEN (MAP_EVERY (fn off =>
          ASSUME_TAC (UNDISCH_ALL (
		      SIMP_RULE (srw_ss()) [rec'sctlrT_def] (
		      SPECL [``(sctlrT 0w T)``,
			     ``(l2_add:bool[32])+^off``,
			     ``(w2w (((l2_add:bool[32])+^off) ⋙ 12)):bool[20]``]
			    guest_write_access_thm
		     ))))
	    [``0w:bool[32]``, ``1w:bool[32]``, ``2w:bool[32]``,
	     ``3w:bool[32]``]
	)
(* POINT 4 *)
       THEN (MAP_EVERY (fn off =>
         ASSUME_TAC (
         UNDISCH_ALL (
         SIMP_RULE (srw_ss()) [] (
          SPEC ``l2_add + ^off`` non_writable_mem_eq_thm
        ))))
        [``0w:bool[32]``, ``1w:bool[32]``, ``2w:bool[32]``,
	 ``3w:bool[32]``]
	 )
(* POINT 5 *)
   THEN ASSUME_TAC (
	UNDISCH_ALL (
	SPEC ``l2_add:bool[32]`` read_mem_eq_thm))
   THEN (Q.ABBREV_TAC `l2_desc = read_mem32 (l2_add,mem)`)
   THEN (PAT_ASSUM ``l2_desc = read_mem32 (l2_add,mem')``
		   (fn thm =>
		       ASSUME_TAC (SYM thm)))
   THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
    ]
;

val mmu_safe_goal = ``
     !c1.
     (c1 = rec'sctlrT 1w ) ==>
     (invariant_ref_cnt c1 c2 c3 mem pgtype pgrefs) ==>
	  (mmu_safe c1 c2 c3 mem F)
``;


val mmu_safe_thm = Q.store_thm(
   "mmu_safe_thm",
   `^mmu_safe_goal`,
        (REPEAT STRIP_TAC)
   THEN (FULL_SIMP_TAC (srw_ss()) [mmu_safe_def])
   THEN GEN_TAC
   THEN (REPEAT STRIP_TAC)
   THEN (FULL_SIMP_TAC (srw_ss()) [mmu_mem_equiv_def,mmu_byte_def])
   THEN (REPEAT STRIP_TAC)
   THEN (FULL_SIMP_TAC (srw_ss()) [rec'sctlrT_def])
(*
we need to show that the word stored at (the entry in the L1)
mmu_tbl_base_addr c2 ‖ mmu_tbl_index add'
has not been changed.
to do this we:
1) show that the four pages of the current L1 have type 1
2) show that the four bytes of the read word have type 1
3) this means that  mmu_phi_byte (sctlrT 0w T) c2 c3 mem F of the
   four address is false
4) since  mmu_phi_byte do no allow to write, then the two memory are
   the same
5) then the memory word read is the same
*) 
(* POINT 1 *)
   THEN (FULL_SIMP_BY_ASSUMPTION_TAC ``c1 = sctlrT 0w T``)
   THEN (THM_KEEP_TAC ``invariant_ref_cnt (sctlrT 0w T) c2 c3 mem pgtype pgrefs``
		      (FULL_SIMP_TAC (srw_ss()) [invariant_ref_cnt_def]))
   THEN INSTANTIATE_INVARIANT_CURRENT_L1_TAC
(* POINT 2 *)
   THEN (ASSUME_TAC (
	 SPEC ``add':bool[32]`` (
	 UNDISCH_ALL (
	 SPECL [``mmu_tbl_base_addr c2``] all_bytes_in_l1_type_1_thm
	))))
   THEN (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_index_def])
   THEN (Q.ABBREV_TAC `l1_entry_add = mmu_tbl_base_addr c2 ‖ add' ⋙ 20 ≪ 2`)
(* page type of each byte *)
   THEN (MAP_EVERY (fn off =>
	   THM_KEEP_TAC ``!byte_idx:bool[2].p``
             (SPEC_ASSUMPTION_TAC ``!byte_idx:bool[2].p`` off))
		   [``0w:bool[2]``, ``1w:bool[2]``, ``2w:bool[2]``,
		    ``3w:bool[2]``])
   THEN (FULL_SIMP_TAC (srw_ss()) [])
(* POINT 3 *)
(* since the page type is 1 it is not 0 *)
   THEN (MAP_EVERY (fn off =>
	`pgtype (w2w (((l1_entry_add:bool[32])+(^off)) ⋙
           12):bool[20]) :bool[2] <> 0w` by 
	 FULL_SIMP_TAC (srw_ss()) [])
	    [``0w:bool[32]``, ``1w:bool[32]``, ``2w:bool[32]``,
	     ``3w:bool[32]``]
	)
   THEN (FULL_SIMP_TAC (srw_ss()) [])
   THEN (MAP_EVERY (fn off =>
          ASSUME_TAC (UNDISCH_ALL (
		      SIMP_RULE (srw_ss()) [rec'sctlrT_def] (
		      SPECL [``(sctlrT 0w T)``,
			     ``(l1_entry_add:bool[32])+^off``,
			     ``(w2w (((l1_entry_add:bool[32])+^off) ⋙ 12)):bool[20]``]
			    guest_write_access_thm
		     ))))
	    [``0w:bool[32]``, ``1w:bool[32]``, ``2w:bool[32]``,
	     ``3w:bool[32]``]
	)
(* POINT 4 *)
   THEN (MAP_EVERY (fn off =>
     ASSUME_TAC (
       UNDISCH_ALL (
       SIMP_RULE (srw_ss()) [] (
       SPEC ``l1_entry_add + ^off`` non_writable_mem_eq_thm
     ))))
     [``0w:bool[32]``, ``1w:bool[32]``, ``2w:bool[32]``,
      ``3w:bool[32]``]
	)
(* POINT 5 *)
   THEN ASSUME_TAC (
	UNDISCH_ALL (
	SPEC ``l1_entry_add:bool[32]`` read_mem_eq_thm))
   THEN (Q.ABBREV_TAC `l1_desc = read_mem32 (l1_entry_add,mem)`)
   THEN (PAT_ASSUM ``l1_desc = read_mem32 (l1_entry_add,mem')``
		   (fn thm =>
		       ASSUME_TAC (SYM thm)))
   THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
   THEN (Q.ABBREV_TAC `l1_type =  mmu_l1_decode_type l1_desc`)
   THEN (Cases_on `l1_type=0w`)
   THENL [
      (* UNMAPPED *)
      (FULL_SIMP_TAC (srw_ss()) []),
      (Cases_on `l1_type=3w`)
      THENL [
      (* UNMAPPED *)
        (FULL_SIMP_TAC (srw_ss()) []),
	(Cases_on `l1_type=2w`)
	THENL [
	(* SECTION OR SUPERSECTION *)
  	       (FULL_SIMP_TAC (srw_ss()) [])
          THEN (Q.ABBREV_TAC `l1_bit_18 = 1w && l1_desc ⋙ 18`)
	  THEN (Cases_on `l1_bit_18 = 0w`)
	  THENL [
               (FULL_SIMP_TAC (srw_ss()) []),
	       ASSUME_TAC (
	       SPEC ``l1_desc:bool[32]  ⋙ 18`` mmu_l1_18bit_values_thm)
	       THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `l1_bit_18`])
	       THEN (METIS_TAC [])
	  ],
	(* PT *)
               (FULL_SIMP_TAC (srw_ss()) [Abbr `l1_type`])
	  THEN (ASSUME_TAC (SPEC ``l1_desc:bool[32]``  mmu_l1_decode_type_values_thm))
	  THEN (`(mmu_l1_decode_type l1_desc = 1w)` by
   	         (METIS_TAC [])
	       )
          THEN (FULL_SIMP_TAC (srw_ss()) [])
	  THEN (mmu_safe_pt())
	]
      ]
   ]
);

val _ = export_theory ();

structure hypSwitchTheory :> hypSwitchTheory =
struct
  val _ = if !Globals.print_thy_loads then print "Loading hypSwitchTheory ... " else ()
  open Type Term Thm
  infixr -->

  fun C s t ty = mk_thy_const{Name=s,Thy=t,Ty=ty}
  fun T s t A = mk_thy_type{Tyop=s, Thy=t,Args=A}
  fun V s q = mk_var(s,q)
  val U     = mk_vartype
  (*  Parents *)
  local open guest_write_access_utilsTheory
  in end;
  val _ = Theory.link_parents
          ("hypSwitch",
          Arbnum.fromString "1387541315",
          Arbnum.fromString "430640")
          [("guest_write_access_utils",
           Arbnum.fromString "1387541258",
           Arbnum.fromString "319939")];
  val _ = Theory.incorporate_types "hypSwitch" [];

  val idvector = 
    let fun ID(thy,oth) = {Thy = thy, Other = oth}
    in Vector.fromList
  []
  end;
  local open SharingTables
  in
  val tyvector = build_type_vector idvector
  []
  end
  val _ = Theory.incorporate_consts "hypSwitch" tyvector [];

  local open SharingTables
  in
  val tmvector = build_term_vector idvector tyvector
  []
  end

  val _ = DB.bindl "hypSwitch" []

  local open Portable GrammarSpecials Parse
    fun UTOFF f = Feedback.trace("Parse.unicode_trace_off_complaints",0)f
  in
  val _ = mk_local_grms [("guest_write_access_utilsTheory.guest_write_access_utils_grammars",
                          guest_write_access_utilsTheory.guest_write_access_utils_grammars)]
  val _ = List.app (update_grms reveal) []

  val hypSwitch_grammars = Parse.current_lgrms()
  end

val _ = if !Globals.print_thy_loads then print "done\n" else ()
val _ = Theory.load_complete "hypSwitch"
end

open HolKernel boolLib bossLib Parse wordsLib;
open hypervisor_modelTheory;
open tinyTheory;
open helperTheory;

load "mmu_utilsLib";
open mmu_utilsLib;

val _ = new_theory "guest_write_access_utils";

val currentl1_dom_thm = Q.store_thm(
  "currentl1_dom_thm",
  `
  invariant_page_type (sctlrT 0w T) c2 c3 mem pgtype pgrefs ==>
  (∀dom.
        (mmu_domain_status (c3,dom) = 0w) ∨
        (mmu_domain_status (c3,dom) = 1w))
  `, (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def]));

(* The current page table address *)
val l1pt_add32 = ``c2 && 0xFFFFC000w:word32``;

val currentl1_tbl_thm = Q.store_thm(
  "currentl1_tbl_thm",
  `
  invariant_page_type (sctlrT 0w T) c2 c3 mem pgtype pgrefs ==> (
  ((mmu_tbl_base_addr c2) = (mmu_tbl_base_addr c2) && 0xFFFFC000w) ∧
  (pgtype (w2w ((mmu_tbl_base_addr c2) ⋙ 12)) = 1w) ∧
  (pgtype ((w2w ((mmu_tbl_base_addr c2) ⋙ 12)) ‖ 1w) = 1w) ∧
  (pgtype ((w2w ((mmu_tbl_base_addr c2) ⋙ 12)) ‖ 2w) = 1w) ∧
  (pgtype ((w2w ((mmu_tbl_base_addr c2) ⋙ 12)) ‖ 3w) = 1w) ∧
  (invariant_page_type_l1 mem pgtype pgrefs (w2w
  ((mmu_tbl_base_addr c2) ⋙ 12))))
  ` , STRIP_TAC
       THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def,
	    mmu_tbl_base_addr_def])
       THEN (LET_HYP_ELIM_TAC [])
       THEN (FULL_SIMP_TAC (srw_ss()) [])
       THEN (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_base_addr_def])
       THEN (SPEC_ASSUMPTION_TAC ``!x:word20. p``
		       ``(w2w((^l1pt_add32) ⋙ 12)):word20``)
       THEN (FULL_SIMP_BY_ASSUMPTION_TAC ``pgtype (w2w (0xFFFFCw && c2
  ⋙ 12)) = 1w``)
       THEN (FULL_SIMP_BY_BLAST_TAC ``
     ((w2w ((0xFFFFCw :word32) && (c2 :word32) ⋙ (12 :num)) :word20) =
      (0xFFFFCw :word20) &&
        (w2w ((0xFFFFCw :word32) && c2 ⋙ (12 :num)) :word20))
          ``)
);

val l2_tbl_thm = Q.store_thm(
  "l2_tbl_thm",
  `
  !ph_page.
  invariant_page_type (sctlrT 0w T) c2 c3 mem pgtype pgrefs ==>
  (pgtype ph_page = 2w) ==>
  invariant_page_type_l2 mem pgtype pgrefs ph_page
`,
       GEN_TAC
  THEN (REPEAT STRIP_TAC)
  THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def])
  THEN (LET_HYP_ELIM_TAC [])
  THEN (FULL_SIMP_TAC (srw_ss()) [])
);



(* used for differen definitions in L1 *)
val l1_desc_idx_thm = Q.store_thm(
  "l1_desc_idx_thm",
  `
    !add'. (w2w (w2w (mmu_tbl_index (add' :word32)) :word12) :word32) =
	       (mmu_tbl_index (add' :word32))`,
         GEN_TAC
    THEN FULL_SIMP_TAC (srw_ss()) [mmu_tbl_index_def,
	 		     blastLib.BBLAST_PROVE ``
		   (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32) =
	       ((add' :word32) ⋙ (20 :num))``
                   ]
	);
val l1_page_add_thm = Q.store_thm(
  "l1_page_add_thm",
  `
       ((w2w ((0xFFFFCw :word20) &&
                 (w2w (mmu_tbl_base_addr (c2 :word32) ⋙ (12 :num)) :
                    word20)) :word32) ≪ (12 :num)) =
	       (mmu_tbl_base_addr (c2 :word32))`,
		   FULL_SIMP_TAC (srw_ss()) [mmu_tbl_base_addr_def,
					     blastLib.BBLAST_PROVE ``((w2w ((0xFFFFCw :word20) &&
                 (w2w ((0xFFFFCw :word32) && (c2 :word32) ⋙ (12 :num)) :
                    word20)) :word32) ≪ (12 :num)) =
	       (c2 && (0xFFFFC000w :word32))``
                   ]
	);

val l2_desc_idx_goal = ``
((w2w (w2w ((l2_base :22 word) ⋙ (2 :num)) :word20) :
                   word32) ≪ (12 :num) ‖
                (w2w
                   ((w2w ((l2_base && (3w :22 word)) ≪ (8 :num)) :
                       word10) ‖
                    (w2w
                       (((add' :word32) && (0xFF000w :word32)) ⋙ (12 :
                        num)) :word10)) :word32) ≪ (2 :num) ) =
((w2w (l2_base :22 word) :word32) ≪ (10 :num) ‖
          (1020w :word32) && (add' :word32) ⋙ (12 :num) ≪ (2 :num))
    ``;
val l2_desc_idx_thm = Q.store_thm(
  "l2_desc_idx_thm",
  `^l2_desc_idx_goal`,
  FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE l2_desc_idx_goal]);

val _ = export_theory ();

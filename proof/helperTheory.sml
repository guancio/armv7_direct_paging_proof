structure helperTheory :> helperTheory =
struct
  val _ = if !Globals.print_thy_loads then print "Loading helperTheory ... " else ()
  open Type Term Thm
  infixr -->

  fun C s t ty = mk_thy_const{Name=s,Thy=t,Ty=ty}
  fun T s t A = mk_thy_type{Tyop=s, Thy=t,Args=A}
  fun V s q = mk_var(s,q)
  val U     = mk_vartype
  (*  Parents *)
  local open blastTheory hypervisor_modelTheory
  in end;
  val _ = Theory.link_parents
          ("helper",
          Arbnum.fromString "1387541074",
          Arbnum.fromString "913350")
          [("hypervisor_model",
           Arbnum.fromString "1387536226",
           Arbnum.fromString "425674"),
           ("blast",
           Arbnum.fromString "1384167954",
           Arbnum.fromString "990749")];
  val _ = Theory.incorporate_types "helper" [];

  val idvector = 
    let fun ID(thy,oth) = {Thy = thy, Other = oth}
    in Vector.fromList
  [ID("min", "fun"), ID("min", "bool"), ID("fcp", "cart"),
   ID("fcp", "bit0"), ID("fcp", "bit1"), ID("one", "one"),
   ID("num", "num"), ID("bool", "!"), ID("arithmetic", "+"),
   ID("pair", ","), ID("pair", "prod"), ID("arithmetic", "-"),
   ID("bool", "/\\"), ID("num", "0"), ID("prim_rec", "<"),
   ID("arithmetic", "<="), ID("min", "="), ID("min", "==>"),
   ID("arithmetic", ">"), ID("arithmetic", "BIT1"),
   ID("arithmetic", "BIT2"), ID("sum_num", "GSUM"),
   ID("arithmetic", "MOD"), ID("arithmetic", "NUMERAL"),
   ID("sum_num", "SUM"), ID("arithmetic", "ZERO"), ID("bool", "\\/"),
   ID("hypervisor_model", "count_pages"),
   ID("hypervisor_model", "count_pages_for_pt"), ID("words", "dimword"),
   ID("bool", "itself"), ID("helper", "inv_counter_part"),
   ID("hypervisor_model", "invariant_page_type_l1"),
   ID("hypervisor_model", "invariant_page_type_l2"),
   ID("hypervisor_model", "l2_count"), ID("helper", "l2_entry_unchanged"),
   ID("tiny", "mmu_l1_decode_type"), ID("words", "n2w"),
   ID("helper", "ph_l1_unchanged"), ID("helper", "ph_page_unchanged"),
   ID("tiny", "read_mem32"), ID("helper", "sound_type_change"),
   ID("bool", "the_value"), ID("words", "w2n"), ID("words", "w2w"),
   ID("words", "word_add"), ID("words", "word_and"),
   ID("words", "word_hi"), ID("words", "word_hs"), ID("words", "word_lo"),
   ID("words", "word_ls"), ID("words", "word_lsl"),
   ID("words", "word_lsr"), ID("words", "word_or"),
   ID("words", "word_sub"), ID("mmu_properties", "write_mem32"),
   ID("bool", "~")]
  end;
  local open SharingTables
  in
  val tyvector = build_type_vector idvector
  [TYOP [1], TYOP [5], TYOP [4, 1], TYOP [4, 2], TYOP [4, 3], TYOP [3, 4],
   TYOP [2, 0, 5], TYOP [3, 1], TYOP [4, 7], TYOP [3, 8], TYOP [3, 9],
   TYOP [2, 0, 10], TYOP [0, 11, 6], TYOP [0, 12, 0], TYOP [2, 0, 7],
   TYOP [0, 11, 14], TYOP [0, 15, 13], TYOP [0, 15, 16], TYOP [3, 7],
   TYOP [3, 18], TYOP [2, 0, 19], TYOP [3, 19], TYOP [3, 21],
   TYOP [2, 0, 22], TYOP [0, 23, 20], TYOP [0, 24, 0], TYOP [0, 24, 25],
   TYOP [0, 11, 26], TYOP [0, 15, 26], TYOP [0, 11, 28], TYOP [2, 0, 9],
   TYOP [0, 30, 26], TYOP [0, 23, 31], TYOP [0, 15, 0], TYOP [0, 24, 33],
   TYOP [0, 12, 34], TYOP [6], TYOP [0, 36, 36], TYOP [0, 23, 0],
   TYOP [0, 38, 0], TYOP [0, 11, 0], TYOP [0, 40, 0], TYOP [0, 30, 0],
   TYOP [0, 42, 0], TYOP [0, 6, 0], TYOP [0, 44, 0], TYOP [0, 25, 0],
   TYOP [0, 13, 0], TYOP [0, 33, 0], TYOP [0, 37, 0], TYOP [0, 49, 0],
   TYOP [0, 36, 0], TYOP [0, 51, 0], TYOP [0, 36, 37], TYOP [10, 23, 24],
   TYOP [0, 24, 54], TYOP [0, 23, 55], TYOP [10, 24, 23],
   TYOP [10, 23, 57], TYOP [0, 57, 58], TYOP [0, 23, 59], TYOP [0, 23, 57],
   TYOP [0, 24, 61], TYOP [10, 36, 36], TYOP [0, 36, 63], TYOP [0, 36, 64],
   TYOP [0, 0, 0], TYOP [0, 0, 66], TYOP [0, 36, 51], TYOP [0, 23, 38],
   TYOP [0, 20, 0], TYOP [0, 20, 70], TYOP [0, 11, 40], TYOP [0, 30, 42],
   TYOP [0, 6, 44], TYOP [0, 14, 0], TYOP [0, 14, 75], TYOP [0, 37, 36],
   TYOP [0, 63, 77], TYOP [0, 36, 77], TYOP [0, 11, 36], TYOP [0, 15, 80],
   TYOP [0, 24, 81], TYOP [0, 11, 80], TYOP [0, 15, 83], TYOP [0, 24, 84],
   TYV "'a", TYOP [30, 86], TYOP [0, 87, 36], TYOP [30, 10],
   TYOP [0, 89, 36], TYOP [30, 9], TYOP [0, 91, 36], TYOP [0, 12, 40],
   TYOP [0, 15, 93], TYOP [0, 24, 94], TYOP [0, 36, 85], TYOP [0, 23, 23],
   TYOP [2, 0, 86], TYOP [0, 36, 98], TYOP [0, 36, 23], TYOP [0, 36, 11],
   TYOP [0, 36, 30], TYOP [0, 36, 6], TYOP [0, 36, 14], TYOP [0, 54, 23],
   TYOP [0, 23, 36], TYOP [0, 30, 36], TYOP [0, 6, 36], TYOP [0, 23, 11],
   TYOP [0, 23, 30], TYOP [0, 30, 23], TYOP [0, 23, 97], TYOP [0, 11, 11],
   TYOP [0, 11, 113], TYOP [0, 6, 6], TYOP [0, 6, 115], TYOP [0, 98, 0],
   TYOP [0, 98, 117], TYOP [0, 23, 100], TYOP [0, 58, 24]]
  end
  val _ = Theory.incorporate_consts "helper" tyvector
     [("sound_type_change", 17), ("ph_page_unchanged", 27),
      ("ph_l1_unchanged", 29), ("l2_entry_unchanged", 32),
      ("inv_counter_part", 35)];

  local open SharingTables
  in
  val tmvector = build_term_vector idvector tyvector
  [TMV("a", 23), TMV("a", 36), TMV("a'", 23), TMV("b", 36),
   TMV("dest_page", 11), TMV("f", 37), TMV("f'", 37), TMV("idx", 30),
   TMV("idx", 36), TMV("k", 36), TMV("l2_base_add", 23), TMV("l2_idx", 30),
   TMV("l2_index", 23), TMV("m", 24), TMV("m'", 24), TMV("mem", 24),
   TMV("mem'", 24), TMV("n", 36), TMV("pgrefs", 12), TMV("pgrefs'", 12),
   TMV("pgtype", 15), TMV("pgtype'", 15), TMV("ph_page", 11),
   TMV("ph_page'", 11), TMV("ph_page''", 11), TMV("v", 23), TMV("x", 23),
   TMV("x", 6), TMV("x", 36), TMV("y", 36), TMC(7, 39), TMC(7, 41),
   TMC(7, 43), TMC(7, 45), TMC(7, 46), TMC(7, 47), TMC(7, 48), TMC(7, 50),
   TMC(7, 52), TMC(8, 53), TMC(9, 56), TMC(9, 60), TMC(9, 62), TMC(9, 65),
   TMC(11, 53), TMC(12, 67), TMC(13, 36), TMC(14, 68), TMC(15, 68),
   TMC(16, 67), TMC(16, 69), TMC(16, 71), TMC(16, 72), TMC(16, 73),
   TMC(16, 74), TMC(16, 76), TMC(16, 26), TMC(16, 68), TMC(17, 67),
   TMC(18, 68), TMC(19, 37), TMC(20, 37), TMC(21, 78), TMC(22, 53),
   TMC(23, 37), TMC(24, 79), TMC(25, 36), TMC(26, 67), TMC(27, 82),
   TMC(28, 85), TMC(29, 88), TMC(29, 90), TMC(29, 92), TMC(31, 35),
   TMC(32, 95), TMC(33, 95), TMC(34, 96), TMC(35, 32), TMC(36, 97),
   TMC(37, 99), TMC(37, 100), TMC(37, 101), TMC(37, 102), TMC(37, 103),
   TMC(37, 104), TMC(38, 29), TMC(39, 27), TMC(40, 105), TMC(41, 17),
   TMC(42, 87), TMC(42, 89), TMC(42, 91), TMC(43, 106), TMC(43, 107),
   TMC(43, 108), TMC(44, 109), TMC(44, 110), TMC(44, 111), TMC(45, 112),
   TMC(45, 114), TMC(45, 116), TMC(46, 112), TMC(46, 114), TMC(47, 69),
   TMC(48, 69), TMC(49, 118), TMC(49, 69), TMC(50, 118), TMC(51, 119),
   TMC(52, 119), TMC(53, 112), TMC(54, 116), TMC(55, 120), TMC(56, 66)]
  end
  local
  val DT = Thm.disk_thm val read = Term.read_raw tmvector
  in
  val op ph_page_unchanged_def =
    DT([],
       [read"(%31 (|%22. (%34 (|%15. (%34 (|%16. ((%49 (((%86 $2) $1) $0)) (%30 (|%0. ((%58 ((%45 ((%52 $3) (%95 ((%109 $0) (%64 (%61 (%60 (%61 %66)))))))) ((%50 $0) ((%101 $0) (%80 (%64 (%61 (%60 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 %66))))))))))))))))))))))))))))))))))))) ((%50 (%87 ((%40 $0) $2))) (%87 ((%40 $0) $1)))))))))))))"])
  val op ph_l1_unchanged_def =
    DT([],
       [read"(%31 (|%22. (%36 (|%20. (%34 (|%15. (%34 (|%16. ((%49 ((((%85 $3) $2) $1) $0)) ((%58 ((%45 ((%55 ($2 $3)) (%84 (%64 (%60 %66))))) ((%52 ((%102 $3) (%81 (%64 (%61 (%60 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 %66))))))))))))))))))))))) $3))) ((%45 (((%86 $3) $1) $0)) ((%45 (((%86 ((%99 $3) (%81 (%64 (%60 %66))))) $1) $0)) ((%45 (((%86 ((%99 $3) (%81 (%64 (%61 %66))))) $1) $0)) (((%86 ((%99 $3) (%81 (%64 (%60 (%60 %66)))))) $1) $0))))))))))))))"])
  val op l2_entry_unchanged_def =
    DT([],
       [read"(%30 (|%10. (%32 (|%11. (%34 (|%15. (%34 (|%16. ((%49 ((((%77 $3) $2) $1) $0)) ((%50 (%87 ((%40 ((%110 ((%101 $3) (%80 (%64 (%61 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 %66))))))))))))))))))))))))))))))))))) ((%108 (%97 $2)) (%64 (%61 %66))))) $1))) (%87 ((%40 ((%110 ((%101 $3) (%80 (%64 (%61 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 %66))))))))))))))))))))))))))))))))))) ((%108 (%97 $2)) (%64 (%61 %66))))) $0))))))))))))"])
  val op inv_counter_part_def =
    DT([],
       [read"(%35 (|%18. (%34 (|%15. (%36 (|%20. ((%49 (((%73 $2) $1) $0)) (%31 (|%23. ((%57 (%94 ($3 $0))) (((%68 $2) $1) $0)))))))))))"])
  val op sound_type_change_def =
    DT([],
       [read"(%36 (|%20. (%36 (|%21. (%35 (|%18. ((%49 (((%88 $2) $1) $0)) (%31 (|%24. ((%58 (%113 ((%55 ($2 $0)) ($3 $0)))) ((%54 ($1 $0)) (%83 %46))))))))))))"])
  val op read_mem_eq_thm =
    DT(["DISK_THM"],
       [read"(%30 (|%0. ((%58 ((%51 (%15 $0)) (%16 $0))) ((%58 ((%51 (%15 ((%98 $0) (%80 (%64 (%60 %66)))))) (%16 ((%98 $0) (%80 (%64 (%60 %66))))))) ((%58 ((%51 (%15 ((%98 $0) (%80 (%64 (%61 %66)))))) (%16 ((%98 $0) (%80 (%64 (%61 %66))))))) ((%58 ((%51 (%15 ((%98 $0) (%80 (%64 (%60 (%60 %66))))))) (%16 ((%98 $0) (%80 (%64 (%60 (%60 %66)))))))) ((%50 (%87 ((%40 $0) %15))) (%87 ((%40 $0) %16)))))))))"])
  val op write_read_thm =
    DT(["DISK_THM"],
       [read"(%30 (|%0. (%30 (|%25. (%34 (|%13. (%34 (|%14. ((%58 ((%56 $0) (%112 ((%41 $3) ((%42 $1) $2))))) ((%50 (%87 ((%40 $3) $0))) $2))))))))))"])
  val op write_read_unch_thm =
    DT(["DISK_THM"],
       [read"(%30 (|%0. (%30 (|%2. (%30 (|%25. (%34 (|%13. (%34 (|%14. ((%58 ((%67 ((%45 ((%106 ((%98 $3) (%80 (%64 (%60 (%60 %66)))))) $4)) ((%45 ((%104 ((%98 $3) (%80 (%64 (%60 (%60 %66)))))) (%80 (%64 (%60 (%60 %66)))))) ((%104 ((%98 $4) (%80 (%64 (%60 (%60 %66)))))) (%80 (%64 (%60 (%60 %66)))))))) ((%45 ((%103 $3) ((%98 $4) (%80 (%64 (%60 (%60 %66))))))) ((%45 ((%104 ((%98 $3) (%80 (%64 (%60 (%60 %66)))))) (%80 (%64 (%60 (%60 %66)))))) ((%104 ((%98 $4) (%80 (%64 (%60 (%60 %66)))))) (%80 (%64 (%60 (%60 %66))))))))) ((%58 ((%56 $0) (%112 ((%41 $4) ((%42 $1) $2))))) ((%50 (%87 ((%40 $3) $0))) (%87 ((%40 $3) $1)))))))))))))))"])
  val op mmu_l1_decode_type_values_thm =
    DT(["DISK_THM"],
       [read"(%30 (|%0. ((%67 ((%50 (%78 $0)) (%80 %46))) ((%67 ((%50 (%78 $0)) (%80 (%64 (%60 %66))))) ((%67 ((%50 (%78 $0)) (%80 (%64 (%61 %66))))) ((%50 (%78 $0)) (%80 (%64 (%60 (%60 %66))))))))))"])
  val op mmu_l1_18bit_values_thm =
    DT(["DISK_THM"],
       [read"(%30 (|%0. ((%67 ((%50 ((%101 (%80 (%64 (%60 %66)))) $0)) (%80 %46))) ((%50 ((%101 (%80 (%64 (%60 %66)))) $0)) (%80 (%64 (%60 %66)))))))"])
  val op Bless_word_thm =
    DT(["DISK_THM"],
       [read"(%38 (|%1. (%38 (|%3. ((%58 ((%47 $0) (%70 %89))) ((%58 ((%47 $1) (%70 %89))) ((%49 ((%47 $1) $0)) ((%105 (%79 $1)) (%79 $0)))))))))"])
  val op Bless_eq_word_thm =
    DT(["DISK_THM"],
       [read"(%38 (|%1. (%38 (|%3. ((%58 ((%45 ((%47 $0) (%70 %89))) ((%47 $1) (%70 %89)))) ((%49 ((%48 $1) $0)) ((%107 (%79 $1)) (%79 $0))))))))"])
  val op thm_temo_1_thm =
    DT(["DISK_THM"],
       [read"(%30 (|%26. ((%57 ((%39 (%92 ((%109 $0) (%64 (%61 (%60 (%61 %66))))))) ((%44 (%64 (%61 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66)))))))))))))))))))))) (%92 ((%109 $0) (%64 (%61 (%60 (%61 %66))))))))) (%64 (%61 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66))))))))))))))))))))))))"])
  val op thm_temo_3_thm =
    DT(["DISK_THM"],
       [read"(%30 (|%26. ((%57 ((%39 (%64 (%60 %66))) ((%44 (%64 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66)))))))))))))))))))))) (%92 ((%109 $0) (%64 (%61 (%60 (%61 %66))))))))) ((%44 (%64 (%61 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66)))))))))))))))))))))) (%92 ((%109 $0) (%64 (%61 (%60 (%61 %66))))))))))"])
  val op thm_tem_1_thm =
    DT(["DISK_THM"],
       [read"(%38 (|%28. ((%58 ((%47 $0) (%92 ((%109 %10) (%64 (%61 (%60 (%61 %66)))))))) ((%47 $0) (%71 %90)))))"])
  val op thm_tem_2_thm =
    DT(["DISK_THM"],
       [read"((%47 (%92 ((%109 %10) (%64 (%61 (%60 (%61 %66))))))) (%71 %90))"])
  val op non_zero_no_overflow_thm =
    DT(["DISK_THM"],
       [read"(%33 (|%27. ((%58 ((%59 (%94 $0)) %46)) ((%57 (%94 ((%111 $0) (%83 (%64 (%60 %66)))))) ((%44 (%94 $0)) (%64 (%60 %66)))))))"])
  val op thm_temo_l2_thm =
    DT(["DISK_THM"],
       [read"(%30 (|%26. ((%57 ((%39 (%92 ((%101 $0) (%80 (%64 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66))))))))))))))) ((%44 (%64 (%61 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66)))))))))))) (%92 ((%101 $0) (%80 (%64 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66))))))))))))))))) (%64 (%61 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66))))))))))))))"])
  val op thm_temo_l3_thm =
    DT(["DISK_THM"],
       [read"(%30 (|%26. ((%57 ((%39 (%64 (%60 %66))) ((%44 (%64 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66)))))))))))) (%92 ((%101 $0) (%80 (%64 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66))))))))))))))))) ((%44 (%64 (%61 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66)))))))))))) (%92 ((%101 $0) (%80 (%64 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66))))))))))))))))))"])
  val op thm_tem_l1_thm =
    DT(["DISK_THM"],
       [read"(%38 (|%28. ((%58 ((%47 $0) (%92 ((%101 %12) (%80 (%64 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66)))))))))))))))) ((%47 $0) (%72 %91)))))"])
  val op thm_tem_l2_thm =
    DT(["DISK_THM"],
       [read"((%47 (%92 ((%101 %12) (%80 (%64 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66))))))))))))))) (%72 %91))"])
  val op mod_less_thm =
    DT(["DISK_THM"],
       [read"(%38 (|%28. (%38 (|%29. ((%58 ((%47 $1) $0)) ((%57 ((%63 $1) $0)) $1))))))"])
  val op thm_tem_xxx_thm =
    DT(["DISK_THM"],
       [read"(%38 (|%28. ((%58 ((%47 $0) (%64 (%61 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66))))))))))))) ((%58 (%113 ((%57 $0) (%92 ((%101 %12) (%80 (%64 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66))))))))))))))))) (%113 ((%53 (%82 $0)) (%96 ((%101 %12) (%80 (%64 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66))))))))))))))))))))"])
  val op thm_tem_yyy_thm =
    DT(["DISK_THM"],
       [read"(%38 (|%28. ((%58 ((%47 $0) (%64 (%61 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66))))))))))))))))))))))) ((%58 (%113 ((%57 $0) (%92 ((%109 %10) (%64 (%61 (%60 (%61 %66))))))))) (%113 ((%52 (%81 $0)) (%95 ((%109 %10) (%64 (%61 (%60 (%61 %66))))))))))))"])
  val op non_max_no_overflow_thm =
    DT(["DISK_THM"],
       [read"(%33 (|%27. ((%58 (%113 ((%54 $0) (%83 (%64 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 (%60 %66))))))))))))))))))))))))))))))))))) ((%57 (%94 ((%100 $0) (%83 (%64 (%60 %66)))))) ((%39 (%94 $0)) (%64 (%60 %66)))))))"])
  val op ref_inv_l2_thm =
    DT(["DISK_THM"],
       [read"(%34 (|%15. (%36 (|%20. (%31 (|%22. ((%58 ((((%75 $2) $1) %18) $0)) ((((%75 $2) $1) %19) $0))))))))"])
  val op ref_inv_l1_thm =
    DT(["DISK_THM"],
       [read"(%34 (|%15. (%36 (|%20. (%31 (|%22. ((%58 ((((%74 $2) $1) %18) $0)) ((((%74 $2) $1) %19) $0))))))))"])
  val op diff_pg_type_diff_pages_thm =
    DT(["DISK_THM"],
       [read"(%36 (|%20. (%31 (|%22. (%31 (|%23. ((%58 ((%55 ($2 $1)) (%84 (%64 (%61 %66))))) ((%58 ((%55 ($2 $0)) (%84 (%64 (%60 %66))))) (%113 ((%52 $1) $0))))))))))"])
  val op invariant_page_type_l2_unchanged_mem_thm =
    DT(["DISK_THM"],
       [read"(%36 (|%20. (%31 (|%22. ((%58 ((%55 ($1 $0)) (%84 (%64 (%61 %66))))) ((%58 ((((%75 %15) $1) %18) $0)) ((%58 (((%86 $0) %15) %16)) ((((%75 %16) $1) %18) $0))))))))"])
  val op invariant_page_type_l1_unchanged_mem_thm =
    DT(["DISK_THM"],
       [read"(%36 (|%20. (%31 (|%22. ((%58 ((%55 ($1 $0)) (%84 (%64 (%60 %66))))) ((%58 ((%52 $0) ((%102 (%81 (%64 (%61 (%60 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 %66)))))))))))))))))))))) $0))) ((%58 ((((%74 %15) $1) %18) $0)) ((%58 (((%86 $0) %15) %16)) ((%58 (((%86 ((%99 $0) (%81 (%64 (%60 %66))))) %15) %16)) ((%58 (((%86 ((%99 $0) (%81 (%64 (%61 %66))))) %15) %16)) ((%58 (((%86 ((%99 $0) (%81 (%64 (%60 (%60 %66)))))) %15) %16)) ((((%74 %16) $1) %18) $0))))))))))))"])
  val op ref_cnt_equality_thm =
    DT(["DISK_THM"],
       [read"(%31 (|%22. (%34 (|%15. (%34 (|%16. (%31 (|%4. ((%58 ((%45 ((%58 ((%55 (%20 $3)) (%84 (%64 (%61 %66))))) (((%86 $3) $2) $1))) ((%58 ((%45 ((%55 (%20 $3)) (%84 (%64 (%60 %66))))) ((%52 ((%102 $3) (%81 (%64 (%61 (%60 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 %66))))))))))))))))))))))) $3))) ((%45 (((%86 $3) $2) $1)) ((%45 (((%86 ((%99 $3) (%81 (%64 (%60 %66))))) $2) $1)) ((%45 (((%86 ((%99 $3) (%81 (%64 (%61 %66))))) $2) $1)) (((%86 ((%99 $3) (%81 (%64 (%60 (%60 %66)))))) $2) $1))))))) ((%57 ((((%69 $2) %20) $0) $3)) ((((%69 $1) %20) $0) $3)))))))))))"])
  val op ref_cnt_equality_l2_thm =
    DT(["DISK_THM"],
       [read"(%30 (|%10. (%32 (|%7. (%34 (|%15. (%34 (|%16. (%36 (|%20. (%31 (|%22. ((%58 ((%55 ($1 (%95 ((%109 $5) (%64 (%61 (%60 (%61 %66)))))))) (%84 (%64 (%61 %66))))) ((%58 ((((%77 $5) $4) $3) $2)) ((%57 (((((%76 (%93 $4)) $3) $1) $0) (%95 ((%109 $5) (%64 (%61 (%60 (%61 %66)))))))) (((((%76 (%93 $4)) $2) $1) $0) (%95 ((%109 $5) (%64 (%61 (%60 (%61 %66))))))))))))))))))))))"])
  val op invariant_page_type_l2_ok_changed_types_thm =
    DT(["DISK_THM"],
       [read"(%36 (|%20. (%31 (|%22. ((%58 ((%55 ($1 $0)) (%84 (%64 (%61 %66))))) ((%58 ((((%75 %15) $1) %18) $0)) ((%58 (((%73 %18) %15) $1)) ((%58 (((%88 $1) %21) %18)) ((((%75 %15) %21) %18) $0)))))))))"])
  val op invariant_page_type_l1_ok_changed_types_thm =
    DT(["DISK_THM"],
       [read"(%36 (|%20. (%31 (|%22. ((%58 ((%55 ($1 $0)) (%84 (%64 (%60 %66))))) ((%58 ((%52 $0) ((%102 (%81 (%64 (%61 (%60 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 (%61 %66)))))))))))))))))))))) $0))) ((%58 ((((%74 %15) $1) %18) $0)) ((%58 (%31 (|%23. ((%57 (%94 (%18 $0))) (((%68 %15) $2) $0))))) ((%58 (%31 (|%24. ((%58 (%113 ((%55 (%21 $0)) ($2 $0)))) ((%54 (%18 $0)) (%83 %46)))))) ((((%74 %15) %21) %18) $0))))))))))"])
  val op ref_cnt_equality_ok_changed_types_thm =
    DT(["DISK_THM"],
       [read"(%31 (|%22. (%34 (|%15. (%36 (|%20. (%36 (|%21. (%31 (|%4. ((%58 ((%55 ($2 $4)) (%84 (%64 (%61 %66))))) ((%58 ((%55 ($1 $4)) (%84 (%64 (%61 %66))))) ((%58 ((((%75 $3) $2) %18) $4)) ((%58 (%31 (|%23. ((%57 (%94 (%18 $0))) (((%68 $4) $3) $0))))) ((%58 (%31 (|%24. ((%58 (%113 ((%55 ($2 $0)) ($3 $0)))) ((%54 (%18 $0)) (%83 %46)))))) ((%57 ((((%69 $3) $2) $0) $4)) ((((%69 $3) $1) $0) $4)))))))))))))))))"])
  val op ref_cnt_l1_equality_ok_changed_types_thm =
    DT(["DISK_THM"],
       [read"(%31 (|%22. (%34 (|%15. (%36 (|%20. (%36 (|%21. (%31 (|%4. ((%58 ((%55 ($2 $4)) (%84 (%64 (%60 %66))))) ((%58 ((%55 ($1 $4)) (%84 (%64 (%60 %66))))) ((%58 ((((%74 $3) $2) %18) $4)) ((%58 (%31 (|%23. ((%57 (%94 (%18 $0))) (((%68 $4) $3) $0))))) ((%58 (%31 (|%24. ((%58 (%113 ((%55 ($2 $0)) ($3 $0)))) ((%54 (%18 $0)) (%83 %46)))))) ((%57 ((((%69 $3) $2) $0) $4)) ((((%69 $3) $1) $0) $4)))))))))))))))))"])
  val op sum_split_thm =
    DT(["DISK_THM"],
       [read"(%38 (|%28. (%38 (|%17. (%37 (|%5. ((%58 ((%47 $2) $1)) ((%57 ((%62 ((%43 %46) $1)) $0)) ((%39 ((%39 ((%62 ((%43 %46) $2)) $0)) ($0 $2))) ((%62 ((%43 ((%39 $2) (%64 (%60 %66)))) ((%44 ((%44 $1) $2)) (%64 (%60 %66))))) $0))))))))))"])
  val op sum_splitter_thm =
    DT(["DISK_THM"],
       [read"(%38 (|%8. (%38 (|%17. (%37 (|%5. (%37 (|%6. (%38 (|%1. (%38 (|%3. ((%58 ((%45 ((%57 ((%39 $1) ($3 $5))) ((%39 ($2 $5)) $0))) ((%45 (%38 (|%28. ((%58 (%113 ((%57 $0) $6))) ((%58 ((%47 $0) $5)) ((%57 ($4 $0)) ($3 $0))))))) ((%47 $5) $4)))) ((%57 ((%39 $1) ((%62 ((%43 %46) $4)) $3))) ((%39 ((%62 ((%43 %46) $4)) $2)) $0)))))))))))))))"])
  val op SUM_GT_thm =
    DT(["DISK_THM"],
       [read"(%37 (|%5. (%38 (|%17. (%38 (|%9. ((%58 ((%47 $0) $1)) ((%58 ((%59 ($2 $0)) %46)) ((%59 ((%65 $1) $2)) %46)))))))))"])
  end
  val _ = DB.bindl "helper"
  [("ph_page_unchanged_def",ph_page_unchanged_def,DB.Def),
   ("ph_l1_unchanged_def",ph_l1_unchanged_def,DB.Def),
   ("l2_entry_unchanged_def",l2_entry_unchanged_def,DB.Def),
   ("inv_counter_part_def",inv_counter_part_def,DB.Def),
   ("sound_type_change_def",sound_type_change_def,DB.Def),
   ("read_mem_eq_thm",read_mem_eq_thm,DB.Thm),
   ("write_read_thm",write_read_thm,DB.Thm),
   ("write_read_unch_thm",write_read_unch_thm,DB.Thm),
   ("mmu_l1_decode_type_values_thm",mmu_l1_decode_type_values_thm,DB.Thm),
   ("mmu_l1_18bit_values_thm",mmu_l1_18bit_values_thm,DB.Thm),
   ("Bless_word_thm",Bless_word_thm,DB.Thm),
   ("Bless_eq_word_thm",Bless_eq_word_thm,DB.Thm),
   ("thm_temo_1_thm",thm_temo_1_thm,DB.Thm),
   ("thm_temo_3_thm",thm_temo_3_thm,DB.Thm),
   ("thm_tem_1_thm",thm_tem_1_thm,DB.Thm),
   ("thm_tem_2_thm",thm_tem_2_thm,DB.Thm),
   ("non_zero_no_overflow_thm",non_zero_no_overflow_thm,DB.Thm),
   ("thm_temo_l2_thm",thm_temo_l2_thm,DB.Thm),
   ("thm_temo_l3_thm",thm_temo_l3_thm,DB.Thm),
   ("thm_tem_l1_thm",thm_tem_l1_thm,DB.Thm),
   ("thm_tem_l2_thm",thm_tem_l2_thm,DB.Thm),
   ("mod_less_thm",mod_less_thm,DB.Thm),
   ("thm_tem_xxx_thm",thm_tem_xxx_thm,DB.Thm),
   ("thm_tem_yyy_thm",thm_tem_yyy_thm,DB.Thm),
   ("non_max_no_overflow_thm",non_max_no_overflow_thm,DB.Thm),
   ("ref_inv_l2_thm",ref_inv_l2_thm,DB.Thm),
   ("ref_inv_l1_thm",ref_inv_l1_thm,DB.Thm),
   ("diff_pg_type_diff_pages_thm",diff_pg_type_diff_pages_thm,DB.Thm),
   ("invariant_page_type_l2_unchanged_mem_thm",
    invariant_page_type_l2_unchanged_mem_thm,
    DB.Thm),
   ("invariant_page_type_l1_unchanged_mem_thm",
    invariant_page_type_l1_unchanged_mem_thm,
    DB.Thm), ("ref_cnt_equality_thm",ref_cnt_equality_thm,DB.Thm),
   ("ref_cnt_equality_l2_thm",ref_cnt_equality_l2_thm,DB.Thm),
   ("invariant_page_type_l2_ok_changed_types_thm",
    invariant_page_type_l2_ok_changed_types_thm,
    DB.Thm),
   ("invariant_page_type_l1_ok_changed_types_thm",
    invariant_page_type_l1_ok_changed_types_thm,
    DB.Thm),
   ("ref_cnt_equality_ok_changed_types_thm",
    ref_cnt_equality_ok_changed_types_thm,
    DB.Thm),
   ("ref_cnt_l1_equality_ok_changed_types_thm",
    ref_cnt_l1_equality_ok_changed_types_thm,
    DB.Thm), ("sum_split_thm",sum_split_thm,DB.Thm),
   ("sum_splitter_thm",sum_splitter_thm,DB.Thm),
   ("SUM_GT_thm",SUM_GT_thm,DB.Thm)]

  local open Portable GrammarSpecials Parse
    fun UTOFF f = Feedback.trace("Parse.unicode_trace_off_complaints",0)f
  in
  val _ = mk_local_grms [("hypervisor_modelTheory.hypervisor_model_grammars",
                          hypervisor_modelTheory.hypervisor_model_grammars),
                         ("blastTheory.blast_grammars",
                          blastTheory.blast_grammars)]
  val _ = List.app (update_grms reveal) []
  val _ = update_grms
       (UTOFF temp_overload_on)
       ("ph_page_unchanged", (Term.prim_mk_const { Name = "ph_page_unchanged", Thy = "helper"}))
  val _ = update_grms
       (UTOFF temp_overload_on)
       ("ph_page_unchanged", (Term.prim_mk_const { Name = "ph_page_unchanged", Thy = "helper"}))
  val _ = update_grms
       (UTOFF temp_overload_on)
       ("ph_l1_unchanged", (Term.prim_mk_const { Name = "ph_l1_unchanged", Thy = "helper"}))
  val _ = update_grms
       (UTOFF temp_overload_on)
       ("ph_l1_unchanged", (Term.prim_mk_const { Name = "ph_l1_unchanged", Thy = "helper"}))
  val _ = update_grms
       (UTOFF temp_overload_on)
       ("l2_entry_unchanged", (Term.prim_mk_const { Name = "l2_entry_unchanged", Thy = "helper"}))
  val _ = update_grms
       (UTOFF temp_overload_on)
       ("l2_entry_unchanged", (Term.prim_mk_const { Name = "l2_entry_unchanged", Thy = "helper"}))
  val _ = update_grms
       (UTOFF temp_overload_on)
       ("inv_counter_part", (Term.prim_mk_const { Name = "inv_counter_part", Thy = "helper"}))
  val _ = update_grms
       (UTOFF temp_overload_on)
       ("inv_counter_part", (Term.prim_mk_const { Name = "inv_counter_part", Thy = "helper"}))
  val _ = update_grms
       (UTOFF temp_overload_on)
       ("sound_type_change", (Term.prim_mk_const { Name = "sound_type_change", Thy = "helper"}))
  val _ = update_grms
       (UTOFF temp_overload_on)
       ("sound_type_change", (Term.prim_mk_const { Name = "sound_type_change", Thy = "helper"}))
  val helper_grammars = Parse.current_lgrms()
  end
  val _ = Theory.LoadableThyData.temp_encoded_update {
    thy = "helper",
    thydataty = "compute",
    data =
        "helper.ph_page_unchanged_def helper.sound_type_change_def helper.inv_counter_part_def helper.l2_entry_unchanged_def helper.ph_l1_unchanged_def"
  }

val _ = if !Globals.print_thy_loads then print "done\n" else ()
val _ = Theory.load_complete "helper"
end

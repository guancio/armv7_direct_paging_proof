open HolKernel boolLib bossLib Parse wordsLib;
open hypervisor_modelTheory;
open mmu_propertiesTheory;
open tinyTheory;
open helperTheory;

open guest_write_access_utilsTheory;

load "mmu_utilsLib";
open mmu_utilsLib;

(*
Common tactics
*)
structure guest_write_accessLib :> guest_write_accessLib =
struct

val INSTANTIATE_INVARIANT_CURRENT_L1_TAC =
    ASSUME_TAC (UNDISCH_ALL currentl1_dom_thm)
    THEN (ASSUME_ALL_TAC (CONJUNCTS (UNDISCH_ALL currentl1_tbl_thm)));

fun INSTANTIATE_INVARIANT_L2_TAC ph_page =
    (ASSUME_TAC (UNDISCH_ALL (SPEC ph_page l2_tbl_thm)));

(* TODO: The blast theorems must take add' as parameter *)
fun INSTANTIATE_INVARIANT_CURRENT_L1_FOR_ADD_TAC add =
     (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_l1_def])
THEN (SPEC_ASSUMPTION_TAC ``!x:word12. X`` ``w2w(mmu_tbl_index
			(^add:bool[32])):bool[12]``)
THEN (FULL_SIMP_TAC (bool_ss) [l1_page_add_thm])
THEN (FULL_SIMP_TAC (bool_ss) [l1_desc_idx_thm])
THEN (LET_HYP_ELIM_TAC [])
THEN (LET_HYP_ELIM_TAC [])
THEN (LET_HYP_ELIM_TAC [])
;

fun INSTANTIATE_INVARIANT_L2_FOR_ADD_TAC l2_base add =
     (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_l2_def])
THEN (SPEC_ASSUMPTION_TAC ``!x:word10. X``
    ``(w2w((l2_base:bool[22] && 3w) <<  8):bool[10]) !!
     (w2w((^add:bool[32] && 0xFF000w) ⋙  12):bool[10])``)
THEN (FULL_SIMP_TAC (bool_ss) [l2_desc_idx_thm])
THEN (REPEAT (LET_HYP_ELIM_TAC []))
;

val invpt_pattern = ``invariant_page_type a c2 c3 mem pgtype pgrefs``;
val INVPT_KEEP_TAC = THM_KEEP_TAC invpt_pattern;
val NO_INVPT_KEEP_TAC = NO_THM_KEEP_TAC invpt_pattern;

end

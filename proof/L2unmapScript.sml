(* 
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/spec/" :: !loadPath;
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/" ::  !loadPath;
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/proof/" :: !loadPath;
load "mmu_utilsLib";
load "hypervisor_modelTheory";
load "mmu_propertiesTheory";
load "helperTheory";
*)


open HolKernel boolLib bossLib Parse wordsLib;
open hypervisor_modelTheory;
open mmu_propertiesTheory;
open tinyTheory;
open helperTheory;

open sum_numTheory;
open wordsTheory;

load "mmu_utilsLib";
open mmu_utilsLib;
load "helperLib";
open helperLib;

open numSyntax;

val _ = new_theory "L2unmap";








(* we handle the failure  *)
(* cases, which do not affect the system state *)
(* we need to keep the definition for the top theorems *)
val  l2_unmap_exception_case_tac  =
  TRY (SYM_ASSUMPTION_TAC ``w = unmap_L2_pageTable_entry x y z``)
  THEN THM_KEEP_TAC ``unmap_L2_pageTable_entry x y z = w`` (
    (FULL_SIMP_TAC (srw_ss()) [unmap_L2_pageTable_entry_def])
    THEN (Cases_on `pgtype (w2w (l2_base_add ⋙ 12)) ≠ 2w`)
    THENL [
      (FULL_SIMP_TAC (srw_ss()) []),
      (FULL_SIMP_TAC (srw_ss()) [])
      THEN (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF])
      THEN (Q.ABBREV_TAC `entryAddrL2:word32 =
             0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2`)
      THEN (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF])
      THEN (Q.ABBREV_TAC `page_desc:word32 = read_mem32 (entryAddrL2,mem)`)
      THEN (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF])
      THEN (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF])
      THEN (Q.ABBREV_TAC`pgdOR:bool = (3w && page_desc = 2w) ∨ (3w && page_desc = 3w)`)
(* Second failure: the entry was unmapped, nothing change in the system *)
      THEN (Cases_on `~pgdOR`)
      THENL [
        (FULL_SIMP_TAC (srw_ss()) []),
        (FULL_SIMP_TAC (srw_ss()) [])
      ]
    ]
  );


val l2_unmap_unchanged_pagetype_thm = Q.store_thm("l2_unmap_unchanged_pagetype_thm", 
  `!(l2_base_add:bool[32]).
   ((c1',c2':bool[32],c3':bool[32],mem',pgtype', pgrefs') = 
       (unmap_L2_pageTable_entry (rec'sctlrT 1w, c2, c3, mem, pgtype, pgrefs)
	     (l2_base_add:bool[32]) (l2_index:bool[32]))) ==>
	     ( pgtype' = pgtype)
`,
  FULL_SIMP_TAC (srw_ss()) [unmap_L2_pageTable_entry_def]
  THEN RW_TAC (srw_ss()) [LET_DEF]
);

val l2_unmap_unchanged_mem_thm = Q.store_thm("l2_unmap_unchanged_mem_thm", 
  `!ptb:word20 (l2_base_add:bool[32]).
   ((c1',c2':bool[32],c3':bool[32],mem',pgtype', pgrefs') = 
       (unmap_L2_pageTable_entry (rec'sctlrT 1w, c2, c3, mem, pgtype, pgrefs) 
	     (l2_base_add:bool[32]) (l2_index:bool[32]))) ==>
	        (
		 (ptb <> w2w(l2_base_add >>> 12):word20) ==>
                 (ph_page_unchanged ptb mem mem')
               )
`,
  (FULL_SIMP_TAC (srw_ss()) [ph_page_unchanged_def])
  THEN (REPEAT STRIP_TAC)
  THEN l2_unmap_exception_case_tac
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN Q.ABBREV_TAC `page_desc'=0xFFFFFFFCw && read_mem32 (entryAddrL2,mem)`
  THEN Q.ABBREV_TAC `mem'=write_mem32 (entryAddrL2,mem,page_desc')`
  THEN FULL_SIMP_TAC (srw_ss()) [Abbr `entryAddrL2`]
  THEN (
      ASSUME_TAC (
       WRITE_READ_UNCH_RULE 
	   ``((w2w ((a :word32) >>> 12) :word20) <>  (w2w ((l2_base_add
  :word32) >>> 12) :word20)) /\ (a = 0xFFFFFFFCw && a)``
           ``0xFFFFF000w && l2_base_add:bool[32] !! 4092w && l2_index << 2``
	   ``a:bool[32]``
           ``page_desc':bool[32]``
	   ``mem:bool[32]->bool[8]``
	   ``mem':bool[32]->bool[8]``
	 ))
  THEN METIS_TAC []
);


val l2_unmap_unchanged_l1_mem_thm = Q.store_thm("l2_unmap_unchanged_l1_mem_thm", 
  `!ptb:word20 (l2_base_add:bool[32]).
   (invariant_page_type (rec'sctlrT 1w) c2 c3 mem pgtype pgrefs) ==>
   ((c1',c2':bool[32],c3':bool[32],mem',pgtype', pgrefs') = 
       (unmap_L2_pageTable_entry (rec'sctlrT 1w, c2, c3, mem, pgtype, pgrefs) 
	     (l2_base_add:bool[32]) (l2_index:bool[32]))) ==>
             (ph_l1_unchanged ptb pgtype mem mem')
`,
  (FULL_SIMP_TAC (srw_ss()) [ph_l1_unchanged_def])
  THEN STRIP_TAC
  THEN (Q.ABBREV_TAC `goal =   ph_page_unchanged ptb mem mem' /\
                               ph_page_unchanged (ptb + 1w) mem mem' /\
                               ph_page_unchanged (ptb + 2w) mem mem' /\
                               ph_page_unchanged (ptb + 3w) mem mem'`)
  THEN (REPEAT STRIP_TAC)
(* check that the four pages has not been changed *)
  THEN l2_unmap_exception_case_tac
  THENL [
     FULL_SIMP_TAC (srw_ss()) [Abbr `goal`]
     THEN METIS_TAC [ph_page_unchanged_def],
     FULL_SIMP_TAC (srw_ss()) [Abbr `goal`]
     THEN METIS_TAC [ph_page_unchanged_def],
     (* REAL GOAL *)
     (SYM_ASSUMPTION_TAC ``unmap_L2_pageTable_entry x y z = w``)
     THEN ASSUME_TAC (UNDISCH (
      SPECL [``ptb:bool[20]``, ``l2_base_add:bool[32]``]
      l2_unmap_unchanged_mem_thm))
     THEN ASSUME_TAC (UNDISCH (
      SPECL [``ptb+1w:bool[20]``, ``l2_base_add:bool[32]``]
      l2_unmap_unchanged_mem_thm))
     THEN ASSUME_TAC (UNDISCH (
      SPECL [``ptb+2w:bool[20]``, ``l2_base_add:bool[32]``]
      l2_unmap_unchanged_mem_thm))
     THEN ASSUME_TAC (UNDISCH (
      SPECL [``ptb+3w:bool[20]``, ``l2_base_add:bool[32]``]
      l2_unmap_unchanged_mem_thm))
     THEN ASSUME_TAC (
     GSYM (UNDISCH ( UNDISCH (
    SPECL [``pgtype:bool[20]->bool[2]``,
	   ``(w2w (l2_base_add:word32 >>> 12)):word20``,
           ``ptb:bool[20]``]
    diff_pg_type_diff_pages_thm
    ))))
     (* instantiate the invariant to check the type of the next pages *)
     THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def])
     THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
    (* use the invariant to guarantee that the ph_page is a valid l1 *)
     THEN (SPEC_ASSUMPTION_TAC ``∀ph_page:bool[20].p``
			       ``ptb:bool[20]``)
     THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``(pgtype ptb = 1w)``)
     THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``0xFFFFCw && ptb = ptb``)
     (* blast stuff *)
    THEN (FULL_SIMP_TAC (srw_ss()) [UNDISCH (
        blastLib.BBLAST_PROVE ``
          (0xFFFFCw && ptb:bool[20] = ptb) ==>
          ((ptb ‖ 1w) = (ptb + 1w))
	``)])
    THEN (FULL_SIMP_TAC (srw_ss()) [UNDISCH (
        blastLib.BBLAST_PROVE ``
          (0xFFFFCw && ptb:bool[20] = ptb) ==>
          ((ptb ‖ 2w) = (ptb + 2w))
	``)])
    THEN (FULL_SIMP_TAC (srw_ss()) [UNDISCH (
        blastLib.BBLAST_PROVE ``
          (0xFFFFCw && ptb:bool[20] = ptb) ==>
          ((ptb ‖ 3w) = (ptb + 3w))
	``)])
    THEN ASSUME_TAC (
    GSYM (UNDISCH ( UNDISCH (
    SPECL [``pgtype:bool[20]->bool[2]``,
	   ``(w2w (l2_base_add:word32 >>> 12)):word20``,
           ``ptb+1w:bool[20]``]
    diff_pg_type_diff_pages_thm
    ))))
    THEN ASSUME_TAC (
    GSYM (UNDISCH ( UNDISCH (
    SPECL [``pgtype:bool[20]->bool[2]``,
	   ``(w2w (l2_base_add:word32 >>> 12)):word20``,
           ``ptb+2w:bool[20]``]
    diff_pg_type_diff_pages_thm
    ))))
    THEN ASSUME_TAC (
    GSYM (UNDISCH ( UNDISCH (
    SPECL [``pgtype:bool[20]->bool[2]``,
	   ``(w2w (l2_base_add:word32 >>> 12)):word20``,
           ``ptb+3w:bool[20]``]
    diff_pg_type_diff_pages_thm
    ))))
     THEN RES_TAC
     THEN METIS_TAC []
  ]
);


val l2_unmap_unchanged_l2_mem_thm = Q.store_thm("l2_unmap_unchanged_l2_mem_thm", 
     `
!l2_idx:word10 c1 pgtype'.
      ((c1,c2',c3',mem',pgtype',pgrefs') = 
            unmap_L2_pageTable_entry (rec'sctlrT 1w,c2:bool[32],c3:bool[32],mem,pgtype,pgrefs)l2_base_add l2_index)
      ==>
       ((l2_idx) <> (w2w (l2_index  && 0x000003FFw)):word10) ==>
       (l2_entry_unchanged l2_base_add l2_idx mem mem')
`,
      (REPEAT STRIP_TAC)
  THEN (FULL_SIMP_TAC (srw_ss()) [l2_entry_unchanged_def])
  THEN l2_unmap_exception_case_tac
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `entryAddrL2`])
  THEN (METIS_TAC [
       WRITE_READ_UNCH_RULE 
	   ``((l2_idx) <> (w2w ( 1023w && l2_index:bool[32])):word10)``
           ``0xFFFFF000w && l2_base_add:bool[32] !! 4092w && l2_index << 2``
	   ``(0xFFFFF000w && l2_base_add ) !! (w2w (l2_idx:bool[10])):word32 << 2``
           ``0xFFFFFFFCw && page_desc:bool[32]``
	   ``mem:bool[32]->bool[8]``
	   ``mem':bool[32]->bool[8]``])
);





val l2_unmap_invariant_page_type_l2_tac =
(* We have to check that all l2 are still valid *)
(* remember that one entry of the l2s is the updated one *)
    Cases_on `ph_page <> (w2w (l2_base_add ⋙ 12))`
    THENL [
    (* the page we are checking is different from the page updated *)
    (* we know that all words in the phisical page are unchanged *)
      ASSUME_TAC (
        SPECL [``ph_page:bool[20]``, ``l2_base_add:bool[32]``]
	      l2_unmap_unchanged_mem_thm)
      THEN (SYM_ASSUMPTION_TAC ``unmap_L2_pageTable_entry x y z = w``)
      THEN (SIMP_BY_ASSUMPTION_TAC ``a = unmap_L2_pageTable_entry x y z``)
      THEN (SIMP_BY_ASSUMPTION_TAC ``ph_page ≠ w2w (l2_base_add ⋙ 12)``)
    (* we instantiate the invariant for the phisical page and we show
    that the invariant hold for the new memory
     trick: we have two invariants
     *)
      THEN (SPEC_ASSUMPTION_TAC ``!ph_page:bool[20].p`` ``ph_page:bool[20]``)
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``pgtype' ph_page = 2w``)
      THEN (ASSUME_TAC  (
           SPECL [``pgtype':bool[20]->bool[2]``, ``ph_page:word20``]
	   invariant_page_type_l2_unchanged_mem_thm
      ))
      THEN (SIMP_BY_ASSUMPTION_TAC ``pgtype' ph_page = 2w``)
      THEN (SIMP_BY_ASSUMPTION_TAC ``invariant_page_type_l2 mem pgtype' pgrefs ph_page``)
      THEN RES_TAC
      THEN FULL_SIMP_TAC (srw_ss()) []
    (* Finally we show that the invariant hold independently
     from the updated ref_cnt
     *)
      THEN (ASSUME_TAC (SPECL [
			``mem':bool[32]->bool[8]``, 
			``pgtype':bool[20]->bool[2]``,
			``ph_page:word20``]
		       ref_inv_l2_thm))
      THEN (METIS_TAC []),
    (* Second case: we are checking the updated page *)
      FULL_SIMP_TAC (srw_ss()) []
      THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_l2_def])
      THEN (STRIP_TAC)
      THEN BasicProvers.LET_ELIM_TAC
      THEN (Cases_on `1023w && l2_index = (w2w l2_idx)`)
      THENL [
      (* First case: we check the updated entry *)
      (* first we show that the address where we read is
	 the same written *)
      (* TODO THIS is really dirty, mainlty to prevent splits *)
	 (Q.ABBREV_TAC `prevent_split =
(3w && l2_desc = 0w) ∨
((3w && l2_desc = 2w) ∨ (3w && l2_desc = 3w)) ∧
if pgtype' (rec'l2SmallT l2_desc).addr ≠ 0w then
  (rec'l2SmallT l2_desc).ap = 2w
else ((rec'l2SmallT l2_desc).ap = 2w) ∨ ((rec'l2SmallT l2_desc).ap =
3w)
`)
	 THEN (Q.ABBREV_TAC `other_split = (3w && page_desc = 2w) ∨ (3w && page_desc = 3w)`)
         THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `entryAddrL2`])
         THEN ( ASSUME_TAC (UNDISCH(
	 blastLib.BBLAST_PROVE ``
( 1023w && l2_index = w2w l2_idx) ==>
(
(w2w (w2w ((l2_base_add :word32) ⋙ (12 :num)) :word20) : word32) ≪ (12 :num) ‖
(w2w (l2_idx :word10) :word32) ≪ (2 :num) =
(0xFFFFF000w :word32) && (l2_base_add :word32) ‖ (4092w :word32)
 &&(l2_index :word32) ≪ (2 :num)
)
``
	 )))
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
	 THEN (Q.ABBREV_TAC `entryAddrL2:word32 =
             0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2`)
      (* we show that what we read is what has been written *)
	 THEN (SYM_ASSUMPTION_TAC ``write_mem32 a = mem'``)
	 THEN (ASSUME_TAC (
	     UNDISCH (
	     SPECL [``entryAddrL2:bool[32]``,
		    ``0xFFFFFFFCw && page_desc:bool[32]``,
		    ``mem:bool[32]->bool[8]``,
		    ``mem':bool[32]->bool[8]``]
	     write_read_thm)))
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
	 (* now we show that the small page is unmapped *)
	 THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `l2_desc`, Abbr
			`prevent_split`]),
       (* Second case, the we read from a different position *)
	 (Q.ABBREV_TAC `prevent_split =
(3w && l2_desc = 0w) ∨
((3w && l2_desc = 2w) ∨ (3w && l2_desc = 3w)) ∧
if pgtype' (rec'l2SmallT l2_desc).addr ≠ 0w then
  (rec'l2SmallT l2_desc).ap = 2w
else ((rec'l2SmallT l2_desc).ap = 2w) ∨ ((rec'l2SmallT l2_desc).ap =
3w)
`)
	 THEN (Q.ABBREV_TAC `other_split = (3w && page_desc = 2w) ∨ (3w && page_desc = 3w)`)
	 THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `entryAddrL2`])
	 THEN (ASSUME_TAC (
	 WRITE_READ_UNCH_RULE
	   ``(1023w && l2_index:bool[32]) ≠ w2w (l2_idx:bool[10])``
	   ``0xFFFFF000w:bool[32] && l2_base_add ‖ 4092w && l2_index ≪	2``
	   ``(w2w (w2w ((l2_base_add :word32) ⋙ (12 :num)) :word20) :
	     word32) ≪ (12 :num) ‖ (w2w (l2_idx :word10) :word32) ≪ (2 :num)``
	   ``0xFFFFFFFCw && page_desc:bool[32]``
	   ``mem:bool[32]->bool[8]``
	   ``mem':bool[32]->bool[8]``))

	 THEN (SIMP_BY_ASSUMPTION_TAC ``(1023w && l2_index) ≠ w2w l2_idx``)
	 THEN (SYM_ASSUMPTION_TAC ``write_mem32 a = mem'``)
	 THEN (SIMP_BY_ASSUMPTION_TAC ``mem' = a``)
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
	 (* now we know that the memory did not changed, we need to use  *)
	 (* the invarnant to verify its property *)
	 THEN (SPEC_ASSUMPTION_TAC ``!ph_page:bool[20].p``
		``(w2w (l2_base_add:bool[32] ⋙ 12) :bool[20])``)
	 THEN (SIMP_BY_ASSUMPTION_TAC ``pgtype' (w2w (l2_base_add ⋙ 12)) = 2w``)
	 THEN (SPEC_ASSUMPTION_TAC ``!l2_idx:bool[10].p`` ``l2_idx:word10``)
	 THEN (LET_HYP_ELIM_TAC [])
	 THEN REABBREV_TAC
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
      ]
    ]
;



val l2_unmap_invariant_page_type_l1_tac =
    (* if we did non change the 4 pages, than the invariant hold *)
    ASSUME_TAC (
    UNDISCH (UNDISCH (
    SPECL [``pgtype':bool[20]->bool[2]``, ``ph_page:bool[20]``]
    invariant_page_type_l1_unchanged_mem_thm
    )))
    THEN (ASSUME_TAC (
      SPECL [``ph_page:bool[20]``, ``l2_base_add:bool[32]``]
	  l2_unmap_unchanged_l1_mem_thm
    ))
    THEN (SYM_ASSUMPTION_TAC ``unmap_L2_pageTable_entry x y z = w``)
    THEN (FULL_SIMP_TAC (srw_ss()) [
	  UNDISCH (
	  SPEC ``l2_base_add:bool[32]``
	  l2_unmap_unchanged_pagetype_thm)
	 ])
    THEN (`ph_l1_unchanged ph_page pgtype mem mem'` by METIS_TAC [])
    (* Enstablish that the l1 invariant hold for the original ph_page *)
    THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def])
    THEN (SPEC_ASSUMPTION_TAC ``!ph_page:bool[20].p`` ``ph_page:bool[20]``)
    THEN (`invariant_page_type_l1 mem pgtype pgrefs ph_page` by METIS_TAC [])
    THEN (FULL_SIMP_TAC (srw_ss()) [ph_l1_unchanged_def])
    THEN (METIS_TAC [ref_inv_l1_thm])
;



(* see line 1273 of th eold proof *)
val l2_unmap_invariant_page_type_tac =
    (FULL_SIMP_TAC (srw_ss()) [ LET_DEF])
    THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def])
    THEN (REPEAT CONJ_TAC)
    (* Three sub goals *)
    THENL [
    (* The MMU is still enabled *)
      (SYM_ASSUMPTION_TAC ``rec'sctlrT 1w = c1'``)
      THEN (FULL_SIMP_TAC (srw_ss()) [rec'sctlrT_def]),
    (* The Domain are still well setupped, keep the invariant for further use *)
      (FULL_SIMP_TAC (srw_ss()) [invariant_ref_cnt_def])
      THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def]),
    (* The real part of the invariant *)
      (FULL_SIMP_TAC (srw_ss()) [ LET_DEF])
      THEN (FULL_SIMP_TAC (srw_ss()) [invariant_ref_cnt_def])    
      THEN (THM_KEEP_TAC ``invariant_page_type c1' c2' c3' mem pgtype' pgrefs``
			 (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def]))
    (* First we check that the main L1 is still 16kb aligned *)
      THEN (LET_HYP_ELIM_TAC [])
      THEN (FULL_SIMP_TAC (srw_ss()) [])
      THEN (REPEAT STRIP_TAC)
    (* Two goals: one for the L1 pages, one for the L2 pages *)
      THENL [
        (* check l2 pages *)
        l2_unmap_invariant_page_type_l2_tac,
        (* check l1 pages *)
	l2_unmap_invariant_page_type_l1_tac
      ]
    ]
;


(* STUFF FOR SUM *)

val sum_split_1048576_thm = prove(``
!x:bool[32] f.
  (GSUM (0,1048576) f) = (GSUM (0, w2n (x >>> 12) ) f) +  (f (w2n (x >>> 12))) +
   (GSUM (w2n (x >>> 12) + 1, 1048576-w2n (x >>> 12) - 1) f)
``,
   REPEAT (STRIP_TAC)
   THEN (REWRITE_TAC [
   SIMP_RULE (srw_ss()) [thm_temo_1_thm] (
   SPECL [``0:num``,
       ``w2n (x:bool[32] ⋙ 12)``,
       ``1048576 - (w2n (x:bool[32] ⋙ 12))``,
       ``f:num->num``
      ] GSUM_ADD)])
   THEN (REWRITE_TAC [
   SIMP_RULE (srw_ss()) [thm_temo_3_thm] (
   SIMP_RULE (arith_ss) [] (
   SPECL [``w2n (x:bool[32] ⋙ 12)``,
	  ``1:num``,
	  ``1048576 - (w2n (x:bool[32] ⋙ 12)) - 1``,
	  ``f:num->num``
      ] GSUM_ADD))
	])
   THEN (FULL_SIMP_TAC (srw_ss()) [GSUM_1])
   THEN (FULL_SIMP_TAC (arith_ss) [])
);


val sum_split_1024_thm = prove(``
!x:bool[32] f.
  (GSUM (0,1024) f) =
   (GSUM (0, w2n (l2_index:word32 && 1023w) ) f) + 
   (f (w2n (l2_index:word32 && 1023w))) +
   (GSUM (w2n (l2_index:word32 && 1023w) + 1, 1024-w2n (l2_index:word32 && 1023w) - 1) f)
``,
   REPEAT (STRIP_TAC)
   THEN (REWRITE_TAC [
   SIMP_RULE (srw_ss()) [thm_temo_l2_thm] (
   SPECL [``0:num``,
       ``w2n (l2_index:word32 && 1023w)``,
       ``1024 - (w2n (l2_index:word32 && 1023w))``,
       ``f:num->num``
      ] GSUM_ADD)])
   THEN (REWRITE_TAC [
   SIMP_RULE (srw_ss()) [thm_temo_l3_thm] (
   SIMP_RULE (arith_ss) [] (
   SPECL [``w2n (l2_index:word32 && 1023w)``,
	  ``1:num``,
	  ``1024 - (w2n (l2_index:word32 && 1023w)) - 1``,
	  ``f:num->num``
      ] GSUM_ADD))
	])
   THEN (FULL_SIMP_TAC (srw_ss()) [GSUM_1])
   THEN (FULL_SIMP_TAC (arith_ss) [])
);



fun SUM_BY_EQ_FUN_TAC (asl, w) =
    let val (a,b) = dest_eq w
	val (f,arg1) = dest_comb a
	val (f1,arg2) = dest_comb b
	val (f,a) = dest_comb f
	val (a1,a2) = pairLib.dest_pair a
	val tmp_thm = SPECL [a1, a2, arg1, arg2]  GSUM_FUN_EQUAL
	val (t_hyp, t_concl) = dest_imp (concl tmp_thm)
    in
	(SUBGOAL_THEN t_hyp (fn thm =>
      ASSUME_TAC thm
      THEN (ACCEPT_TAC (UNDISCH tmp_thm))
   )) (asl, w)
    end;



(* Check that the references from page tables before the current are  *)
(* unchanged *)
(* line 613 *)
val sum_less_l2_base_unchaged_tac =
    FULL_SIMP_TAC (arith_ss) []
    THEN SUM_BY_EQ_FUN_TAC
    THEN (REPEAT STRIP_TAC)
    THEN (FULL_SIMP_TAC (arith_ss) [Abbr `f`, Abbr `f'`, LET_DEF])
    THEN (ASSUME_TAC (SPECL [
      ``(n2w x):word20``, ``mem:word32->word8``,  ``mem':word32->word8``, ``ph_page:bool[20]``
      ] ref_cnt_equality_thm))
    THEN (SYM_ASSUMPTION_TAC ``unmap_L2_pageTable_entry a b c = x``)
    THEN (Cases_on `pgtype (n2w x) = 0b10w`)
    THENL [
      (* L2 page table *)
      (FULL_SIMP_TAC (srw_ss()) [])
      (* That just to show that n2w x <> w2w (l2_base_add ⋙ 12) *)
      THEN (ASSUME_TAC (
	    SPECL [``(n2w x):word20``, ``l2_base_add:word32``]
		  l2_unmap_unchanged_mem_thm))
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC 
		``x = unmap_L2_pageTable_entry a b c``)
      THEN (ASSUME_TAC (SPECL [``x:num``, `` w2n (l2_base_add:bool[32] ⋙ 12)``] (Thm.INST_TYPE[alpha|->``:20``] Bless_word_thm)))
      THEN (FULL_SIMP_TAC (srw_ss()) [thm_tem_2_thm])
      THEN (ASSUME_TAC (SPEC ``x:num`` thm_tem_1_thm))
      THEN (SIMP_BY_ASSUMPTION_TAC ``x:num < a``)
      THEN (SIMP_BY_ASSUMPTION_TAC ``x:num < 1048576``)
      THEN (FULL_SIMP_TAC (srw_ss()) [GSYM w2w_def])
      THEN (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE ``
(n2w (x:num) <₊ (w2w (l2_base_add ⋙ 12)):bool[20]) ==>
(n2w x ≠ ((w2w (l2_base_add:bool[32] ⋙ 12)):bool[20]))
``])
      THEN METIS_TAC [],
      (* L1 PAGES *)
      (FULL_SIMP_TAC (srw_ss()) [])
      THEN (Cases_on `(pgtype (n2w x) = 1w) ∧ (0xFFFFCw && n2w x = ((n2w x):bool[20]))`)
      THENL [
        (FULL_SIMP_TAC (srw_ss()) [])
	THEN (ASSUME_TAC (
	      SPECL [``(n2w x):bool[20]``, ``l2_base_add:bool[32]``]
		    l2_unmap_unchanged_l1_mem_thm
	     ))
	(* we show that we do not change the page type *)
	THEN (FULL_SIMP_TAC (srw_ss()) [
	  UNDISCH (
	  SPEC ``l2_base_add:bool[32]``
	  l2_unmap_unchanged_pagetype_thm)
	 ])
	THEN (`ph_l1_unchanged (n2w x) pgtype mem mem'` by METIS_TAC [])
	THEN (FULL_SIMP_TAC (srw_ss()) [ph_l1_unchanged_def]),
	(FULL_SIMP_TAC (srw_ss()) [count_pages_for_pt_def])
	THENL [
	  METIS_TAC [],
	  METIS_TAC []
	]
      ]
    ]
;

(* Check that the references from page tables after the current are  *)
(* unchanged *)
val sum_gt_l2_base_unchaged_tac =
    FULL_SIMP_TAC (srw_ss()) []
    THEN SUM_BY_EQ_FUN_TAC
    THEN (REPEAT STRIP_TAC)
    THEN (FULL_SIMP_TAC (arith_ss) [Abbr `f`, Abbr `f'`, LET_DEF])
    THEN (ASSUME_TAC (SPECL [
      ``(n2w x):word20``, ``mem:word32->word8``,  ``mem':word32->word8``, ``ph_page:bool[20]``
      ] ref_cnt_equality_thm))
    THEN (SYM_ASSUMPTION_TAC ``unmap_L2_pageTable_entry a b c = x``)
    THEN (Cases_on `pgtype (n2w x) = 0b10w`)
    THENL [
      (* L2 page table *)
      (FULL_SIMP_TAC (srw_ss()) [])
      (* That just to show that n2w x <> w2w (l2_base_add ⋙ 12) *)
      THEN (ASSUME_TAC (
	    SPECL [``(n2w x):word20``, ``l2_base_add:word32``]
		  l2_unmap_unchanged_mem_thm))
      THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC 
		``x = unmap_L2_pageTable_entry a b c``)
      THEN (ASSUME_TAC (SPECL [`` w2n (l2_base_add:bool[32] ⋙ 12)``, ``x:num`` ] (Thm.INST_TYPE[alpha|->``:20``] Bless_word_thm)))
      THEN (FULL_SIMP_TAC (srw_ss()) [thm_tem_2_thm])
      THEN (`w2n (l2_base_add ⋙ 12) + 1 + (1048575 − w2n (l2_base_add ⋙ 12)) =
             w2n (l2_base_add ⋙ 12) + (1 + (1048575 − w2n (l2_base_add ⋙ 12)))`
		by FULL_SIMP_TAC (arith_ss) [])
      THEN (FULL_SIMP_TAC (arith_ss) [(SPEC ``l2_base_add:bool[32]`` thm_temo_3_thm)])
      THEN (SIMP_BY_ASSUMPTION_TAC ``x:num < a``)
      (* Strange behavior with w2n *)
      THEN (Q.ABBREV_TAC `idx = w2n (l2_base_add ⋙ 12)`)
      THEN (`!a. (a+1 ≤ x) ==> (a < x)` by  (FULL_SIMP_TAC (arith_ss) []))
      THEN (SPEC_ASSUMPTION_TAC ``!a:num  .p`` ``idx:num``)
      THEN (SIMP_BY_ASSUMPTION_TAC ``idx+1 ≤ x:num``)
      THEN (SIMP_BY_ASSUMPTION_TAC ``idx < x:num``)
      THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `idx`])
      THEN (FULL_SIMP_TAC (srw_ss()) [GSYM w2w_def])
      THEN (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE ``
((w2w (l2_base_add ⋙ 12)):bool[20] <₊ n2w (x:num)) ==>
(n2w x ≠ ((w2w (l2_base_add:bool[32] ⋙ 12)):bool[20]))
``])
      THEN (FULL_SIMP_TAC (srw_ss()) [
	  UNDISCH (
	  SPEC ``l2_base_add:bool[32]``
	  l2_unmap_unchanged_pagetype_thm)
	 ]),
      (* L1 PAGES *)
      (FULL_SIMP_TAC (srw_ss()) [])
      THEN (Cases_on `(pgtype (n2w x) = 1w) ∧ (0xFFFFCw && n2w x = ((n2w x):bool[20]))`)
      THENL [
        (FULL_SIMP_TAC (srw_ss()) [])
	THEN (ASSUME_TAC (
	      SPECL [``(n2w x):bool[20]``, ``l2_base_add:bool[32]``]
		    l2_unmap_unchanged_l1_mem_thm
	     ))
	(* we show that we do not change the page type *)
	THEN (`ph_l1_unchanged (n2w x) pgtype mem mem'` by METIS_TAC [])
	THEN (FULL_SIMP_TAC (srw_ss()) [ph_l1_unchanged_def])
        THEN (FULL_SIMP_TAC (srw_ss()) [
	  UNDISCH (
	  SPEC ``l2_base_add:bool[32]``
	  l2_unmap_unchanged_pagetype_thm)
	 ]) ,
	(FULL_SIMP_TAC (srw_ss()) [count_pages_for_pt_def])
	THENL [
	  METIS_TAC [],
	  METIS_TAC []
	]
      ]
    ]
;


fun WEAK_PAT_ASSUME pat =
    (PAT_ASSUM pat (fn thm =>
      ASSUME_TAC (ASSUME ``T``)
      THEN (FULL_SIMP_TAC (bool_ss) [])
    ));


val add_thm = (blastLib.BBLAST_PROVE ``
((w2w (w2w (l2_base_add ⋙ 12) :word20) :word32) ≪ 12 ‖
(w2w (w2w ((1023w :word32) && l2_index) :word10) :word32) ≪ 2) =
(0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2)
	``);
fun sum_l2_base_chaged_pt_change_entry_tac (asl, w) =
    let val add_thm = (blastLib.BBLAST_PROVE ``
((w2w (w2w (l2_base_add ⋙ 12) :word20) :word32) ≪ 12 ‖
(w2w (w2w ((1023w :word32) && l2_index) :word10) :word32) ≪ 2) =
(0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2)
	``)
    in (
    (* first we show that the new counter is always 0 *)
    (`f'l2 (w2n (l2_index && 1023w)) = 0` by
	((Q.UNABBREV_TAC `f'l2`)
	THEN (computeLib.RESTR_EVAL_TAC [``l2_count``])
	THEN (SIMP_TAC(srw_ss())[l2_count_def])
	THEN (SIMP_TAC (srw_ss()) [LET_DEF, GSYM w2w_def, w2w_id])
	(* first we show that we are reading on the same address that has been updated *)
	THEN (REWRITE_TAC [add_thm])
	THEN (REABBREV_TAC)
	THEN (SYM_ASSUMPTION_TAC ``write_mem32 a = mem'``)
	THEN (ASSUME_TAC (
	     UNDISCH (
	     SPECL [``entryAddrL2:bool[32]``,
		    ``0xFFFFFFFCw && page_desc:bool[32]``,
		    ``mem:bool[32]->bool[8]``,
		    ``mem':bool[32]->bool[8]``]
	     write_read_thm)))
	THEN (FULL_SIMP_TAC (srw_ss()) []))
    )
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THEN (Q.UNABBREV_TAC `fl2`)
    THEN (computeLib.RESTR_EVAL_TAC [``l2_count``, ``count_pages``])
    THEN (SIMP_TAC(srw_ss())[l2_count_def])
    THEN (SIMP_TAC (srw_ss()) [LET_DEF, GSYM w2w_def, w2w_id])
    THEN (REWRITE_TAC [add_thm])
    THEN (REABBREV_TAC)
    THEN (Cases_on `(rec'l2SmallT page_desc).addr = ph_page`)
    THENL [
      (* first case: th updated entry was pointing to the page we are counting: ph_page *)
       (Q.ABBREV_TAC `no_spit = ((3w && page_desc = 2w) ∨ (3w && page_desc = 3w))`)
       THEN (FULL_SIMP_TAC(srw_ss())[])
       THEN (Cases_on`(rec'l2SmallT page_desc).ap <> 3w`)
       THENL [
         (* first case: the page not mapped *)
         (FULL_SIMP_TAC(srw_ss())[])
	 THEN (METIS_TAC []),
	 (* first case: the page was mapped *)
	 (FULL_SIMP_TAC(srw_ss())[])
	 THEN (ASSUME_TAC (SPECL [
			   ``pgrefs:word20->word30``,
			   ``ph_page:word20``,
			   ``((pgrefs:word20->word30) (ph_page:word20)) + (-1w:word30)``,
			   ``ph_page:word20``]
  	      (INST_TYPE[alpha|->``:word20``,beta|->``:word30``]combinTheory.APPLY_UPDATE_THM)))
	 THEN (FULL_SIMP_TAC(srw_ss())[])
	 THEN (SYM_ASSUMPTION_TAC ``(ph_page =+ pgrefs ph_page + -1w) pgrefs = pgrefs'``)
	 THEN (FULL_SIMP_TAC(srw_ss())[])
	 (* to continue we need to show that *)
	 (* - (pgrefs ph_page + -1w) + 1 = (pgrefs ph_page + -1w + 1w) *)
	 (* that is possible if *)
         (* - (pgrefs ph_page) <> 0w *)  
         (* - based on the fact that count page is bigger than 0 *)  
	 THEN (`count_pages mem pgtype ph_page > 0` by 
	    (* same staff for the external sum *)
	    ((FULL_SIMP_TAC (srw_ss()) [count_pages_def])
	    THEN (Q.ABBREV_TAC `f = (λit.(let global_page = n2w it in count_pages_for_pt mem pgtype ph_page global_page))`)
	    THEN (ASSUME_TAC (SPECL [``f:num->num``, ``1048576:num``, ``w2n(l2_base_add:bool[32] ⋙ 12)``] SUM_GT_thm))
	    THEN (FULL_SIMP_TAC (arith_ss) [SIMP_RULE (srw_ss()) [] thm_tem_2_thm])
	    THEN (SUBGOAL_THEN ``f (w2n ((l2_base_add:bool[32]) ⋙ 12)) > 0:num``
  	         (fn thm => ASSUME_TAC thm
		            THEN METIS_TAC []))
	    THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `f`])
	    THEN (SIMP_TAC (srw_ss()) [count_pages_for_pt_def])
	    THEN (SIMP_TAC (srw_ss()) [GSYM w2w_def])
	    THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
	    (* same staff for the internal sum *)
	    THEN (Q.ABBREV_TAC `f = (λit. l2_count it mem pgtype ph_page (w2w (l2_base_add ⋙ 12)))`)
	    THEN (ASSUME_TAC (SPECL [``f:num->num``, ``1024:num``, ``(w2n (1023w && l2_index:bool[32]))``] SUM_GT_thm))
  	    THEN (FULL_SIMP_TAC (arith_ss) [SIMP_RULE (srw_ss()) [] thm_tem_l2_thm])
	    THEN (SUBGOAL_THEN ``f (w2n (1023w && l2_index:bool[32])) > 0:num``
  	         (fn thm => ASSUME_TAC thm
		            THEN METIS_TAC []))
	    THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `f`])
	    THEN (SIMP_TAC (srw_ss()) [l2_count_def])
	    THEN (SIMP_TAC (srw_ss()) [LET_DEF, GSYM w2w_def, w2w_id])
	    THEN (REWRITE_TAC [add_thm])
	    THEN (REABBREV_TAC)
	    THEN (FULL_SIMP_TAC (srw_ss()) [])
	      ))
	 (* now we know that the counter is bigger than 0,  *)
	 THEN ASSUME_TAC (
	    SPEC ``(pgrefs:bool[20]->bool[30]) ph_page`` non_zero_no_overflow_thm
	 )
	 THEN (`w2n (pgrefs ph_page) > 0` by METIS_TAC [])
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
	 THEN (FULL_SIMP_TAC (arith_ss) [])
       ],
      (* second case: th updated entry was not pointing to the page we are counting: ph_page *)
       (Q.ABBREV_TAC `no_spit = ((3w && page_desc = 2w) ∨ (3w && page_desc = 3w))`)
       THEN (FULL_SIMP_TAC(srw_ss())[])
    (* We are checking a page that is not pointed by the new entry *)
       THEN (SYM_ASSUMPTION_TAC ``f = pgrefs':bool[20]->bool[30]``)
       THEN (FULL_SIMP_TAC (srw_ss()) [])
       THEN (FULL_SIMP_TAC (srw_ss()) [COND_RATOR])
       THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM])
    ]
    )(asl, w)
    end;


val sum_l2_base_chaged_pt_unchanged_entry_tac =
    FULL_SIMP_TAC (arith_ss) []
    THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `fl2`, Abbr `f'l2`])
    THEN (ASSUME_TAC (SPECL [
		      ``l2_base_add:bool[32]``,
		      ``(n2w x):word10``, ``mem:word32->word8``,  ``mem':word32->word8``,
		      ``pgtype:bool[20]->bool[2]``,
		      ``ph_page:bool[20]``
      ] ref_cnt_equality_l2_thm))
    THEN (SIMP_BY_ASSUMPTION_TAC ``pgtype (w2w (l2_base_add ⋙ 12)) = 2w``)
    THEN (ASSUME_TAC (SPECL [``(n2w x):word10``,
			     ``c1':sctlrT``,
			     ``pgtype:bool[20]->bool[2]``
			    ]  l2_unmap_unchanged_l2_mem_thm))
    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x =  unmap_L2_pageTable_entry y l2_base_add l2_index``)

    THEN (ASSUME_TAC (SPECL [``x:num``, `` w2n (1023w && l2_index:bool[32])``]
			    (Thm.INST_TYPE[alpha |-> ``:10``] (GSYM wordsTheory.n2w_11))))
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THEN (ASSUME_TAC (SPEC ``x:num`` thm_tem_xxx_thm))
    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x:num < 1024``)
    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``x:num ≠ w2n (1023w && l2_index)``)
    THEN (RES_TAC)
    THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``l2_entry_unchanged l2_base_add (n2w x) mem mem'``)
    THEN (FULL_SIMP_TAC (srw_ss()) [])
;




(* old line 523*)
(* Manage the changed page table (l2) *)
val sum_l2_base_chaged_pt_tac =
    (WEAK_PAT_ASSUME ``GSUM (a, b) f = GSUM (a, b) f'``)
    THEN (WEAK_PAT_ASSUME ``GSUM (a, b) f = GSUM (a, b) f'``)
    THEN (WEAK_PAT_ASSUME ``(a = b) = (c = d) ``)
    THEN (Q.UNABBREV_TAC `f`)
    THEN (Q.UNABBREV_TAC `f'`)
    THEN (computeLib.RESTR_EVAL_TAC [``count_pages_for_pt``, ``count_pages`` ])
    THEN (SYM_ASSUMPTION_TAC ``unmap_L2_pageTable_entry a b c = x``)
    (* we show that we do not change the page type *)
    THEN (FULL_SIMP_TAC (srw_ss()) [
	  UNDISCH (
	  SPEC ``l2_base_add:bool[32]``
	  l2_unmap_unchanged_pagetype_thm)
	 ])
    THEN (SIMP_TAC (srw_ss()) [count_pages_for_pt_def])
    (* we know that the page is a valid l2 *)
    THEN (FULL_SIMP_TAC (srw_ss()) [GSYM w2w_def])
    (* First we manage the case where we are checking a non-updated page, since in this case all the sum are the same *)
    THEN (Cases_on `pgtype ph_page <> 0w`)
    THENL [
    (* if the page type is not zero, we have to show that we have not changed the pgref *)
       (FULL_SIMP_TAC (srw_ss()) [])
       (* We reinstantiate the invariant for the page checked: *)
	(* ph_page, to show that the access permission can not be 3w*)
	THEN (THM_KEEP_TAC ``invariant_page_type c1' c2' c3' mem pgtype pgrefs``
			   (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def]))
    (* we instantiate the invariant with the current l2 *)
    (* to show that the access permission was not 3 if the ph_page was pointed *)
	THEN (LET_HYP_ELIM_TAC [])
	THEN (FULL_SIMP_TAC (srw_ss()) [])
        THEN (SPEC_ASSUMPTION_TAC ``!ph_page:bool[20].p`` ``(w2w (l2_base_add:bool[32] ⋙ 12)):bool[20]``)
	THEN (FULL_SIMP_TAC (srw_ss()) [])
        THEN (SIMP_BY_ASSUMPTION_TAC ``pgtype (w2w (l2_base_add ⋙ 12)) = 2w``)
	THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_l2_def])
	THEN (SPEC_ASSUMPTION_TAC ``!idx:bool[10].p`` ``w2w(1023w && (l2_index:bool[32])):bool[10]``)
        THEN (FULL_SIMP_TAC (srw_ss()) [
	      blastLib.BBLAST_PROVE ``((w2w (w2w ((l2_base_add :word32) ⋙ (12 :num)) :word20) :
                   word32) ≪ (12 :num)) = ((0xFFFFF000w :word32) && (l2_base_add :word32))``
				    ])
        THEN (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE
	      ``(w2w(w2w ((1023w :word32) && (l2_index :word32)):word10) :word32) ≪ (2 :num) = 
	      ((4092w :word32) && (l2_index :word32) ≪ (2 :num))``])
        THEN (REABBREV_TAC)
	THEN (LET_HYP_ELIM_TAC [])
	(* trick to prevent the split in full simp tac *)
	THEN (Q.ABBREV_TAC `no_split = (3w && page_desc = 2w) ∨ (3w && page_desc = 3w)`)
	THEN (Q.ABBREV_TAC `no_split2 = 
       (3w && page_desc = 0w) ∨
       no_split ∧
       if pgtype (rec'l2SmallT page_desc).addr ≠ 0w then
         ((rec'l2SmallT page_desc).ap = 1w) \/
         ((rec'l2SmallT page_desc).ap = 2w)
       else
         ((rec'l2SmallT page_desc).ap = 1w) \/
         ((rec'l2SmallT page_desc).ap = 2w) ∨
         ((rec'l2SmallT page_desc).ap = 3w)`)
	THEN (Cases_on `ph_page <> (rec'l2SmallT page_desc).addr`)
	THENL [
	   (* We are checking a page that is not pointed by the new entry *)
	   (SYM_ASSUMPTION_TAC ``f = pgrefs':bool[20]->bool[30]``)
	   THEN (FULL_SIMP_TAC (srw_ss()) [])
	   THEN (FULL_SIMP_TAC (srw_ss()) [COND_RATOR])
	   THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.APPLY_UPDATE_THM]),
	   (* We are cheching a page that is pointed by the new entry *)
	   (FULL_SIMP_TAC (srw_ss()) [])
	   THEN (ASSUME_TAC (blastLib.BBLAST_PROVE ``((3w && page_desc = 2w) ∨ (3w && page_desc = 3w)) ==>
	   ~(3w && page_desc:bool[32] = 0w)``
   	   ))
           THEN (REABBREV_TAC)
           THEN (SIMP_BY_ASSUMPTION_TAC ``no_split``)
           THEN (SIMP_BY_ASSUMPTION_TAC ``(3w && page_desc) ≠ 0w``)
	   THEN (SIMP_BY_ASSUMPTION_TAC ``pgtype (rec'l2SmallT page_desc).addr ≠ 0w``)
	   THEN (Q.UNABBREV_TAC `no_split2`)
	   THEN (ASSUME_TAC (UNDISCH (blastLib.BBLAST_PROVE ``
	     (((rec'l2SmallT page_desc).ap = 1w) ∨ ((rec'l2SmallT page_desc).ap = 2w)) ==>
	     ((rec'l2SmallT page_desc).ap <> 3w:bool[3])
	     ``)))
	   THEN METIS_TAC []
	],
       (* We are checking a data page that has been unmapped, so the new counter is 0 and the previous one was 1*)
         (FULL_SIMP_TAC (srw_ss()) [])
	 THEN (Q.ABBREV_TAC` fl2 = (λit. l2_count it mem pgtype ph_page (w2w (l2_base_add ⋙ 12)))`)
	 THEN (Q.ABBREV_TAC` f'l2 = (λit. l2_count it mem' pgtype ph_page (w2w (l2_base_add ⋙ 12)))`)
	 THEN (FULL_SIMP_TAC (srw_ss()) [SUM])
	 THEN (SPLITTER_SUM_TAC
		   ``w2n (l2_index:bool[32] && 1023w)``
		   ``1024:num``
		   ``fl2:num->num``
		   ``f'l2:num->num``
		   ``w2n ((pgrefs':bool[20]->bool[30]) ph_page)``
		   ``count_pages mem pgtype ph_page``)
	 THENL [
	    sum_l2_base_chaged_pt_change_entry_tac,
	    sum_l2_base_chaged_pt_unchanged_entry_tac,
	    REWRITE_TAC [blastLib.BBLAST_PROVE (``1023w && l2_index:bool[32] = l2_index:bool[32] && 1023w``)]
            THEN (ASSUME_TAC thm_tem_l2_thm)
	    THEN FULL_SIMP_TAC (srw_ss()) []
	 ]
    ]
;







(* REF COUNTER *)
val l2_unmap_invariant_ref_cnt_tac =
    (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
(* To prove the goal we use the invariant and then we compare the *)
(*two sum *)
    THEN (FULL_SIMP_TAC (srw_ss()) [invariant_ref_cnt_def])
    THEN (SPEC_ASSUMPTION_TAC ``!x:word20. X`` ``(ph_page):word20``)
(* We trasform the goal into the goal *)
(* pgrefs'  + count_pages = pgrefs  + count_pages' *)
    THEN (SUBGOAL_THEN ``
(w2n ((pgrefs' :word20 -> word30) (ph_page :word20)) +
 count_pages (mem :word32 -> word8) (pgtype :word20 -> word2) ph_page) =
(w2n ((pgrefs :word20 -> word30) (ph_page :word20)) +
count_pages (mem' :word32 -> word8) (pgtype' :word20 -> word2) ph_page)
``
 (fn thm =>
  ASSUME_TAC thm
  THEN (SIMP_BY_ASSUMPTION_TAC ``pgtype:bool[20]->bool[2] = pgtype'``)
  THEN (SIMP_AND_KEEP_BY_ASSUMPTION_TAC ``w2n (pgrefs ph_page) = count_pages mem pgtype' ph_page``)
  THEN (FULL_SIMP_TAC (arith_ss) [arithmeticTheory.EQ_ADD_RCANCEL])
))
    (* we restructure the sums to split in several intervals *)
    THEN (SIMP_TAC (srw_ss()) [count_pages_def])
    THEN (Q.ABBREV_TAC `f = (λit.
           (let global_page:bool[20] = n2w it in
           count_pages_for_pt mem pgtype ph_page global_page))`)
    THEN (Q.ABBREV_TAC `f' = (λit.
           (let global_page:bool[20] = n2w it in
           count_pages_for_pt mem' pgtype' ph_page global_page))`)
    THEN (SIMP_TAC (srw_ss()) [SUM])   

    (* We split the sum in three section < l2, l2, > l2 *)
    THEN (REWRITE_TAC [
	  SPECL [``l2_base_add:bool[32]``, ``f:num->num``]
         sum_split_1048576_thm])
    (* first we show that the pages < l2 has not been changed *)
    THEN (`(GSUM (0,w2n (l2_base_add ⋙ 12)) f) =
           (GSUM (0,w2n (l2_base_add ⋙ 12)) f')`
	      by (
	        sum_less_l2_base_unchaged_tac
	      )
	 )
    (* Simplify knowing that a = a' *)
    THEN (FULL_SIMP_TAC (arith_ss) [])
    (* Simplify knowing that a+b = c+a *)
    THEN ADD_CANC_TAC
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    (* first we show that the pages > l2 has not been changed *)
    THEN (`GSUM (w2n (l2_base_add ⋙ 12) + 1,1048575 − w2n (l2_base_add ⋙ 12)) f =
           GSUM (w2n (l2_base_add ⋙ 12) + 1,1048575 − w2n (l2_base_add ⋙ 12)) f'`
	      by (
	        (* FULL_SIMP_TAC (srw_ss()) [] *)
	        sum_gt_l2_base_unchaged_tac
	      )
	 )
    (* Simplify knowing that a = a' *)
    THEN (FULL_SIMP_TAC (arith_ss) [])
    (* Simplify knowing that a+b = c+a *)
    THEN ADD_CANC_TAC
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    (* We reach the l1 page actually changed *)
    THEN sum_l2_base_chaged_pt_tac
;

(* MAIN THEOREM *)
val l2_unmap_goal = ``
  ! c1 va:word32. 
    (c1 = rec'sctlrT 1w ) ==>
    ((invariant_ref_cnt c1 c2 c3 mem pgtype pgrefs) ==>
       (let (c1',c2',c3',mem',pgtype', pgrefs') = 
       (unmap_L2_pageTable_entry (c1, c2, c3, mem, pgtype, pgrefs) (l2_base_add:bool[32]) (l2_index:bool[32])) in
       (invariant_ref_cnt c1' c2' c3' mem' pgtype' pgrefs')))
``;


val let_ss = simpLib.mk_simpset [boolSimps.LET_ss] ;


val l2_unmap_thm = Q.store_thm("l2_unmap_thm",
  `^l2_unmap_goal`,
  (RW_TAC (srw_ss()) [])
(* Before opening the invariant to check, we handle the failures *)
  THEN l2_unmap_exception_case_tac
  THEN (RW_TAC (srw_ss()) [invariant_ref_cnt_def])
  THENL [
(* Split for the two main part of the invariant *)
    l2_unmap_invariant_page_type_tac,
    l2_unmap_invariant_ref_cnt_tac
  ]
);


val _ = export_theory ();


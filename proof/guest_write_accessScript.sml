(* 
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/spec/" :: !loadPath;
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/" ::  !loadPath;
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/proof/" :: !loadPath;
load "mmu_utilsLib";
load "hypervisor_modelTheory";
load "mmu_propertiesTheory";
load "helperTheory";
*)

open HolKernel boolLib bossLib Parse wordsLib;

open hypervisor_modelTheory;
open mmu_propertiesTheory;
open tinyTheory;
open helperTheory;
open guest_write_access_utilsTheory;

load "mmu_utilsLib";
open mmu_utilsLib;

load "guest_write_accessLib";
open guest_write_accessLib;

val _ = new_theory "guest_write_access";

val let_ss = simpLib.mk_simpset [boolSimps.LET_ss] ;

(*
Main theorem
*)


val mmu_safe_section_tac = 
(* We must prove that the pointed phisical page is not *)
(* writable or is different from ph_add *)
(* Trick otherwise we lost that l1_decode_type is 2 *)
        UNABBREV_ALL_TAC
   THEN (FULL_SIMP_TAC (srw_ss()) [mmu_byte_section_def])
   THEN (Q.ABBREV_TAC `l1_add = mmu_tbl_base_addr c2 ‖ mmu_tbl_index add' ≪ 2`)
   THEN BasicProvers.LET_ELIM_TAC
   THEN (FULL_SIMP_TAC (srw_ss()) [])
   THEN INSTANTIATE_INVARIANT_CURRENT_L1_TAC
(* We use the L1 invariant to verify the section *)
   THEN (INSTANTIATE_INVARIANT_CURRENT_L1_FOR_ADD_TAC ``add':bool[32]``)
(* We use the assumption of this case: we are checking a logical *)
(* address that is mapped by a section *)
   THEN REABBREV_TAC
   THEN (FULL_SIMP_BY_ASSUMPTION_TAC ``mmu_l1_decode_type a = 2w``)
   THEN (LET_HYP_ELIM_TAC [])
   THEN (FULL_SIMP_BY_ASSUMPTION_TAC ``1w && read_mem32 (l1_add,mem) ⋙ 18 = 0w``)
   THEN (LET_HYP_ELIM_TAC [])
   THEN (Q.ABBREV_TAC `sec_desc = rec'l1SecT (read_mem32 (l1_add,mem))`)
   THEN (REPEAT (LET_HYP_ELIM_TAC []))
   THEN (SPEC_ASSUMPTION_TAC ``!x:word32. X``  ``add':word32``)
   THEN (Cases_on `ph_add <> (0xFFFFFw && add':bool[32] ‖
			   w2w (sec_desc:l1SecT).addr  ≪ 20)`)
(* two cases according if the selected address point to the checked *)
(* phisical address *)
   THENL [
     (RW_TAC (srw_ss()) []),
(* second case, the selected address points to the
checked phisical address *)
          (FULL_SIMP_TAC (srw_ss()) [])
(* we are interested only in the write access *)
     THEN (DISJX_TAC 3)
     THEN (FULL_SIMP_TAC (srw_ss()) [mmu_check_access_def])
     THEN (SPEC_ASSUMPTION_TAC ``!dom:bool[4].p`` ``sec_desc:l1SecT.dom``)
     THEN (FULL_SIMP_TAC (srw_ss()) [])
(* two cases according if the domain is enabled or not *)
     THENL [
        (FULL_SIMP_TAC (srw_ss()) []),
  	       (FULL_SIMP_TAC (srw_ss()) [])
         THEN (REPEAT (LET_HYP_ELIM_TAC []))
         THEN (FULL_SIMP_BY_ASSUMPTION_TAC ``
          (pgtype :word20 -> word2) (w2w
          ((255w :word32) && (add' :word32) ⋙ (12 :num) ‖
               (w2w (sec_desc :l1SecT).addr :word32) ≪ (20 :num) ⋙ (12 :
               num)) :word20) ≠ (0w :word2)
	  ``)
         THEN (FULL_SIMP_TAC (srw_ss()) [mmu_decode_l1_ap_def])
	 THEN (UNDISCH_MATCH_TAC ``a =  (sup',rd',wt,ex')``)
	 THEN EVAL_TAC
	 THEN (FULL_SIMP_TAC (srw_ss()) [])
     ]
   ]
;

val mmu_safe_supersection_tac = 
(* We must prove that this case is impossible *)
(* since we do not allow supersections *)
(* supersection: we know that our invariant prevent this settings *)
(* Trick otherwise we lost that l1_decode_type is 2 *)
        UNABBREV_ALL_TAC
   THEN (FULL_SIMP_TAC (srw_ss()) [])
   THEN INSTANTIATE_INVARIANT_CURRENT_L1_TAC
(* We use the L1 invariant to verify the supersection *)
   THEN (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_l1_def])
   THEN (INSTANTIATE_INVARIANT_CURRENT_L1_FOR_ADD_TAC ``add':bool[32]``)
   THEN (FULL_SIMP_BY_ASSUMPTION_TAC ``mmu_l1_decode_type a = 2w``)
   THEN (LET_HYP_ELIM_TAC [])
   THEN (REPEAT (LET_HYP_ELIM_TAC []))
   THEN (Q.ABBREV_TAC `l18bit = read_mem32 (mmu_tbl_base_addr c2 ‖ mmu_tbl_index add' ≪ 2,mem) ⋙  18`)
   THEN (FULL_SIMP_BY_BLAST_TAC ``(1w && l18bit:bool[32] = 1w) ==> ~(1w && l18bit:bool[32] = 0w)``)
;

val mmu_safe_small_page_tac = 
(* trick to prevent full simp to diverge *)
         (Q.ABBREV_TAC `is_small_page = (l2_type = 2w) ∨ (l2_type = 3w)`)
   THEN (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def])
(* We chech first the case where the logical selected address does not
point to the protected phisical address *)
   THEN (Cases_on `(4095w && add' ‖ w2w (rec'l2SmallT l2_desc).addr ≪
			  12) <> ph_add:bool[32]`)
   THENL [
(* case of logical address pointing to a different phisical address *)
           BasicProvers.LET_ELIM_TAC
      THEN EVAL_TAC
      THEN (FULL_SIMP_TAC (bool_ss) []),
(* case of logical address pointing to the phisical address *)
           (FULL_SIMP_TAC (srw_ss()) [])
      THEN (FULL_SIMP_TAC (srw_ss()) [mmu_check_access_def])
      THEN (SPEC_ASSUMPTION_TAC  ``!dom:bool[4].p`` ``(rec'l1PTT l1_desc).dom``)
      THEN (FULL_SIMP_TAC (srw_ss()) [])
      THENL [
(* case domain disabled *)
         EVAL_TAC,
(* case domain enabled *)
(* check that pgtype (rec'l2SmallT l2_desc).addr ≠ 0w *)
	 (FULL_SIMP_TAC (srw_ss()) [rec'l2SmallT_def])
	  THEN (FULL_SIMP_TAC (srw_ss()) [
	    UNDISCH (
	    blastLib.BBLAST_PROVE ``
		    ((4095w :word32) && (add' :word32) ‖
       (w2w (((31 :num) >< (12 :num)) (l2_desc :word32) :word20) :
          word32) ≪ (12 :num) =
       (ph_add :word32))
==>
(
(((31 :num) >< (12 :num)) (l2_desc :word32) :word20) =
(w2w ((ph_add :word32) ⋙ (12 :num)) :word20)
)``)])
     THEN (FULL_SIMP_BY_ASSUMPTION_TAC ``pgtype (w2w (ph_add ⋙ 12)) ≠ 0w``)
     THEN (FULL_SIMP_TAC (srw_ss()) [mmu_decode_l1_ap_def])
     THEN EVAL_TAC
      ]
   ]
;


val mmu_safe_pt_tac = 
(* Page table *)
(* check that the page descriptor is a pt *)
        UNABBREV_ALL_TAC
   THEN (Q.ABBREV_TAC `l1_add = mmu_tbl_base_addr c2 ‖ mmu_tbl_index add' ≪ 2`)
   THEN (Q.ABBREV_TAC `l1_desc = (read_mem32 (l1_add,mem))`)
   THEN (`(mmu_l1_decode_type l1_desc = 1w)`  by (       
	      (SIMP_TAC (srw_ss()) [])
	 THEN (ASSUME_TAC (SPEC ``l1_desc:bool[32]`` mmu_l1_decode_type_values_thm))
	 THEN (ASSUME_TAC (SPEC ``l1_desc:bool[32] ⋙ 18``
	      mmu_l1_18bit_values_thm))
	 THEN (METIS_TAC []))
	)
(* Now we know that the l1_decode_type is 1w *)
   (* THEN RES_TAC *)
   THEN (FULL_SIMP_TAC (srw_ss()) [])
(* We need to verify that the used l2 is marked as l2, using the *)
(* invariant on the l1*)
   THEN INSTANTIATE_INVARIANT_CURRENT_L1_TAC
   THEN (INSTANTIATE_INVARIANT_CURRENT_L1_FOR_ADD_TAC ``add':bool[32]``)
   THEN REABBREV_TAC
   THEN (SIMP_BY_ASSUMPTION_TAC ``mmu_l1_decode_type l1_desc = 1w``)
   THEN (REPEAT (LET_HYP_ELIM_TAC []))
   THEN (FULL_SIMP_TAC (bool_ss) [])
(* Now we can check the pointed l2 *)
   THEN (SIMP_TAC (bool_ss) [mmu_byte_pt_def])
   THEN (FULL_SIMP_BY_ASSUMPTION_TAC ``¬(x:l1PTT).pxn``)
   THEN (Q.ABBREV_TAC `l2_base:bool[22] = (rec'l1PTT l1_desc).addr`)
   THEN (FULL_SIMP_TAC (bool_ss) [mmu_byte_l2_def])
(* Reinstantiate the invariant for the L2 *)
   THEN (INSTANTIATE_INVARIANT_L2_TAC
	     ``(w2w (l2_base:bool[22] ⋙ 2)):bool[20]``)
   THEN (INSTANTIATE_INVARIANT_L2_FOR_ADD_TAC
	     ``l2_base:bool[22]``
	     ``add':bool[32]``)
   THEN (Q.ABBREV_TAC `l2_add = w2w l2_base ≪ 10 ‖ 1020w && add' ⋙ 12 ≪ 2`)
   THEN BasicProvers.LET_ELIM_TAC
   THEN (REPEAT IF_CASES_TAC)
   (* UNMAPPED PAGE, LARGE PAGE, SMALL PAGE *)
   THENL [
       (* UNMAPPED PAGE *)
       (FULL_SIMP_TAC (srw_ss()) []),
       (* large page, we found a contradiction on the hypotesis *)
       (FULL_SIMP_BY_BLAST_TAC ``~((1w:bool[32] = 2w) ∨ (1w:bool[32] = 3w))``),
       mmu_safe_small_page_tac
   ]
;


val goal_guest_write_access = ``
     ! c1 ph_add ph_frame.
       (c1 = rec'sctlrT 1w ) ==>
       (ph_frame = w2w(ph_add >>> 12):bool[20]) ==>
       ((pgtype ph_frame) <> 0b00w) ==>
       (invariant_ref_cnt c1 c2 c3 mem pgtype pgrefs) ==>
         (let (und,rd,wt,ex) = 
           (mmu_phi_byte c1 c2 c3 mem F ph_add) in
	       ~wt)
``;

val guest_write_access_thm = Q.store_thm(
   "guest_write_access_thm",
   `^goal_guest_write_access`,
  (
          (RW_TAC (bool_ss) [invariant_ref_cnt_def, rec'sctlrT_def])
     THEN (FULL_SIMP_TAC (srw_ss()) [mmu_phi_byte_def])
(* We open the external let, without expending the
internals that generate  existential quantification *)
     THEN (SIMP_TAC (srw_ss()) [Ntimes LET_DEF 4])
(* We are interested only in the wr case *)
     THEN (DISJX_TAC 3)
     THEN (STRIP_TAC)
     THEN BasicProvers.LET_ELIM_TAC
(* We open the mmu_byte_def *)
     THEN (SIMP_TAC (srw_ss()) [mmu_byte_def])
     THEN (RW_TAC (srw_ss()) [])
     THEN (REPEAT IF_CASES_TAC)
(* 5 cases: unmapped, unmapped, section, supersection, pt *)
     THENL [
        (FULL_SIMP_TAC (srw_ss()) []),
	(FULL_SIMP_TAC (srw_ss()) []),
	mmu_safe_section_tac,
	mmu_safe_supersection_tac,
	mmu_safe_pt_tac
     ]
)
);

val _ = export_theory ();

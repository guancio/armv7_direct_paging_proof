open HolKernel boolLib bossLib Parse wordsLib;
open hypervisor_modelTheory;
open mmu_propertiesTheory;
open tinyTheory;
open helperTheory;
open sum_numTheory;

(*
Common tactics
*)
structure helperLib :> helperLib =
struct

fun WRITE_READ_UNCH_RULE others wr_add rd_add value mem mem1 =
    let val goal = 
	    SPECL [wr_add, rd_add, value, mem, mem1]
		  write_read_unch_thm
	val ([], prop) = dest_thm goal
	val (hyp, concl) = dest_imp prop
	val hyp_thm = blastLib.BBLAST_PROVE ``^others ==> (^hyp)``
    in
	DISCH_ALL (MP goal (UNDISCH hyp_thm))
    end;

fun SPLITTER_SUM_TAC idx n f1 f2 a b (asl, w)  =
    let val thm = (SPECL [idx, n, f1, f2, a, b] sum_splitter_thm)
	val thm_concl = concl thm
	val (a,b) = dest_imp thm_concl
    in
	((SUBGOAL_THEN a (
	  fn thm1 => ASSUME_TAC thm1
		    THEN (ASSUME_TAC (UNDISCH thm))
		    THEN (FULL_SIMP_TAC (arith_ss) [])
	))
        THEN (REPEAT STRIP_TAC))
        (asl, w)
    end;

fun SUM_BY_EQ_FUN_TAC (asl, w) =
    let val (a,b) = dest_eq w
	val (f,arg1) = dest_comb a
	val (f1,arg2) = dest_comb b
	val (f,a) = dest_comb f
	val (a1,a2) = pairLib.dest_pair a
	val tmp_thm = SPECL [a1, a2, arg1, arg2]  GSUM_FUN_EQUAL
	val (t_hyp, t_concl) = dest_imp (concl tmp_thm)
    in
	(SUBGOAL_THEN t_hyp (fn thm =>
      ASSUME_TAC thm
      THEN (ACCEPT_TAC (UNDISCH tmp_thm))
   )) (asl, w)
    end;

end

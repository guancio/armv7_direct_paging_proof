loadPath := "/NOBACKUP/workspaces/hnnemati/mmu/experiments_mmu/l3/" :: !loadPath; 

open tinyTheory;
open mmu_utilsTheory;
open wordsLib;
open example_mmu_proofTheory;



(* ALL DOMAIN DISABLED *)
val mmu_supported_def = Define `
  mmu_supported c1 c2 c3 mem =
  ! PL1 add.
    (let (u,rd,wt,ex,pa) = mmu_byte (c1,c2,c3,mem,PL1,add)
     in u)
`;


(* DOMAIN DISABLED, MEMORY UNCHANGED *)

val domain_disabled = prove(``
   let domain_status = mmu_domain_status(0w,d) in
   domain_status = 0w
``,
   (FULL_SIMP_TAC (srw_ss()) [mmu_domain_status_def])
   THEN EVAL_TAC THEN blastLib.BBLAST_TAC);


val goal = ``!ph_add.
  mmu_supported (rec'sctlrT 1w) c2 0w mem ==>
  (
  let (u,rd,wt,ex) = mmu_phi_byte (rec'sctlrT 1w) c2 0w mem PL1 ph_add in
      ~wt
  )
``;

g `^goal`;
e (REPEAT GEN_TAC);
e (REPEAT STRIP_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_phi_byte_def]);
e (computeLib.RESTR_EVAL_TAC [``mmu_byte`` ]);
e (REPEAT STRIP_TAC);
e (Q.PAT_UNDISCH_TAC `f b`);
e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_def]);
e (FULL_SIMP_TAC (srw_ss()) [rec'sctlrT_def]);
e BasicProvers.LET_ELIM_TAC;

(* PAGE FAULT *)
e (Cases_on `l1_type = 0w`);
e (FULL_SIMP_TAC (srw_ss()) []);

(* PAGE FAULT *)
e (FULL_SIMP_TAC (srw_ss()) []);
e (Cases_on `l1_type = 3w`);
e (FULL_SIMP_TAC (srw_ss()) []);

(* SECTION *)
e (FULL_SIMP_TAC (srw_ss()) []);
e (Cases_on `(l1_type = 2w) /\ (l1_bit_18 = 0w)`);
e (FULL_SIMP_TAC (srw_ss()) []);
e (SIMP_TAC (srw_ss()) [mmu_byte_section_def]);
e (FULL_SIMP_TAC (srw_ss()) [SIMP_RULE std_ss [LET_DEF] domain_disabled]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_check_access_def]);
e EVAL_TAC;

(* SUPER SECTION *)
e (FULL_SIMP_TAC (srw_ss()) []);
r 1;
e (ASSUME_TAC (UNDISCH(
blastLib.BBLAST_PROVE(
``
(l1_bit_18 <> 0w) ==>
(l1_bit_18 = (1w && (l1_desc:bool[32]) ⋙ 18)) ==>
(l1_bit_18 = 1w)
``
))));
e (Q.UNABBREV_TAC `l1_bit_18`);
e (FULL_SIMP_TAC (srw_ss()) []);

e (Cases_on `l1_type = 2w`);
e (FULL_SIMP_TAC (srw_ss()) []);
e (SIMP_TAC (srw_ss()) [mmu_byte_supsection_def]);
e (FULL_SIMP_TAC (srw_ss()) [SIMP_RULE std_ss [LET_DEF] domain_disabled]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_check_access_def]);
e EVAL_TAC;
    


(* PAGE *)
e (FULL_SIMP_TAC (srw_ss()) []);

e (SIMP_TAC (srw_ss()) [mmu_byte_pt_def]);

(* I try (rec'l1PTT l1_desc).pxn and then verify that is a contradiction *)
e (Cases_on `(rec'l1PTT l1_desc).pxn`);
e (FULL_SIMP_TAC (srw_ss()) []);
e (Q.PAT_UNDISCH_TAC `mmu_supported a b c d`);

e (FULL_SIMP_TAC (srw_ss()) [mmu_supported_def]);

e(SUBGOAL_THEN ``~((add':word32) <> (ph_add:word32)) ==> ~(∀PL1 add.
   (let (u,rd,wt,ex,pa) = mmu_byte (sctlrT 0w T,c2,0w,mem,PL1,add)
    in
      u))`` (fn th => (MP_TAC th THEN METIS_TAC [])));

e(RW_TAC (srw_ss()) []);


e(EXISTS_TAC ``T``);
e(EXISTS_TAC ``add':word32``);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_def]);
e BasicProvers.LET_ELIM_TAC;
e (Q.UNABBREV_TAC `l1_type`);
e (Q.UNABBREV_TAC `l1_desc`);
e (Q.UNABBREV_TAC `l1_bit_18`);
e (Q.UNABBREV_TAC `l1_type'`);
e (FULL_SIMP_TAC (srw_ss()) []);

e (SIMP_TAC (srw_ss()) [mmu_byte_pt_def]);

(* I try (rec'l1PTT l1_desc).pxn and then verify that is a contradiction *)
e (Cases_on `(rec'l1PTT l1_desc').pxn`);
e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) []);
(*----------------------------------------*)



e (SIMP_TAC (srw_ss()) [mmu_byte_l2_def]);
e BasicProvers.LET_ELIM_TAC;


(* PAGE FAULT *)
e (Cases_on `l2_type = 3w`);
e (FULL_SIMP_TAC (srw_ss()) []);

(* PAGE FAULT *)
e (FULL_SIMP_TAC (srw_ss()) []);
e (Cases_on `l2_type = 1w`);


(*large page*)
e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_large_page_def]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_check_access_def]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_domain_status_def]);
e (Q.UNABBREV_TAC `l1_desc`);
e EVAL_TAC;
e (FULL_SIMP_TAC (srw_ss()) []);
(*Small page*)
e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_check_access_def]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_domain_status_def]);
e (Q.UNABBREV_TAC `l1_desc`);
e EVAL_TAC;
e (FULL_SIMP_TAC (srw_ss()) []);




e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_pt_def]);
e (Cases_on `(rec'l1PTT l1_desc).pxn`);
e (FULL_SIMP_TAC (srw_ss()) []);


(*-------------------------------------------*)


e (Q.PAT_UNDISCH_TAC `mmu_supported a b c d`);

e (FULL_SIMP_TAC (srw_ss()) [mmu_supported_def]);

<<<<<<< HEAD
CONTRAPOS;
e STRIP_TAC;



(* all pages unmapped *)
val all_pages_unmapped_def = Define `
  all_pages_unmapped (c1:sctlrT) c2 (c3:bool[32]) mem =
  !pg_idx:bool[32]. pg_idx<+4096w ==>
  let tbl_base = mmu_tbl_base_addr c2 in
  let desc_add = (tbl_base !! (pg_idx << 2) !! 0w) in
  let l1_desc = read_mem32(desc_add,mem) in
  let l1_type = mmu_l1_decode_type(l1_desc) in
  l1_type = 0b00w
`;

val goal = ``
  let c1 = rec'sctlrT 1w in
  all_pages_unmapped c1 c2 c3 mem  ==>
  !ph_add.
  let (u,rd,wt,ex) = mmu_phi_byte c1 c2 c3 mem PL1 ph_add in
      ~wt
``;
g `^goal`;

e (computeLib.RESTR_EVAL_TAC
       [``mmu_phi_byte``,``all_pages_unmapped``]);
e (REPEAT STRIP_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_phi_byte_def]);
e (computeLib.RESTR_EVAL_TAC [``mmu_byte`` ]);
e (REPEAT STRIP_TAC);
e (Q.PAT_UNDISCH_TAC `f b`);
e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_def]);
e (FULL_SIMP_TAC (srw_ss()) [rec'sctlrT_def]);
e (FULL_SIMP_TAC (srw_ss()) [all_pages_unmapped_def]);
e (PAT_ASSUM ``p`` (fn thm =>
  ASSUME_TAC (SPEC ``mmu_tbl_index add'`` thm)
));
e (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_index_def]);
e (ASSUME_TAC (blastLib.BBLAST_PROVE(``(add':bool[32]) ⋙ 20 <₊ 4096w``)));

e (PAT_ASSUM ``p==>q`` (fn thm =>
  ASSUME_TAC (UNDISCH_ALL thm)
));
e (Q.PAT_UNDISCH_TAC `f b`);
e BasicProvers.LET_ELIM_TAC;
e (FULL_SIMP_TAC (srw_ss()) []);



(* autoprotecting section based *)
val autoprotection_section_def = Define `
  autoprotection_section (c1:sctlrT) (c2:bool[32]) (c3:bool[32]) mem =
  (!dom . ((mmu_domain_status (c3,dom) = 0w) \/ 
          (mmu_domain_status (c3,dom) = 0b01w))
  ) /\
  (c2 = c2 && 0xFFF00000w) /\
  let tbl_base = mmu_tbl_base_addr c2 in
  !pg_idx:bool[32]. pg_idx<+4096w ==>
  let desc_add = (tbl_base !! (pg_idx << 2) !! 0w) in
  let l1_desc = read_mem32(desc_add,mem) in
  let l1_type = mmu_l1_decode_type(l1_desc) in
  (l1_type = 0b00w) \/ ((l1_type = 0b10w) /\ (
    let l1_bit_18 = ((l1_desc && 0x40000w) >>> 18) in
    (l1_bit_18 = 0w) /\ 
    let l1_sec_desc = rec'l1SecT l1_desc in
    let l1_sec_add = (w2w(l1_sec_desc.addr) << 20) in
    if l1_sec_add >=+ c2 /\
       l1_sec_add <+ c2+0x100000w
    then l1_sec_desc.ap = 0b010w
    else T
  ))
`;

val autoprotection_no_pt_update = prove(``
  let c1 = rec'sctlrT 1w in
  autoprotection_section c1 c2 c3 mem  ==>
  !ph_add.
  ph_add >=+ c2 ==>
  ph_add <+ c2+0x100000w ==>
  let (u,rd,wt,ex) = mmu_phi_byte c1 c2 c3 mem F ph_add in
      ~wt
``,
  (computeLib.RESTR_EVAL_TAC  [``mmu_phi_byte``,``autoprotection_section``])
THEN (REPEAT STRIP_TAC)
THEN (FULL_SIMP_TAC (srw_ss()) [mmu_phi_byte_def])
THEN (computeLib.RESTR_EVAL_TAC [``mmu_byte`` ])
THEN (REPEAT STRIP_TAC)
THEN (Q.PAT_UNDISCH_TAC `f b`)
THEN (FULL_SIMP_TAC (srw_ss()) [])
THEN (FULL_SIMP_TAC (srw_ss()) [mmu_byte_def])
THEN (FULL_SIMP_TAC (srw_ss()) [rec'sctlrT_def])
THEN BasicProvers.LET_ELIM_TAC
(* Expand the hypotesys *)
THEN (PAT_ASSUM ``autoprotection_section a b c d`` (fn thm =>
  ASSUME_TAC (EVAL_RULE thm)
))
THEN (FULL_SIMP_TAC (srw_ss()) [])
(* Instantiate the page with the page containing the L1 pt *)
THEN (PAT_ASSUM ``! pg_idx:bool[32]. p`` (fn thm =>
  ASSUME_TAC (SPEC ``mmu_tbl_index add'`` thm)
))
(* Prove the assumption *)
THEN (FULL_SIMP_TAC (srw_ss()) [Once mmu_tbl_index_def])
THEN (ASSUME_TAC (blastLib.BBLAST_PROVE(``(add':bool[32]) ⋙ 20 <₊ 4096w``)))
THEN (PAT_ASSUM ``a ==> b`` (fn thm =>
  ASSUME_TAC (UNDISCH thm)
))
THEN (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_index_def])
(* two subgoals: page of the pt is unmapped or is a L1 *)
THENL [
   (FULL_SIMP_TAC (srw_ss()) [Abbr `l1_desc`])
,
   (* Second case, the pt is L1 *)
   (* Expand the abreviations   *)
   (* THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `l1_desc`]) *)
   (FULL_SIMP_TAC (srw_ss()) [Abbr `l1_type`])
   THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `l1_bit_18`])
   (* Abbreviate the common address   *)
   THEN (Q.ABBREV_TAC `aaa = rec'l1SecT
              (read_mem32 (mmu_tbl_base_addr c2 ‖ add' ⋙ 20 ≪ 2,mem))`)
   (* Case on the address is not inside the pt or yes   *)
   THEN (Cases_on `~((w2w (aaa:l1SecT).addr):bool[32] ≪ 20 ≥₊ c2 ∧
                (w2w aaa.addr):bool[32] ≪ 20 <₊ c2 + 0x100000w)`)
   THENL [
      (* Case on the address is not inside the pt *)
      (FULL_SIMP_TAC (srw_ss()) [])
      (* two subgoal:  < c2 and >= c2+4096*4 *)
      THENL [
         ((FULL_SIMP_TAC (srw_ss()) [mmu_byte_section_def])
         THEN BasicProvers.LET_ELIM_TAC
         THEN EVAL_TAC)
         THEN (ASSUME_TAC (UNDISCH(UNDISCH(UNDISCH(
         blastLib.BBLAST_PROVE(``
         (c2 = 0xFFF00000w && c2) ==>
         ¬((w2w (aaa:l1SecT).addr):bool[32] ≪ 20 ≥₊ c2) ==>
         ph_add ≥₊ c2 ==>
         ~((0xFFFFFw && add' ‖ (w2w aaa.addr):bool[32] ≪ 20) = ph_add)
         ``))))))
         THEN (FULL_SIMP_TAC (srw_ss()) [])
      (* second subgoal:   >= c2+4096*4 *)
      ,
         ((FULL_SIMP_TAC (srw_ss()) [mmu_byte_section_def])
         THEN BasicProvers.LET_ELIM_TAC
         THEN EVAL_TAC)
         THEN (ASSUME_TAC (UNDISCH(UNDISCH(UNDISCH(
         blastLib.BBLAST_PROVE(``
         (c2 = 0xFFF00000w && c2) ==>
         ¬((w2w (aaa:l1SecT).addr):bool[32] ≪ 20 <+ (c2+0x100000w)) ==>
         ph_add ≥₊ c2 ==>
         ph_add <₊ c2 + 0x100000w ==>
         ~((0xFFFFFw && add' ‖ (w2w aaa.addr):bool[32] ≪ 20) = ph_add)
         ``))))))
         THEN (FULL_SIMP_TAC (srw_ss()) [])
      ]
   ,
   (* Second case: The page containing the page table *)
      (FULL_SIMP_TAC (srw_ss()) [
                      Abbr `l1_desc`])
      THEN (FULL_SIMP_TAC (srw_ss()) [mmu_byte_section_def])
      THEN (FULL_SIMP_TAC (srw_ss()) [mmu_check_access_def])
      (* Instantiate the domain with the domain of the L1 pt *)
      THEN (PAT_ASSUM ``! dom:bool[4]. p`` (fn thm =>
        ASSUME_TAC (SPEC ``(aaa:l1SecT).dom`` thm)
      ))
      THEN (FULL_SIMP_TAC (srw_ss()) [])
      (* other 2 subgoal: domain is 0 or domain is 1*)
      THENL [
         EVAL_TAC,
         (FULL_SIMP_TAC (srw_ss()) [mmu_decode_l1_ap_def])
         THEN EVAL_TAC
      ]
   ]
]
);


val read_mem_eq = prove(``
  !a.
  (mem(a) = mem'(a)) ==>
  (mem(a+1w) = mem'(a+1w)) ==>
  (mem(a+2w) = mem'(a+2w)) ==>
  (mem(a+3w) = mem'(a+3w)) ==>
  (read_mem32(a,mem) = read_mem32(a,mem'))
``, FULL_SIMP_TAC (srw_ss()) [read_mem32_def]);



val goal = ``
  let c1 = rec'sctlrT 1w in
  autoprotection_section c1 c2 c3 mem  ==>
  mmu_safe c1 c2 c3 mem F
``;
g `^goal`;

e (computeLib.RESTR_EVAL_TAC  [``mmu_safe``,``autoprotection_section``]);
e (REPEAT STRIP_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_safe_def]);
e (REPEAT STRIP_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_mem_equiv_def]);
e (REPEAT STRIP_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_def]);
e BasicProvers.LET_ELIM_TAC;
e (FULL_SIMP_TAC (srw_ss()) [rec'sctlrT_def]);

e (`(c2 = c2 && 0xFFF00000w)
    ==> ((mmu_tbl_base_addr c2 ‖ mmu_tbl_index add' ≪ 2) ≥₊ c2)`
 by (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_base_addr_def, mmu_tbl_index_def]
     THEN EVAL_TAC
     THEN blastLib.BBLAST_TAC
    )
);

SPEC ``mmu_tbl_base_addr c2 ‖ mmu_tbl_index add' ≪ 2`` (
UNDISCH (
computeLib.RESTR_EVAL_RULE [``mmu_phi_byte``, ``autoprotection_section`` ]
autoprotection_no_pt_update));

autoprotection_no_pt_update;
autoprotection_section_def;


e (PAT_ASSUM ``! ph_add:bool[32]. p`` (fn thm =>
  ASSUME_TAC (
  computeLib.RESTR_EVAL_RULE [``mmu_phi_byte`` ] (
  SPEC ``mmu_tbl_base_addr c2 ‖ mmu_tbl_index add' ≪ 2`` thm)
)));
e (FULL_SIMP_TAC (srw_ss()) []);


(* invariant on page types *)
val invariant_page_type_def = Define `
  invariant_page_type (c1:sctlrT) (c2:bool[32]) (c3:bool[32]) mem
  (ptl1s:bool[12]->bool) =
  (!dom . ((mmu_domain_status (c3,dom) = 0w) \/ 
          (mmu_domain_status (c3,dom) = 0b01w))
  ) /\
  let tbl_base = mmu_tbl_base_addr c2 in
  (* The page table is aligned to 1MB *)
  (tbl_base = tbl_base && 0xFFF00000w) /\
  (* The page table point to a page markes as L1 *)
  (ptl1s (w2w(tbl_base >>> 20):bool[12])) /\
  (* All pages marked as L1 contains well defined pt *)
  !global_page:bool[12]. (ptl1s global_page) ==>
  !pg_idx:bool[12].
  let global_page_add = ((w2w global_page):bool[32] << 20) in
  let desc_add = (global_page_add !! ((w2w pg_idx):bool[32] << 2) !! 0w) in
  let l1_desc = read_mem32(desc_add,mem) in
  let l1_type = mmu_l1_decode_type(l1_desc) in
  (l1_type = 0b00w) \/ ((l1_type = 0b10w) /\ (
    let l1_bit_18 = ((l1_desc && 0x40000w) >>> 18) in
    (l1_bit_18 = 0w) /\ 
    let l1_sec_desc = rec'l1SecT l1_desc in
    let pointed_ph_l1_page = l1_sec_desc.addr in
    if (ptl1s pointed_ph_l1_page)
    then l1_sec_desc.ap = 0b010w
    else T
  ))
`;

val strange_eq_thm = prove(``!f x:bool[12] y:bool[12]. f x <> (f y):bool ==> x <> y``,
   (REPEAT STRIP_TAC)
   THEN (FULL_SIMP_TAC (srw_ss()) [])
);


val goal = ``
let c1 = rec'sctlrT 1w in
invariant_page_type c1 c2 c3 mem ptl1s ==>
  !ph_add.
  let ph_page = w2w(ph_add >>> 20):bool[12] in
  (ptl1s ph_page) ==>
  let (u,rd,wt,ex) = mmu_phi_byte c1 c2 c3 mem F ph_add in
      ~wt
``;

g `^goal`;
e (computeLib.RESTR_EVAL_TAC  [``mmu_phi_byte``,``invariant_page_type``]);
e (REPEAT STRIP_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_phi_byte_def]);
e (computeLib.RESTR_EVAL_TAC [``mmu_byte`` ]);
e (REPEAT STRIP_TAC);
e (Q.PAT_UNDISCH_TAC `f b`);
e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_def]);
e (FULL_SIMP_TAC (srw_ss()) [rec'sctlrT_def]);
e BasicProvers.LET_ELIM_TAC;
e (PAT_ASSUM ``invariant_page_type a b c d e`` (fn thm =>
  ASSUME_TAC (EVAL_RULE thm)
));
e (FULL_SIMP_TAC (srw_ss()) []);
(* Instantiate global page with the page containing the current L1 *)
e (PAT_ASSUM ``! (global_page:bool[12]). p`` (fn thm =>
  ASSUME_TAC (
  UNDISCH (
  SPEC ``(w2w((mmu_tbl_base_addr c2) >>> 20):bool[12])`` thm))
));
(* Instantiate the page entry index with the current checked entry *)
e (PAT_ASSUM ``! pg_idx:bool[12]. p`` (fn thm =>
  ASSUME_TAC (SPEC ``w2w(mmu_tbl_index add'):bool[12]`` thm)
));
e (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_index_def]);

(* two subgoals: page of the pt is unmapped or is a L1 *)
(* First case: pt unmapped *)
e (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_base_addr_def]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
((w2w ((w2w (4095w && (c2:bool[32]) ⋙ 20)):bool[12])):bool[32]) ≪ 20 ‖
            (w2w ((w2w (add':bool[32] ⋙ 20)):bool[12])):bool[32] ≪ 2
=
0xFFF00000w:bool[32] && c2 ‖ add' ⋙ 20 ≪ 2
``)
]);
e (FULL_SIMP_TAC (srw_ss()) [Abbr `l1_desc`]);
(* Second case: pt is L1 *)
e (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_base_addr_def]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
((w2w ((w2w (4095w && (c2:bool[32]) ⋙ 20)):bool[12])):bool[32]) ≪ 20 ‖
            (w2w ((w2w (add':bool[32] ⋙ 20)):bool[12])):bool[32] ≪ 2
=
0xFFF00000w:bool[32] && c2 ‖ add' ⋙ 20 ≪ 2
``)
]);
(* Expand the abreviations   *)
e (FULL_SIMP_TAC (srw_ss()) [Abbr `l1_desc`]);
e (FULL_SIMP_TAC (srw_ss()) [Abbr `l1_type`]);
e (FULL_SIMP_TAC (srw_ss()) [Abbr `l1_bit_18`]);
e (Q.ABBREV_TAC `aaa = rec'l1SecT
    (read_mem32 (0xFFF00000w && (c2:bool[32]) ‖ add' ⋙ 20 ≪ 2,mem))`);
(* Case on the address translation does not point the a pt or yes   *)
e (Cases_on `~ ptl1s aaa.addr`);
(* Case on the address translation does not point the a pt   *)
e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_section_def]);
e BasicProvers.LET_ELIM_TAC;
e EVAL_TAC;
e (`(ptl1s ((w2w (ph_add ⋙ 20)):bool[12])) <> (ptl1s ((aaa:l1SecT).addr))` by
  (FULL_SIMP_TAC (srw_ss()) []));
e (ASSUME_TAC
       (UNDISCH
       (SPEC ``((aaa:l1SecT).addr)``
       (SPEC ``((w2w (ph_add:bool[32] ⋙ 20)):bool[12])``
       (SPEC ``ptl1s:bool[12]->bool``
	     strange_eq_thm)))));
e (ASSUME_TAC (
   UNDISCH (
blastLib.BBLAST_PROVE(``
(((w2w (ph_add ⋙ 20)):bool[12]) <>  ((aaa:l1SecT).addr)) ==>
~(0xFFFFFw && add' ‖ (w2w aaa.addr):bool[32] ≪ 20 = ph_add)
``))));
e (FULL_SIMP_TAC (srw_ss()) []);

(* Case on the address translation points the a pt   *)
e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_section_def]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_check_access_def]);
e (PAT_ASSUM ``! dom:bool[4]. p`` (fn thm =>
        ASSUME_TAC (SPEC ``(aaa:l1SecT).dom`` thm)
      ));
e (FULL_SIMP_TAC (srw_ss()) []);
(* other 2 subgoal: domain is 0 or domain is 1*)
e EVAL_TAC;
e (FULL_SIMP_TAC (srw_ss()) [mmu_decode_l1_ap_def]);
e EVAL_TAC;



(* Try the hypervisor API *)
val hyper_switch_def = Define `
hyper_switch (c1, c2, c3, mem, ptl1s) param_c2 =
  if (param_c2 <> (param_c2 && 0xFFF00000w:bool[32])) then
      (c1,c2,c3,mem,ptl1s)
  else if ~(ptl1s (w2w(param_c2 >>> 20):bool[12]))  then
      (c1,c2,c3,mem,ptl1s)
  else
      (c1,param_c2,c3,mem,ptl1s)
`;

val write_mem32_def = Define `
write_mem32 (add:bool[32], mem, value:bool[32]) =
  let mem = (add =+ (w2w(value && 0xFFw):bool[8])) mem in
  let mem = (add+1w =+ (w2w((value >>> 8) && 0xFFw):bool[8])) mem in
  let mem = (add+2w =+ (w2w((value >>> 16) && 0xFFw):bool[8])) mem in
  let mem = (add+3w =+ (w2w((value >>> 24) && 0xFFw):bool[8])) mem in
      mem
`;

val hyper_unmap_section_def = Define `
hyper_unmap_section (c1, c2, c3, mem, ptl1s) add =
  let pt_add = mmu_tbl_base_addr c2 in
  let page_idx = add >>> 20 in
  let sec_add = pt_add ‖ (page_idx ≪ 2) in
  let page_desc = read_mem32(sec_add, mem) in
  let page_desc' = page_desc && 0xfffffffcw in
  let mem' = write_mem32(sec_add, mem, page_desc') in
      (c1,c2,c3,mem',ptl1s)
`;

val hyper_map_section_def = Define `
hyper_map_section (c1, c2, c3, mem, ptl1s) add phadd (attrs:bool[32]) =
  let ph_page_idx = (w2w(phadd >>> 20)):bool[12] in
  let ap = ((attrs >>  10) && 0b11w) !! (((attrs >>  13) && 0b100w)) in
  if (ap <> 2w) /\ (ptl1s ph_page_idx) then
      (c1, c2, c3, mem, ptl1s)
  else
      let pt_add = mmu_tbl_base_addr c2 in
      let page_idx = add >>> 20 in
      let sec_add = pt_add ‖ (page_idx ≪ 2) in
      let page_desc =
	  (0xFFF00000w && phadd) !!
	  0b10w !!
	  (0x000BFFFCw && attrs)
      in
      let mem' = write_mem32(sec_add, mem, page_desc) in
	  (c1,c2,c3,mem',ptl1s)
`;

val hyper_free_pt_def = Define `
hyper_free_pt (c1, c2, c3, mem, ptl1s) phadd =
  if 0xFFF00000w && c2 = 0xFFF00000w && phadd then
      (c1, c2, c3, mem, ptl1s)
  else
      let ph_page_idx = (w2w(phadd >>> 20)):bool[12] in
      let ptl1s' = (ph_page_idx =+ F) ptl1s in
	  (c1,c2,c3,mem,ptl1s')
`;

val goal = ``
  let c1 = 1w in
  let c1_rec = rec'sctlrT c1 in
  (invariant_page_type c1_rec c2 c3 mem ptl1s) ==>
  let (c1',c2',c3',mem',ptl1s') =
      (hyper_switch (c1, c2, c3, mem, ptl1s) param_c2) in
  let c1_rec' = rec'sctlrT c1' in
      (invariant_page_type c1_rec' c2' c3' mem' ptl1s')
``;

g `^goal`;
e BasicProvers.LET_ELIM_TAC;
e (FULL_SIMP_TAC (srw_ss()) [hyper_switch_def]);
e (Cases_on `(param_c2 <> (param_c2 && 0xFFF00000w:bool[32]))`);

e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) [Abbr `c1_rec`,
			     Abbr `c1_rec'`,
			     Abbr `c1`]);
e (RW_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) []);
e (Cases_on `~(ptl1s (w2w(param_c2 >>> 20):bool[12]))`);

e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) [Abbr `c1_rec`,
			     Abbr `c1_rec'`,
			     Abbr `c1`]);

e (RW_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def]);
e (REPEAT STRIP_TAC);

e (RW_TAC (srw_ss()) []);

e (`mem'=mem` by (FULL_SIMP_TAC (srw_ss()) []));
e (`ptl1s'=ptl1s` by (FULL_SIMP_TAC (srw_ss()) []));
e (`c1'=c1` by (FULL_SIMP_TAC (srw_ss()) []));
e (FULL_SIMP_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_base_addr_def]);
e (PAT_ASSUM ``let a=b in c`` (fn thm =>
  ASSUME_TAC (SIMP_RULE (srw_ss()) [Once LET_DEF] thm)
));
e (FULL_SIMP_TAC (srw_ss()) []);
e EVAL_TAC;
e (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_base_addr_def]);

e (ASSUME_TAC (UNDISCH_ALL (
blastLib.BBLAST_PROVE(``
  (param_c2 = c2':bool[32]) ==>
  (param_c2 = 0xFFF00000w && param_c2) ==>
  (0xFFFFC000w && c2' = 0xFFF00000w && c2')
``))));
e (FULL_SIMP_TAC (srw_ss()) []);

e (ASSUME_TAC (UNDISCH_ALL (
blastLib.BBLAST_PROVE(``
  (param_c2 = c2':bool[32]) ==>
  (param_c2 = 0xFFF00000w && param_c2) ==>
  ((w2w (param_c2 ⋙ 20) :bool[12]) = (w2w (4095w && c2' ⋙ 20):bool[12]))
``))));
e (FULL_SIMP_TAC (srw_ss()) []);


val write_read_thm = prove(
``!a v m m'. (m' = write_mem32(a,m,v)) ==> 
      (read_mem32(a,m') = v)
``,
  (REPEAT GEN_TAC)
THEN EVAL_TAC
THEN (RW_TAC (srw_ss()) [read_mem32_def])
THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.UPDATE_def])
THEN (RW_TAC (srw_ss()) [])
THENL [
 (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE
  ``~(((a:bool[32]) + 3w) = a)``]),
 (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE
  ``~(((a:bool[32]) + 2w) = a)``]),
 (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE
  ``~(((a:bool[32]) + 1w) = a)``]),
 (fn (asl, w) =>
 let val t = blastLib.BBLAST_PROVE w in
 blastLib.BBLAST_TAC (asl, w)
 end)
]);


val write_read_unch_thm = prove(
``!a a' v m m'. 
  (a'+3w <+ a /\ a'+3w>=+3w /\ a+3w>=+3w) \/
  (a' >+ a+3w /\ a'+3w>=+3w /\ a+3w>=+3w) ==>
  (m' = write_mem32(a,m,v)) ==> 
  (read_mem32(a',m') = read_mem32(a',m))
``,
  (REPEAT GEN_TAC)
THEN EVAL_TAC
THEN (RW_TAC (srw_ss()) [read_mem32_def])
THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.UPDATE_def])
THENL [
   (MAP_EVERY (fn (tm1, tm2) =>
   (ASSUME_TAC (UNDISCH (UNDISCH (
   (blastLib.BBLAST_PROVE ``
   (((a':bool[32]) + 3w) <₊ a) ==>
   (((a':bool[32]) + 3w) ≥₊ 3w) ==>
   (((a:bool[32]) + 3w) ≥₊ 3w) ==>
   (^tm1 <> ^tm2)
   ``))))))
   (List.concat (
     List.map (fn tm1 =>
       List.map (fn tm2 =>
        (tm1,tm2)
       )
       [``a':bool[32]``, ``a'+1w:bool[32]``, ``a'+2w:bool[32]``, ``a'+3w:bool[32]``]
     )
     [``a:bool[32]``, ``a+1w:bool[32]``, ``a+2w:bool[32]``, ``a+3w:bool[32]``]
   )))
   THEN (FULL_SIMP_TAC (srw_ss()) [])
,
   (MAP_EVERY (fn (tm1, tm2) =>
   (ASSUME_TAC (UNDISCH (UNDISCH (
   (blastLib.BBLAST_PROVE ``
   (((a':bool[32])) >+ a+3w) ==>
   (((a':bool[32]) + 3w) ≥₊ 3w) ==>
   (((a:bool[32]) + 3w) ≥₊ 3w) ==>
   (^tm1 <> ^tm2)
   ``))))))
   (List.concat (
     List.map (fn tm1 =>
       List.map (fn tm2 =>
        (tm1,tm2)
       )
       [``a':bool[32]``, ``a'+1w:bool[32]``, ``a'+2w:bool[32]``, ``a'+3w:bool[32]``]
     )
     [``a:bool[32]``, ``a+1w:bool[32]``, ``a+2w:bool[32]``, ``a+3w:bool[32]``]
   )))
   THEN (FULL_SIMP_TAC (srw_ss()) [])
]);
=======
e(SUBGOAL_THEN ``~((add':word32) <> (ph_add:word32)) ==> ~(∀PL1 add.
   (let (u,rd,wt,ex,pa) = mmu_byte (sctlrT 0w T,c2,0w,mem,PL1,add)
    in
      u))`` (fn th => (MP_TAC th THEN METIS_TAC [])));

e(RW_TAC (srw_ss()) []);


e(EXISTS_TAC ``T``);
e(EXISTS_TAC ``add':word32``);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_def]);
e BasicProvers.LET_ELIM_TAC;
e (Q.UNABBREV_TAC `l1_type`);
e (Q.UNABBREV_TAC `l1_desc`);
e (Q.UNABBREV_TAC `l1_bit_18`);
e (Q.UNABBREV_TAC `l1_type'`);
e (FULL_SIMP_TAC (srw_ss()) []);

e (SIMP_TAC (srw_ss()) [mmu_byte_pt_def]);

(* I try (rec'l1PTT l1_desc).pxn and then verify that is a contradiction *)
e (Cases_on `(rec'l1PTT l1_desc').pxn`);
e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) []);
(*----------------------------------------*)



e (SIMP_TAC (srw_ss()) [mmu_byte_l2_def]);
e BasicProvers.LET_ELIM_TAC;


(* PAGE FAULT *)
e (Cases_on `l2_type = 3w`);
e (FULL_SIMP_TAC (srw_ss()) []);

(* PAGE FAULT *)
e (FULL_SIMP_TAC (srw_ss()) []);
e (Cases_on `l2_type = 1w`);


(*large page*)
e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_large_page_def]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_check_access_def]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_domain_status_def]);
e (Q.UNABBREV_TAC `l1_desc`);
e EVAL_TAC;
e (FULL_SIMP_TAC (srw_ss()) []);

(*Small page*)
e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_check_access_def]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_domain_status_def]);
e (Q.UNABBREV_TAC `l1_desc`);
e EVAL_TAC;
e (FULL_SIMP_TAC (srw_ss()) []);

val mmu_1 = top_thm();










--------------------------------------------------------------------------------------






>>>>>>> 956d4ab5c964247a59a81d86aec01aeac5231e9e



val goal = ``
<<<<<<< HEAD
  let c1 = 1w in
  let c1_rec = rec'sctlrT c1 in
  (invariant_page_type c1_rec c2 c3 mem ptl1s) ==>
  let (c1',c2',c3',mem',ptl1s') =
      (hyper_unmap_section (c1, c2, c3, mem, ptl1s) sec_to_unmap) in
  let c1_rec' = rec'sctlrT c1' in
      (invariant_page_type c1_rec' c2' c3' mem' ptl1s')
``;

g `^goal`;
e BasicProvers.LET_ELIM_TAC;
e (FULL_SIMP_TAC (srw_ss()) [hyper_unmap_section_def, LET_DEF]);
e (`c1':bool[32] = c1` by (FULL_SIMP_TAC (srw_ss()) []));
e (`c2':bool[32] = c2` by (FULL_SIMP_TAC (srw_ss()) []));
e (`c3':bool[32] = c3` by (FULL_SIMP_TAC (srw_ss()) []));
e (`ptl1s':bool[12]->bool = ptl1s` by (FULL_SIMP_TAC (srw_ss()) []));
e (FULL_SIMP_TAC (srw_ss()) [Abbr `c1`, rec'sctlrT_def,
			     Abbr `c1_rec'`,
			     Abbr `c1_rec`]);

e (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_base_addr_def, Once LET_DEF]);
e GEN_TAC;
e STRIP_TAC;
e GEN_TAC;

e (Cases_on `
  ((w2w global_page):bool[32] ≪ 20 = 0xFFFFC000w && c2') /\
  ((w2w pg_idx):bool[32] << 20 = 0xFFF00000w && sec_to_unmap)
`);

e (FULL_SIMP_TAC (srw_ss()) []);
e BasicProvers.LET_ELIM_TAC;

e (FULL_SIMP_TAC (srw_ss()) [Abbr `desc_add`, Abbr `global_page_add`]);
e (ASSUME_TAC (UNDISCH_ALL(
blastLib.BBLAST_PROVE(``
((w2w (pg_idx:bool[12])):bool[32] ≪ 20 = 0xFFF00000w && sec_to_unmap) ==>
((0xFFF00000w && c2:bool[32] ‖ sec_to_unmap ⋙ 20 ≪ 2) = (0xFFF00000w && c2:bool[32] ‖ (w2w pg_idx):bool[32] ≪ 2))
``))));

e (Q.ABBREV_TAC `add_desc = (0xFFF00000w && c2:bool[32] ‖ (w2w pg_idx):bool[32] ≪ 2)`);
e (FULL_SIMP_TAC (srw_ss()) []);


e (`mem' = write_mem32
     (add_desc,mem,0xFFFFFFFCw && read_mem32 (add_desc,mem))` by
   (FULL_SIMP_TAC (srw_ss()) []));
      
e (ASSUME_TAC (UNDISCH_ALL (
SPECL [``add_desc:bool[32]``,
       ``0xFFFFFFFCw && read_mem32 (add_desc,mem)``,
      ``mem:bool[32]->bool[8]``,
      ``mem':bool[32]->bool[8]``] write_read_thm
)));
e (FULL_SIMP_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) [Abbr `l1_desc`, mmu_l1_decode_type_def]);

(*Second case *)

e (FULL_SIMP_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF]);
e (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF]);
e (PAT_ASSUM ``∀ global_page'.p==>q`` (fn thm =>
  ASSUME_TAC (
  SPEC ``pg_idx:bool[12]``(
  UNDISCH_ALL (
  SPEC ``global_page:bool[12]`` thm))
)));
e (PAT_ASSUM ``write_mem32 a = mem'`` (fn thm => ASSUME_TAC (SYM thm)));
e (FULL_SIMP_TAC (srw_ss()) [ASSUME ``c2':bool[32] = c2``]);


val a2 = ``(w2w (global_page:bool[12])):bool[32] ≪ 20 ‖ w2w (pg_idx:bool[12]) ≪ 2``;
val a1 = ``0xFFF00000w:bool[32] && c2 ‖ sec_to_unmap ⋙ 20 ≪ 2``;

e (ASSUME_TAC(UNDISCH_ALL (
blastLib.BBLAST_PROVE(``
  (0xFFFFC000w && c2 = 0xFFF00000w && c2) ==>
  ((w2w global_page) ≪ 20 ≠ (0xFFFFC000w && c2:bool[32])) ==>
  (((^a2 +3w <+ ^a1) /\
    (^a2 +3w >=+ 3w) /\
    (^a1 +3w >=+ 3w)
    ) \/
   ((^a2 >+ ^a1+3w)  /\
    (^a2 +3w >=+ 3w) /\
    (^a1 +3w >=+ 3w)
    )
  )
``))));

e (ASSUME_TAC (
UNDISCH_ALL (
SPECL [
   a1,a2,
   ``0xFFFFFFFCw && read_mem32 (^a1, mem)``,
   ``mem:bool[32]->bool[8]``,
   ``mem':bool[32]->bool[8]``
] write_read_unch_thm
)));

e (PAT_ASSUM ``mem' = write_mem32 a`` (fn thm =>
  ASSUME_TAC thm
  THEN (FULL_SIMP_TAC (srw_ss()) [thm])
));




(* Third case: no the updated page table entry *)
e (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF]);
e (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF]);
e (PAT_ASSUM ``∀ global_page'.p==>q`` (fn thm =>
  ASSUME_TAC (
  SPEC ``pg_idx:bool[12]``(
  UNDISCH_ALL (
  SPEC ``global_page:bool[12]`` thm))
)));
e (PAT_ASSUM ``write_mem32 a = mem'`` (fn thm => ASSUME_TAC (SYM thm)));
e (FULL_SIMP_TAC (srw_ss()) [ASSUME ``c2':bool[32] = c2``]);


val a2 = ``(w2w (global_page:bool[12])):bool[32] ≪ 20 ‖ w2w (pg_idx:bool[12]) ≪ 2``;
val a1 = ``0xFFF00000w:bool[32] && c2 ‖ sec_to_unmap ⋙ 20 ≪ 2``;

e (ASSUME_TAC(UNDISCH_ALL (
blastLib.BBLAST_PROVE(``
  (0xFFFFC000w && c2 = 0xFFF00000w && c2) ==>
  ((w2w pg_idx) ≪ 20 ≠ (0xFFF00000w && sec_to_unmap:bool[32])) ==>
  (((^a2 +3w <+ ^a1) /\
    (^a2 +3w >=+ 3w) /\
    (^a1 +3w >=+ 3w)
    ) \/
   ((^a2 >+ ^a1+3w)  /\
    (^a2 +3w >=+ 3w) /\
    (^a1 +3w >=+ 3w)
    )
  )
``))));

e (ASSUME_TAC (
UNDISCH_ALL (
SPECL [
   a1,a2,
   ``0xFFFFFFFCw && read_mem32 (^a1, mem)``,
   ``mem:bool[32]->bool[8]``,
   ``mem':bool[32]->bool[8]``
] write_read_unch_thm
)));

e (PAT_ASSUM ``mem' = write_mem32 a`` (fn thm =>
  ASSUME_TAC thm
  THEN (FULL_SIMP_TAC (srw_ss()) [thm])
));


val goal = ``
  let c1 = 1w in
  let c1_rec = rec'sctlrT c1 in
  (invariant_page_type c1_rec c2 c3 mem ptl1s) ==>
  let (c1',c2',c3',mem',ptl1s') =
      (hyper_map_section (c1, c2, c3, mem, ptl1s) addr phaddr attrs) in
  let c1_rec' = rec'sctlrT c1' in
      (invariant_page_type c1_rec' c2' c3' mem' ptl1s')
``;

g `^goal`;
e BasicProvers.LET_ELIM_TAC;
e (FULL_SIMP_TAC (srw_ss()) [hyper_map_section_def, LET_DEF]);

e (Cases_on `
  ((4w && attrs:bool[32] ≫ 13 ‖ 3w && attrs ≫ 10) <> 2w) ∧
         ptl1s (w2w (phaddr ⋙ 20))
`);

e (bossLib.UNABBREV_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) []);
e (RW_TAC (srw_ss()) []);

e (Q.ABBREV_TAC `cnd = ((4w && attrs ≫ 13 ‖ 3w && attrs ≫ 10) <> 2w) ∧
         ptl1s (w2w (phaddr ⋙ 20))`);
e (FULL_SIMP_TAC (srw_ss()) []);
e (`c1':bool[32] = c1` by (FULL_SIMP_TAC (srw_ss()) []));
e (`c2':bool[32] = c2` by (FULL_SIMP_TAC (srw_ss()) []));
e (`c3':bool[32] = c3` by (FULL_SIMP_TAC (srw_ss()) []));
e (`ptl1s':bool[12]->bool = ptl1s` by (FULL_SIMP_TAC (srw_ss()) []));
e (FULL_SIMP_TAC (srw_ss()) [Abbr `c1`, rec'sctlrT_def,
			     Abbr `c1_rec'`,
			     Abbr `c1_rec`]);
e (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_base_addr_def, Once LET_DEF]);
e GEN_TAC;
e STRIP_TAC;
e GEN_TAC;

e (Cases_on `
  ((w2w global_page):bool[32] ≪ 20 = 0xFFFFC000w && c2') /\
  ((w2w pg_idx):bool[32] << 20 = 0xFFF00000w && addr)
`);

e (FULL_SIMP_TAC (srw_ss()) []);
e BasicProvers.LET_ELIM_TAC;
e (FULL_SIMP_TAC (srw_ss()) [Abbr `desc_add`, Abbr `global_page_add`]);

e (ASSUME_TAC (UNDISCH_ALL(
blastLib.BBLAST_PROVE(``
((w2w (pg_idx:bool[12])):bool[32] ≪ 20 = 0xFFF00000w && addr) ==>
((0xFFF00000w && c2:bool[32] ‖ addr ⋙ 20 ≪ 2) = (0xFFF00000w && c2:bool[32] ‖ (w2w pg_idx):bool[32] ≪ 2))
``))));

e (Q.ABBREV_TAC `add_desc = (0xFFF00000w && c2:bool[32] ‖ (w2w pg_idx):bool[32] ≪ 2)`);
e (FULL_SIMP_TAC (srw_ss()) []);

e (`mem' = write_mem32
     (add_desc,mem,0xBFFFCw && attrs ‖ 0xFFF00000w && phaddr ‖ 2w)` by
   (FULL_SIMP_TAC (srw_ss()) []));

e (ASSUME_TAC (UNDISCH_ALL (
SPECL [``add_desc:bool[32]``,
       ``0xBFFFCw && attrs ‖ 0xFFF00000w && phaddr ‖ 2w:bool[32]``,
      ``mem:bool[32]->bool[8]``,
      ``mem':bool[32]->bool[8]``] write_read_thm
)));
e (FULL_SIMP_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) [
   Abbr `l1_type`,
   mmu_l1_decode_type_def,
   Abbr `l1_desc`]);
e (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE(``
(3w && (0xBFFFCw && attrs:bool[32] ‖ 0xFFF00000w && phaddr ‖ 2w) = 2w)
``)]);

e (FULL_SIMP_TAC (srw_ss()) [
   Abbr `l1_bit_18`]);
e (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE(``
(1w && (2w && attrs ⋙ 18 ‖ 16380w && phaddr ⋙ 18) = 0w:bool[32]) 
``)]);

e (FULL_SIMP_TAC (srw_ss()) [
   Abbr `pointed_ph_l1_page`,
   Abbr `l1_sec_desc`,
   rec'l1SecT_def
  ]);
e (SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
((31 >< 20) (0xBFFFCw && attrs:bool[32] ‖ 0xFFF00000w && phaddr ‖ 2w))
   =
(w2w (phaddr ⋙ 20)):bool[12]
``)]);

e (SIMP_TAC (srw_ss()) [
  blastLib.BBLAST_PROVE(``
    ((((15 >< 15) (0xBFFFCw && attrs ‖ 0xFFF00000w && phaddr ‖ 2w)):bool[1] @@
    ((11 >< 10) (0xBFFFCw && attrs ‖ 0xFFF00000w && phaddr ‖ 2w)):bool[2])
   = 2w:bool[3]) =
((4w && attrs:bool[32] ≫ 13 ‖ 3w && attrs ≫ 10) = 2w:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [Abbr `cnd`]);


val goal = ``
  let c1 = 1w in
  let c1_rec = rec'sctlrT c1 in
  (invariant_page_type c1_rec c2 c3 mem ptl1s) ==>
  let (c1',c2',c3',mem',ptl1s') =
      (hyper_free_pt (c1, c2, c3, mem, ptl1s) phaddr) in
  let c1_rec' = rec'sctlrT c1' in
      (invariant_page_type c1_rec' c2' c3' mem' ptl1s')
``;
g `^goal`;
e BasicProvers.LET_ELIM_TAC;
e (FULL_SIMP_TAC (srw_ss()) [hyper_free_pt_def, LET_DEF]);
e (Cases_on `0xFFF00000w && c2 = 0xFFF00000w && phaddr`);

e (bossLib.UNABBREV_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) []);
e (RW_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) []);
e (RW_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) [combinTheory.UPDATE_def]);
e (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def]);

e (FULL_SIMP_TAC (srw_ss()) [hyper_free_pt_def, LET_DEF,
			     mmu_tbl_base_addr_def]);
e (ASSUME_TAC(UNDISCH(
blastLib.BBLAST_PROVE(``
((0xFFF00000w && c2) ≠ (0xFFF00000w && phaddr:bool[32])) ==>
((w2w (phaddr ⋙ 20):bool[12]) ≠ (w2w (4095w && c2 ⋙ 20)):bool[12])
``))));

e (METIS_TAC []);

(* e (FULL_SIMP_TAC (srw_ss()) []); *)

(* e GEN_TAC; *)

(* e (Cases_on `c=(w2w (phaddr ⋙ 20):bool[12])`); *)

(* e (FULL_SIMP_TAC (srw_ss()) []); *)

(* e (FULL_SIMP_TAC (srw_ss()) []); *)
(* e STRIP_TAC; *)
(* e (PAT_ASSUM ``!global_page. p ==> q`` (fn thm => *)
(*   ASSUME_TAC ( *)
(*   UNDISCH ( *)
(*   SPEC ``c:bool[12]`` thm) *)
(* ))); *)
(* e (RW_TAC (srw_ss()) []); *)
(* e (PAT_ASSUM ``!pg_idx. p`` (fn thm => *)
(*   ASSUME_TAC ( *)
(*   SPEC ``pg_idx:bool[12]`` thm) *)
(* )); *)
(* e (FULL_SIMP_TAC (srw_ss()) []); *)
=======
  (mmu_supported (rec'sctlrT 1w) c2 0w mem) ==>
  (mmu_safe (rec'sctlrT 1w) c2 0w mem F)
``;
g `^goal`;
e (REPEAT STRIP_TAC);
e(IMP_RES_TAC mmu_1);
e (FULL_SIMP_TAC (srw_ss()) [mmu_safe_def]);
e(RW_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) [mmu_mem_equiv_def]);
e (REPEAT STRIP_TAC);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_def]);
e(RW_TAC (srw_ss()) []);



e(`!ph_add.  mem ph_add = mem' ph_add ` by (RW_TAC (srw_ss()) []
   THEN REPEAT (PAT_ASSUM ``!p. X`` (fn th => ASSUME_TAC (SPEC ``ph_add:word32`` th)))
   THEN PAT_ASSUM ``!p. X`` (fn th => ASSUME_TAC (SPEC ``F`` th))
   THEN (Cases_on `mmu_phi_byte (rec'sctlrT 1w) c2 0w mem F ph_add` THEN Cases_on `r` THEN FULL_SIMP_TAC (srw_ss()) [] THEN Cases_on `r'`)
   THEN FULL_SIMP_TAC (srw_ss()) [LET_DEF]));

e(FULL_SIMP_TAC (srw_ss()) [read_mem32_def]);
e(`l1_type' = l1_type` by METIS_TAC []);
e(`l1_bit_18' = l1_bit_18` by (UNABBREV_ALL_TAC THEN FULL_SIMP_TAC (srw_ss()) []));
e(`mem = mem'` by METIS_TAC []);
e((REPEAT (CASE_TAC THEN FULL_SIMP_TAC (srw_ss()) [])));









































Globals.show_assums := true;

local val tm = Parse.Term `f = ((\x y. x + y) y)`
 in
    val x = ASSUME tm
 end;
BETA_RULE x;
>>>>>>> 956d4ab5c964247a59a81d86aec01aeac5231e9e

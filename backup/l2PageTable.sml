loadPath := "/NOBACKUP/workspaces/hnnemati/mmu/experiments_mmu/l3/" :: !loadPath; 

open tinyTheory;
open mmu_utilsTheory;
open wordsLib;
open example_mmu_proofTheory;

val write_mem32_def = Define `
write_mem32 (add:bool[32], mem, value:bool[32]) =
  let mem = (add =+ (w2w(value && 0xFFw):bool[8])) mem in
  let mem = (add+1w =+ (w2w((value >>> 8) && 0xFFw):bool[8])) mem in
  let mem = (add+2w =+ (w2w((value >>> 16) && 0xFFw):bool[8])) mem in
  let mem = (add+3w =+ (w2w((value >>> 24) && 0xFFw):bool[8])) mem in
      mem
`;

(*
  Page directory entry type :
  PDIR_ENTRY_PAGETABLE
  PDIR_ENTRY_SECTION 
*)
val pdirEntryAddr = Define `
pdirEntryAddr (c2:bool[32]) (va:bool[32]) =
	let PDIR_BASE_MASK:bool[32] = 0xFFFFC000w in (*[31:14]...18bits*)
        let PDIR_INDEX_MASK:bool[32] = 0xFFF00000w in (*[31:20]...12bits,divide 4GB space into 1MB subspaces*)
	    (*page directory entry index(offset)*)
	let pdir_base = (c2 && PDIR_BASE_MASK) in
	let  pdir_index = ((((va) && PDIR_INDEX_MASK) >>> 20) << 2) in
	    (pdir_base !! pdir_index)
`;

(*First level descriptor reader*)
val FstLvlDesc = Define `
       FstLvlDesc  (c2:bool[32]) (va:bool[32]) mem=
       let entryAddr = (pdirEntryAddr c2 va) in
       read_mem32(entryAddr, mem)
`;    
(*
  page table entry type :
  PTAB_ENTRY_LARGE 
  PTAB_ENTRY_SMALL 
*)
(* 
 INPUT : 
 ptb that is first level descriptor
 OUTPUT :
 second level descriptor address
*)
val ptabEntryAddr = Define `
	ptabEntryAddr ptb (va:bool[32]) =
	(*page table base address*)
	let PTAB_BASE_MASK:bool[32] = 0xfffffc00w in (*[31:10]...22bits,aligned by 0x400*)
	let ptab_base = ((ptb) && PTAB_BASE_MASK) in
	(*page table entry index(offset)*)
	let PTAB_INDEX_MASK:bool[32] = 0x000ff000w in (*[19:12]...8bits,divide 4GB space into 4kB subspaces*)
	let PTAB_INDEX_SHIFT = 10 in
	let ptab_index  = (((va) && PTAB_INDEX_MASK) >>> PTAB_INDEX_SHIFT) in
	   (ptab_base !! ptab_index)
`;
(*Second level descriptor extractor*)
val SndLvlDesc = Define `
       SndLvlDesc  (c2:bool[32]) (va:bool[32]) mem =
       let entryAddrL1 = (pdirEntryAddr c2 va) in
       let descL1 = read_mem32(entryAddrL1, mem) in
       let entryAddrL2 = (ptabEntryAddr descL1 va) in
	   read_mem32(entryAddrL2, mem) 
`;

val outputAddress = Define `
	outputAddress pte (va:bool[32]) =
	let PTAB_ENTRY_MASK:bool[32] = 0xfffff000w in
        let ptab_entry = ((pte) && PTAB_ENTRY_MASK) in
        (*page index(offset)*)
	let PTE_INDEX_MASK:bool[32] = 0x00000fffw in
	let pte_index  = ((va) && PTE_INDEX_MASK) in
	    (ptab_entry + pte_index)
`;

(*=====================================================================================================*)
(*Check if an L1 descriptor points to an L2 page table *)
(*beside this function, there is an uninterpreted function to check list of L1 and L2 page tables and data pages for a given base address that are : pgtype*)
val ptl2sDesc = Define `
    ptl2sDesc (fstLvlDesc:bool[32]) =
      if ((fstLvlDesc && 0x00000003w) = 0b01w)
	 then T
	 else F
`;

(*Finds the corresponding L1 descriptors for a given L2 page table base physical address*)
val l1DescExtract = zDefine `
    l1DescExtract (c2:bool[32]) ptBase mem = 
    let l1_tbl_base =  mmu_tbl_base_addr c2 in
    let l1_page_table = MAP (\n. read_mem32 ((l1_tbl_base + n2w(n)) , mem)) (GENLIST (\n. n) 4096) in
	FOLDL (\lst.\l1_desc. if (((l1_desc && 0xfffffc00w) >> 10) = ptBase ) then lst else (l1_desc::lst)) [] l1_page_table `;

val list_dist_elements = Define `
list_dist_elements ls = 
 let ls_1 = FILTER (\el. ((el && 0x00000003w)  = 0b01w)) ls in
 let ls_2 = MAP (\el_1. ( el_1, (LENGTH (FILTER (\el_2. (el_2 = el_1)) ls_1)) ) ) ls_1 in
     FOLDL(\in_1.\in_2. if((FST (HD in_1)) = (FST in_2)) then in_1 else (in_2::in_1)) ([(HD ls_2)]) ls_2
`;
(*====================================================================================================
 page table entry physical address mask :
 PTAB_ENTRY_PHY_MASK_SECTION 0xfff00000	//aligned 1MB
 PTAB_ENTRY_PHY_MASK_LARGE 0xffff0000	//aligned 64KB
 PTAB_ENTRY_PHY_MASK_SMALL 0xfffff000	//aligned 4KB

 PHYS_OFFSET = curr_vm->mem_info.phys_offset;
 guest_size = curr_vm->mem_info.guest_size;
=====================================================================================================*)

(*function to find the first unmapped entry of the active l1 page table*)
val find_l1_unmapped = Define `
find_l1_unmapped c2 mem = 
    let l1_tbl_base =  mmu_tbl_base_addr c2 in
    let l1_page_table = AYMAP (\n:bool[32]. read_mem32 ((l1_tbl_base + n) , mem)) ( GENARRAY (\x:bool[12].(w2w (x)):bool[32]) ) in
    let idx = AYFIND (\l1_entry. let l1_desc =  l1_entry in
				 if (mmu_l1_decode_type(l1_desc) = 0b00w)
				 then T
				 else F
		 ) l1_page_table 0 in
	w2n(idx)
`;

val create_l1_pt_def = Define `
create_l1_pt (c1, c2, c3, mem, pgtype:bool[20]->bool[2], pgrefs:bool[20]->bool[30]) pa attrs  =
 (* The page directory is aligned to 16KB*)
 if ~(pa = pa && 0xFFFFC000w)
 then (c1, c2, c3, mem, pgtype, pgrefs)
 else  if ( (pgtype((w2w (pa >> 12)&& 0x000FFFFFw):bool[20]) = 0b01w ) /\
	    (pgtype((w2w ((pa >> 12)+ 0x1w) && 0x000FFFFFw):bool[20]) = 0b01w ) /\
	    (pgtype((w2w ((pa >> 12)+ 0x2w) && 0x000FFFFFw):bool[20]) = 0b01w ) /\
	    (pgtype((w2w ((pa >> 12)+ 0x3w) && 0x000FFFFFw):bool[20]) = 0b01w ) )
       (*there is already mapping as L1 for then given address pa*)
       then (c1, c2, c3, mem, pgtype, pgrefs)
       (*reference counter as l2 page/ data page *)
       else if ( ((pgtype((w2w (pa >> 12)&& 0x000FFFFFw):bool[20]) = 0b10w )           \/ (pgtype((w2w ((pa >> 12) + 0x1w) && 0x000FFFFFw):bool[20]) = 0b10w )   \/
		  (pgtype((w2w ((pa >> 12) + 0x2w) && 0x000FFFFFw):bool[20]) = 0b10w ) \/ (pgtype(((w2w (pa >> 12) + 0x3w) && 0x000FFFFFw):bool[20]) = 0b10w ))
	       ) 
            then  (c1, c2, c3, mem, pgtype, pgrefs)
	else if( ((pgtype((w2w (pa >> 12)&& 0x000FFFFFw):bool[20]) = 0b00w )             /\ (pgrefs((w2w (pa >> 12) && 0x000FFFFFw):bool[20]) > 0w)) \/
		   ((pgtype((w2w ((pa >> 12)+ 0x1w) && 0x000FFFFFw):bool[20]) = 0b00w )  /\ (pgrefs((w2w ((pa >> 12) + 0x1w) && 0x000FFFFFw):bool[20]) > 0w)) \/
		   ((pgtype((w2w ((pa >> 12)+ 0x2w) && 0x000FFFFFw):bool[20]) = 0b00w )  /\ (pgrefs((w2w ((pa >> 12) + 0x2w) && 0x000FFFFFw):bool[20]) > 0w)) \/
		   ((pgtype((w2w ((pa >> 12)+ 0x3w) && 0x000FFFFFw):bool[20]) = 0b00w )  /\ (pgrefs((w2w ((pa >> 12) + 0x3w) && 0x000FFFFFw):bool[20]) > 0w))
	       )
            then (c1, c2, c3, mem, pgtype, pgrefs)
            else if(!l1Idx:bool[32]. (l1Idx <+ 4096w)  ==>
		      let l1_desc_add = ((pa && 0xFFFFC000w) !! (l1Idx << 2) !! 0w) in (* base address is 16KB aligned*)
		      let l1_desc = read_mem32(l1_desc_add,mem) in
		      let l1_type = mmu_l1_decode_type(l1_desc) in
			  (l1_type = 0b00w) \/ ((l1_type = 0b01w) /\ ( 
						let l1_pg_desc =  rec'l1PTT l1_desc in 
                                                    ((pgtype ((w2w l1_pg_desc.addr >> 2):bool[20])) = 0b10w) )
					       ) \/
			                       ( (l1_type = 0b10w) /\ (
						let l1_sec_desc =  rec'l1SecT l1_desc in
						    (l1_sec_desc.typ = 0w) /\
                                                     ((l1_sec_desc.ap = 0b011w) ==> ~(?secIdx:bool[8]. 
						 				       ((pgtype ((w2w(l1_sec_desc.addr):bool[20] << 8) !! (w2w secIdx):bool[20])) <> 0b00w) 
										      ))
													 
										 )
					       )
		   )
	    then
		let pt = AYMAP (\n. read_mem32 ((pa + n) , mem)) ( GENARRAY (\x:bool[12].(w2w (x)):bool[32]) ) in

                let pgrefs' = AYFOLDL (\inPgrefs.\ptDesc.
         	    let l1_type = (ptDesc && 0x00000003w) in
			if ((l1_type = 0b10w) )
			then
			        let l1_sec_desc = rec'l1SecT ptDesc in 
                                let l1_bit_18 = (l1_sec_desc.typ) in
				    (*increase ref counter just for write references*)
			        if ((l1_bit_18 = 0w) /\ ((l1_sec_desc.dom = 0b01w) /\ (l1_sec_desc.ap = 0b011w))) 
				then 
			           let l1_sec_desc = rec'l1SecT ptDesc in
				   let old_sec_refs = AYMAP (\n. (inPgrefs (w2w((w2w l1_sec_desc.addr):bool[20] !! w2w(n)):bool[20]), n)) ( GENARRAY (\x:bool[8].(w2w (x)):bool[32]) ) in
				       AYFOLDL(* P *)( \inPgrefs. \refs.
				       	     ((w2w ((w2w l1_sec_desc.addr):bool[20] !! (w2w (SND refs)):bool[20])):bool[20] =+ ((FST refs) + 1w)) inPgrefs
				       ) inPgrefs old_sec_refs
			       else inPgrefs
			else 
			    (*increase the ref counter for entries of type page-table- once for each l2 page table*)
			    if ((l1_type = 0b01w))
			    then
       				let l1_pt_desc = rec'l1PTT ptDesc in
				let old_pointed_ph_page:bool[22] = l1_pt_desc.addr in
				let old_refs:bool[30] = inPgrefs  (w2w (old_pointed_ph_page>>2):bool[20]) in
		  	            ((w2w (old_pointed_ph_page >> 2):bool[20]) =+ (old_refs + 1w)) inPgrefs
			    else inPgrefs				 
			) pgrefs pt in

		  let pgtype0 = (
	             let pointed_ph_page = (w2w (0xFFFFF000w && pa):bool[20]) in
		       ((pointed_ph_page =+ (0b01w)) pgtype )) in
		  let pgtype1 = (
	             let pointed_ph_page = (w2w (0xFFFFF000w && pa):bool[20]) in
		       (((pointed_ph_page + 0x1w) =+ (0b01w)) pgtype0 )) in
		  let pgtype2 = (
	             let pointed_ph_page = (w2w (0xFFFFF000w && pa):bool[20]) in
		       (((pointed_ph_page + 0x2w) =+ (0b01w)) pgtype1 )) in
		  let pgtype' = (
	             let pointed_ph_page = (w2w (0xFFFFF000w && pa):bool[20]) in
		       (((pointed_ph_page + 0x3w) =+ (0b01w)) pgtype2 )) in
		             (c1,c2,c3,mem(* ' *),  pgtype', pgrefs') 
	      else (c1, c2, c3, mem,  pgtype, pgrefs)
`;

val create_l2_pt_def = Define `
create_l2_pt (c1, c2, c3, mem, pgtype:bool[20]->bool[2], pgrefs:bool[20]->bool[30]) (pa:bool[32]) attrs =
 (* The page table is aligned to 4KB(0xFFFFF000)/64KB(0xFFFF0000)- design choice*)
   if ~(pa = pa && 0xFFFFF000w)
   then (c1, c2, c3, mem, pgtype, pgrefs)
   else if (pgtype((w2w (pa >>> 12)&& 0x000FFFFFw):bool[20]) = 0b10w )
       (*the page is already an L2 page-table*)
       then (c1, c2, c3, mem,  pgtype, pgrefs)
       (*reference counter as l1 page/ data page *)
       else if ( ((pgtype((w2w (pa >>> 12)&& 0x000FFFFFw):bool[20]) = 0b01w )  (* /\ (pgrefs((w2w (pa >> 12)&& 0x000FFFFFw):bool[20]) > 0w) *))  \/
		 ((pgtype((w2w (pa >>> 12)&& 0x000FFFFFw):bool[20]) = 0b00w )  /\ (pgrefs((w2w (pa >>> 12)&& 0x000FFFFFw):bool[20]) > 0w)) )
            then (c1, c2, c3, mem,  pgtype, pgrefs)
            else if(!l2Idx:bool[32]. (l2Idx <+ 1024w) ==>
		          let l2_desc_add = ((pa && 0xFFFFF000w) !! (l2Idx << 2) !! 0w) in
		          let l2_desc = read_mem32(l2_desc_add,mem) in
  		          (*Extracing the type of the l2 page table descriptor*)
		          let l2_type = l2_desc_add && 0x00000003w  in 
			     (l2_type = 0b00w) \/ (((l2_type = 0b10w) (* \/ (l2_type = 0b11w) *)) /\ (
					               let l2_pg_desc =  rec'l2SmallT l2_desc in
						       (*Self reference with write access*)
                                                       (((l2_pg_desc.addr = (w2w ((pa && 0xFFFFF000w) >>> 12):bool[20])) /\ (l2_pg_desc.ap = 0b011w)) ==> F) /\
                                                       if ~((pgtype l2_pg_desc.addr) = 0b00w)
	 					       then (l2_pg_desc.ap = 0b010w)
	 					       else T
		   )))  
		 then 
		  let pgtype' = (
	             let pointed_ph_page = (w2w (0xFFFFF000w && pa):bool[20]) in
	             let pageType:bool[2] = pgtype pointed_ph_page  in
		       ((pointed_ph_page =+ (0b10w)) pgtype )) in
    	          let l2Pt = AYMAP (\n. read_mem32 ((pa + n) , mem)) ( GENARRAY (\x:bool[10].(w2w (x)):bool[32]) ) in
	          (* increase the ref counter for the Data page *)
                  let pgrefs' = AYFOLDL (\inPgrefs.\ptDesc. 
		      let l2_pt_desc = rec'l2SmallT ptDesc in
		      let old_pointed_ph_page:bool[20] = l2_pt_desc.addr in
		      if(((pgtype old_pointed_ph_page) = 0b00w) /\ (l2_pt_desc.ap = 0b011w))
		      then
		          let old_refs:bool[30] = inPgrefs old_pointed_ph_page in
		      	         (old_pointed_ph_page =+ (old_refs + 1w)) inPgrefs
		      else inPgrefs
		      ) pgrefs l2Pt in
		         (c1,c2,c3,mem,  pgtype', pgrefs')
        else (c1, c2, c3, mem,  pgtype, pgrefs)
`;

(*=====================================================================================================*)
(*TODO just check if the entry that we want to write new descriptor inside is marked as unmapped and raise an error otherwise*)
(*for mapping the given L2 to an L1 entry*)
val l1_pt_map_def = Define `
l1_pt_map (c1, c2, c3, mem, pgtype:bool[20]->bool[2], pgrefs:bool[20]->bool[30]) 
  va phadd (attrs:bool[32]) =
      let l1Base_add = mmu_tbl_base_addr c2 in
      let l1_idx = va >>> 20 in
      let l1_desc_add = l1Base_add ‖ (l1_idx ≪ 2) in  
      let page_desc =
	  (0xFFFFFC00w && phadd) !!
	  (0x000003FCw && attrs) !!
	  (0x01w)
      in 
      let l1_desc = read_mem32(l1_desc_add, mem) in 
      if((l1_desc_add && 0x00000003w) <> 0b00w)
      then
	  (c1, c2, c3, mem, pgtype,pgrefs)
      else
          let pgrefs' = (let pointed_ph_page:bool[20] = w2w((0xFFFFF000w && phadd) >>> 12):bool[20] in
                         let refs:bool[32] = (w2w (pgrefs pointed_ph_page)):bool[32]       in
  	                 if((refs + 1w) <+ 0x3FFFFFFFw)
			 then
   	      	           (pointed_ph_page =+ w2w(refs + 1w):bool[30]) pgrefs
			 else
			   (pointed_ph_page =+ (0w:bool[30])) pgrefs)in
	  if((pgrefs' (w2w((0xFFFFF000w && phadd) >>> 12):bool[20])) <> 0w)
	  then
	     let mem' = write_mem32(l1_desc_add, mem, page_desc)  in
	           (c1,c2,c3,mem',pgtype, pgrefs')
	  else
	     (c1,c2,c3,mem,pgtype, pgrefs)    
`;
(*The story here*)
(*for mapping the given L2 to an L1 entry*)
val l1_sec_map_def = Define`
l1_sec_map (c1, c2, c3, mem, pgtype:bool[20]->bool[2], pgrefs:bool[20]->bool[30]) 
  va phadd (attrs:bool[32]) =
   let ap = ((attrs >>  10) && 0b11w) !! (((attrs >>  13) && 0b100w)) in
   let l1Base_add = mmu_tbl_base_addr c2 in
   let l1_idx = va >>> 20 in
   let l1_desc_add = l1Base_add ‖ (l1_idx ≪ 2) in  
   let page_desc =
	  (0xFFF00000w && phadd) !!
	  0b10w !!
	  (0x000BFFFCw && attrs) !!
	  (0x01w)
   in
   let l1_desc = read_mem32(l1_desc_add, mem) in 
   if((l1_desc_add && 0x00000003w) <> 0b00w)
   then
       (c1, c2, c3, mem, pgtype,pgrefs)  
   else
       if(ap = 0b011w)
       then
	   let pointed_ph_page:bool[20] = w2w((0xFFFFF000w && phadd) >>> 12):bool[20] in
           let pgrefs' = (let old_sec_refs = AYMAP (\n. (pgrefs (w2w(pointed_ph_page !! n):bool[20]), n)) (GENARRAY (\x:bool[8].(w2w (x)):bool[20]))in  
 	  	                      AYFOLDL(\inPgrefs.\refs.
					    if( (((w2w (FST refs)):bool[32]) + 1w) <+ 0x3FFFFFFFw ) 
					    then
			      			((w2w (pointed_ph_page !! (SND refs)):bool[20]) =+ ((FST refs) + 1w)) inPgrefs
					    else
						((w2w (pointed_ph_page !! (SND refs)):bool[20]) =+ 0w) inPgrefs
    			               )pgrefs old_sec_refs
			 ) in
	  let zero_els = (let refls = AYMAP (\n. pgrefs' (w2w(pointed_ph_page !! n):bool[20])) (GENARRAY (\x:bool[8].(w2w (x)):bool[20])) in
	  		     (AYEXIST (\ref:bool[30]. (ref = 0w)) refls 0) ) in
	  if(zero_els)
	  then
              let mem' = write_mem32(l1_desc_add, mem, page_desc)  in
	  	  (c1,c2,c3,mem',pgtype, pgrefs')
	  else
	      (c1, c2, c3, mem, pgtype,pgrefs)
   else 
       if(ap = 0b010w)
       then 
            let mem' = write_mem32(l1_desc_add, mem, page_desc)  in
	     (c1,c2,c3,mem',pgtype, pgrefs)
       else
	    (c1, c2, c3, mem, pgtype,pgrefs)
`;

(*maps a virtual address to a given physical one*)
val hyper_l1_map_pagetable_def = Define `
hyper_l1_map_pagetable (c1, c2, c3, mem, pgtype:bool[20]->bool[2], pgrefs:bool[20]->bool[30]) 
  va phadd (attrs:bool[32]) =
   let ap = ((attrs >>  10) && 0b11w) !! (((attrs >>  13) && 0b100w)) in 
   if (pgtype((w2w (phadd >>> 12)&& 0x000FFFFFw):bool[20]) = 0b10w )
   then l1_pt_map (c1, c2, c3, mem, pgtype:bool[20]->bool[2], pgrefs:bool[20]->bool[30]) va phadd (attrs:bool[32])
   else if((phadd = phadd && 0xFFF00000w) /\
              (((ap = 0b011w) /\ (!secIdx:bool[8]. let sec_pg_add = ((w2w phadd):bool[20] !! (w2w (secIdx)):bool[20]) in 
                                                                    ((pgtype sec_pg_add) = 0b00w) 
				)
	       ) \/ (ap = 0b010w)
	      )
	     ) 
        then 
	       l1_sec_map (c1, c2, c3, mem, pgtype:bool[20]->bool[2], pgrefs:bool[20]->bool[30]) va phadd (attrs:bool[32]) 
        else
	       (c1,c2,c3,mem,pgtype, pgrefs)
	       
`;

val hyper_l2_map_pagetable_def = Define `
hyper_l2_map_pagetable (c1, c2, c3, mem, pgtype:bool[20]->bool[2], pgrefs:bool[20]->bool[30]) 
  va phadd (attrs:bool[32]) =
  let ap =  (((attrs >> 7) && 0b100w)) !! ((attrs >> 4) && 0b11w)  in 
  if ((ap = 0b011w) /\ ((pgtype((w2w (phadd && 0xFFFFF000w) >>> 12):bool[20]) = 0b01w ) \/ (pgtype((w2w (phadd && 0xFFFFF000w) >>> 12):bool[20]) = 0b10w )))    
  then
      (c1, c2, c3, mem, pgtype,pgrefs)
  else
      let l1Base_add = mmu_tbl_base_addr c2 in
      let l1_idx = va >>> 20 in
      let l1_desc_add = l1Base_add ‖ (l1_idx ≪ 2) in
      let l1_desc = read_mem32(l1_desc_add,mem) in
      let l1_ptb_desc = rec'l1PTT l1_desc in  
      let l2_desc_add = ((w2w (l1_ptb_desc.addr >>> 2)):bool[32] ‖ (((va && 0x000ff000w) >>> 12) ≪ 2)) in
      let l2_desc = read_mem32(l2_desc_add, mem) in
      if((l2_desc && 0x00000003w) <> 0b00w)
      then
	  (c1, c2, c3, mem, pgtype,pgrefs)
      else
          let page_desc =
		   (0xFFFFF000w && phadd) !!
                   (0x00000FFDw && attrs) !!
	           (0x10w)
          in 
          let pgrefs' = (
	      if(((pgtype (w2w((0xFFFFF000w && phadd) >>> 12):bool[20])) = 0b00w) /\ (ap = 0b011w))
	      then
      	         let pointed_ph_page:bool[20] = w2w((0xFFFFF000w && phadd) >>> 12):bool[20] in
   	         let refs:bool[30] = pgrefs pointed_ph_page  in
		     if((((w2w refs):bool[32]) + 1w) <+ 0x3FFFFFFFw)
		     then
      	  	        (pointed_ph_page =+ (refs + 1w)) pgrefs
		     else
			 (pointed_ph_page =+ 0w) pgrefs
	      else
	         pgrefs) in
      if((pgrefs' (w2w((0xFFFFF000w && phadd) >>> 12):bool[20])) <> 0w)
      then
          let mem' = write_mem32(l2_desc_add, mem, page_desc)  in 
	         (c1,c2,c3,mem',pgtype, pgrefs')
      else
         (c1,c2,c3,mem,pgtype, pgrefs)
`;
(*=====================================================================================================*)

(* Unmapping an entry of the L2 page table *)
val unmap_L2_pageTable_entry_def = Define `
unmap_L2_pageTable_entry (c1, c2, c3, mem, (pgtype:bool[20]->bool[2]) , pgrefs:bool[20]->bool[30]) va =
  let entryAddrL1 = (c2 && (0xFFFFC000w:word32) !! (((va && (0xFFF00000w:word32)) >>>20) << 2)) in
  if ~(pgtype ((w2w (entryAddrL1 >>> 12)):word20) = 0b01w)
  then  (c1, c2, c3, mem, pgtype , pgrefs)
  else
  let 
      descL1 = read_mem32(entryAddrL1, mem) in
      if ~(mmu_l1_decode_type descL1 = 0b01w)
      then 
	  (c1, c2, c3, mem, pgtype , pgrefs)
      else 
	  let ptEntry = rec'l1PTT descL1 in
	  let entryAddrL2 = (((w2w ptEntry.addr):word32 << 10) !! ((va && (0x000FF000w:word32)) >>> 10)) in
          if ~(pgtype ((w2w (entryAddrL2 >>> 12)):word20) = 0b10w)
	  then
	      (c1, c2, c3, mem, pgtype , pgrefs)
	  else
	      let page_desc = read_mem32(entryAddrL2, mem) in
  	      let l2_type = page_desc && 0x00000003w in (* mmu_l2_decode_type(page_desc)  *)
  	      if (l2_type = 0b10w)
	      then
		  let l2_pt_desc = rec'l2SmallT page_desc in
 	          let old_pointed_ph_page = l2_pt_desc.addr in
		  let pgrefs' = (
   		      if(((pgtype old_pointed_ph_page) = 0b00w) /\ (l2_pt_desc.ap = 0b011w))
		      then
			  let old_refs:bool[30] = pgrefs (w2w((old_pointed_ph_page && 0xFFFFF000w)>>12):bool[20]) in
			      (old_pointed_ph_page =+ (old_refs - 1w)) pgrefs
     		      else
     			  pgrefs
		      ) in
		  let page_desc' = page_desc && (0xfffffffcw:word32) in
 	          let mem' = write_mem32(entryAddrL2, mem, page_desc') in
		      (c1, c2, c3, mem',pgtype, pgrefs')
	      else
		  (c1, c2, c3, mem, pgtype , pgrefs)
`;

(* Unmapping an L2 page table. Note that there should be no reference to the requested page, this means there is no L1 page table which has a pointer to this L2 *)
val unmap_pageTable_l2_def = Define `
unmap_pageTable_l2 (c1, c2, c3, mem, pgtype:bool[20]->bool[2], pgrefs:bool[20]->bool[30]) baseAddr =
  if ~(pgtype((w2w (baseAddr >> 12) && 0x000FFFFFw):bool[20]) = 0b10w ) 
  then 
      (c1, c2, c3, mem, pgtype, pgrefs)
  else
     let entryAddrL1 = (pdirEntryAddr c2 baseAddr)     in 
     let descL1 = read_mem32(entryAddrL1, mem)         in
     let entryAddrL2 = (ptabEntryAddr descL1 baseAddr) in
   (*there are references from other page tables*)
     if ((pgrefs (w2w((baseAddr && 0xFFFFF000w) >> 12):bool[20])) > 0w)
     then
        (c1, c2, c3, mem, pgtype, pgrefs) (*Error: unchanged*)
     else 
         let pt = MAP (\n. read_mem32 ((baseAddr + n2w(n)) , mem)) (GENLIST (\n. n) 1024) in 
                let pgrefs' = FOLDL (\inPgrefs.\ptDesc.    
         	        let l2_type = ptDesc && 0x00000003w in
                        if l2_type = 0b00w then inPgrefs
                        else if ((l2_type = 0b10w) \/ (l2_type = 0b11w))
                             then
                             let l2_pt_desc = rec'l2SmallT ptDesc in
                             let old_pointed_ph_page = l2_pt_desc.addr in 
                             if(((pgtype old_pointed_ph_page) = 0b00w) /\ (l2_pt_desc.ap = 0b011w))
                             then
                               let old_refs:bool[30] = inPgrefs (w2w((old_pointed_ph_page && 0xFFFFF000w)>>12):bool[20]) in
                                   (old_pointed_ph_page =+ (old_refs - 1w)) inPgrefs 
			     else
				 inPgrefs
			else
			    inPgrefs
				     ) pgrefs pt
	        in 
       	        let pt' = MAP (\ptDesc. (ptDesc && 0xFFFFFFFCw)) pt in
                let mem' = FOLDL(\inMem.\indx.
                                 write_mem32((baseAddr + n2w(indx)), inMem, (EL indx pt')) 
				) mem (GENLIST (\n. n) 1024)
		in 
                let pgtype' = (( (w2w ((baseAddr && 0xFFFFF000w) >> 12):bool[20])=+ 0b00w ) pgtype) in
		              (c1, c2, c3, mem', pgtype', pgrefs')
	       
`;

val unmap_L1_pageTable_entry_def = Define `
unmap_pageTable_entry (c1, c2, c3, mem, pgrefs:bool[20]->bool[30], (pgtype:bool[20]->bool[2])) add =
  let pt_add = mmu_tbl_base_addr c2 in
  let page_idx = add >>> 20 in
  let sec_add = pt_add ‖ (page_idx ≪ 2)in
  let l1_desc = read_mem32(sec_add, mem ) in
  let l1_type = mmu_l1_decode_type(l1_desc) in
  if(l1_type = 0b01w)
  then
      let l1_pt_desc = rec'l1PTT l1_desc in
      let pt = MAP (\n. read_mem32 ((pt_add + n2w(n)) , mem)) (GENLIST (\n. n) 4096) in
      let distEl = list_dist_elements pt in
      let entry_desc = FILTER (\el. ((FST el) = l1_desc)) distEl in
      if((SND (HD entry_desc)) > 1)
      	then 
      	  let l1_desc' = l1_desc && 0xfffffffcw in
          let mem' = write_mem32(sec_add, mem, l1_desc') in
      	      (c1, c2, c3, mem', pgrefs,pgtype)
        else
          let l1_desc' = l1_desc && 0xfffffffcw in
          let mem' = write_mem32(sec_add, mem, l1_desc') in
          let old_pointed_ph_page = l1_pt_desc.addr in
      	  let old_refs:bool[30] = pgrefs (w2w((old_pointed_ph_page && 0xFFFFF000w)>>12):bool[20]) in
          let pgrefs' = (( (w2w((old_pointed_ph_page && 0xFFFFF000w)>>12):bool[20]) =+ (old_refs - 1w)) pgrefs) in
      	      (c1, c2, c3, mem', pgrefs',pgtype) 
  else 	
      let l1_sec_desc = rec'l1SecT l1_desc in
      let l1_bit_18 = ((l1_desc && 0x40000w) >>> 18) in
      if((l1_type = 0b10w) /\ (l1_bit_18 = 0w) )
      then
	  if(l1_sec_desc.ap = 0b011w)
	  then 
	      let l1_desc' = l1_desc && 0xfffffffcw in
	      let mem' = write_mem32(sec_add, mem, l1_desc') in
	      let old_sec_refs = MAP (\n. (pgrefs (w2w(l1_sec_desc.addr + n2w(n)):bool[20]), n)) (GENLIST (\n. n) 256) in 
	      let pgrefs' =  FOLDL(\inPgrefs.\refs.
	                           ((w2w (l1_sec_desc.addr + n2w(SND refs)):bool[20]) =+ ((FST refs) - 1w)) pgrefs
	                     ) pgrefs old_sec_refs in
		  (c1, c2, c3, mem', pgrefs',pgtype)
	  else
	      let l1_desc' = l1_desc && 0xfffffffcw in
	      let mem' = write_mem32(sec_add, mem, l1_desc') in
		  (c1, c2, c3, mem', pgrefs,pgtype)
      else
	  (c1, c2, c3, mem, pgrefs,pgtype)
`;

(* We do not need to check if the pointed l2 is safe, because at this point we assume that every thing has been built according to our safety criteria *)
(*TODO for future add PHYS_OFFSET guest_size to the invariant*)

(* Try the switch hypervisor API *)
val hyper_switch_def = Define `
hyper_switch (c1, c2, c3, mem, pgtype, pgrefs) param_c2 =
  if (param_c2 <> (param_c2 && 0xFFFFC000w:bool[32])) 
  then
      (c1,c2,c3,mem,pgtype,pgrefs)
  else 
      if ~( (pgtype((w2w (param_c2 >>> 12)&& 0x000FFFFFw):bool[20]) = 0b01w ) /\
	    (pgtype((w2w ((param_c2 >>> 12)+ 0x1w) && 0x000FFFFFw):bool[20]) = 0b01w ) /\
	    (pgtype((w2w ((param_c2 >>> 12)+ 0x2w) && 0x000FFFFFw):bool[20]) = 0b01w ) /\
	    (pgtype((w2w ((param_c2 >>> 12)+ 0x3w) && 0x000FFFFFw):bool[20]) = 0b01w ) )
      then
	  (c1,c2,c3,mem,pgtype,pgrefs)
  else let tbl_base = mmu_tbl_base_addr param_c2 in 
       if  (!va:bool[32]. 
		     (!byte_idx:bool[32].
				    ((pgtype ((w2w ((tbl_base ) >>> 12)):bool[20]) = 0b01w)) ==> 
				    ((pgtype ((w2w(((tbl_base !! ((va >>> 20) << 2)) +  byte_idx) >>> 12)):bool[20])) = 0b01w)
		     )
	    )
       then
	   (c1,param_c2,c3,mem,pgtype,pgrefs)
   else
      (c1, c2, c3, mem, pgtype, pgrefs)
`;


(*=====================================================================================================*)
val count_pages_for_pt_def = Define `
count_pages_for_pt mem (pgtype:bool[20]->bool[2])  ph_page pt_page =
  if ~( (((pgtype pt_page) = 0b01w) /\  (pt_page = pt_page  && 0xFFFFC000w)) \/ ((pgtype pt_page) = 0b10w))
  then 0 
  else if((((pgtype pt_page) = 0b01w) /\  (pt_page = pt_page  && 0xFFFFC000w)) /\ ((pgtype ph_page) = 0b10w))
       then
          let pointed = (?idx:bool[12].
                         let l1_desc = read_mem32 ((((w2w pt_page):bool[32]) !! (w2w(idx):bool[32] << 2)) , mem) in
			 let l1_pt_desc = rec'l1PTT l1_desc in (
			     ((l1_desc && 0x00000003w)  = 0b01w) /\ ((w2w (l1_pt_desc.addr >> 2):bool[20]) = ph_page)
			     )
			) in
	   if pointed then 1 else 0
      else if((((pgtype pt_page) = 0b01w) /\  (pt_page = pt_page  && 0xFFFFC000w)) /\ ((pgtype ph_page) = 0b00w)) 
           then
	        SUM 4096 (\it.
			  let pg_idx:bool[12] = n2w(it) in
   		         (*this 0xFFFFC000 has been used because the base address of an L1 page table should be 16KB aligned*)
	  	          let global_page_add = ((w2w pt_page):bool[32] && 0xFFFFC000w) in (*(w2w pt_page):bool[32] << 14*)
			  let desc_add = (global_page_add !! ((w2w pg_idx):bool[32] << 2) !! 0w) in
			  let l1_desc = read_mem32(desc_add,mem) in
			  let l1_type = mmu_l1_decode_type(l1_desc) in
			  let l1_sec_desc = rec'l1SecT l1_desc in
			  if ((l1_type = 0b10w) /\  (l1_sec_desc.typ = 0w) /\ (l1_sec_desc.ap = 0b011w)) 
			  then (* we just consider the 20 most significant bits of each address *)
			       if (?indx:bool[8]. ( ((((w2w l1_sec_desc.addr):bool[20] << 8) !! (w2w indx):bool[20])) = ph_page)) 
			       then 1
			       else 0
			 else 0
			 ) 
      else  if(((pgtype pt_page) = 0b10w) /\ ((pgtype ph_page) = 0b00w)) 
	    then SUM 1024 (\it.	
			  let pt_idx:bool[8] = n2w(it) in
	  	          let global_page_add = (w2w pt_page):bool[32]  in
			  let desc_add = (global_page_add !! ((w2w pt_idx):bool[32] << 2) !! 0w) in
			  let l2_desc = read_mem32(desc_add,mem) in
			  let l2_type = (l2_desc && 0x00000003w) in
			  let l2_pt_desc = rec'l2SmallT l2_desc in  
			      if (l2_pt_desc.addr = ph_page) 
			      then
				  if (((l2_type = 0b10w)\/(l2_type = 0b11w)) /\ (l2_pt_desc.ap = 0b011w)) 
				  then 1 
				  else 0
			      else 
				  0
 			  )
		     else 0
`;

val count_pages_def =  Define `
count_pages mem (pgtype:bool[20]->bool[2])  ph_page =
SUM 1048576 (\it.
  let global_page:bool[20] = n2w(it) in
  count_pages_for_pt mem pgtype ph_page global_page)
`;



(* val invariant_page_type_l1_def = Define ` *)
(*  invariant_page_type_l1_def (c1:sctlrT) (c2:bool[32]) (c3:bool[32]) mem *)
(*     (pgtype:bool[20]->bool[2]) (pgrefs:bool[20]->bool[30]) (ph_page:bool[20]) = *)
(*      !pg_idx:bool[12]. *)
(*       let global_page_add = ((w2w (ph_page && 0xFFFFCw)):bool[32] << 12) in *)
(*       let desc_add = (global_page_add !! ((w2w pg_idx):bool[32] << 2) !! 0w) in  *)
(*       let l1_desc = read_mem32(desc_add,mem) in  *)
(*       let l1_type = mmu_l1_decode_type(l1_desc) in *)
(*       let l1_bit_18 = ((l1_desc && 0x40000w) >>> 18) in *)
(*       let l1_sec_desc = rec'l1SecT l1_desc in *)
(*       let l1_pt_desc = rec'l1PTT l1_desc in *)
(*         (l1_type = 0b00w) \/  *)
(* 	((l1_type = 0b10w) /\ *)
(*          (l1_bit_18 = 0w) /\   *)
(*                   (((!sec_idx:bool[8]. *)
(*                          ((pgtype (((w2w (l1_sec_desc.addr)):bool[20] << 8) ‖ ((w2w sec_idx):bool[20]))) = 0b00w)  /\		     *)
(* 				((l1_sec_desc.ap = 0b011w) \/ (l1_sec_desc.ap = 0b010w)))) *)
(*                          \/ (l1_sec_desc.ap = 0b010w)) *)
(* 		) \/   *)
(* 	((l1_type = 0b01w) /\ *)
(* 	 ((pgtype ((w2w (l1_pt_desc.addr >>> 2)):bool[20])) = 0b10w) *)
(* 	) *)
(* `; *)

val invariant_page_type_l1_def = Define `
 invariant_page_type_l1_def (c1:sctlrT) (c2:bool[32]) (c3:bool[32]) mem
    (pgtype:bool[20]->bool[2]) (pgrefs:bool[20]->bool[30]) (ph_page:bool[20]) =
     !pg_idx:bool[12].
      let global_page_add = ((w2w (ph_page && 0xFFFFCw)):bool[32] << 12) in
      let desc_add = (global_page_add !! ((w2w pg_idx):bool[32] << 2) !! 0w) in 
      let l1_desc = read_mem32(desc_add,mem) in 
      let l1_type = mmu_l1_decode_type(l1_desc) in
      let l1_bit_18 = ((l1_desc && 0x40000w) >>> 18) in
      let l1_sec_desc = rec'l1SecT l1_desc in
      let l1_pt_desc = rec'l1PTT l1_desc in
        (l1_type = 0b00w) \/ 
	((l1_type = 0b10w) /\
         (l1_bit_18 = 0w) /\  
                  ( 
		   (* !ph_page'.((l1_sec_desc.addr = (w2w (ph_page' >>> 8)):bool[12]) /\  *)
                   (*           pgtype ph_page' <> 0x0w) ==> (l1_sec_desc.ap = 0b010w) *)
		   (l1_sec_desc.addr = (w2w (ph_page >>> 8)):bool[12]) /\ (l1_sec_desc.ap = 0b010w)
		  )
	) \/
	((l1_type = 0b01w) /\
	 (((pgtype ((w2w (l1_pt_desc.addr >>> 2)):bool[20])) = 0b10w) /\
              (!va:bool[32]. 
	       (!byte_idx:bool[32].
               let tbl_base = ((w2w (l1_pt_desc.addr >>> 2)):bool[32]) in
	       ((pgtype ((w2w ((tbl_base ) >>> 12)):bool[20])) = 0b10w) ==>
	       ((pgtype ((w2w(((((w2w l1_pt_desc.addr):bool[32] << 10) !! (((va && (0x000ff000w:bool[32])) >>> 12) << 2)) +  byte_idx) >>> 12)):bool[20])) = 0b10w)
	     )) /\
         (!l2_idx:bool[8]. 
         let l2_desc = (read_mem32((((w2w l1_pt_desc.addr):bool[32] << 10) !! (w2w l2_idx):bool[32] << 2 !! 0w),mem)) 
	 in(*This because we are missing the condition where, in l3 implementation, the l2 entry type is 0b00w *)
	     (((l2_desc && 0b11w) = 0b00w) /\ ((rec'l2SmallT l2_desc).ap <> 0b011w)) \/ 
	     (((l2_desc && 0b11w) = 0b10w) /\ 
		(
		((rec'l2SmallT l2_desc).addr = ph_page) /\  ((rec'l2SmallT l2_desc).ap = 0b010w)
		)
	     )
	 ))
	)

`;


(* val invariant_page_type_def = Define ` *)
(*   invariant_page_type (c1:sctlrT) (c2:bool[32]) (c3:bool[32]) mem *)
(*     (pgtype:bool[20]->bool[2]) (pgrefs:bool[20]->bool[30]) = *)
(*   c1.M ==> *)
(*   ((!dom . ((mmu_domain_status (c3,dom) = 0w) \/  *)
(*             (mmu_domain_status (c3,dom) = 0b01w)) *)
(*   ) /\ *)
(*   let tbl_base = mmu_tbl_base_addr c2 in *)
(*   (* The page table is aligned to 16KB *) *)
(*   (tbl_base = tbl_base && 0xFFFFC000w) /\ *)
(*   (* The page table points to a page marked as L1 *) *)
(*   (pgtype ((w2w ((tbl_base ) >>> 12)):bool[20]) = 0b01w) /\ *)
  
(*   (* All pages marked as L1 contain well defined pt *) *)
(*   (* I assume that a  L1/L2 page tables are located at the beginning of sections *) *)
(*   !ph_page:bool[20]. *)
(*   ( *)
(*    (!ty:bool[2]. (pgtype ph_page = ty) ==>          *)
(*       (!pg_idx:bool[12]. *)
(* 	let pg_add = ((w2w ph_page):bool[32]) !! ((w2w pg_idx):bool[32])  in *)
(* 	       ((pgtype ((w2w (pg_add >>> 12)):bool[20])) = ty) )) /\ *)
(*    (((pgtype ph_page) = 0b01w) ==> (ph_page = ph_page && 0xFFFFCw) /\ *)
(* 	 ((pgtype ph_page) <> 0b11w) /\  *)
(*       ( *)
(*         ( *)
(* 	((pgtype ph_page) = 0b01w) ==>  *)
(* 		invariant_page_type_l1_def c1 c2 c3 mem pgtype pgrefs ph_page *)
(* 	)  *)
(*       ) *)
(*    ) *)
(*   ) *)
(* ) *)
(* `; *)
val invariant_page_type_def = Define `
  invariant_page_type (c1:sctlrT) (c2:bool[32]) (c3:bool[32]) mem
    (pgtype:bool[20]->bool[2]) (pgrefs:bool[20]->bool[30]) =
  c1.M ==>
  ((!dom . ((mmu_domain_status (c3,dom) = 0w) \/ 
            (mmu_domain_status (c3,dom) = 0b01w))
  ) /\
  let tbl_base = mmu_tbl_base_addr c2 in
  (* The page table is aligned to 16KB *)
  (tbl_base = tbl_base && 0xFFFFC000w) /\
  (* The page table points to a page marked as L1 *)
  (pgtype ((w2w ((tbl_base ) >>> 12)):bool[20]) = 0b01w) /\
  (!va:bool[32]. 
   (!byte_idx:bool[32].
   ((pgtype ((w2w ((tbl_base ) >>> 12)):bool[20]) = 0b01w)) ==> 
       ((pgtype ((w2w(((tbl_base !! ((va >>> 20) << 2)) +  byte_idx) >>> 12)):bool[20])) = 0b01w)
   )
  )/\
  (* All pages marked as L1 contain well defined pt *)
  (* I assume that a  L1/L2 page tables are located at the beginning of sections *)
  !ph_page:bool[20].
  (
   (((pgtype ph_page) = 0b01w) ==> ((ph_page = ph_page && 0xFFFFCw) ==>
    ((pgtype (ph_page !! 0b00w) = 0b01w ) /\ (pgtype (ph_page !! 0b01w) = 0b01w )  /\
     (pgtype (ph_page !! 0b10w) = 0b01w ) /\ (pgtype (ph_page !! 0b11w) = 0b01w ))) /\
	 ((pgtype ph_page) <> 0b11w) /\ 
		invariant_page_type_l1_def c1 c2 c3 mem pgtype pgrefs ph_page
   )
  )
)
`;
(* /\ *)
(* || *)

(* invariant on page types *)
val invariant_ref_cnt_def = Define `
  invariant_ref_cnt (c1:sctlrT) (c2:bool[32]) (c3:bool[32]) mem
    (pgtype:bool[20]->bool[2])
    (pgrefs:bool[20]->bool[30]) =
  c1.M ==>
  ((!dom . ((mmu_domain_status (c3,dom) = 0w) \/ 
            (mmu_domain_status (c3,dom) = 0b01w))
  ) /\
  let tbl_base = mmu_tbl_base_addr c2 in
  (* The page table is aligned to 16KB *)
  (tbl_base = tbl_base && 0xFFFFC000w) /\
  (* The page table points to a page marked as L1 *)
  (pgtype ((w2w ((tbl_base ) >>> 12)):bool[20]) = 0b01w) /\
  (!va:bool[32]. 
   (!byte_idx:bool[32].
   ((pgtype ((w2w ((tbl_base ) >>> 12)):bool[20]) = 0b01w)) ==> 
       ((pgtype ((w2w(((tbl_base !! ((va >>> 20) << 2)) +  byte_idx) >>> 12)):bool[20])) = 0b01w)
   )
  )/\
  (* All pages marked as L1 contain well defined pt *)
  (* I assume that a  L1/L2 page tables are located at the beginning of sections *)
  !ph_page:bool[20].
 (
  (
   (((pgtype ph_page) = 0b01w) ==> ((ph_page = ph_page && 0xFFFFCw) ==>
    ((pgtype (ph_page !! 0b00w) = 0b01w ) /\ (pgtype (ph_page !! 0b01w) = 0b01w )  /\
     (pgtype (ph_page !! 0b10w) = 0b01w ) /\ (pgtype (ph_page !! 0b11w) = 0b01w ))) /\
	 ((pgtype ph_page) <> 0b11w) /\ 
		invariant_page_type_l1_def c1 c2 c3 mem pgtype pgrefs ph_page
   )
  )
   /\ ( w2n(pgrefs ph_page) = (count_pages mem pgtype ph_page ))
 )
)
`;

(*=====================================================================================================
GOALS
=====================================================================================================*)

val goal = ``
  let c1 = 1w in
  let c1_rec = rec'sctlrT c1 in
  (invariant_page_type c1_rec c2 c3 mem pgtype pgrefs) ==>
  let (c1',c2',c3',mem',pgtype', pgrefs') =
      (unmap_pageTable_l2 (c1, c2, c3, mem, pgtype, pgrefs) pt_to_unmap) in
  let c1_rec' = rec'sctlrT c1' in
      (invariant_page_type c1_rec' c2' c3' mem' pgtype' pgrefs')
``;
val goal = ``
  let c1 = 1w in
  let c1_rec = rec'sctlrT c1 in
  (invariant_ref_cnt c1_rec c2 c3 mem pgtype ptrefs) ==>
  let (c1',c2',c3',mem',pgtype', pgrefs') =
      (unmap_pageTable_l2 (c1, c2, c3, mem, pgtype, pgrefs) sec_to_unmap) in
  let c1_rec' = rec'sctlrT c1' in
      (invariant_ref_cnt c1_rec' c2' c3' mem' pgtype' ptrefs')
``;

open HolKernel boolLib bossLib Parse;

open tinyTheory;
open mmu_utilsTheory;
open wordsLib;

val _ = new_theory "example_mmu_proof";

(* MMU DISABLED *)
Q.store_thm("mmu_disabled_thm", `
  let (c1, c2, c3) = (rec'sctlrT 0w, 0w, 0w) in
   mmu_safe c1 c2 c3 mem F
`,
  (computeLib.RESTR_EVAL_TAC [``mmu_safe`` ])
THEN (SIMP_TAC (srw_ss()) [mmu_safe_def])
THEN (REPEAT GEN_TAC)
THEN (REPEAT STRIP_TAC)
THEN (SIMP_TAC (srw_ss()) [mmu_mem_equiv_def])
THEN (REPEAT GEN_TAC)
THEN (SIMP_TAC (srw_ss()) [mmu_byte_def])
THEN (FULL_SIMP_TAC (srw_ss()) [rec'sctlrT_def])
);


val _ = export_theory ();

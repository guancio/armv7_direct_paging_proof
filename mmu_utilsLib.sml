open HolKernel boolLib bossLib Parse wordsLib;

open numSyntax;

structure mmu_utilsLib :> mmu_utilsLib =
struct

val UNDISCH_MATCH_TAC = fn MATCH => 
			   (PAT_ASSUM MATCH (fn th => (MP_TAC th)));

val UNDISCH_ALL_TAC = (REPEAT (UNDISCH_MATCH_TAC ``X``));

fun THM_KEEP_TAC pattern tac = 
    PAT_ASSUM pattern (fn thm =>
       (ASSUME_TAC thm) THEN
        tac THEN
	(ASSUME_TAC thm)
    );
fun NO_THM_KEEP_TAC pattern tac = 
    PAT_ASSUM pattern (fn thm =>
        tac THEN
	(ASSUME_TAC thm)
    );
fun LET_HYP_PAT_ELIM_TAC pat thms = 
    PAT_ASSUM pat
	      (fn thm => 
		  ASSUME_TAC (
		  SIMP_RULE (srw_ss()) 
			    ([Once combinTheory.LET_FORALL_ELIM,
			     combinTheory.S_DEF,
			     markerTheory.Abbrev_def
			     ]@thms) thm))
;
fun LET_HYP_ELIM_TAC thms = 
    LET_HYP_PAT_ELIM_TAC ``let x = a in P`` thms;

fun FULL_SIMP_BY_ASSUMPTION_TAC pattern = 
    PAT_ASSUM pattern (fn thm => FULL_SIMP_TAC
    (srw_ss()) [thm]);
fun SIMP_BY_ASSUMPTION_TAC pattern = 
    PAT_ASSUM pattern (fn thm => 
    RULE_ASSUM_TAC (SIMP_RULE (srw_ss()) [thm]));

fun SIMP_AND_KEEP_BY_ASSUMPTION_TAC pattern = 
    THM_KEEP_TAC pattern (
    SIMP_BY_ASSUMPTION_TAC pattern);

fun SPEC_ASSUMPTION_TAC pattern term = 
  PAT_ASSUM pattern (fn thm => ASSUME_TAC (SPEC term thm));

fun FULL_SIMP_BY_BLAST_TAC term =
    (FULL_SIMP_TAC (srw_ss()) [
     blastLib.BBLAST_PROVE(term)]);

fun DISJX_TAC nth =
MAP_EVERY (fn x =>
	      if x = 0 then DISJ1_TAC
	      else DISJ2_TAC)
(List.tabulate (nth, fn x => nth-x-1));

fun ASSUME_ALL_TAC thms =
MAP_EVERY (fn x => ASSUME_TAC x) thms;

fun SYM_ASSUMPTION_TAC pattern = 
  PAT_ASSUM pattern (fn thm => ASSUME_TAC (GSYM thm));

fun member y []      = false
 |  member y (x::xs) = x=y orelse member y xs;


fun list_comp l1 l2 =
   let fun comp rls ls =
       case (null rls) of true => ls
	    | _ => 
	      (if (member (hd rls) l2 )
	       then (comp (tl rls) (ls@[(hd rls)]))
	       else (comp (tl rls) ls))
   in
       comp l1 []
   end;
fun remFirst _ [] rest = (false, rev rest)
  | remFirst x (y::ys) rest =
    if x = y then
        (true, rev rest @ ys)
    else
        remFirst x ys (y :: rest);

fun ADD_CANC_TAC (asl, w) =
    let val (eq_1, eq_2) = dest_eq w
	val l1 = numSyntax.strip_plus eq_1
	val l2 = numSyntax.strip_plus eq_2
	val com = hd (list_comp l1 l2)
	val eq_11 = snd (remFirst com l1 [])
	val eq_22 = snd (remFirst com l2 [])
	val eq = mk_eq((foldl mk_plus (hd eq_11) (tl eq_11)),(foldl mk_plus (hd eq_22) (tl eq_22)))
    in
    (* FULL_SIMP_TAC(arith_ss)[ *)
  (`^w = ^eq` by FULL_SIMP_TAC(arith_ss)[])
    end (asl,w);


end


signature mmu_utilsLib =
sig
  val UNDISCH_MATCH_TAC  : Abbrev.term -> Abbrev.tactic
  val UNDISCH_ALL_TAC : Abbrev.tactic
  val THM_KEEP_TAC : Abbrev.term -> Abbrev.tactic -> Abbrev.tactic
  val NO_THM_KEEP_TAC : Abbrev.term -> Abbrev.tactic -> Abbrev.tactic
  val LET_HYP_ELIM_TAC : BoundedRewrites.thm list -> Abbrev.tactic
  val FULL_SIMP_BY_ASSUMPTION_TAC :Abbrev.term -> Abbrev.tactic
  val SIMP_BY_ASSUMPTION_TAC :Abbrev.term -> Abbrev.tactic
  val SIMP_AND_KEEP_BY_ASSUMPTION_TAC :Abbrev.term -> Abbrev.tactic
  val SPEC_ASSUMPTION_TAC : Abbrev.term -> Abbrev.term ->
			    Abbrev.tactic
  val FULL_SIMP_BY_BLAST_TAC : Abbrev.term -> Abbrev.tactic
  val DISJX_TAC : int -> Abbrev.tactic
  val ASSUME_ALL_TAC:  Abbrev.thm list -> Abbrev.tactic
  val SYM_ASSUMPTION_TAC  : Abbrev.term -> Abbrev.tactic
  val ADD_CANC_TAC : Abbrev.tactic
end

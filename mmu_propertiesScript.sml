
open HolKernel boolLib bossLib Parse;
open tinyTheory;

val _ = new_theory "mmu_properties";

val mmu_phi_byte_def = Define `
  mmu_phi_byte (c1:sctlrT) c2 (c3:bool[32]) mem PL1 ph_add =
   let und = (!add. 
       let (u,rd,wt,ex,pa) = mmu_byte(c1, c2, c3, mem, PL1, add) in u
   ) in 
   let rd = (?add. 
       let (u,rd,wt,ex,pa) = mmu_byte(c1, c2, c3, mem, PL1, add) in
       (rd /\ (pa=ph_add))
   ) in
   let wt = (?add. 
       let (u,rd,wt,ex,pa) = mmu_byte(c1, c2, c3, mem, PL1, add) in
       (wt /\ (pa=ph_add))
   ) in
   let ex = (?add. 
       let (u,rd,wt,ex,pa) = mmu_byte(c1, c2, c3, mem, PL1, add) in
       (ex /\ (pa=ph_add))
   ) in (und, rd, wt, ex)
`;


(* **************************************** *)
(* Utility definitions *)
(* **************************************** *)

val mmu_mem_equiv_def = Define `
  mmu_mem_equiv (c1:sctlrT) c2 (c3:bool[32]) mem mem' =
    !PL1 add. 
	 mmu_byte(c1, c2, c3, mem, PL1, add) =
              mmu_byte(c1, c2, c3, mem', PL1, add)
`;

(* val mmu_read_set_def = Define ` *)
(*   mmu_read_set (c1:sctlrT) c2 (c3:bool[32]) mem PL1 = *)
(*    {ph_add | *)
(*     let (u,rd,wt,ex) = mmu_phi_byte c1 c2 c3 mem PL1 ph_add in rd *)
(*    } *)
(* `; *)

(* val mmu_write_set_def = Define ` *)
(*   mmu_write_set (c1:sctlrT) c2 (c3:bool[32]) mem PL1 = *)
(*    {ph_add | *)
(*     let (u,rd,wt,ex) = mmu_phi_byte c1 c2 c3 mem PL1 ph_add in wt *)
(*    } *)
(* `; *)

val mmu_is_reachable_memory_def = Define `
  mmu_is_reachable_memory (c1:sctlrT) c2 (c3:bool[32]) mem PL1 mem' =
    (!ph_add.(mem(ph_add) <> mem'(ph_add) ==> (
       let (u,rd,wt,ex) = mmu_phi_byte c1 c2 c3 mem PL1 ph_add in
       u /\ wt
    )))
`;

val mmu_safe_def = Define `
  mmu_safe (c1:sctlrT) c2 (c3:bool[32]) mem PL1 =
    !mem'.
    (mmu_is_reachable_memory (c1:sctlrT) c2 (c3:bool[32]) mem PL1 mem')  ==>
    (mmu_mem_equiv c1 c2 c3 mem mem')
`;

val write_mem32_def = Define `
write_mem32 (add:bool[32], mem, value:bool[32]) =
  let mem = (add =+ (w2w(value && 0xFFw):bool[8])) mem in
  let mem = (add+1w =+ (w2w((value >>> 8) && 0xFFw):bool[8])) mem in
  let mem = (add+2w =+ (w2w((value >>> 16) && 0xFFw):bool[8])) mem in
  let mem = (add+3w =+ (w2w((value >>> 24) && 0xFFw):bool[8])) mem in
      mem
`;


val _ = export_theory ();

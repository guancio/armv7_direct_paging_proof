(* 
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/spec/" :: !loadPath;
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/" ::  !loadPath;
loadPath := "/NOBACKUP/workspaces/robertog/experiments_mmu/l3/proof/" :: !loadPath;
load "mmu_utilsLib";
load "hypervisor_modelTheory";
load "mmu_propertiesTheory";
load "helperTheory";
*)




val read_mem_eq = prove(``
  !a.
  (mem(a) = mem'(a)) ==>
  (mem(a+1w) = mem'(a+1w)) ==>
  (mem(a+2w) = mem'(a+2w)) ==>
  (mem(a+3w) = mem'(a+3w)) ==>
  (read_mem32(a,mem) = read_mem32(a,mem'))
``, FULL_SIMP_TAC (srw_ss()) [read_mem32_def]);

val mmu_safe_def = Define `
  mmu_safe (c1:sctlrT) c2 (c3:bool[32]) mem PL1 =
    !mem'. 
    (!ph_add.(mem(ph_add) <> mem'(ph_add) ==> (
       let (u,rd,wt,ex) = mmu_phi_byte c1 c2 c3 mem PL1 ph_add in
       u /\ wt
    )))  ==>
    (mmu_mem_equiv c1 c2 c3 mem mem')
`;

val goal3 = ``
     !c1.
     (c1 = rec'sctlrT 1w ) ==>
     (invariant_page_type c1 c2 c3 mem pgtype pgrefs) ==>
	  (mmu_safe c1 c2 c3 mem F)
``;
(* val goal3 = `` *)
(*      !c1. *)
(*      (c1 = rec'sctlrT 1w ) ==> *)
(*      (invariant_ref_cnt c1 c2 c3 mem pgtype pgrefs) ==> *)
(* 	  (mmu_safe c1 c2 c3 mem F) *)
(* ``; *)
g `^goal3`;



e (REPEAT STRIP_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_safe_def]);
e (IMP_RES_TAC hlp);
e (IMP_RES_TAC pg_l1);
e (IMP_RES_TAC pg_l2);
(* e (IMP_RES_TAC invariant_ref_cnt_def); *)
e (IMP_RES_TAC invariant_page_type_def);
e(`c1.M` by  (FULL_SIMP_TAC (srw_ss()) [rec'sctlrT_def]));
e RES_TAC;
e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) [LET_DEF(* ,mmu_tbl_base_addr_def *)]);
e (REPEAT STRIP_TAC);
(* mmu_tbl_base_addr_def,mmu_tbl_index_def *)
e (FULL_SIMP_TAC (srw_ss()) [mmu_mem_equiv_def,mmu_byte_def]);
e (REPEAT STRIP_TAC);

e (PAT_ASSUM ``!x:word20->word2 y:word32. X`` (fn thm =>  ASSUME_TAC (SPEC ``pgtype:word20->word2`` thm)));

e (PAT_ASSUM `` ∀c2'.
         (pgtype (w2w (mmu_tbl_base_addr c2' ⋙ 12)) = 1w) ∧
         (mmu_tbl_base_addr c2' =
          0xFFFFC000w && mmu_tbl_base_addr c2') ∧
         ((mmu_tbl_base_addr c2' =
           0xFFFFC000w && mmu_tbl_base_addr c2') ⇒
          (pgtype (w2w (mmu_tbl_base_addr c2' ⋙ 12) ‖ 1w) = 1w) ∧
          (pgtype (w2w (mmu_tbl_base_addr c2' ⋙ 12) ‖ 2w) = 1w) ∧
          (pgtype (w2w (mmu_tbl_base_addr c2' ⋙ 12) ‖ 3w) = 1w)) ⇒
         ∀va byte_idx.
           pgtype
             (w2w
                ((w2w byte_idx +
                  (mmu_tbl_base_addr c2' ‖ va ⋙ 20 ≪ 2)) ⋙ 12)) =
           1w``(fn thm =>  ASSUME_TAC (SPEC ``c2:word32`` thm)));
e (FULL_SIMP_TAC(bool_ss)[mmu_tbl_index_def, mmu_tbl_base_addr_def]);

e (PAT_ASSUM ``!x:word20. X`` 
         (fn thm =>  ASSUME_TAC (SPEC ``(w2w (0xFFFFCw && c2:word32 >>> 12)):word20`` thm)));
e(` ((w2w ((0xFFFFCw :word32) && c2 ⋙ (12 :num)) :word20) =
         (0xFFFFCw :word20) &&
         (w2w ((0xFFFFCw :word32) && c2 ⋙ (12 :num)) :word20))` by  blastLib.BBLAST_TAC);
e (FULL_SIMP_TAC(srw_ss())[]);
e RES_TAC;
e (FULL_SIMP_TAC(srw_ss())[]);
e(RW_TAC(srw_ss())[]);


e (PAT_ASSUM ``!x:word32 y:word2. X`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC (SPEC ``add':bool[32]`` thm)));
e (PAT_ASSUM ``!y:word2. X`` (fn thm => ASSUME_TAC (SPEC ``0b00w:bool[2]`` thm)));

e (PAT_ASSUM ``!x:word32 y:word2. X`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC (SPEC ``add':bool[32]`` thm)));
e (PAT_ASSUM ``!y:word2. X`` (fn thm => ASSUME_TAC (SPEC ``0b01w:bool[2]`` thm)));

e (PAT_ASSUM ``!x:word32 y:word2. X`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC (SPEC ``add':bool[32]`` thm)));
e (PAT_ASSUM ``!y:word2. X`` (fn thm => ASSUME_TAC (SPEC ``0b10w:bool[2]`` thm)));

e (PAT_ASSUM ``!x:word32 y:word2. X`` (fn thm => ASSUME_TAC (* thm THEN ASSUME_TAC  *)(SPEC ``add':bool[32]`` thm)));
e (PAT_ASSUM ``!y:word2. X`` (fn thm => ASSUME_TAC (SPEC ``0b11w:bool[2]`` thm)));



e (PAT_ASSUM ``!x:word32 y:word20->word2. X`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC(SPEC ``
((((0xFFFFC000w :word32) && (c2 :word32)) ‖ (((add' :word32) ⋙ (20 :num)) ≪ (2 :num))) + (0b00w:bool[32]))`` thm)));
e (PAT_ASSUM ``!x:(word20->word2). X`` (fn thm => ASSUME_TAC (SPEC ``pgtype:word20->word2`` thm)));

e (PAT_ASSUM ``!x:word32 y:word20->word2. X`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC(SPEC ``
((((0xFFFFC000w :word32) && (c2 :word32)) ‖ (((add' :word32) ⋙ (20 :num)) ≪ (2 :num))) + (0b01w:bool[32]))`` thm)));
e (PAT_ASSUM ``!x:(word20->word2). X`` (fn thm => ASSUME_TAC (SPEC ``pgtype:word20->word2`` thm)));

e (PAT_ASSUM ``!x:word32 y:word20->word2. X`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC(SPEC ``
((((0xFFFFC000w :word32) && (c2 :word32)) ‖ (((add' :word32) ⋙ (20 :num)) ≪ (2 :num))) + (0b10w:bool[32]))`` thm)));
e (PAT_ASSUM ``!x:(word20->word2). X`` (fn thm => ASSUME_TAC (SPEC ``pgtype:word20->word2`` thm)));


e (PAT_ASSUM ``!x:word32 y:word20->word2. X`` (fn thm => ASSUME_TAC thm THEN ASSUME_TAC(SPEC ``
((((0xFFFFC000w :word32) && (c2 :word32)) ‖ (((add' :word32) ⋙ (20 :num)) ≪ (2 :num))) + (0b11w:bool[32]))`` thm)));
e (PAT_ASSUM ``!x:(word20->word2). X`` (fn thm => ASSUME_TAC (SPEC ``pgtype:word20->word2`` thm)));

e(` pgtype (w2w (((0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2) + 1w) ⋙ 12)) <> 0w` by FULL_SIMP_TAC(srw_ss()) []);
e(` pgtype (w2w (((0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2) + 2w) ⋙ 12)) <> 0w` by FULL_SIMP_TAC(srw_ss()) []);
e(` pgtype (w2w (((0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2) + 3w) ⋙ 12)) <> 0w` by FULL_SIMP_TAC(srw_ss()) []);
e(` pgtype (w2w (((0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2) + 0w) ⋙ 12)) <> 0w` by FULL_SIMP_TAC(srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) []);
e RES_TAC;

(* e (UNDISCH_ALL_TAC); *)
(* e (RW_TAC (srw_ss()) []); *)
(* e (PAT_ASSUM ``!x:word20. X`` (fn thm => ASSUME_TAC (SPEC ``(w2w((c2 && 0xFFFFC000w:word32) ⋙ 12)):word20`` thm))); *)
(* e (PAT_ASSUM ``!x:word32 y:word20->word2. X`` (fn thm => ASSUME_TAC (SPEC ``(c2 && 0xFFFFC000w:word32)`` thm))); *)
(* e (PAT_ASSUM ``!x:word32. X`` (fn thm => ASSUME_TAC thm THEN ASSUME_TAC (SPEC ``(c2 && 0xFFFFC000w:word32)`` thm))); *)
(* e (PAT_ASSUM ``!x:word32. X`` (fn thm => ASSUME_TAC (SPEC ``(c2 && 0xFFFFC000w:word32)`` thm))); *)

e (PAT_ASSUM ``!x:word32. X==>Y`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC (SPEC ``
((((0xFFFFC000w :word32) && (c2 :word32)) ‖ (((add' :word32) ⋙ (20 :num)) ≪ (2 :num))) + 0b00w:bool[32])
`` thm)));

e (PAT_ASSUM ``!x:word32. X==>Y`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC (SPEC ``
((((0xFFFFC000w :word32) && (c2 :word32)) ‖ (((add' :word32) ⋙ (20 :num)) ≪ (2 :num))) + 0b01w:bool[32])
`` thm)));

e (PAT_ASSUM ``!x:word32. X==>Y`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC (SPEC ``
((((0xFFFFC000w :word32) && (c2 :word32)) ‖ (((add' :word32) ⋙ (20 :num)) ≪ (2 :num))) + 0b10w:bool[32])
`` thm)));

e (PAT_ASSUM ``!x:word32. X==>Y`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC(SPEC ``
((((0xFFFFC000w :word32) && (c2 :word32)) ‖ (((add' :word32) ⋙ (20 :num)) ≪ (2 :num))) + 0b11w:bool[32])
`` thm)));


e RES_TAC;
(* e (RW_TAC (srw_ss()) []); *)
(* e (FULL_SIMP_TAC (srw_ss()) []); *)
(* e (UNDISCH_ALL_TAC); *)
(* e (RW_TAC (srw_ss()) []); *)

(* /////// *)

e(Cases_on`
 mem (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2) ≠
       mem' (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2)
`);
e (RES_TAC);
e(Cases_on `
(mmu_phi_byte (rec'sctlrT 1w) c2 c3 mem F
            (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2))
`);
e(Cases_on `r`);
e(Cases_on `r'`);
e (FULL_SIMP_TAC (srw_ss()) []);


e(Cases_on`
mem ((0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2) + 1w) ≠
       mem' ((0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2) + 1w)
`);
e (RES_TAC);
e(Cases_on `
(mmu_phi_byte (rec'sctlrT 1w) c2 c3 mem F
            ((0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2) + 1w))
`);
e(Cases_on `r`);
e(Cases_on `r'`);
e (FULL_SIMP_TAC (srw_ss()) []);

e(Cases_on`
mem ((0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2) + 2w) ≠
       mem' ((0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2) + 2w)
`);
e (RES_TAC);
e(Cases_on `
(mmu_phi_byte (rec'sctlrT 1w) c2 c3 mem F
            ((0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2) + 2w))
`);
e(Cases_on `r`);
e(Cases_on `r'`);
e (FULL_SIMP_TAC (srw_ss()) []);


e(Cases_on`
mem ((0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2) + 3w) ≠
       mem' ((0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2) + 3w)
`);
e (RES_TAC);
e(Cases_on `
(mmu_phi_byte (rec'sctlrT 1w) c2 c3 mem F
            ((0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2) + 3w))
`);
e(Cases_on `r`);
e(Cases_on `r'`);
e (FULL_SIMP_TAC (srw_ss()) []);

// first part has done to this point

e (FULL_SIMP_TAC (srw_ss()) []);
e(bossLib.UNABBREV_ALL_TAC);


e(`
(read_mem32
        ((0xFFFFC000w :word32) && (c2 :word32) ‖
         (add' :word32) ⋙ (20 :num) ≪ (2 :num),
         (mem :word32 -> word8))) =
(read_mem32
       ((0xFFFFC000w :word32) && c2 ‖ add' ⋙ (20 :num) ≪ (2 :num),
        (mem' :word32 -> word8)))
` by (* (METIS_TAC []) *)( (FULL_SIMP_TAC (srw_ss()) [read_mem_eq])) ) ;
(* e (RW_TAC (srw_ss()) []); *)
e (FULL_SIMP_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_def]);

//start point//
e (UNDISCH_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) []);
e (RW_TAC (srw_ss()) []);
//I am here

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_pt_def,mmu_byte_l2_def]);


(* e (PAT_ASSUM ``!x:word20. X`` (fn thm => ASSUME_TAC  (SPEC `` *)
(* (w2w *)
(*             ((0xFFFFCw :word32) && (c2 :word32) ⋙ (12 :num) ‖ *)
(*              (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙ (12 :num)) : *)
(*             word20) *)
(* `` thm))); *)

(* e (RES_TAC); *)
(* e (UNDISCH_ALL_TAC); *)
(* e (FULL_SIMP_TAC (srw_ss()) []); *)
(* e (RW_TAC (srw_ss()) []); *)

e (IMP_RES_TAC invariant_page_type_l1_def);

e (PAT_ASSUM ``!x:word12. X`` (fn thm => ASSUME_TAC  (SPEC ``
(w2w((add':bool[32]) >>> 20)):bool[12]
`` thm)));

e (FULL_SIMP_TAC (srw_ss()) [LET_DEF(* ,mmu_tbl_base_addr_def *)]);

/////////////

(*Using the following statements I can reach the pt L2*)

(* e(` *)
(* ((w2w *)
(*                 ((0xFFFFCw :word20) && *)
(*                  (w2w *)
(*                     ((0xFFFFCw :word32) && (c2 :word32) ⋙ (12 :num) ‖ *)
(*                      (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙ (12 : *)
(*                      num)) :word20)) :word32) ≪ (12 :num) ‖ *)
(*              (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪ (2 :num)) *)
(* = *)
(* ((0xFFFFC000w :word32) && (c2 :word32) ‖ *)
(*              (add' :word32) ⋙ (20 :num) ≪ (2 :num)) *)
(* ` by (blastLib.BBLAST_TAC)); *)

(* e(` *)
(* (read_mem32 *)
(*             ((w2w *)
(*                 ((0xFFFFCw :word20) && *)
(*                  (w2w *)
(*                     ((0xFFFFCw :word32) && (c2 :word32) ⋙ (12 :num) ‖ *)
(*                      (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙ (12 : *)
(*                      num)) :word20)) :word32) ≪ (12 :num) ‖ *)
(*              (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪ (2 :num), *)
(*              (mem :word32 -> word8))) *)
(* = *)
(* (read_mem32 *)
(*             ((0xFFFFC000w :word32) && (c2 :word32) ‖ *)
(*              (add' :word32) ⋙ (20 :num) ≪ (2 :num), *)
(*              (mem' :word32 -> word8))) *)
(* `  by (FULL_SIMP_TAC (srw_ss()) [])); *)
(* e (FULL_SIMP_TAC (srw_ss()) []); *)

(* ================== *)
e(`
((w2w
                ((0xFFFFCw :word20) &&
                 (w2w ((0xFFFFCw :word32) && (c2 :word32) ⋙ (12 :num)) :
                    word20)) :word32) ≪ (12 :num) ‖
             (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32) ≪
             (2 :num)) =
((0xFFFFC000w :word32) && (c2 :word32) ‖
                (add' :word32) ⋙ (20 :num) ≪ (2 :num))`
by blastLib.BBLAST_TAC);
e(`
read_mem32((w2w
                ((0xFFFFCw :word20) &&
                 (w2w ((0xFFFFCw :word32) && (c2 :word32) ⋙ (12 :num)) :
                    word20)) :word32) ≪ (12 :num) ‖
             (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32) ≪
             (2 :num), mem) =
read_mem32((0xFFFFC000w :word32) && (c2 :word32) ‖
                (add' :word32) ⋙ (20 :num) ≪ (2 :num),mem)`
by FULL_SIMP_TAC(srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) []);
(* ================== *)
e (PAT_ASSUM ``!x:bool[22]. X`` (fn thm =>  ASSUME_TAC(SPEC``
						    ((rec'l1PTT
							  (read_mem32 (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2,mem))).
					             addr):bool[22]`` thm)));
e (PAT_ASSUM ``!x:bool[20]->bool[2]. X``(fn thm =>  ASSUME_TAC(SPEC``pgtype:word20->word2`` thm)));
e (PAT_ASSUM ``(pgtype
          (w2w
             ((rec'l1PTT
                 (read_mem32 (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2,mem))).
              addr ⋙ 2)) =
        2w) ⇒
       ∀va byte_idx.
         pgtype
           (w2w
              ((w2w byte_idx +
                (w2w
                   (rec'l1PTT
                      (read_mem32
                         (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2,mem))).
                   addr ≪ 10 ‖ 1020w && va ⋙ 12 ≪ 2)) ⋙ 12)) =
         2w`` (fn thm => IMP_RES_TAC thm));
e (FULL_SIMP_TAC (srw_ss()) []);
(* ================== *)
e (PAT_ASSUM ``!x:word32 y:word2. X`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC(SPEC ``add':bool[32]`` thm)));
e (PAT_ASSUM ``!y:word2. X`` (fn thm => ASSUME_TAC (SPEC ``0b00w:bool[2]`` thm)));

e (PAT_ASSUM ``!x:word32 y:word2. X`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC(SPEC ``add':bool[32]`` thm)));
e (PAT_ASSUM ``!y:word2. X`` (fn thm => ASSUME_TAC (SPEC ``0b01w:bool[2]`` thm)));

e (PAT_ASSUM ``!x:word32 y:word2. X`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC(SPEC ``add':bool[32]`` thm)));
e (PAT_ASSUM ``!y:word2. X`` (fn thm => ASSUME_TAC (SPEC ``0b10w:bool[2]`` thm)));

e (PAT_ASSUM ``!x:word32 y:word2. X`` (fn thm => ASSUME_TAC  (* thm THEN ASSUME_TAC  *)(SPEC ``add':bool[32]`` thm)));
e (PAT_ASSUM ``!y:word2. X`` (fn thm => ASSUME_TAC (SPEC ``0b11w:bool[2]`` thm)));



e (PAT_ASSUM ``!x:word32 y:word20->word2. X`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC(SPEC ``
(((w2w(rec'l1PTT(read_mem32((0xFFFFC000w :word32) && c2 ‖add' ⋙ (20 :num) ≪ (2 :num),mem'))).addr:word32) ≪ (10 :num) ‖
             (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) + (0b00w:bool[32]))`` thm)));
e (PAT_ASSUM ``!x:(word20->word2). X`` (fn thm => ASSUME_TAC (SPEC ``pgtype:word20->word2`` thm)));

e (PAT_ASSUM ``!x:word32 y:word20->word2. X`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC(SPEC ``
(((w2w(rec'l1PTT(read_mem32((0xFFFFC000w :word32) && c2 ‖add' ⋙ (20 :num) ≪ (2 :num),mem'))).addr:word32) ≪ (10 :num) ‖
             (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) + (0b01w:bool[32]))`` thm)));
e (PAT_ASSUM ``!x:(word20->word2). X`` (fn thm => ASSUME_TAC (SPEC ``pgtype:word20->word2`` thm)));


e (PAT_ASSUM ``!x:word32 y:word20->word2. X`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC(SPEC ``
(((w2w(rec'l1PTT(read_mem32((0xFFFFC000w :word32) && c2 ‖add' ⋙ (20 :num) ≪ (2 :num),mem'))).addr:word32) ≪ (10 :num) ‖
             (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) + (0b10w:bool[32]))`` thm)));
e (PAT_ASSUM ``!x:(word20->word2). X`` (fn thm => ASSUME_TAC (SPEC ``pgtype:word20->word2`` thm)));


e (PAT_ASSUM ``!x:word32 y:word20->word2. X`` (fn thm => ASSUME_TAC  (* thm THEN ASSUME_TAC *)(SPEC ``
(((w2w(rec'l1PTT(read_mem32((0xFFFFC000w :word32) && c2 ‖add' ⋙ (20 :num) ≪ (2 :num),mem'))).addr:word32) ≪ (10 :num) ‖
             (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) + (0b11w:bool[32]))`` thm)));
e (PAT_ASSUM ``!x:(word20->word2). X`` (fn thm => ASSUME_TAC (SPEC ``pgtype:word20->word2`` thm)));


(* ============================= *)
e(`l1_type = 1w` by METIS_TAC[]);
e (FULL_SIMP_TAC (srw_ss()) []);
e(` pgtype
         (w2w
            (((w2w
                 (rec'l1PTT
                    (read_mem32
                       (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2,mem'))).addr ≪
               10 ‖ 1020w && add' ⋙ 12 ≪ 2) + 3w) ⋙ 12)) <> 0w` by FULL_SIMP_TAC(srw_ss())[]);
e(` pgtype
         (w2w
            (((w2w
                 (rec'l1PTT
                    (read_mem32
                       (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2,mem'))).addr ≪
               10 ‖ 1020w && add' ⋙ 12 ≪ 2) + 2w) ⋙ 12)) <> 0w` by FULL_SIMP_TAC(srw_ss())[]);
e(` pgtype
         (w2w
            (((w2w
                 (rec'l1PTT
                    (read_mem32
                       (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2,mem'))).addr ≪
               10 ‖ 1020w && add' ⋙ 12 ≪ 2) + 1w) ⋙ 12)) <> 0w` by METIS_TAC[]);
e(` pgtype
         (w2w
            (((w2w
                 (rec'l1PTT
                    (read_mem32
                       (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2,mem'))).addr ≪
               10 ‖ 1020w && add' ⋙ 12 ≪ 2)) ⋙ 12)) <> 0w` by FULL_SIMP_TAC(srw_ss())[]);


e (FULL_SIMP_TAC (srw_ss()) []);
e RES_TAC;

(* ============================= *)


e (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_base_addr_def]);
(* e (RW_TAC (srw_ss()) []); *)

e (PAT_ASSUM ``!x:word32. X`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC (SPEC ``
(((w2w(rec'l1PTT(read_mem32((0xFFFFC000w :word32) && c2 ‖add' ⋙ (20 :num) ≪ (2 :num),mem'))).addr:word32) ≪ (10 :num) ‖
             (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) + 0b00w:bool[32])
`` thm)));

e (PAT_ASSUM ``!x:word32. X`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC (SPEC ``
(((w2w(rec'l1PTT(read_mem32((0xFFFFC000w :word32) && c2 ‖add' ⋙ (20 :num) ≪ (2 :num),mem'))).addr:word32) ≪ (10 :num) ‖
             (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) + 0b01w:bool[32])
`` thm)));
e (PAT_ASSUM ``!x:word32. X`` (fn thm => ASSUME_TAC  thm THEN ASSUME_TAC (SPEC ``
(((w2w(rec'l1PTT(read_mem32((0xFFFFC000w :word32) && c2 ‖add' ⋙ (20 :num) ≪ (2 :num),mem'))).addr:word32) ≪ (10 :num) ‖
             (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) + 0b10w:bool[32])
`` thm)));
e (PAT_ASSUM ``!x:word32. X`` (fn thm => ASSUME_TAC  (* thm THEN ASSUME_TAC *) (SPEC ``
(((w2w(rec'l1PTT(read_mem32((0xFFFFC000w :word32) && c2 ‖add' ⋙ (20 :num) ≪ (2 :num),mem'))).addr:word32) ≪ (10 :num) ‖
             (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) + 0b11w:bool[32])
`` thm)));


e RES_TAC;
e (FULL_SIMP_TAC (srw_ss()) []);

(* //good relations////////////////////////////////////////////////////////////////////////////////////////////////////////// *)

e(`
((0xFFFFC000w :word32) && (c2 :word32) ‖
                       (add' :word32) ⋙ (20 :num) ≪ (2 :num))
=
((w2w
                       ((0xFFFFCw :word20) &&
                        (w2w
                           ((0xFFFFCw :word32) &&
                            (c2 :word32) ⋙ (12 :num) ‖
                            (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                            (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                    (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                    (2 :num))
` by (blastLib.BBLAST_TAC));


e(`
(read_mem32
                      ((0xFFFFC000w :word32) && (c2 :word32) ‖
                       (add' :word32) ⋙ (20 :num) ≪ (2 :num),
                       (mem' :word32 -> word8)))
=
(read_mem32
                   ((w2w
                       ((0xFFFFCw :word20) &&
                        (w2w
                           ((0xFFFFCw :word32) &&
                            (c2 :word32) ⋙ (12 :num) ‖
                            (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                            (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                    (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                    (2 :num),(mem :word32 -> word8)))
` by (FULL_SIMP_TAC (srw_ss()) []));

e(`
(rec'l1PTT
                   (read_mem32
                      ((0xFFFFC000w :word32) && (c2 :word32) ‖
                       (add' :word32) ⋙ (20 :num) ≪ (2 :num),
                       (mem' :word32 -> word8)))).addr
=
(rec'l1PTT
                (read_mem32
                   ((w2w
                       ((0xFFFFCw :word20) &&
                        (w2w
                           ((0xFFFFCw :word32) &&
                            (c2 :word32) ⋙ (12 :num) ‖
                            (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                            (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                    (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                    (2 :num),(mem :word32 -> word8)))).addr

` by (FULL_SIMP_TAC (srw_ss()) []));
e(FULL_SIMP_TAC (srw_ss()) []);
(* +0////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// *)
e(`
(w2w
            ((w2w
                (rec'l1PTT
                   (read_mem32
                      ((w2w
                          ((0xFFFFCw :word20) &&
                           (w2w
                              ((0xFFFFCw :word32) &&
                               (c2 :word32) ⋙ (12 :num) ‖
                               (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                               (12 :num)) :word20)) :word32) ≪ (12 :
                       num) ‖
                       (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                       (2 :num),(mem :word32 -> word8)))).addr :
                word32) ≪ (10 :num) ⋙ (12 :num)) :word20)
=
(w2w
            ((rec'l1PTT
                (read_mem32
                   ((w2w
                       ((0xFFFFCw :word20) &&
                        (w2w
                           ((0xFFFFCw :word32) &&
                            (c2 :word32) ⋙ (12 :num) ‖
                            (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                            (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                    (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                    (2 :num),(mem :word32 -> word8)))).addr ⋙ (2 :
             num)) :word20)
` by (blastLib.BBLAST_TAC) (* (FULL_SIMP_TAC (srw_ss()) []) *));

e(`
pgtype (w2w
            ((w2w
                (rec'l1PTT
                   (read_mem32
                      ((w2w
                          ((0xFFFFCw :word20) &&
                           (w2w
                              ((0xFFFFCw :word32) &&
                               (c2 :word32) ⋙ (12 :num) ‖
                               (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                               (12 :num)) :word20)) :word32) ≪ (12 :
                       num) ‖
                       (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                       (2 :num),(mem :word32 -> word8)))).addr :
                word32) ≪ (10 :num) ⋙ (12 :num)) :word20)
=
pgtype (w2w
            ((rec'l1PTT
                (read_mem32
                   ((w2w
                       ((0xFFFFCw :word20) &&
                        (w2w
                           ((0xFFFFCw :word32) &&
                            (c2 :word32) ⋙ (12 :num) ‖
                            (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                            (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                    (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                    (2 :num),(mem :word32 -> word8)))).addr ⋙ (2 :
             num)) :word20)
` by  (FULL_SIMP_TAC (srw_ss()) []));
e(`
pgtype (w2w
            ((rec'l1PTT
                (read_mem32
                   ((w2w
                       ((0xFFFFCw :word20) &&
                        (w2w
                           ((0xFFFFCw :word32) &&
                            (c2 :word32) ⋙ (12 :num) ‖
                            (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                            (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                    (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                    (2 :num),(mem :word32 -> word8)))).addr ⋙ (2 :
             num)) :word20) ≠
       0w
` by (FULL_SIMP_TAC (srw_ss()) []));

e (FULL_SIMP_TAC (srw_ss()) []);


e(Cases_on`
(mem :word32 -> word8)
         ((w2w
             (rec'l1PTT
                (read_mem32
                   ((w2w
                       ((0xFFFFCw :word20) &&
                        (w2w
                           ((0xFFFFCw :word32) &&
                            (c2 :word32) ⋙ (12 :num) ‖
                            (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                            (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                    (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                    (2 :num),mem))).addr :word32) ≪ (10 :num) ‖
          (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) ≠
       (mem' :word32 -> word8)
         ((w2w
             (rec'l1PTT
                (read_mem32
                   ((w2w
                       ((0xFFFFCw :word20) &&
                        (w2w
                           ((0xFFFFCw :word32) && c2 ⋙ (12 :num) ‖
                            add' ⋙ (20 :num) ≪ (2 :num) ⋙ (12 :num)) :
                           word20)) :word32) ≪ (12 :num) ‖
                    (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                    (2 :num),mem))).addr :word32) ≪ (10 :num) ‖
          (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num))
`);
e (FULL_SIMP_TAC (srw_ss()) []);
e (RES_TAC);
e(Cases_on `
(mmu_phi_byte (rec'sctlrT (1w :word32)) (c2 :word32)
            (c3 :word32) (mem :word32 -> word8) F
            ((w2w
                (rec'l1PTT
                   (read_mem32
                      ((w2w
                          ((0xFFFFCw :word20) &&
                           (w2w
                              ((0xFFFFCw :word32) && c2 ⋙ (12 :num) ‖
                               (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                               (12 :num)) :word20)) :word32) ≪ (12 :
                       num) ‖
                       (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                       (2 :num),mem))).addr :word32) ≪ (10 :num) ‖
             (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)))
`);
e(Cases_on `r`);
e(Cases_on `r'`);
e (FULL_SIMP_TAC (srw_ss()) []);

(* +1/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// *)
e(`
(w2w
            ((((w2w
                  (rec'l1PTT
                     (read_mem32
                        ((w2w
                            ((0xFFFFCw :word20) &&
                             (w2w
                                ((0xFFFFCw :word32) &&
                                 (c2 :word32) ⋙ (12 :num) ‖
                                 (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                                 (12 :num)) :word20)) :word32) ≪ (12 :
                         num) ‖
                         (w2w (w2w (add' ⋙ (20 :num)) :word12) :
                            word32) ≪ (2 :num),
                         (mem :word32 -> word8)))).addr :word32) ≪ (10 :
               num) ‖ (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) +
              (1w :word32)) ⋙ (12 :num)) :word20)
=
(w2w
            ((rec'l1PTT
                (read_mem32
                   ((w2w
                       ((0xFFFFCw :word20) &&
                        (w2w
                           ((0xFFFFCw :word32) &&
                            (c2 :word32) ⋙ (12 :num) ‖
                            (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                            (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                    (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                    (2 :num),(mem :word32 -> word8)))).addr ⋙ (2 :
             num)) :word20)
` by (blastLib.BBLAST_TAC) (* (FULL_SIMP_TAC (srw_ss()) []) *));

e(`
pgtype (w2w
            ((((w2w
                  (rec'l1PTT
                     (read_mem32
                        ((w2w
                            ((0xFFFFCw :word20) &&
                             (w2w
                                ((0xFFFFCw :word32) &&
                                 (c2 :word32) ⋙ (12 :num) ‖
                                 (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                                 (12 :num)) :word20)) :word32) ≪ (12 :
                         num) ‖
                         (w2w (w2w (add' ⋙ (20 :num)) :word12) :
                            word32) ≪ (2 :num),
                         (mem :word32 -> word8)))).addr :word32) ≪ (10 :
               num) ‖ (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) +
              (1w :word32)) ⋙ (12 :num)) :word20)
=
pgtype (w2w
            ((rec'l1PTT
                (read_mem32
                   ((w2w
                       ((0xFFFFCw :word20) &&
                        (w2w
                           ((0xFFFFCw :word32) &&
                            (c2 :word32) ⋙ (12 :num) ‖
                            (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                            (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                    (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                    (2 :num),(mem :word32 -> word8)))).addr ⋙ (2 :
             num)) :word20)
` by  (FULL_SIMP_TAC (srw_ss()) []) );

e(`
pgtype (w2w
            ((((w2w
                  (rec'l1PTT
                     (read_mem32
                        ((w2w
                            ((0xFFFFCw :word20) &&
                             (w2w
                                ((0xFFFFCw :word32) &&
                                 (c2 :word32) ⋙ (12 :num) ‖
                                 (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                                 (12 :num)) :word20)) :word32) ≪ (12 :
                         num) ‖
                         (w2w (w2w (add' ⋙ (20 :num)) :word12) :
                            word32) ≪ (2 :num),
                         (mem :word32 -> word8)))).addr :word32) ≪ (10 :
               num) ‖ (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) +
              (1w :word32)) ⋙ (12 :num)) :word20) ≠
       0w
` by (FULL_SIMP_TAC (srw_ss()) []));

e (FULL_SIMP_TAC (srw_ss()) []);

e(Cases_on`
(mem :word32 -> word8)
         (((w2w
              (rec'l1PTT
                 (read_mem32
                    ((w2w
                        ((0xFFFFCw :word20) &&
                         (w2w
                            ((0xFFFFCw :word32) &&
                             (c2 :word32) ⋙ (12 :num) ‖
                             (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                             (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                     (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                     (2 :num),mem))).addr :word32) ≪ (10 :num) ‖
           (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) + (1w
            :word32)) ≠
       (mem' :word32 -> word8)
         (((w2w
              (rec'l1PTT
                 (read_mem32
                    ((w2w
                        ((0xFFFFCw :word20) &&
                         (w2w
                            ((0xFFFFCw :word32) && c2 ⋙ (12 :num) ‖
                             add' ⋙ (20 :num) ≪ (2 :num) ⋙ (12 :num)) :
                            word20)) :word32) ≪ (12 :num) ‖
                     (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                     (2 :num),mem))).addr :word32) ≪ (10 :num) ‖
           (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) + (1w
            :word32))
`);

e (FULL_SIMP_TAC (srw_ss()) []);
e (RES_TAC);
e(Cases_on `
(mmu_phi_byte (rec'sctlrT (1w :word32)) c2 (c3 :word32) mem F
            (((w2w
                 (rec'l1PTT
                    (read_mem32
                       ((w2w
                           ((0xFFFFCw :word20) &&
                            (w2w
                               ((0xFFFFCw :word32) && c2 ⋙ (12 :num) ‖
                                add' ⋙ (20 :num) ≪ (2 :num) ⋙ (12 :
                                num)) :word20)) :word32) ≪ (12 :num) ‖
                        (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                        (2 :num),mem))).addr :word32) ≪ (10 :num) ‖
              (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) + (1w
               :word32)))
`);
e(Cases_on `r`);
e(Cases_on `r'`);
e (FULL_SIMP_TAC (srw_ss()) []);

(* +2/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// *)
e(`
(w2w
            ((((w2w
                  (rec'l1PTT
                     (read_mem32
                        ((w2w
                            ((0xFFFFCw :word20) &&
                             (w2w
                                ((0xFFFFCw :word32) &&
                                 (c2 :word32) ⋙ (12 :num) ‖
                                 (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                                 (12 :num)) :word20)) :word32) ≪ (12 :
                         num) ‖
                         (w2w (w2w (add' ⋙ (20 :num)) :word12) :
                            word32) ≪ (2 :num),
                         (mem :word32 -> word8)))).addr :word32) ≪ (10 :
               num) ‖ (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) +
              (2w :word32)) ⋙ (12 :num)) :word20)
=
(w2w
            ((rec'l1PTT
                (read_mem32
                   ((w2w
                       ((0xFFFFCw :word20) &&
                        (w2w
                           ((0xFFFFCw :word32) &&
                            (c2 :word32) ⋙ (12 :num) ‖
                            (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                            (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                    (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                    (2 :num),(mem :word32 -> word8)))).addr ⋙ (2 :
             num)) :word20)
` by (blastLib.BBLAST_TAC) (* (FULL_SIMP_TAC (srw_ss()) []) *));

e(`
pgtype (w2w
            ((((w2w
                  (rec'l1PTT
                     (read_mem32
                        ((w2w
                            ((0xFFFFCw :word20) &&
                             (w2w
                                ((0xFFFFCw :word32) &&
                                 (c2 :word32) ⋙ (12 :num) ‖
                                 (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                                 (12 :num)) :word20)) :word32) ≪ (12 :
                         num) ‖
                         (w2w (w2w (add' ⋙ (20 :num)) :word12) :
                            word32) ≪ (2 :num),
                         (mem :word32 -> word8)))).addr :word32) ≪ (10 :
               num) ‖ (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) +
              (2w :word32)) ⋙ (12 :num)) :word20)
=
pgtype (w2w
            ((rec'l1PTT
                (read_mem32
                   ((w2w
                       ((0xFFFFCw :word20) &&
                        (w2w
                           ((0xFFFFCw :word32) &&
                            (c2 :word32) ⋙ (12 :num) ‖
                            (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                            (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                    (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                    (2 :num),(mem :word32 -> word8)))).addr ⋙ (2 :
             num)) :word20)
` by  (FULL_SIMP_TAC (srw_ss()) []) );

e(`
pgtype (w2w
            ((((w2w
                  (rec'l1PTT
                     (read_mem32
                        ((w2w
                            ((0xFFFFCw :word20) &&
                             (w2w
                                ((0xFFFFCw :word32) &&
                                 (c2 :word32) ⋙ (12 :num) ‖
                                 (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                                 (12 :num)) :word20)) :word32) ≪ (12 :
                         num) ‖
                         (w2w (w2w (add' ⋙ (20 :num)) :word12) :
                            word32) ≪ (2 :num),
                         (mem :word32 -> word8)))).addr :word32) ≪ (10 :
               num) ‖ (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) +
              (2w :word32)) ⋙ (12 :num)) :word20) ≠
       0w
` by (FULL_SIMP_TAC (srw_ss()) []));

e (FULL_SIMP_TAC (srw_ss()) []);
e(Cases_on`
(mem :word32 -> word8)
         (((w2w
              (rec'l1PTT
                 (read_mem32
                    ((w2w
                        ((0xFFFFCw :word20) &&
                         (w2w
                            ((0xFFFFCw :word32) &&
                             (c2 :word32) ⋙ (12 :num) ‖
                             (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                             (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                     (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                     (2 :num),mem))).addr :word32) ≪ (10 :num) ‖
           (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) + (2w
            :word32)) ≠
       (mem' :word32 -> word8)
         (((w2w
              (rec'l1PTT
                 (read_mem32
                    ((w2w
                        ((0xFFFFCw :word20) &&
                         (w2w
                            ((0xFFFFCw :word32) && c2 ⋙ (12 :num) ‖
                             add' ⋙ (20 :num) ≪ (2 :num) ⋙ (12 :num)) :
                            word20)) :word32) ≪ (12 :num) ‖
                     (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                     (2 :num),mem))).addr :word32) ≪ (10 :num) ‖
           (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) + (2w
            :word32))
`);

e (FULL_SIMP_TAC (srw_ss()) []);
e (RES_TAC);
e(Cases_on `
(mmu_phi_byte (rec'sctlrT (1w :word32)) c2 (c3 :word32) mem F
            (((w2w
                 (rec'l1PTT
                    (read_mem32
                       ((w2w
                           ((0xFFFFCw :word20) &&
                            (w2w
                               ((0xFFFFCw :word32) && c2 ⋙ (12 :num) ‖
                                add' ⋙ (20 :num) ≪ (2 :num) ⋙ (12 :
                                num)) :word20)) :word32) ≪ (12 :num) ‖
                        (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                        (2 :num),mem))).addr :word32) ≪ (10 :num) ‖
              (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) + (2w
               :word32)))
`);
e(Cases_on `r`);
e(Cases_on `r'`);
e (FULL_SIMP_TAC (srw_ss()) []);
(* +3/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// *)
e(`
(w2w
            ((((w2w
                  (rec'l1PTT
                     (read_mem32
                        ((w2w
                            ((0xFFFFCw :word20) &&
                             (w2w
                                ((0xFFFFCw :word32) &&
                                 (c2 :word32) ⋙ (12 :num) ‖
                                 (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                                 (12 :num)) :word20)) :word32) ≪ (12 :
                         num) ‖
                         (w2w (w2w (add' ⋙ (20 :num)) :word12) :
                            word32) ≪ (2 :num),
                         (mem :word32 -> word8)))).addr :word32) ≪ (10 :
               num) ‖ (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) +
              (3w :word32)) ⋙ (12 :num)) :word20)
=
(w2w
            ((rec'l1PTT
                (read_mem32
                   ((w2w
                       ((0xFFFFCw :word20) &&
                        (w2w
                           ((0xFFFFCw :word32) &&
                            (c2 :word32) ⋙ (12 :num) ‖
                            (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                            (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                    (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                    (2 :num),(mem :word32 -> word8)))).addr ⋙ (2 :
             num)) :word20)
` by (blastLib.BBLAST_TAC) (* (FULL_SIMP_TAC (srw_ss()) []) *));

e(`
pgtype (w2w
            ((((w2w
                  (rec'l1PTT
                     (read_mem32
                        ((w2w
                            ((0xFFFFCw :word20) &&
                             (w2w
                                ((0xFFFFCw :word32) &&
                                 (c2 :word32) ⋙ (12 :num) ‖
                                 (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                                 (12 :num)) :word20)) :word32) ≪ (12 :
                         num) ‖
                         (w2w (w2w (add' ⋙ (20 :num)) :word12) :
                            word32) ≪ (2 :num),
                         (mem :word32 -> word8)))).addr :word32) ≪ (10 :
               num) ‖ (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) +
              (3w :word32)) ⋙ (12 :num)) :word20)
=
pgtype (w2w
            ((rec'l1PTT
                (read_mem32
                   ((w2w
                       ((0xFFFFCw :word20) &&
                        (w2w
                           ((0xFFFFCw :word32) &&
                            (c2 :word32) ⋙ (12 :num) ‖
                            (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                            (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                    (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                    (2 :num),(mem :word32 -> word8)))).addr ⋙ (2 :
             num)) :word20)
` by  (FULL_SIMP_TAC (srw_ss()) []) );

e(`
pgtype (w2w
            ((((w2w
                  (rec'l1PTT
                     (read_mem32
                        ((w2w
                            ((0xFFFFCw :word20) &&
                             (w2w
                                ((0xFFFFCw :word32) &&
                                 (c2 :word32) ⋙ (12 :num) ‖
                                 (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                                 (12 :num)) :word20)) :word32) ≪ (12 :
                         num) ‖
                         (w2w (w2w (add' ⋙ (20 :num)) :word12) :
                            word32) ≪ (2 :num),
                         (mem :word32 -> word8)))).addr :word32) ≪ (10 :
               num) ‖ (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) +
              (3w :word32)) ⋙ (12 :num)) :word20) ≠
       0w
` by (FULL_SIMP_TAC (srw_ss()) []));

e (FULL_SIMP_TAC (srw_ss()) []);

e(Cases_on`
(mem :word32 -> word8)
         (((w2w
              (rec'l1PTT
                 (read_mem32
                    ((w2w
                        ((0xFFFFCw :word20) &&
                         (w2w
                            ((0xFFFFCw :word32) &&
                             (c2 :word32) ⋙ (12 :num) ‖
                             (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                             (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                     (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                     (2 :num),mem))).addr :word32) ≪ (10 :num) ‖
           (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) + (3w
            :word32)) ≠
       (mem' :word32 -> word8)
         (((w2w
              (rec'l1PTT
                 (read_mem32
                    ((w2w
                        ((0xFFFFCw :word20) &&
                         (w2w
                            ((0xFFFFCw :word32) && c2 ⋙ (12 :num) ‖
                             add' ⋙ (20 :num) ≪ (2 :num) ⋙ (12 :num)) :
                            word20)) :word32) ≪ (12 :num) ‖
                     (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                     (2 :num),mem))).addr :word32) ≪ (10 :num) ‖
           (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) + (3w
            :word32))
`);

e (FULL_SIMP_TAC (srw_ss()) []);
e (RES_TAC);
e(Cases_on `
(mmu_phi_byte (rec'sctlrT (1w :word32)) c2 (c3 :word32) mem F
            (((w2w
                 (rec'l1PTT
                    (read_mem32
                       ((w2w
                           ((0xFFFFCw :word20) &&
                            (w2w
                               ((0xFFFFCw :word32) && c2 ⋙ (12 :num) ‖
                                add' ⋙ (20 :num) ≪ (2 :num) ⋙ (12 :
                                num)) :word20)) :word32) ≪ (12 :num) ‖
                        (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                        (2 :num),mem))).addr :word32) ≪ (10 :num) ‖
              (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)) + (3w
               :word32)))
`);
e(Cases_on `r`);
e(Cases_on `r'`);
e (FULL_SIMP_TAC (srw_ss()) []);


e (FULL_SIMP_TAC (srw_ss()) []);
(* e(bossLib.UNABBREV_ALL_TAC); *)


e(`
(read_mem32
        (
((w2w
             (rec'l1PTT
                (read_mem32
                   ((w2w
                       ((0xFFFFCw :word20) &&
                        (w2w
                           ((0xFFFFCw :word32) &&
                            (c2 :word32) ⋙ (12 :num) ‖
                            (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                            (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                    (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                    (2 :num),mem))).addr :word32) ≪ (10 :num) ‖
          (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)),
         (mem :word32 -> word8))) =
(read_mem32
       (
((w2w
             (rec'l1PTT
                (read_mem32
                   ((w2w
                       ((0xFFFFCw :word20) &&
                        (w2w
                           ((0xFFFFCw :word32) &&
                            (c2 :word32) ⋙ (12 :num) ‖
                            (add' :word32) ⋙ (20 :num) ≪ (2 :num) ⋙
                            (12 :num)) :word20)) :word32) ≪ (12 :num) ‖
                    (w2w (w2w (add' ⋙ (20 :num)) :word12) :word32) ≪
                    (2 :num),mem))).addr :word32) ≪ (10 :num) ‖
          (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)),
        (mem' :word32 -> word8)))
` by (* (METIS_TAC [read_mem_eq]) *) ( (FULL_SIMP_TAC (srw_ss()) [read_mem_eq])) ) ;



e (RW_TAC (srw_ss()) []);

(* next pt should be done in the same way *)

val u1 = top_thm();

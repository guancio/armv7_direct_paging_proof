val UNDISCH_MATCH_TAC = fn MATCH => (PAT_ASSUM MATCH (fn th => (MP_TAC th)));
val UNDISCH_ALL_TAC = (REPEAT (UNDISCH_MATCH_TAC ``X``));

val let_ss = simpLib.mk_simpset [boolSimps.LET_ss] ;


val addr_ineq_1_thm = Q.prove(
 `!a:word20 b:word20 idx:bool[10] idx':bool[10]. (a<>b) ==> 
     (
    (((w2w a):word32 << 12) !! ((w2w idx):word32) << 2) <>
    (((w2w b):word32 << 12) !! ((w2w idx'):word32) << 2)  
     )
 `, blastLib.BBLAST_TAC);

val l1_decode_hlpr_thm = Q.prove( `!a:word32. mmu_l1_decode_type (0xFFFFFFFCw && a) = 0w`,
		(FULL_SIMP_TAC(srw_ss()) [mmu_l1_decode_type_def,thm_mmu_l1_decode_type_values]));

val addr_ineq_2_thm = Q.prove(
		     `!a:word32 b:word20 idx:word12. ((a = a && 0xffffc000w) /\ (((w2w (a >>> 12)):word20) <> b))
						     ==> (a <> (((w2w b):word32 << 12) !! ((w2w idx):word32)))`,
		     (blastLib.BBLAST_TAC));

val write_read_thm = prove(
``!a v m m'. (m' = write_mem32(a,m,v)) ==> 
      (read_mem32(a,m') = v)
``,
  (REPEAT GEN_TAC)
THEN EVAL_TAC
THEN (RW_TAC (srw_ss()) [read_mem32_def])
THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.UPDATE_def])
THEN (RW_TAC (srw_ss()) [])
THENL [
 (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE
  ``~(((a:bool[32]) + 3w) = a)``]),
 (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE
  ``~(((a:bool[32]) + 2w) = a)``]),
 (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE
  ``~(((a:bool[32]) + 1w) = a)``]),
 (fn (asl, w) =>
 let val t = blastLib.BBLAST_PROVE w in
 blastLib.BBLAST_TAC (asl, w)
 end)
]);


val write_read_unch_thm = prove(
``!a a' v m m'. 
  (a'+3w <+ a /\ a'+3w >=+ 3w /\ a+3w >=+ 3w) \/
  (a' >+ a+3w /\ a'+3w >=+ 3w /\ a+3w >=+ 3w) ==>
  (m' = write_mem32(a,m,v)) ==> 
  (read_mem32(a',m') = read_mem32(a',m))
``,
  (REPEAT GEN_TAC)
THEN EVAL_TAC
THEN (RW_TAC (srw_ss()) [read_mem32_def])
THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.UPDATE_def])
THENL [
   (MAP_EVERY (fn (tm1, tm2) =>
   (ASSUME_TAC (UNDISCH (UNDISCH (
   (blastLib.BBLAST_PROVE ``
   (((a':bool[32]) + 3w) <₊ a) ==>
   (((a':bool[32]) + 3w) ≥₊ 3w) ==>
   (((a:bool[32]) + 3w) ≥₊ 3w) ==>
   (^tm1 <> ^tm2)
   ``))))))
   (List.concat (
     List.map (fn tm1 =>
       List.map (fn tm2 =>
        (tm1,tm2)
       )
       [``a':bool[32]``, ``a'+1w:bool[32]``, ``a'+2w:bool[32]``, ``a'+3w:bool[32]``]
     )
     [``a:bool[32]``, ``a+1w:bool[32]``, ``a+2w:bool[32]``, ``a+3w:bool[32]``]
   )))
   THEN (FULL_SIMP_TAC (srw_ss()) [])
,
   (MAP_EVERY (fn (tm1, tm2) =>
   (ASSUME_TAC (UNDISCH (UNDISCH (
   (blastLib.BBLAST_PROVE ``
   (((a':bool[32])) >+ a+3w) ==>
   (((a':bool[32]) + 3w) ≥₊ 3w) ==>
   (((a:bool[32]) + 3w) ≥₊ 3w) ==>
   (^tm1 <> ^tm2)
   ``))))))
   (List.concat (
     List.map (fn tm1 =>
       List.map (fn tm2 =>
        (tm1,tm2)
       )
       [``a':bool[32]``, ``a'+1w:bool[32]``, ``a'+2w:bool[32]``, ``a'+3w:bool[32]``]
     )
     [``a:bool[32]``, ``a+1w:bool[32]``, ``a+2w:bool[32]``, ``a+3w:bool[32]``]
   )))
   THEN (FULL_SIMP_TAC (srw_ss()) [])
]);


val ref_inv_l1_thm =Q.prove(
     `! c1 va:word32. 
       (c1 = rec'sctlrT 1w ) ==>
       ((invariant_page_type c1 c2 c3 mem pgtype pgrefs) ==>
       (invariant_page_type c1 c2 c3 mem pgtype pgrefs'))
       `,
    (FULL_SIMP_TAC (srw_ss()) [LET_DEF,invariant_page_type_l1_def,invariant_page_type_l2_def,invariant_page_type_def]));


val mem_location_thm = Q.prove (
       `! a:word32 b:word32. ((a <> b) /\ (a = a && 0xFFFFFFFCw) /\  (b = b && 0xFFFFFFFCw))
	       ==>  (((a + 3w) <+ b) \/ (a >+ (b + 3w)))`,
  blastLib.BBLAST_TAC);

val helperump_1_thm = Q.prove(
    `!a:word32 va:word32 idx:word2. (a = (a && 0xFFFFC000w)) ==> 
	 ((w2w((a !! ((va >>> 20)<< 2)) >>> 12)):word20
	       = 
 	         (w2w((a !! (((va ) >>> 30)<< 12)) >>> 12)):word20)`,

  (blastLib.BBLAST_TAC));
val thm_tmp1 = blastLib.BBLAST_PROVE (``  ((va:bool[32] ⋙ 30) = 0w) \/
    ((va:bool[32] ⋙ 30) = 1w) \/
    ((va:bool[32] ⋙ 30) = 2w) \/
    ((va:bool[32] ⋙ 30) = 3w)``);

val l2Unmap = ``
  ! c1 va:word32. 
    (c1 = rec'sctlrT 1w ) ==>
    ((invariant_page_type c1 c2 c3 mem pgtype pgrefs) ==>
       (let (c1,c2',c3',mem',pgtype', pgrefs') = 
       (unmap_L2_pageTable_entry (c1, c2, c3, mem, pgtype, pgrefs) (l2_base_add:bool[32]) (l2_index:bool[32])) in
       (invariant_page_type c1 c2' c3' mem' pgtype' pgrefs')))
``;

g `^l2Unmap`;

e (FULL_SIMP_TAC (srw_ss()) [unmap_L2_pageTable_entry_def]);
e (RW_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) []);


e(Cases_on `pgtype (w2w (l2_base_add ⋙ (12 :num)) :word20) ≠ (2w :word2)`);
e (FULL_SIMP_TAC (srw_ss()) []);


e(Cases_on `((l2_type = 2w) ∨ (l2_type = 3w))`);
r 1;
e (FULL_SIMP_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) [COND_EXPAND_IMP]);

e (RW_TAC (srw_ss()) []);


e (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def]);
e (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF,mmu_tbl_base_addr_def]);
 e (RW_TAC (srw_ss()) []);

r 1;



e (PAT_ASSUM ``!x:word20. X`` (fn thm => ASSUME_TAC (SPEC ``(ph_page):word20`` thm)
					 THEN (ASSUME_TAC thm)
  ));
e (PAT_ASSUM ``
  pgtype ph_page ≠ 3w ∧
       ((pgtype ph_page = 2w) ⇒
        invariant_page_type_l2_def (rec'sctlrT 1w) c2 c3 mem pgtype
          pgrefs ph_page) ∧
       ((pgtype ph_page = 1w) ⇒
        (ph_page = 0xFFFFCw && ph_page) ⇒
        (pgtype (ph_page ‖ 1w) = 1w) ∧ (pgtype (ph_page ‖ 2w) = 1w) ∧
        (pgtype (ph_page ‖ 3w) = 1w) ∧
        invariant_page_type_l1_def (rec'sctlrT 1w) c2 c3 mem pgtype
          pgrefs ph_page)`` (fn thm => IMP_RES_TAC thm));


e (PAT_ASSUM ``invariant_page_type_l1_def (rec'sctlrT 1w) c2 c3 mem pgtype
         pgrefs ph_page `` (
   fn thm => ASSUME_TAC (
	     SIMP_RULE (srw_ss()) [invariant_page_type_l1_def] ( thm))
  ));

e( SIMP_TAC (srw_ss()) [invariant_page_type_l1_def] );
e(RW_TAC (srw_ss()) []);

e (PAT_ASSUM ``!x:word12. X`` (fn thm => ASSUME_TAC (SPEC ``pg_idx:word12`` thm)));

e(FULL_SIMP_TAC (srw_ss()) [Once LET_DEF]);
e(FULL_SIMP_TAC (srw_ss()) [Once LET_DEF]);


e(Cases_on `(global_page_add ‖ w2w pg_idx ≪ 2) = entryAddrL2`);

e (ASSUME_TAC ( (
SPECL [``
(entryAddrL2:word32)
``,
``
(page_desc':word32)
``,
``mem:bool[32]->bool[8]``,
``mem':bool[32]->bool[8]``] write_read_thm
)));

e((bossLib.UNABBREV_ALL_TAC));

e(FULL_SIMP_TAC(srw_ss()) [l1_decode_hlpr_thm]);
------------------------------------
e ( Q.UNABBREV_TAC `entryAddrL2`);
e(`
(0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2)+3w >=+ 3w 
` by  (blastLib.BBLAST_TAC)); 
e ( Q.UNABBREV_TAC `global_page_add`);
e(`
((w2w ((0xFFFFCw :word20) && (ph_page :word20)) :word32) ≪ (12 :
        num) ‖ (w2w (pg_idx :word12) :word32) ≪ (2 :num))+ 3w >=+ 3w` by blastLib.BBLAST_TAC);

e(`((w2w ((0xFFFFCw :word20) && (ph_page :word20)) :word32) ≪ (12 :
        num) ‖ (w2w (pg_idx :word12) :word32) ≪ (2 :num)) =
( ((w2w ((0xFFFFCw :word20) && (ph_page :word20)) :word32) ≪ (12 :
        num) ‖ (w2w (pg_idx :word12) :word32) ≪ (2 :num))&& (0xFFFFFFFCw:word32))` 
by blastLib.BBLAST_TAC);

e(`(0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2) =
((0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2) && 0xFFFFFFFCw:word32)`
by blastLib.BBLAST_TAC);


e (Q.ABBREV_TAC `gAdd:bool[32] =  ((w2w ((0xFFFFCw :word20) && (ph_page :word20)) :word32) ≪ (12 :
        num) ‖ (w2w (pg_idx :word12) :word32) ≪ (2 :num))`);
e (Q.ABBREV_TAC `aAdd:bool[32] = (0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2)`);


e(`
 (((gAdd + 3w) <+ aAdd) \/ (gAdd >+ (aAdd + 3w)))`
by (METIS_TAC[mem_location_thm]));

e(`
read_mem32 (gAdd,mem') = read_mem32 (gAdd,mem)
` by METIS_TAC[write_read_unch_thm]);

e(METIS_TAC[]);

e(`
read_mem32 (gAdd,mem') = read_mem32 (gAdd,mem)
` by METIS_TAC[write_read_unch_thm]);
e(METIS_TAC[]);
(* =========================== *)
e (PAT_ASSUM ``!x:word20. X`` (fn thm => ASSUME_TAC (SPEC ``(ph_page):word20`` thm)
					 THEN (ASSUME_TAC thm)
  ));
e (PAT_ASSUM ``
  pgtype ph_page ≠ 3w ∧
       ((pgtype ph_page = 2w) ⇒
        invariant_page_type_l2_def (rec'sctlrT 1w) c2 c3 mem pgtype
          pgrefs ph_page) ∧
       ((pgtype ph_page = 1w) ⇒
        (ph_page = 0xFFFFCw && ph_page) ⇒
        (pgtype (ph_page ‖ 1w) = 1w) ∧ (pgtype (ph_page ‖ 2w) = 1w) ∧
        (pgtype (ph_page ‖ 3w) = 1w) ∧
        invariant_page_type_l1_def (rec'sctlrT 1w) c2 c3 mem pgtype
          pgrefs ph_page)`` (fn thm => IMP_RES_TAC thm));

e (PAT_ASSUM ``invariant_page_type_l2_def (rec'sctlrT 1w) c2 c3 mem pgtype
         pgrefs ph_page `` (
   fn thm => ASSUME_TAC (
	     SIMP_RULE (srw_ss()) [invariant_page_type_l2_def] ( thm))
  ));

e(SIMP_TAC(srw_ss()) [invariant_page_type_l2_def]);

e(RW_TAC(srw_ss())[]);
e (PAT_ASSUM ``!x:word10. X`` (fn thm => ASSUME_TAC (SPEC ``l2_idx:word10`` thm)));

e(Cases_on `((w2w (ph_page :word20) :word32) ≪ (12 :num) ‖
                (w2w (l2_idx :word10) :word32) ≪ (2 :num)) = entryAddrL2`);

e(FULL_SIMP_TAC (srw_ss()) []);




e (ASSUME_TAC ( (
SPECL [``
(entryAddrL2:word32)
``,
``
(page_desc':word32)
``,
``mem:bool[32]->bool[8]``,
``mem':bool[32]->bool[8]``] write_read_thm
)));


e((bossLib.UNABBREV_ALL_TAC));

e(FULL_SIMP_TAC(srw_ss()) [l1_decode_hlpr_thm]);

(* --------------------------------------------------------------- *)
e ( Q.UNABBREV_TAC `entryAddrL2`);
e(`
(0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2)+3w >=+ 3w 
` by  (blastLib.BBLAST_TAC)); 
e(`
((w2w (ph_page :word20) :word32) ≪ (12 :num) ‖
        (w2w (l2_idx :word10) :word32) ≪ (2 :num))+ 3w >=+ 3w` by blastLib.BBLAST_TAC);

e(`((w2w (ph_page :word20) :word32) ≪ (12 :num) ‖
        (w2w (l2_idx :word10) :word32) ≪ (2 :num)) =
( ((w2w (ph_page :word20) :word32) ≪ (12 :num) ‖
        (w2w (l2_idx :word10) :word32) ≪ (2 :num))&& (0xFFFFFFFCw:word32))` 
by blastLib.BBLAST_TAC);

e(`(0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2) =
((0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2) && 0xFFFFFFFCw:word32)`
by blastLib.BBLAST_TAC);


e (Q.ABBREV_TAC `gAdd:bool[32] = ((w2w (ph_page :word20) :word32) ≪ (12 :num) ‖
        (w2w (l2_idx :word10) :word32) ≪ (2 :num)) `);
e (Q.ABBREV_TAC `aAdd:bool[32] = (0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2)`);


e(`
 (((gAdd + 3w) <+ aAdd) \/ (gAdd >+ (aAdd + 3w)))`
by (METIS_TAC[mem_location_thm]));

e(`
read_mem32 (gAdd,mem') = read_mem32 (gAdd,mem)
` by METIS_TAC[write_read_unch_thm]);

e(METIS_TAC[]);

e(`
read_mem32 (gAdd,mem') = read_mem32 (gAdd,mem)
` by METIS_TAC[write_read_unch_thm]);

e(METIS_TAC[]);


(* ================================================================================================================== *)

val l2El_unmap_thm = ``!a:word32 ptb:word20 (l2_base_add:bool[32]) (* (pgtype:word20-> word2) pgtype' *).
           let (c1,c2',c3',mem',pgtype', pgrefs') = 
		(unmap_L2_pageTable_entry (c1, c2, c3, mem, pgtype, pgrefs) (l2_base_add:bool[32]) (l2_index:bool[32])) in
	    let ph_page = (w2w (a >>> 12)):word20 in
		(
		 ((ptb <> (w2w(l2_base_add >>> 12):word20)) /\ (ph_page = ptb) /\ 
		   (a = a && 0xFFFFFFFCw ) /\ (a+3w >=+ 3w)) ==>
                   (read_mem32(a,mem) = read_mem32(a,mem'))
		)
``;	


g`^l2El_unmap_thm`;
e  (FULL_SIMP_TAC (srw_ss()) [unmap_L2_pageTable_entry_def]);
e  (RW_TAC (srw_ss()) []) ;
e  (Cases_on `pgtype (w2w (l2_base_add ⋙ (12 :num)) :word20) ≠ (2w :word2)`);
e  (FULL_SIMP_TAC (srw_ss()) []);
e  (Cases_on `~((l2_type = 2w) ∨ (l2_type = 3w))`);



e  (FULL_SIMP_TAC (srw_ss()) []);
e  (RW_TAC (srw_ss()) []);

e(Cases_on `a = entryAddrL2`);
e(`(w2w(l2_base_add >>> 12):word20) = (w2w((0xFFFFF000w && l2_base_add) >>> 12):word20)` by blastLib.BBLAST_TAC);
e(`(a = entryAddrL2) ==> ((w2w(a >>> 12):word20) = (w2w (entryAddrL2 >>> 12):word20))` by blastLib.BBLAST_TAC);

e(`(w2w ((0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2) >>> 12):word20) = 
   (w2w((0xFFFFF000w && l2_base_add) >>> 12):word20)` by blastLib.BBLAST_TAC);
e(METIS_TAC []);



e ( Q.UNABBREV_TAC `entryAddrL2`);
e(`
(0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2)+3w >=+ 3w 
` by  (blastLib.BBLAST_TAC)); 

e(`(0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2) =
((0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2) && 0xFFFFFFFCw:word32)`
by blastLib.BBLAST_TAC);

e (Q.ABBREV_TAC `aAdd:bool[32] = (0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2)`);
e(`
 (((a + 3w) <+ aAdd) \/ (a >+ (aAdd + 3w)))`
by (FULL_SIMP_TAC (srw_ss())[mem_location_thm]));
e(`
read_mem32 (a,mem') = read_mem32 (a,mem)
` by METIS_TAC[write_read_unch_thm]);
e(METIS_TAC[]);
e(`
read_mem32 (a,mem') = read_mem32 (a,mem)
` by METIS_TAC[write_read_unch_thm]);
e(METIS_TAC[]);
-------------------------------------------------------------------------------------------------------------
(* val rr_thm = ``!ptb:word20  (pgtype:word20-> word2) pgtype'. *)
(*                (pgtype ptb = 0b10w ) /\ *)
(*  	       (!indx:word10.		  *)
(*                 (mem(((w2w ptb):word32 << 12) !! (w2w indx):word32 << 2 ) = mem'(((w2w ptb):word32 << 12) !! (w2w indx):word32 << 2)) /\  *)
(*  	        (pgtype=pgtype')) ==> *)
(* 		(	  *)
(* 			   (count_pages_for_pt mem pgtype  ph_page ptb) = *)
(* 			   (count_pages_for_pt mem' pgtype'  ph_page ptb) *)
(* 		) *)
(* ``; *)
val rr_thm = ``!ptb:word20  (pgtype:word20-> word2) pgtype'.
               (pgtype ptb = 0b10w ) /\
 	       (!indx:word10.		 
                (read_mem32(((w2w ptb):word32 << 12) !! (w2w indx):word32 << 2 , mem) = 
		 read_mem32(((w2w ptb):word32 << 12) !! (w2w indx):word32 << 2 , mem')) /\ 
 	        (pgtype=pgtype')) ==>
		(	 
			   (count_pages_for_pt mem pgtype  ph_page ptb) =
			   (count_pages_for_pt mem' pgtype'  ph_page ptb)
		)
``;
g `^rr_thm`;

e(RW_TAC (srw_ss()) []);

e(FULL_SIMP_TAC(srw_ss()) [count_pages_for_pt_def]);
e(RW_TAC (srw_ss()) []);

e GUANCIO_SUM_1024_FN_EQ;

e (RW_TAC (srw_ss()) []);

e(`desc_add' = desc_add` by METIS_TAC []);
e(FULL_SIMP_TAC(srw_ss()) []);
e (PAT_ASSUM ``!x:word10. X`` (fn thm => ASSUME_TAC (SPEC ``pt_idx:word10`` thm)));

e (METIS_TAC []);
=============================================================================================================================

fun GUANCIO_SUM_4096_FN_EQ (asl, w) =
 let val (a,b) = dest_eq w
     val (_, f1) = dest_comb a
     val (_, f2) = dest_comb b in
     SUBGOAL_THEN ``(∀x. x < 4096 ⇒ (^f1 x = ^f2 x))`` (fn thm =>
       ASSUME_TAC thm
       THEN ASSUME_TAC ( 
         UNDISCH(
	 SPEC ``4096:num``(
	 GEN ``n:num`` (
	 SPECL [f1, f2] sum_numTheory.SUM_FUN_EQUAL)))
       )
       THEN (FULL_SIMP_TAC (arith_ss) [])
     ) (asl, w)
 end;


fun GUANCIO_SUM_1024_FN_EQ (asl, w) =
 let val (a,b) = dest_eq w
     val (_, f1) = dest_comb a
     val (_, f2) = dest_comb b in
     SUBGOAL_THEN ``(∀x. x < 1024 ⇒ (^f1 x = ^f2 x))`` (fn thm =>
       ASSUME_TAC thm
       THEN ASSUME_TAC ( 
         UNDISCH(
	 SPEC ``1024:num``(
	 GEN ``n:num`` (
	 SPECL [f1, f2] sum_numTheory.SUM_FUN_EQUAL)))
       )
       THEN (FULL_SIMP_TAC (arith_ss) [])
     ) (asl, w)
 end;

val ref_cnt_equality_thm =
    ``!(ptb:word20) (mem:word32->word8) mem'.
          (!a:word32.
		  ( (ptb = ((w2w (a >>> 12)):word20)) /\ (mem(a) = mem'(a)) ) 
	  )
	  ==>
	  (
	   (count_pages_for_pt mem pgtype  ph_page ptb) =
	   (count_pages_for_pt mem' pgtype  ph_page ptb)
	  )``;

g`^ref_cnt_equality_thm`;
e(RW_TAC (srw_ss()) []);
e(FULL_SIMP_TAC(srw_ss()) []);

e(FULL_SIMP_TAC(srw_ss()) [count_pages_for_pt_def]);
e(RW_TAC (srw_ss()) []);
e(FULL_SIMP_TAC(srw_ss()) []);
r 1;
e(FULL_SIMP_TAC(srw_ss()) []);
e(RW_TAC (srw_ss()) []);

e GUANCIO_SUM_1024_FN_EQ;

e (RW_TAC (srw_ss()) []);
(* //from === *)

e(FULL_SIMP_TAC(srw_ss()) []);
e(RW_TAC (srw_ss()) []);

e GUANCIO_SUM_4096_FN_EQ;

e (RW_TAC (srw_ss()) []);
(* //from === *)
===============================================

e(`desc_add' = desc_add` by METIS_TAC []);
e(FULL_SIMP_TAC(srw_ss()) []);


e (PAT_ASSUM ``!x:word32. X`` (fn thm => ASSUME_TAC (SPEC ``desc_add:word32`` thm)
					 THEN (ASSUME_TAC thm)));
e (PAT_ASSUM ``!x:word32. X`` (fn thm => ASSUME_TAC (SPEC ``(desc_add:word32) + (1w:word32)`` thm)
					 THEN (ASSUME_TAC thm)));
e (PAT_ASSUM ``!x:word32. X`` (fn thm => ASSUME_TAC (SPEC ``(desc_add:word32) + (2w:word32)`` thm)
					 THEN (ASSUME_TAC thm)));
e (PAT_ASSUM ``!x:word32. X`` (fn thm => ASSUME_TAC (SPEC ``(desc_add:word32) + (3w:word32)`` thm)
					 THEN (ASSUME_TAC thm)));
e (METIS_TAC []);
(* e ( Q.UNABBREV_TAC `desc_add`); *)
(* e ( Q.UNABBREV_TAC `global_page_add`); *)
(* e(`((ptb :word20) = *)
(*         (w2w *)
(*            (((w2w ptb :word32) ≪ (12 :num) ‖ *)
(*              (w2w (pt_idx :word10) :word32) ≪ (2 :num)) ⋙ (12 :num)) : *)
(*            word20))` by blastLib.BBLAST_TAC); *)

(* e(`((ptb :word20) = *)
(*         (w2w *)
(*            ((((w2w ptb :word32) ≪ (12 :num) ‖ *)
(*               (w2w (pt_idx :word10) :word32) ≪ (2 :num)) + (1w *)
(*                :word32)) ⋙ (12 :num)) :word20))` by blastLib.BBLAST_TAC); *)
(* e(`((ptb :word20) = *)
(*         (w2w *)
(*            ((((w2w ptb :word32) ≪ (12 :num) ‖ *)
(*               (w2w (pt_idx :word10) :word32) ≪ (2 :num)) + (2w *)
(*                :word32)) ⋙ (12 :num)) :word20))` by blastLib.BBLAST_TAC); *)
(* e(`((ptb :word20) = *)
(*         (w2w *)
(*            ((((w2w ptb :word32) ≪ (12 :num) ‖ *)
(*               (w2w (pt_idx :word10) :word32) ≪ (2 :num)) + (3w *)
(*                :word32)) ⋙ (12 :num)) :word20))` by blastLib.BBLAST_TAC); *)
(* e RES_TAC; *)


(* e(` *)
(* read_mem32 *)
(*            ((w2w (ptb :word20) :word32) ≪ (12 :num) ‖ *)
(*             (w2w (pt_idx :word10) :word32) ≪ (2 :num), *)
(*             (mem :word32 -> word8)) *)
(* = *)
(*  read_mem32 *)
(*            ((w2w (ptb :word20) :word32) ≪ (12 :num) ‖ *)
(*             (w2w (pt_idx :word10) :word32) ≪ (2 :num), *)
(*             (mem' :word32 -> word8)) *)
(* ` by ( (FULL_SIMP_TAC (srw_ss()) [read_mem_eq]))) ; *)

(* e (FULL_SIMP_TAC (srw_ss()) []); *)

(* e((bossLib.UNABBREV_ALL_TAC)); *)
(* e(FULL_SIMP_TAC(srw_ss()) []); *)
(* e (METIS_TAC[]); *)
========================================================



val UNDISCH_MATCH_TAC = fn MATCH => (PAT_ASSUM MATCH (fn th => (MP_TAC th)));
val UNDISCH_ALL_TAC = (REPEAT (UNDISCH_MATCH_TAC ``X``));
val mmu_phi_byte_def = Define `
  mmu_phi_byte (c1:sctlrT) c2 (c3:bool[32]) mem PL1 ph_add =
   let und = (!add.
       let (u,rd,wt,ex,pa) = mmu_byte(c1, c2, c3, mem, PL1, add) in u
   ) in
   let rd = (?add.
       let (u,rd,wt,ex,pa) = mmu_byte(c1, c2, c3, mem, PL1, add) in (u /\ rd /\ (pa=ph_add))
   ) in
   let wt = (?add.
       let (u,rd,wt,ex,pa) = mmu_byte(c1, c2, c3, mem, PL1, add) in (u /\ wt /\ (pa=ph_add))
   ) in
   let ex = (?add.
       let (u,rd,wt,ex,pa) = mmu_byte(c1, c2, c3, mem, PL1, add) in (u /\ ex /\ (pa=ph_add))
   ) in
   (und, rd, wt, ex)
`;


val goal_guest_write_access = ``
     ! c1 ph_add ph_frame.
       (c1 = rec'sctlrT 1w ) ==>
       (ph_frame = w2w(ph_add >>> 12):bool[20]) ==>
       ((pgtype ph_frame) <> 0b00w) ==>
       (invariant_page_type c1 c2 c3 mem pgtype pgrefs) ==>
        let (u,rd,wt,ex) = mmu_phi_byte c1 c2 c3 mem F ph_add in
	       ~wt
``;

(* val goal_guest_write_access = `` *)
(*      ! c1 ph_add ph_frame.  *)
(*        (c1 = rec'sctlrT 1w ) ==> *)
(*        (ph_frame = w2w(ph_add >>> 12):bool[20]) ==> *)
(*        ((pgtype ph_frame) <> 0b00w) ==>  *)
(*        (invariant_ref_cnt c1 c2 c3 mem pgtype pgrefs) ==> *)
(*         let (u,rd,wt,ex) = mmu_phi_byte c1 c2 c3 mem F ph_add in    *)
(* 	       ~wt *)
(* ``; *)
val let_ss = simpLib.mk_simpset [boolSimps.LET_ss] ;


g `^goal_guest_write_access`;

e (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def,rec'sctlrT_def]);
(* e (FULL_SIMP_TAC (srw_ss()) [invariant_ref_cnt_def,rec'sctlrT_def]); *)
e (RW_TAC (let_ss) []);
e (PAT_ASSUM ``!x:word20. X`` (fn thm => ASSUME_TAC (SPEC ``(w2w((c2 && 0xFFFFC000w:word32) ⋙ 12)):word20`` thm)));

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
((w2w ((0xFFFFCw :word32) && (c2 :word32) ⋙ (12 :num)) :word20) =
        (0xFFFFCw :word20) &&
        (w2w ((0xFFFFCw :word32) && c2 ⋙ (12 :num)) :word20))
``)
]);


e  (FULL_SIMP_TAC (srw_ss()) [rec'sctlrT_def,mmu_tbl_base_addr_def]); 
e (RW_TAC (srw_ss()) []);
e RES_TAC;


e (FULL_SIMP_TAC (srw_ss()) [mmu_phi_byte_def,invariant_page_type_l1_def]);
e (UNDISCH_ALL_TAC);
e (RW_TAC (let_ss) []);
e (RW_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_base_addr_def]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
((w2w ((0xFFFFCw :word32) && (c2 :word32) ⋙ (12 :num)) :word20) =
        (0xFFFFCw :word20) &&
        (w2w ((0xFFFFCw :word32) && c2 ⋙ (12 :num)) :word20))
``)
]);
e (PAT_ASSUM ``!x:word12. X`` (fn thm => ASSUME_TAC (SPEC ``(w2w((add':word32) >>> 20)):bool[12]`` thm)));
e (UNDISCH_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_def]);
e (RW_TAC (let_ss) []);

//6 repeat this
e  (FULL_SIMP_TAC (srw_ss()) [rec'sctlrT_def, mmu_tbl_index_def,mmu_tbl_base_addr_def]); 


e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_section_def,mmu_tbl_index_def]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)] THEN
 FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);

//10 goals rem
e (FULL_SIMP_TAC (srw_ss()) [rec'sctlrT_def, mmu_tbl_index_def,mmu_tbl_base_addr_def]); 

----------------------------------------------------------------------------------------------------------

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (UNDISCH_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_section_def,mmu_check_access_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_section_def,mmu_check_access_def]);
e (PAT_ASSUM ``!dom:word4. p`` (fn thm =>
  ASSUME_TAC (
 
  SPEC ``(rec'l1SecT
            (read_mem32 (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2,mem))).dom`` thm))
);
//4 times
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_section_def,mmu_check_access_def]); 

e (PAT_ASSUM ``!dom:word4. p`` (fn thm =>
  ASSUME_TAC (
 
  SPEC ``(rec'l1SecT
            (read_mem32 (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2,mem))).dom`` thm))
);
//repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_section_def,mmu_check_access_def]);


e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (UNDISCH_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_decode_l1_ap_def]);
e (RW_TAC (srw_ss()) []);


e (PAT_ASSUM ``!x:word32. X`` (fn thm => ASSUME_TAC (SPEC ``add':word32`` thm)));
e (FULL_SIMP_TAC (srw_ss()) []);

e STRIP_TAC;
e(`
(w2w
            ((255w :word32) && (add' :word32) ⋙ (12 :num) ‖
             (w2w
                (rec'l1SecT
                   (read_mem32
                      ((0xFFFFC000w :word32) && (c2 :word32) ‖
                       add' ⋙ (20 :num) ≪ (2 :num),
                       (mem :word32 -> word8)))).addr :word32) ≪ (20 :
             num) ⋙ (12 :num))) :word20
=
(w2w ((
(0xFFFFFw :word32) && (add' :word32) ‖
       (w2w
          (rec'l1SecT
             (read_mem32
                ((0xFFFFC000w :word32) && (c2 :word32) ‖
                 add' ⋙ (20 :num) ≪ (2 :num),(mem :word32 -> word8)))).
          addr :word32) ≪ (20 :num)
) >>> 12)):word20
` by (blastLib.BBLAST_TAC));


e(`
pgtype((w2w
            ((255w :word32) && (add' :word32) ⋙ (12 :num) ‖
             (w2w
                (rec'l1SecT
                   (read_mem32
                      ((0xFFFFC000w :word32) && (c2 :word32) ‖
                       add' ⋙ (20 :num) ≪ (2 :num),
                       (mem :word32 -> word8)))).addr :word32) ≪ (20 :
             num) ⋙ (12 :num))) :word20)
=
pgtype ((w2w ((
(0xFFFFFw :word32) && (add' :word32) ‖
       (w2w
          (rec'l1SecT
             (read_mem32
                ((0xFFFFC000w :word32) && (c2 :word32) ‖
                 add' ⋙ (20 :num) ≪ (2 :num),(mem :word32 -> word8)))).
          addr :word32) ≪ (20 :num)
) >>> 12)):word20)
` by  FULL_SIMP_TAC (srw_ss()) [chert_thm]);



e(`
(w2w ((0xFFFFFw && add' ‖
       w2w
         (rec'l1SecT
            (read_mem32 (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2,mem))).addr ≪
       20) >>> 12)):word20 =
(w2w (ph_add >>> 12)):word20
` by FULL_SIMP_TAC (srw_ss()) [chert_thm]);

e(`
pgtype ((w2w ((0xFFFFFw && add' ‖
       w2w
         (rec'l1SecT
            (read_mem32 (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2,mem))).addr ≪
       20) >>> 12)):word20) =
pgtype((w2w (ph_add >>> 12)):word20)
` by FULL_SIMP_TAC (srw_ss()) [chert_thm]);




e(`
pgtype((w2w
            ((255w :word32) && (add' :word32) ⋙ (12 :num) ‖
             (w2w
                (rec'l1SecT
                   (read_mem32
                      ((0xFFFFC000w :word32) && (c2 :word32) ‖
                       add' ⋙ (20 :num) ≪ (2 :num),
                       (mem :word32 -> word8)))).addr :word32) ≪ (20 :
             num) ⋙ (12 :num))) :word20)
= 
pgtype((w2w (ph_add >>> 12)):word20)
` by FULL_SIMP_TAC (srw_ss()) [chert_thm]);
e (FULL_SIMP_TAC (srw_ss()) []);


g`! a:word32 b:word32. (a = b) ==> ((w2w(a >>> 12)):word20 = (w2w(b >>> 12)):word20 )`;

e (blastLib.BBLAST_TAC);
val chert_thm = top_thm();

/////// next goal started here//////////

e (UNDISCH_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_section_def,mmu_check_access_def]);
e (RW_TAC (let_ss) []);
//repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_section_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);


e (PAT_ASSUM ``!dom:word4. p`` (fn thm =>
  ASSUME_TAC (
 
  SPEC ``(rec'l1SecT
            (read_mem32 (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2,mem))).dom`` thm))
);

//repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_section_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (UNDISCH_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_decode_l1_ap_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_section_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);


e (PAT_ASSUM ``!dom:word4. p`` (fn thm =>
  ASSUME_TAC (
 
  SPEC ``(rec'l1SecT
            (read_mem32 (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2,mem))).dom`` thm))
);
//repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_section_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
// repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_section_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
// section finished here//

//supsection

//repeat


e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)] THEN
 FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_supsection_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);


/// ///////////////////// ///
// page table starts here//
// ///////////////////// ///

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_pt_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)] THEN
 FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);



//next pt starts here

e (PAT_ASSUM ``!x:word8. X`` (fn thm => ASSUME_TAC (SPEC `` (w2w((add':bool[32] && 0x000FF000w:bool[32]) >> 12)):word8`` thm)));

e (UNDISCH_ALL_TAC);
//generates two goals
e (RW_TAC (let_ss) []);

e (PAT_ASSUM ``!dom:word4. p`` (fn thm =>
  ASSUME_TAC (
 
  SPEC ``(rec'l1PTT(read_mem32 (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2,mem))).dom`` thm))
);
//generates 8 sub goals
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_pt_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);



//repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
/repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);


e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (UNDISCH_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_decode_l1_ap_def]);
e (RW_TAC (let_ss) [mmu_l1_decode_type_def]);
//repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
/repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);

//repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);

//repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]); 


e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);


//repeat

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]); 


e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (UNDISCH_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_decode_l1_ap_def]);
e (RW_TAC (let_ss) [mmu_l1_decode_type_def]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]); 


e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
//repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]); 

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (UNDISCH_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_decode_l1_ap_def]);
e (RW_TAC (let_ss) [mmu_l1_decode_type_def]);
//repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);
//repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (RW_TAC (let_ss) []);
//repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);


e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (RW_TAC (let_ss) []);
//repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);


e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);


e (UNDISCH_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_decode_l1_ap_def]);
e (RW_TAC (let_ss) [mmu_l1_decode_type_def]);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);


// the last part of the goal starts here
e (PAT_ASSUM ``!dom:word4. p`` (fn thm =>
  ASSUME_TAC (
 
  SPEC ``(rec'l1PTT(read_mem32 (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2,mem))).dom`` thm))
);
//generates 8 sub goals
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_pt_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
//***//
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
//***//
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
//***//
e (UNDISCH_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_decode_l1_ap_def]);
e (RW_TAC (let_ss) [mmu_l1_decode_type_def]);
//repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);
//repeat
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
//***//
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);


e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

//***//
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);


e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (UNDISCH_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_decode_l1_ap_def]);
e (RW_TAC (let_ss) [mmu_l1_decode_type_def]);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

//***// :)
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);


e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);


//***//
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);


e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (UNDISCH_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_decode_l1_ap_def]);
e (RW_TAC (let_ss) [mmu_l1_decode_type_def]);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

//***// :)
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);


e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);


//***//
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);


e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_l2_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (RW_TAC (let_ss) []);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

e (UNDISCH_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_decode_l1_ap_def]);
e (RW_TAC (let_ss) [mmu_l1_decode_type_def]);

e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
w2w (0xFFFFCw:bool[20] && w2w (0xFFFFCw:bool[32] && c2 ⋙ 12)) ≪ 12 =
0xFFFFC000w && (c2:bool[32])
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (w2w ((add' :word32) ⋙ (20 :num)) :word12) :word32)= add' ⋙ 20
``)]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
(w2w(w2w (((0xFF000w :word32) && (add' :word32)) ≫ (12 :num)) :word8) :word32) ≪ (2 :num) =
(1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num)
``)]);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_small_page_def,mmu_check_access_def,mmu_tbl_base_addr_def,mmu_tbl_index_def]);

// I am here


val hlp = top_thm();














e(`
(w2w (((4095w :word32) && (add' :word32) ‖
 (w2w
    (rec'l2SmallT
       (read_mem32
          ((w2w
              (rec'l1PTT
                 (read_mem32
                    ((0xFFFFC000w :word32) && (c2 :word32) ‖
                     add' ⋙ (20 :num) ≪ (2 :num),
                     (mem :word32 -> word8)))).addr :word32) ≪ (10 :
           num) ‖ (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num),mem))).
    addr :word32) ≪ (12 :num)) >>> 12)):word20
=
(rec'l2SmallT
            (read_mem32
               ((w2w
                   (rec'l1PTT
                      (read_mem32
                         ((0xFFFFC000w :word32) && (c2 :word32) ‖
                          (add' :word32) ⋙ (20 :num) ≪ (2 :num),
                          (mem :word32 -> word8)))).addr :word32) ≪
                (10 :num) ‖
                (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num),mem))).
         addr
 `by (blastLib.BBLAST_TAC));



e(`
pgtype ((w2w (((4095w :word32) && (add' :word32) ‖
 (w2w
    (rec'l2SmallT
       (read_mem32
          ((w2w
              (rec'l1PTT
                 (read_mem32
                    ((0xFFFFC000w :word32) && (c2 :word32) ‖
                     add' ⋙ (20 :num) ≪ (2 :num),
                     (mem :word32 -> word8)))).addr :word32) ≪ (10 :
           num) ‖ (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num),mem))).
    addr :word32) ≪ (12 :num)) >>> 12)):word20)
=
pgtype ((rec'l2SmallT
            (read_mem32
               ((w2w
                   (rec'l1PTT
                      (read_mem32
                         ((0xFFFFC000w :word32) && (c2 :word32) ‖
                          (add' :word32) ⋙ (20 :num) ≪ (2 :num),
                          (mem :word32 -> word8)))).addr :word32) ≪
                (10 :num) ‖
                (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num),mem))).
         addr)
 `by FULL_SIMP_TAC(srw_ss()) []);

e STRIP_TAC;



e(`
(w2w ((4095w && add' ‖
       w2w
         (rec'l2SmallT
            (read_mem32
               (w2w
                  (rec'l1PTT
                     (read_mem32
                        (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2,mem))).addr ≪
                10 ‖ 1020w && add' ⋙ 12 ≪ 2,mem))).addr ≪ 12) >>> 12)):word20 =
(w2w (ph_add >>> 12)):word20
 `by FULL_SIMP_TAC(srw_ss()) [chert_thm]);

e(`
pgtype ((w2w ((4095w && add' ‖
       w2w
         (rec'l2SmallT
            (read_mem32
               (w2w
                  (rec'l1PTT
                     (read_mem32
                        (0xFFFFC000w && c2 ‖ add' ⋙ 20 ≪ 2,mem))).addr ≪
                10 ‖ 1020w && add' ⋙ 12 ≪ 2,mem))).addr ≪ 12) >>> 12)):word20) =
pgtype ((w2w (ph_add >>> 12)):word20)
 `by FULL_SIMP_TAC(srw_ss()) [chert_thm]);




e(`
pgtype ((rec'l2SmallT
            (read_mem32
               ((w2w
                   (rec'l1PTT
                      (read_mem32
                         ((0xFFFFC000w :word32) && (c2 :word32) ‖
                          (add' :word32) ⋙ (20 :num) ≪ (2 :num),
                          (mem :word32 -> word8)))).addr :word32) ≪
                (10 :num) ‖
                (1020w :word32) && add' ⋙ (12 :num) ≪ (2 :num),mem))).
         addr)
=
pgtype ((w2w (ph_add >>> 12)):word20)
 `by FULL_SIMP_TAC(srw_ss()) [chert_thm]);
e (UNDISCH_ALL_TAC);
e (RW_TAC (srw_ss()) []);

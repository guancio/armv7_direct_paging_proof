val UNDISCH_MATCH_TAC = fn MATCH => (PAT_ASSUM MATCH (fn th => (MP_TAC th)));
val UNDISCH_ALL_TAC = (REPEAT (UNDISCH_MATCH_TAC ``X``));
val let_ss = simpLib.mk_simpset [boolSimps.LET_ss] ;

val mmu_phi_byte_def = Define `
  mmu_phi_byte (c1:sctlrT) c2 (c3:bool[32]) mem PL1 ph_add =
   let und = (!add.
       let (u,rd,wt,ex,pa) = mmu_byte(c1, c2, c3, mem, PL1, add) in u
   ) in
   let rd = (?add.
       let (u,rd,wt,ex,pa) = mmu_byte(c1, c2, c3, mem, PL1, add) in (rd /\ (pa=ph_add))
   ) in
   let wt = (?add.
       let (u,rd,wt,ex,pa) = mmu_byte(c1, c2, c3, mem, PL1, add) in (wt /\ (pa=ph_add))
   ) in
   let ex = (?add.
       let (u,rd,wt,ex,pa) = mmu_byte(c1, c2, c3, mem, PL1, add) in ( ex /\ (pa=ph_add))
   ) in
   (und, rd, wt, ex)
`;


val goal_guest_write_access = ``
     ! c1 ph_add ph_frame.
       (c1 = rec'sctlrT 1w ) ==>
       (ph_frame = w2w(ph_add >>> 12):bool[20]) ==>
       ((pgtype ph_frame) = 0b10w) ==>
       (invariant_page_type c1 c2 c3 mem pgtype pgrefs) ==>
        let (u,rd,wt,ex) = mmu_phi_byte c1 c2 c3 mem F ph_add in
	       ~wt
``;


(* val  t2 = ``let l1_desc = read_mem32(((0xFFFFC000w && c2:bool[32]) !! (( ((add':bool[32]) >>> 20)) << 2) ), mem) in  *)
(* 		  let l2_base = (w2w ((rec'l1PTT l1_desc).addr >>> 2)):bool[20] in *)
(* 		  if((mmu_l1_decode_type(l1_desc) = 0b01w) /\  *)
(*                       (* ((rec'l1PTT l1_desc).addr = (0xFFFFF0w:bool[22] && (rec'l1PTT l1_desc).addr)) /\ *) *)
(* 			(((pgtype:bool[20]->bool[2]) l2_base) = 0b10w)  *)
(* 		    ) *)
(* 		then *)
(* 		    l2_base *)
(* 		else 0w *)
(* ``; *)

(* val  t2 = ``let l1_desc = read_mem32(((0xFFFFC000w && c2:bool[32]) !! (( ((add':bool[32]) >>> 20)) << 2) ), mem) in  *)
(* 		  let l2_base = (w2w ((rec'l1PTT l1_desc).addr >>> 2)):bool[20] in *)
(* 		    l2_base *)
(* ``; *)


g `^goal_guest_write_access`;


e (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def,rec'sctlrT_def]);
(* e (FULL_SIMP_TAC (srw_ss()) [invariant_ref_cnt_def,rec'sctlrT_def]); *)
e (RW_TAC (let_ss) []);
e (PAT_ASSUM ``!x:word20. X`` (fn thm => ASSUME_TAC (SPEC ``(* (w2w((c2 && 0xFFFFC000w:word32) ⋙ 12)):word20 *)^t2`` thm)));
e (PAT_ASSUM ``!x:word20. X`` (fn thm => ASSUME_TAC (SPEC ``(w2w (ph_page:word32 >>> 12)):word20`` thm)));




e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
((w2w ((0xFFFFCw :word32) && (c2 :word32) ⋙ (12 :num)) :word20) =
        (0xFFFFCw :word20) &&
        (w2w ((0xFFFFCw :word32) && c2 ⋙ (12 :num)) :word20))
``)
]);


e  (FULL_SIMP_TAC (srw_ss()) [rec'sctlrT_def,mmu_tbl_base_addr_def]); 
e (RW_TAC (srw_ss()) []);
e RES_TAC;

e (UNDISCH_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_def]);
e (RW_TAC (let_ss) []);

e (FULL_SIMP_TAC (srw_ss()) [mmu_phi_byte_def(* invariant_page_type_l1_def *),invariant_page_type_l2_def]);
e (UNDISCH_ALL_TAC);
e (RW_TAC (let_ss) []);
e (RW_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_base_addr_def]);
e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
((w2w ((0xFFFFCw :word32) && (c2 :word32) ⋙ (12 :num)) :word20) =
        (0xFFFFCw :word20) &&
        (w2w ((0xFFFFCw :word32) && c2 ⋙ (12 :num)) :word20))
``)
]);
e (PAT_ASSUM ``!x:word12. X`` (fn thm => ASSUME_TAC (SPEC ``(w2w((add':word32) >>> 20)):bool[12]`` thm)));
e (UNDISCH_ALL_TAC);
e (FULL_SIMP_TAC (srw_ss()) [mmu_byte_def]);
e (RW_TAC (let_ss) []);

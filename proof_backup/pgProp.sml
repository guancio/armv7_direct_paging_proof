val UNDISCH_MATCH_TAC = fn MATCH => (PAT_ASSUM MATCH (fn th => (MP_TAC th)));
val UNDISCH_ALL_TAC = (REPEAT (UNDISCH_MATCH_TAC ``X``));
val helper2_thm = Q.prove(
    `!a:word32 va:word32 idx:word2. (a = (a && 0xFFFFC000w)) ==> 
	 ((w2w((((w2w idx):word32) + (a !! ((va >>> 20)<< 2))) >>> 12)):word20
	       = 
 	         (w2w((a !! (((va ) >>> 30)<< 12)) >>> 12)):word20)`,

  (blastLib.BBLAST_TAC));


val thm_tmp1 = blastLib.BBLAST_PROVE (``  ((va:bool[32] ⋙ 30) = 0w) \/
    ((va:bool[32] ⋙ 30) = 1w) \/
    ((va:bool[32] ⋙ 30) = 2w) \/
    ((va:bool[32] ⋙ 30) = 3w)``);



val pgc = ``
! c1 c2:word32.
       (c1 = rec'sctlrT 1w ) ==>
  let tbl_base = mmu_tbl_base_addr c2 in
   ((pgtype ((w2w ((tbl_base ) >>> 12)):bool[20]) = 0b01w:word2) /\
    (tbl_base = tbl_base && 0xFFFFC000w) /\
    ((tbl_base = tbl_base && 0xFFFFC000w) ==>
     (
      (pgtype (((w2w ((tbl_base ) >>> 12)):bool[20]) !! (0b01w:bool[20])) = 0b01w) /\
      (pgtype (((w2w ((tbl_base ) >>> 12)):bool[20]) !! (0b10w:bool[20])) = 0b01w) /\
      (pgtype (((w2w ((tbl_base ) >>> 12)):bool[20]) !! (0b11w:bool[20])) = 0b01w)
     ))) ==>
     (!va:bool[32].
      (!byte_idx:bool[2].
        ((pgtype ((w2w ((tbl_base ) >>> 12)):bool[20]) = 0b01w)) ==>
     	  ((pgtype ((w2w(((tbl_base !! ((va >>> 20) << 2)) +  (w2w byte_idx):word32) >>> 12)):bool[20])) = 0b01w)
      )
     )
``;
g `^pgc`;

e (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_base_addr_def]);
e (RW_TAC (srw_ss()) []);

e RES_TAC;
e (FULL_SIMP_TAC (srw_ss()) []);


e(`
(w2w
     (((w2w (byte_idx :word2) :word32) +
       ((tbl_base :word32) ‖ (va :word32) ⋙ (20 :num) ≪ (2 :num))) ⋙
      (12 :num)) :word20)
=

((w2w ((tbl_base !! (((va) >>> 30) << 12)) >>> 12)):word20)
`
by (FULL_SIMP_TAC (srw_ss()) [helper2_thm]));

e(`
pgtype(w2w
     (((w2w (byte_idx :word2) :word32) +
       ((tbl_base :word32) ‖ (va :word32) ⋙ (20 :num) ≪ (2 :num))) ⋙
      (12 :num)) :word20)
=

pgtype((w2w ((tbl_base !! (((va) >>> 30) << 12)) >>> 12)):word20)
`
by (FULL_SIMP_TAC (srw_ss()) [helper2_thm]));


e (FULL_SIMP_TAC (srw_ss()) []);



e(`
((va :word32) ⋙ (30 :num) ≪ (12 :num)) ⋙ (12 :num)
=
(va :word32) ⋙ (30 :num)
` by (blastLib.BBLAST_TAC));

e (FULL_SIMP_TAC (srw_ss()) []);

e (UNDISCH_ALL_TAC);
e (RW_TAC (srw_ss()) []);

e(`
(w2w ((tbl_base :word32) ⋙ (12 :num) ‖ (va :word32) ⋙ (30 :num)) :
     word20)
=
((w2w ((tbl_base :word32) ⋙ (12 :num))):word20) ‖ ((w2w ((va :word32) ⋙ (30 :num))):word20)
` by (blastLib.BBLAST_TAC));
e (FULL_SIMP_TAC (srw_ss()) []);


e (ASSUME_TAC thm_tmp1);

e (FULL_SIMP_TAC (srw_ss()) []);

val pg_l1 = top_thm();

=============================================================================================================
val pgc_l2 = ``
! c1 c2:word32.
       (c1 = rec'sctlrT 1w ) ==>
   (! tbl_base:bool[22] va:bool[32] byte_idx:bool[2].
   (pgtype ((w2w ((tbl_base ) >>> 2)):bool[20]) = (0b10w:word2)) ==>
         ((pgtype ((w2w(((((w2w tbl_base):bool[32] << 10) !!
	 		(((va && (0x000ff000w:bool[32])) >>> 12) << 2)) +
	 		 ((w2w byte_idx):word32)) >>> 12)):bool[20])) = 0b10w))
``;
g `^pgc_l2`;

e (RW_TAC (srw_ss()) []);

e(`
(w2w
     (((w2w (byte_idx :word2) :word32) +
       ((w2w (tbl_base :22 word) :word32) ≪ (10 :num) ‖
        (1020w :word32) && (va :word32) ⋙ (12 :num) ≪ (2 :num))) ⋙ (12 :
      num)) :word20)
=
(w2w ((tbl_base :22 word) ⋙ (2 :num)) :word20)
`by (blastLib.BBLAST_TAC));
e (FULL_SIMP_TAC (srw_ss()) []);

val pg_l2 = top_thm();

=============================================================================================================


val pgc_l2 = ``
! c1 c2:word32.
       (c1 = rec'sctlrT 1w ) ==>
   (!tbl_base:bool[22] va:bool[32] byte_idx:bool[2].
    let pg_base = ((w2w ((tbl_base ) >>> 2)):bool[20]) in
    let pg_idx =  (w2w (tbl_base && 0x000003w:bool[22]):word2) in
   (pgtype pg_base = (0b10w:word2)) ==>
         ((pgtype ((w2w((( ((w2w pg_base):word32 << 12) !!  ( (w2w pg_idx):word32 << 10) !!
	 		(((va && (0x000ff000w:bool[32])) >>> 12) << 2)) +
	 		 ((w2w byte_idx):word32)) >>> 12)):bool[20])) = 0b10w))
``;
g `^pgc_l2`;

e (RW_TAC (srw_ss()) []);
e(bossLib.UNABBREV_ALL_TAC);

e(`
(w2w
     (((w2w (byte_idx :word2) :word32) +
       ((w2w (w2w ((3w :22 word) && (tbl_base :22 word)) :word2) :
           word32) ≪ (10 :num) ‖
        (w2w (w2w (tbl_base ⋙ (2 :num)) :word20) :word32) ≪ (12 :num) ‖
        (1020w :word32) && (va :word32) ⋙ (12 :num) ≪ (2 :num))) ⋙ (12 :
      num)) :word20)
=
(w2w ((tbl_base :22 word) ⋙ (2 :num)) :word20)
`by (blastLib.BBLAST_TAC));
e (FULL_SIMP_TAC (srw_ss()) []);
val pg_l22 = top_thm();








































val entryTyp_thm = Q.prove (
		   `!page_add:bool[32] va:bool[10] byte_idx:bool[2] value:bool[2].
	           (pgtype ((w2w (page_add >>> 12)):bool[20]) = value) ==> 
		   let add = (w2w (((page_add :word32) !! ((w2w va):word32 << 2) + (w2w byte_idx):word32) >>> 12)):word20 in
	           (pgtype add) =  value`,
 		   (RW_TAC (srw_ss()) []) THEN
		   (`(w2w (page_add ⋙ 12 ‖ (w2w byte_idx + w2w va ≪ 2) ⋙ 12)):word20
		       =
		         (w2w (page_add ⋙ 12)):word20
		    ` by (blastLib.BBLAST_TAC)) THEN
		   (FULL_SIMP_TAC (srw_ss()) []) );

======================================================================================================
g`!page_add:bool[32] va:bool[32] byte_idx:bool[2] value:bool[2].
	           (pgtype ((w2w (page_add >>> 12)):bool[20]) = value) ==> 
		   let add = (w2w (((page_add :word32) !! 
		       ((w2w ((w2w ((w2w (va >>> 20)):bool[12] && 0x3ffw)):bool[10]) ):word32 << 2) + (w2w byte_idx):word32) >>> 12)):word20 in
	           (pgtype add) =  value`;
e(FULL_SIMP_TAC (srw_ss()) []);
e (RW_TAC (srw_ss()) []);
e(bossLib.UNABBREV_ALL_TAC);

e(`
  (w2w
     ((page_add :word32) ⋙ (12 :num) ‖
      ((w2w (byte_idx :word2) :word32) +
       (w2w
          (w2w
             ((1023w :word12) &&
              (w2w ((va :word32) ⋙ (20 :num)) :word12)) :word10) :
          word32) ≪ (2 :num)) ⋙ (12 :num)) :word20) =
(w2w (page_add ⋙ (12 :num)) :word20)`
by (blastLib.BBLAST_TAC));

e(FULL_SIMP_TAC (srw_ss()) []);

val uu = top_thm();
===================================================================================================

g`!page_add:bool[32] sel:bool[2] va:bool[32] byte_idx:bool[2] value:bool[2].
                   let sel = (w2w ((va && (0xC0000000w:bool[32])) >>> 30)):bool[2] in
	           (pgtype ((w2w ((page_add !! ((w2w sel):word32 << 12)) >>> 12)):bool[20]) = value) ==> 
		   let add = (w2w (( (page_add !! ((w2w sel):word32 << 12))!! 
		       ((w2w ((w2w ((w2w (va >>> 20)):bool[12] && 0x3ffw)):bool[10]) ):word32 << 2) + (w2w byte_idx):word32) >>> 12)):word20 in
	           (pgtype add) =  value`;

e(FULL_SIMP_TAC (srw_ss()) []);
e (RW_TAC (srw_ss()) []);
e(bossLib.UNABBREV_ALL_TAC);

e(`
  (w2w
     ((page_add :word32) ⋙ (12 :num) ‖
      ((w2w (byte_idx :word2) :word32) +
       (w2w
          (w2w
             ((1023w :word12) &&
              (w2w ((va :word32) ⋙ (20 :num)) :word12)) :word10) :
          word32) ≪ (2 :num)) ⋙ (12 :num) ‖
      (w2w (w2w ((3w :word32) && va ⋙ (30 :num)) :word2) :word32) ≪
      (12 :num) ⋙ (12 :num)) :word20) =

  (w2w
     (page_add ⋙ (12 :num) ‖
      (w2w (w2w ((3w :word32) && va ⋙ (30 :num)) :word2) :word32) ≪
      (12 :num) ⋙ (12 :num)) :word20)`
by  (blastLib.BBLAST_TAC));
e(FULL_SIMP_TAC (srw_ss()) []);
val uu1 = top_thm();


=============================================================================================


val hlper_thm = Q.prove(
     `!a:word32 f:(bool[20]->bool[2]) (idx:word2). 
       (
	 (* (a = (a && 0xFFFFC000w)) /\  *)(idx >= 0w) /\
	    ((f ((w2w (a >>> 12)):word20 !! (w2w (0b00w:word2)):word20)) = 0b01w) /\
	    ((f ((w2w (a >>> 12)):word20 !! (w2w (0b01w:word2)):word20)) = 0b01w) /\
	    ((f ((w2w (a >>> 12)):word20 !! (w2w (0b10w:word2)):word20)) = 0b01w) /\
	    ((f ((w2w (a >>> 12)):word20 !! (w2w (0b11w:word2)):word20)) = 0b01w)
       ) 
       ==> 
	    ((f ((w2w (a >>> 12)):word20 !! (w2w (1w:word2)):word20)) = 0b01w)
     `,
   (blastLib.BBLAST_TAC)
);

e(`
(w2w
     (((w2w (byte_idx :word2) :word32) +
       ((tbl_base :word32) ‖ (va :word32) ⋙ (20 :num) ≪ (2 :num))) ⋙
      (12 :num)) :word20)
=

((w2w ((tbl_base !! (((va && 0xC0000000w) >>> 30) << 12)) >>> 12)):word20)
`
by (FULL_SIMP_TAC (srw_ss()) [helper2_thm]));

e(`
pgtype(w2w
     (((w2w (byte_idx :word2) :word32) +
       ((tbl_base :word32) ‖ (va :word32) ⋙ (20 :num) ≪ (2 :num))) ⋙
      (12 :num)) :word20)
=

pgtype((w2w ((tbl_base !! (((va && 0xC0000000w) >>> 30) << 12)) >>> 12)):word20)
`
by (FULL_SIMP_TAC (srw_ss()) [helper2_thm]));

e(`
pgtype((w2w ((tbl_base !! (((va) >>> 30) << 12)) >>> 12)):word20) = 0b01w
`
by (FULL_SIMP_TAC (srw_ss()) [helper2_thm]));

val helper2_thm = Q.prove(
    `!a:word32 va:word32 idx:word2. (a = (a && 0xFFFFC000w)) ==> 
	 ((w2w((((w2w idx):word32) + (a !! ((va >>> 20)<< 2))) >>> 12)):word20
	       = 
 	         (w2w((a !! (((va && 0xC0000000w) >>> 30)<< 12)) >>> 12)):word20)`,

  (blastLib.BBLAST_TAC));
%%%%%%%%%%%%%%%%%%%%%%%%%



e(`
pgtype((w2w ((tbl_base !! (((va) >>> 30) << 12)) >>> 12)):word20) = 0b01w
`
by (FULL_SIMP_TAC (srw_ss()) [helper2_thm]));












((w2w (a >>> 12)):word20)

val mt = Q.prove(
	 `!a:word32  (idx:word2). 
       (
	 (a = (a && 0xFFFFC000w)) /\  
	   ((((w2w (a >>> 12)):word20) = 0b01w) /\
	     ((((w2w (a >>> 12)):word20) !! 0b01w) = 0b01w) /\ 
              ((((w2w (a >>> 12)):word20) !! 0b10w) = 0b01w) /\
		((((w2w (a >>> 12)):word20) !! 0b11w) = 0b01w))
       ) 
       ==> 
	    (((w2w (a >>> 12)):word20) !! ((w2w idx):word20) = 0b01w)
     `,
 (blastLib.BBLAST_TAC));




















g`!a:word32  va:bool[10] idx:bool[2]. (w2w (a >>> 12)):word20 = (w2w ((a !! ((w2w va):word32 << 2) + (w2w idx):word32) >>> 12)):word20`;
e (blastLib.BBLAST_TAC);
val tt = top_thm ();

g`!f:(word20->word2) a:word20 b:word20. (a = b) ==> ((f a) = (f b))`;
val rr = top_thm();

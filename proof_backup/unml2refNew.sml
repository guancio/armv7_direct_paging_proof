open wordsTheory;
val UNDISCH_MATCH_TAC = fn MATCH => (PAT_ASSUM MATCH (fn th => (MP_TAC th)));
val UNDISCH_ALL_TAC = (REPEAT (UNDISCH_MATCH_TAC ``X``));

val let_ss = simpLib.mk_simpset [boolSimps.LET_ss] ;


val addr_ineq_1_thm = Q.prove(
 `!a:word20 b:word20 idx:bool[10] idx':bool[10]. (a<>b) ==> 
     (
    (((w2w a):word32 << 12) !! ((w2w idx):word32) << 2) <>
    (((w2w b):word32 << 12) !! ((w2w idx'):word32) << 2)  
     )
 `, blastLib.BBLAST_TAC);

val l1_decode_hlpr_thm = Q.prove( `!a:word32. mmu_l1_decode_type (0xFFFFFFFCw && a) = 0w`,
		(FULL_SIMP_TAC(srw_ss()) [mmu_l1_decode_type_def,thm_mmu_l1_decode_type_values]));

val addr_ineq_2_thm = Q.prove(
		     `!a:word32 b:word20 idx:word12. ((a = a && 0xffffc000w) /\ (((w2w (a >>> 12)):word20) <> b))
						     ==> (a <> (((w2w b):word32 << 12) !! ((w2w idx):word32)))`,
		     (blastLib.BBLAST_TAC));

val write_read_thm = prove(
``!a v m m'. (m' = write_mem32(a,m,v)) ==> 
      (read_mem32(a,m') = v)
``,
  (REPEAT GEN_TAC)
THEN EVAL_TAC
THEN (RW_TAC (srw_ss()) [read_mem32_def])
THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.UPDATE_def])
THEN (RW_TAC (srw_ss()) [])
THENL [
 (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE
  ``~(((a:bool[32]) + 3w) = a)``]),
 (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE
  ``~(((a:bool[32]) + 2w) = a)``]),
 (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE
  ``~(((a:bool[32]) + 1w) = a)``]),
 (fn (asl, w) =>
 let val t = blastLib.BBLAST_PROVE w in
 blastLib.BBLAST_TAC (asl, w)
 end)
]);


val write_read_unch_thm = prove(
``!a a' v m m'. 
  (a'+3w <+ a /\ a'+3w >=+ 3w /\ a+3w >=+ 3w) \/
  (a' >+ a+3w /\ a'+3w >=+ 3w /\ a+3w >=+ 3w) ==>
  (m' = write_mem32(a,m,v)) ==> 
  (read_mem32(a',m') = read_mem32(a',m))
``,
  (REPEAT GEN_TAC)
THEN EVAL_TAC
THEN (RW_TAC (srw_ss()) [read_mem32_def])
THEN (FULL_SIMP_TAC (srw_ss()) [combinTheory.UPDATE_def])
THENL [
   (MAP_EVERY (fn (tm1, tm2) =>
   (ASSUME_TAC (UNDISCH (UNDISCH (
   (blastLib.BBLAST_PROVE ``
   (((a':bool[32]) + 3w) <₊ a) ==>
   (((a':bool[32]) + 3w) ≥₊ 3w) ==>
   (((a:bool[32]) + 3w) ≥₊ 3w) ==>
   (^tm1 <> ^tm2)
   ``))))))
   (List.concat (
     List.map (fn tm1 =>
       List.map (fn tm2 =>
        (tm1,tm2)
       )
       [``a':bool[32]``, ``a'+1w:bool[32]``, ``a'+2w:bool[32]``, ``a'+3w:bool[32]``]
     )
     [``a:bool[32]``, ``a+1w:bool[32]``, ``a+2w:bool[32]``, ``a+3w:bool[32]``]
   )))
   THEN (FULL_SIMP_TAC (srw_ss()) [])
,
   (MAP_EVERY (fn (tm1, tm2) =>
   (ASSUME_TAC (UNDISCH (UNDISCH (
   (blastLib.BBLAST_PROVE ``
   (((a':bool[32])) >+ a+3w) ==>
   (((a':bool[32]) + 3w) ≥₊ 3w) ==>
   (((a:bool[32]) + 3w) ≥₊ 3w) ==>
   (^tm1 <> ^tm2)
   ``))))))
   (List.concat (
     List.map (fn tm1 =>
       List.map (fn tm2 =>
        (tm1,tm2)
       )
       [``a':bool[32]``, ``a'+1w:bool[32]``, ``a'+2w:bool[32]``, ``a'+3w:bool[32]``]
     )
     [``a:bool[32]``, ``a+1w:bool[32]``, ``a+2w:bool[32]``, ``a+3w:bool[32]``]
   )))
   THEN (FULL_SIMP_TAC (srw_ss()) [])
]);
val read_mem_eq = prove(``
  !a.
  (mem(a) = mem'(a)) ==>
  (mem(a+1w) = mem'(a+1w)) ==>
  (mem(a+2w) = mem'(a+2w)) ==>
  (mem(a+3w) = mem'(a+3w)) ==>
  (read_mem32(a,mem) = read_mem32(a,mem'))
``, FULL_SIMP_TAC (srw_ss()) [read_mem32_def]);


val ref_inv_l1_thm =Q.prove(
     `! c1 va:word32. 
       (c1 = rec'sctlrT 1w ) ==>
       ((invariant_page_type c1 c2 c3 mem pgtype pgrefs) ==>
       (invariant_page_type c1 c2 c3 mem pgtype pgrefs'))
       `,
    (FULL_SIMP_TAC (srw_ss()) [LET_DEF,invariant_page_type_l1_def,invariant_page_type_l2_def,invariant_page_type_def]));


val mem_location_thm = Q.prove (
       `! a:word32 b:word32. ((a <> b) /\ (a = a && 0xFFFFFFFCw) /\  (b = b && 0xFFFFFFFCw))
	       ==>  (((a + 3w) <+ b) \/ (a >+ (b + 3w)))`,
  blastLib.BBLAST_TAC);

val helperump_1_thm = Q.prove(
    `!a:word32 va:word32 idx:word2. (a = (a && 0xFFFFC000w)) ==> 
	 ((w2w((a !! ((va >>> 20)<< 2)) >>> 12)):word20
	       = 
 	         (w2w((a !! (((va ) >>> 30)<< 12)) >>> 12)):word20)`,

  (blastLib.BBLAST_TAC));
val thm_tmp1 = blastLib.BBLAST_PROVE (``  ((va:bool[32] ⋙ 30) = 0w) \/
    ((va:bool[32] ⋙ 30) = 1w) \/
    ((va:bool[32] ⋙ 30) = 2w) \/
    ((va:bool[32] ⋙ 30) = 3w)``);

val Bless_word_thm = Q.prove (
		`!a b. ((b < dimword (:'a)) /\ (a < dimword (:'a))) ==> ((a < b) =  (((n2w a):'a word) <+ ((n2w b):'a word)))`,
		     RW_TAC (srw_ss()) [word_lo_n2w]);
(* ====================================================================================================================== *)
open sum_numTheory;


fun inv_keep_tac tac = 
    PAT_ASSUM ``(invariant_ref_cnt c1 c2 c3 mem pgtype pgrefs)`` (fn thm =>
       (ASSUME_TAC thm) THEN
        tac THEN
	(ASSUME_TAC thm)
    );
fun no_inv_keep_tac tac = 
    PAT_ASSUM ``(invariant_ref_cnt c1 c2 c3 mem pgtype pgrefs)`` (fn thm =>
        tac THEN
	(ASSUME_TAC thm)
    );

val l2Unmap = ``
  ! c1 va:word32. 
    (c1 = rec'sctlrT 1w ) ==>
    ((invariant_ref_cnt c1 c2 c3 mem pgtype pgrefs) ==>
       (let (c1,c2',c3',mem',pgtype', pgrefs') = 
       (unmap_L2_pageTable_entry (c1, c2, c3, mem, pgtype, pgrefs) (l2_base_add:bool[32]) (l2_index:bool[32])) in
       (invariant_ref_cnt c1 c2' c3' mem' pgtype' pgrefs')))
``;

g `^l2Unmap`;

e (RW_TAC (srw_ss()) []);

e (inv_keep_tac (FULL_SIMP_TAC (srw_ss()) [invariant_ref_cnt_def]));

e (RW_TAC (srw_ss()) []);
r 10;

(* rotate up to the goal *)
(* w2n (pgrefs' ph_page) = count_pages mem' pgtype' ph_page *)
e (FULL_SIMP_TAC (srw_ss()) [LET_DEF]);
e (PAT_ASSUM ``!x:word20. X`` (fn thm => ASSUME_TAC (SPEC ``(ph_page):word20`` thm)));
e (RW_TAC (srw_ss()) []);
e( SUBGOAL_THEN ``
(w2n ((pgrefs' :word20 -> word30) (ph_page :word20)) +
 count_pages (mem :word32 -> word8) (pgtype :word20 -> word2) ph_page) =
(w2n ((pgrefs :word20 -> word30) (ph_page :word20)) +
count_pages (mem' :word32 -> word8) (pgtype' :word20 -> word2) ph_page)
``
 (fn thm =>
  ASSUME_TAC thm
  THEN (FULL_SIMP_TAC (srw_ss())[])
(* to be finished using the fillowing steps *)
(* w2n (pgrefs' ph_page) + count_pages mem pgtype ph_page = w2n (pgrefs ph_page) + count_pages mem' pgtype' ph_page *)
(* w2n (pgrefs' ph_page) + count_pages mem pgtype ph_page - w2n (pgrefs ph_page) = w2n (pgrefs ph_page) + count_pages mem' pgtype' ph_page - w2n (pgrefs ph_page) *)
(* w2n (pgrefs' ph_page) + count_pages mem pgtype ph_page - w2n (pgrefs ph_page) = count_pages mem' pgtype' ph_page *)
(* w2n (pgrefs' ph_page) = count_pages mem' pgtype' ph_page *)
  
));
e (SIMP_TAC (srw_ss()) [count_pages_def]);

e (Q.ABBREV_TAC `f = (λit.
           (let global_page:bool[20] = n2w it in
           count_pages_for_pt mem pgtype ph_page global_page))`);
e (Q.ABBREV_TAC `f' = (λit.
           (let global_page:bool[20] = n2w it in
           count_pages_for_pt mem' pgtype' ph_page global_page))`);

e (SIMP_TAC (srw_ss()) [SUM]);


val thm_max_20 = blastLib.BBLAST_PROVE(``(l2_base_add:bool[32] ⋙ 12) <= 1048576w``);
val thm_max_21 = blastLib.BBLAST_PROVE(``~ word_msb (l2_base_add:bool[32] ⋙ 12)``);
val thm_temo_1 = prove(``w2n (l2_base_add:bool[32] ⋙ 12) + (1048576 − w2n (l2_base_add ⋙ 12)) = 1048576``,
			   ASSUME_TAC thm_max_20
		      THEN  (ASSUME_TAC thm_max_21)
		      THEN (ASSUME_TAC (SPECL [``(l2_base_add:bool[32] ⋙ 12)``, ``1048576w:bool[32]``]
					      (Thm.INST_TYPE[alpha |-> ``:32``] wordsTheory.WORD_LE)))
		      THEN (FULL_SIMP_TAC (srw_ss()) [])
		      THEN (FULL_SIMP_TAC (arith_ss) []));
e (REWRITE_TAC [
   SIMP_RULE (srw_ss()) [thm_temo_1] (
   SPECL [``0:num``,
       ``w2n (l2_base_add:bool[32] ⋙ 12)``,
       ``1048576 - (w2n (l2_base_add:bool[32] ⋙ 12))``,
       ``f:num->num``
      ] GSUM_ADD)]);



val thm_max_30 = blastLib.BBLAST_PROVE(``(l2_base_add:bool[32] ⋙ 12) <= 1048575w``);
val thm_max_31 = blastLib.BBLAST_PROVE(``~ word_msb (l2_base_add:bool[32] ⋙ 12)``);
val thm_temo_3 = prove(``1 + (1048575 − w2n (l2_base_add ⋙ 12)) = 1048576 − w2n (l2_base_add:bool[32] ⋙ 12)``,
			   ASSUME_TAC thm_max_30
		      THEN  (ASSUME_TAC thm_max_31)
		      THEN (ASSUME_TAC (SPECL [``(l2_base_add:bool[32] ⋙ 12)``, ``1048575w:bool[32]``]
					      (Thm.INST_TYPE[alpha |-> ``:32``] wordsTheory.WORD_LE)))
		      THEN (FULL_SIMP_TAC (srw_ss()) [])
		      THEN (FULL_SIMP_TAC (arith_ss) []));

e (REWRITE_TAC [
   SIMP_RULE (srw_ss()) [thm_temo_3] (
   SIMP_RULE (arith_ss) [] (
   SPECL [``w2n (l2_base_add:bool[32] ⋙ 12)``,
	  ``1:num``,
	  ``1048576 - (w2n (l2_base_add:bool[32] ⋙ 12)) - 1``,
	  ``f:num->num``
      ] GSUM_ADD))
]);

e (`(GSUM (0,w2n (l2_base_add ⋙ 12)) f) = (GSUM (0,w2n (l2_base_add ⋙ 12)) f')` by (FULL_SIMP_TAC (arith_ss) []));

(* First subgoal *)
val tmp_thm = SPECL [``0:num``, ``w2n (l2_base_add:bool[32] ⋙ 12)``, ``f:num->num``, ``f':num->num``] GSUM_FUN_EQUAL;
val (t_hyp, t_concl) = dest_imp (concl tmp_thm);
e (SUBGOAL_THEN t_hyp (fn thm =>
  ASSUME_TAC thm
  THEN (ACCEPT_TAC (UNDISCH tmp_thm))
));
e (REPEAT STRIP_TAC);
e (FULL_SIMP_TAC (arith_ss) [Abbr `f`, Abbr `f'`, LET_DEF]);

(* change the theorem to use specific types *)
e (ASSUME_TAC (SIMP_RULE (let_ss) [] (
	  ((SPECL [``pgtype:word20->word2``,
		   ``pgrefs:word20->word30``,
		   ``mem:word32->word8``,
		   ``l2_index:word32``,
		   ``c3:word32``,
		   ``c2:word32``,
		   ``rec'sctlrT 1w``,
		   ``(n2w x):bool[20]``, ``l2_base_add:bool[32]``] 
		  (INST_TYPE [alpha |-> ``:sctlrT``,
			      beta  |-> ``:word32``,
			      gamma |-> ``:word32``] 
			     (GEN_ALL thm_2)))))
  ));
(* That's the way to remove the abstraction? *)
e(UNDISCH_ALL_TAC);
e (RW_TAC (srw_ss()) []);




(* Reinstantiate the invariant for the l1 we are checking *)
e (inv_keep_tac (FULL_SIMP_TAC (srw_ss()) [invariant_ref_cnt_def]));
e (FULL_SIMP_TAC (let_ss) []);
e (PAT_ASSUM ``!ph_page:bool[20].p`` (fn thm=>
  ASSUME_TAC (SPECL [``(n2w x):bool[20]``] thm)
));
e (FULL_SIMP_TAC (srw_ss()) []);


(* SECTION TO FIX since it use byte reasoning: *)
(* Prove that  0w ≤₊ n2w x ∧ n2w x <₊ w2w (l2_base_add ⋙ 12) *)
val thm_tem_1 = prove(`` x < (w2n (l2_base_add:bool[32] ⋙ 12)) ==> (x < dimword(:20))``,
			   ASSUME_TAC thm_max_20
		      THEN  (ASSUME_TAC thm_max_21)
		      THEN (ASSUME_TAC (SPECL [``(l2_base_add:bool[32] ⋙ 12)``, ``1048576w:bool[32]``]
					      (Thm.INST_TYPE[alpha |-> ``:32``] wordsTheory.WORD_LE)))
		      THEN (FULL_SIMP_TAC (srw_ss()) [])
		      THEN (FULL_SIMP_TAC (arith_ss) []));

e (ASSUME_TAC (UNDISCH thm_tem_1));

val thm_max_40 = blastLib.BBLAST_PROVE(``(l2_base_add:bool[32] ⋙ 12) < 1048576w``);
val thm_tem_2 = prove(
		`` ((l2_base_add:bool[32] ⋙ 12) < 0x100000w) ==>  ((w2n (l2_base_add:bool[32] ⋙ 12)) < dimword(:20))``,
			   ASSUME_TAC thm_max_40
		      THEN (ASSUME_TAC thm_max_21)
	              THEN (`(w2n (0x100000w :word32)) = dimword(:20)` by blastLib.BBLAST_TAC)
		      THEN (ASSUME_TAC (SPECL [``(l2_base_add:bool[32] ⋙ 12)``, ``1048576w:bool[32]``]
					      (Thm.INST_TYPE[alpha |-> ``:32``] wordsTheory.WORD_LT)))
		      THEN  (FULL_SIMP_TAC (srw_ss()) []));
e (ASSUME_TAC thm_max_40);
e (ASSUME_TAC (UNDISCH thm_tem_2));


e (ASSUME_TAC (SPECL [``x:num``, `` w2n (l2_base_add:bool[32] ⋙ 12)``] (Thm.INST_TYPE[alpha|->``:20``] Bless_word_thm))) ;
e RES_TAC;

e(`((n2w (x :num) :word20) <₊
            (n2w (w2n ((l2_base_add :word32) ⋙ (12 :num))) :word20))
  =
((n2w (x :num) :word20) <₊
            (w2w ((l2_base_add :word32) ⋙ (12 :num))) :word20) ` by (RW_TAC (srw_ss()) [wordsTheory.w2w_def]));

e (FULL_SIMP_TAC (srw_ss()) []);


(* END OF  0w ≤₊ n2w x ∧ n2w x <₊ w2w (l2_base_add ⋙ 12) *)


val tmp_thm_to_enable = SPECL [``(n2w x):bool[20]``, ``mem:word32->word8``, ``mem':word32->word8``] thm_1;
val (tmp_hy, tmp_concl) = dest_imp (concl tmp_thm_to_enable);

e (`^tmp_hy` by FULL_SIMP_TAC (bool_ss) []);
e (RW_TAC (srw_ss()) []);
e (ASSUME_TAC (UNDISCH tmp_thm_to_enable));

(* Show that the page type has not changed *)
e (ASSUME_TAC (
SPECL [``((c1 :sctlrT),(c2' :word32),(c3' :word32),(mem' :word32 -> word8),(pgtype' :word20 -> word2),(pgrefs' :word20 -> word30))``] (
SIMP_RULE (srw_ss()) [combinTheory.LET_FORALL_ELIM,combinTheory.S_DEF]
	  (SPECL [``l2_base_add:bool[32]``, ``rec'sctlrT 1w``] thm_3)
)));
e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) [markerTheory.Abbrev_def]);



(* FINISH THIS GOAL, I just rotate *)
fun sym_tac (asl, w) =
    let val (a1,a2) = dest_eq w
	val new_thm = SPECL [a2,a1] (Thm.INST_TYPE[alpha |-> (type_of a1)] EQ_SYM)
    in
	(SUBGOAL_THEN  ``^a2=^a1``
		      (fn thm =>
			  (ASSUME_TAC thm)
  		      THEN (ASSUME_TAC new_thm)
		      THEN (FULL_SIMP_TAC (bool_ss) [])))
		     (asl, w)
    end;


e (PAT_ASSUM ``GSUM a b = GSUM a c`` (
   fn thm =>
      SIMP_TAC (arith_ss) [thm]
	       THEN ASSUME_TAC thm
  ));

(* STUPID STAFF TO SIMPLIFY *)
(* fix all this stupid stuff *)
e (SIMP_TAC (bool_ss) [Once arithmeticTheory.ADD_ASSOC]);
e (SIMP_TAC (bool_ss) [Once arithmeticTheory.ADD_COMM]);
e (SIMP_TAC (bool_ss) [Once arithmeticTheory.ADD_ASSOC]);
e (SIMP_TAC (bool_ss) [Once arithmeticTheory.ADD_COMM]);

e sym_tac;
e (SIMP_TAC (bool_ss) [Once arithmeticTheory.ADD_ASSOC]);
e (SIMP_TAC (bool_ss) [Once arithmeticTheory.ADD_COMM]);
e (SIMP_TAC (bool_ss) [Once arithmeticTheory.ADD_ASSOC]);
e (SIMP_TAC (bool_ss) [Once arithmeticTheory.ADD_COMM]);

e (SIMP_TAC (bool_ss) [Once arithmeticTheory.EQ_ADD_LCANCEL]);


e (`(GSUM (w2n (l2_base_add ⋙ 12n) + 1n,1048575n − w2n (l2_base_add ⋙ 12n))f) =
    (GSUM (w2n (l2_base_add ⋙ 12n) + 1n,1048575n − w2n (l2_base_add ⋙ 12n))f')` by (FULL_SIMP_TAC (arith_ss) []));
(* FINISH THIS GOAL, I just rotate *)

r 1;

e (PAT_ASSUM ``GSUM a b = GSUM a c`` (
   fn thm =>
      SIMP_TAC (arith_ss) [thm]
	       THEN ASSUME_TAC thm
  ));



(* FIX ALSO THIS BAD STUFF *)
e (SIMP_TAC (bool_ss) [Once arithmeticTheory.ADD_ASSOC]);
e (SIMP_TAC (bool_ss) [Once arithmeticTheory.ADD_COMM]);

e sym_tac;
e (SIMP_TAC (bool_ss) [Once arithmeticTheory.ADD_ASSOC]);
e (SIMP_TAC (bool_ss) [Once arithmeticTheory.ADD_COMM]);

e (SIMP_TAC (bool_ss) [Once arithmeticTheory.EQ_ADD_LCANCEL]);

e (PAT_ASSUM ``w2n (pgrefs ph_page) = count_pages mem pgtype ph_page`` (
   fn thm =>
      ASSUME_TAC (GSYM thm)
  ));

e (FULL_SIMP_TAC (bool_ss) [GSUM_1]);



(* GO DOWN THE SPECIFIC PT *)
e (Q.UNABBREV_TAC `f`);
e (Q.UNABBREV_TAC `f'`);
e (computeLib.RESTR_EVAL_TAC [``count_pages_for_pt`` ]);



(* check that the type is the same *)
e (ASSUME_TAC (
SPECL [``((c1 :sctlrT),(c2' :word32),(c3' :word32),(mem' :word32 -> word8),(pgtype' :word20 -> word2),(pgrefs' :word20 -> word30))``] (
SIMP_RULE (srw_ss()) [combinTheory.LET_FORALL_ELIM,combinTheory.S_DEF]
	  (SPECL [``l2_base_add:bool[32]``, ``rec'sctlrT 1w``] thm_3)
)));
e (FULL_SIMP_TAC (srw_ss()) []);
e (FULL_SIMP_TAC (srw_ss()) [markerTheory.Abbrev_def]);

e (SIMP_TAC (srw_ss()) [count_pages_for_pt_def]);

e (Q.ABBREV_TAC `no_l2_page = (pgtype (n2w (w2n (l2_base_add ⋙ (12 :num))) :word20) ≠ (2w :word2))`);
e (Cases_on `no_l2_page`);


e (FULL_SIMP_TAC (srw_ss()) [unmap_L2_pageTable_entry_def]);

e cheat;



e (FULL_SIMP_TAC (srw_ss()) []);
e UNABBREV_ALL_TAC;
e (FULL_SIMP_TAC (srw_ss()) []);

e (Cases_on `pgtype ph_page = 0w`);
e (FULL_SIMP_TAC (srw_ss()) []);













   |- !ptb idx mem mem'.
      ((pgtype ptb = 2w) ==>
      (
       (read_mem32 (ptb+idx << 2,mem) = read_mem32 (ptb+idx << 2,mem'))
	==> 
       (l2_count idx mem pgtype ph_page ptb =
       l2_count idx mem' pgtype ph_page ptb)
     )





















for all the enries of the page table that are not equal to the entry that has been chaged  the read_mem(mem) = read_mem(mem')





   !l2_idx.
     (let (c1,c2',c3',mem',pgtype',pgrefs') =
            unmap_L2_pageTable_entry (c1,c2,c3,mem,pgtype,pgrefs)
              l2_base_add l2_index
      in
       (l2_idx <> l2_index) ==>
         (read_mem32 (l2_base_add + l2_idx << 2,mem) = read_mem32 (l2_base_add + l2_idx << 2,mem'))
         






































































































































































































































































(* ================================================================================================================== *)

val l2El_unmap_mem_thm = ``!ptb:word20 (l2_base_add:bool[32]).
           let (c1,c2',c3',mem',pgtype', pgrefs') = 
		(unmap_L2_pageTable_entry (c1, c2, c3, mem, pgtype, pgrefs) (l2_base_add:bool[32]) (l2_index:bool[32])) in
	        (
		 (ptb <> w2w(l2_base_add >>> 12):word20) ==>
                 (!a:bool[32].(((ptb = ((w2w (a >>> 12)):word20)) /\ (a = a && 0xFFFFFFFCw )) ==>
                     (read_mem32(a,mem) = read_mem32(a,mem')))))	
``;
g`^l2El_unmap_mem_thm`;

e  (FULL_SIMP_TAC (srw_ss()) [unmap_L2_pageTable_entry_def]);
e  (RW_TAC (srw_ss()) []) ;
e  (Cases_on `~((l2_type = 2w) ∨ (l2_type = 3w))`);

(* FIRST CASE *)
e  (FULL_SIMP_TAC (srw_ss()) []);

(* SECOND CASE *)
e (Q.ABBREV_TAC `cnd = ((l2_type = 2w) ∨ (l2_type = 3w))`);
e  (FULL_SIMP_TAC (srw_ss()) []);
e  (RW_TAC (srw_ss()) []) ;

val tmp_thm = SPECL [``entryAddrL2:bool[32]``, ``a:bool[32]``, ``page_desc':bool[32]``, ``mem:bool[32]->bool[8]``, ``mem':bool[32]->bool[8]``] write_read_unch_thm;
val (tmp_hyp, tmp_concl) = (dest_imp (concl tmp_thm));

e (SUBGOAL_THEN ``^tmp_hyp`` (fn thm =>
   ASSUME_TAC thm
   THEN (ASSUME_TAC (UNDISCH tmp_thm))
   THEN (RW_TAC (srw_ss()) [])
  ));

e (FULL_SIMP_TAC (srw_ss()) [blastLib.BBLAST_PROVE (``(a = 0xFFFFFFFCw && a:bool[32]) ==> (a + 3w ≥₊ 3w)``)]);
e (Q.UNABBREV_TAC `entryAddrL2`);
e (FULL_SIMP_TAC (srw_ss()) [
   blastLib.BBLAST_PROVE (``(0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2) + 3w ≥₊ 3w:bool[32]``)
]);

e UNABBREV_ALL_TAC;

e (FULL_SIMP_TAC (srw_ss()) [
blastLib.BBLAST_PROVE (``(w2w (a:bool[32] ⋙ 12):bool[20] ≠ w2w (l2_base_add:bool[32] ⋙ 12)) ==>
(a = 0xFFFFFFFCw && a) ==>
(a + 3w <₊ (0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2) ∨
 a >₊ (0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2) + 3w)``)
]);

val thm_l2El_unmap_mem = top_thm();

val l2El_unmap_thm = ``!ptb:word20 (l2_base_add:bool[32]).
           let (c1,c2',c3',mem',pgtype', pgrefs') = 
		(unmap_L2_pageTable_entry (c1, c2, c3, mem, pgtype, pgrefs) (l2_base_add:bool[32]) (l2_index:bool[32])) in
	        (
		 (ptb <+ w2w(l2_base_add >>> 12):word20) ==>

(!a:word32. (
((pgtype ptb = 0b10w) ==> (
     ((ptb = ((w2w (a >>> 12)):word20)) /\ (a = a && 0xFFFFFFFCw )) ==>
      (read_mem32(a,mem) = read_mem32(a,mem')))
) /\
(((pgtype ptb = 0b01w) /\ (ptb && 0xffffcw = ptb)) ==> (
     (((ptb && 0xffffcw) = ((w2w (a >>> 12)):word20 && 0xffffcw)) /\ (a = a && 0xFFFFFFFCw)) ==>
      (read_mem32(a,mem) = read_mem32(a,mem')))
)))
		)
		
``;


g`^l2El_unmap_thm`;

e  (RW_TAC (srw_ss()) []) ;

(* FIRST CASE *)
e (ASSUME_TAC (SPECL [``w2w (a:bool[32] ⋙ 12):bool[20]``, ``l2_base_add:bool[32]``] thm_l2El_unmap_mem));
(* MAGIC SEQUENCE TO MANAGE HYPOTESYS WITH LET. WTIRE ON A STONE *)
e UNDISCH_ALL_TAC;
e (RW_TAC (let_ss) []);
e UNDISCH_ALL_TAC;
e (RW_TAC (srw_ss()) []);

e (RW_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (a ⋙ 12):bool[20] <₊ w2w (l2_base_add ⋙ 12)) ==>
  (w2w (a:bool[32] ⋙ 12):bool[20] ≠ w2w (l2_base_add:bool[32] ⋙ 12):bool[20])
``)
]);

(* SECOND CASE *)
e (`∀ph_page.
          ((pgtype ph_page = 1w) ⇒
           (ph_page = ph_page && 0xFFFFCw) ⇒
           (pgtype (ph_page ‖ 1w) = 1w) ∧ (pgtype (ph_page ‖ 2w) = 1w) ∧
           (pgtype (ph_page ‖ 3w) = 1w))` by FULL_SIMP_TAC (srw_ss()) []);

r 1;



e (ASSUME_TAC (SPECL [``ptb:bool[20]``, ``l2_base_add:bool[32]``] thm_l2El_unmap_mem));
(* MAGIC SEQUENCE TO MANAGE HYPOTESYS WITH LET. WTIRE ON A STONE *)
e UNDISCH_ALL_TAC;
e (RW_TAC (let_ss) []);
e UNDISCH_ALL_TAC;
e (RW_TAC (srw_ss()) []);

e (ASSUME_TAC (
blastLib.BBLAST_PROVE(``
  (ptb:bool[20] <₊ w2w (l2_base_add ⋙ 12)) ==>
  (ptb:bool[20] ≠ w2w (l2_base_add:bool[32] ⋙ 12):bool[20])
``)));
e (FULL_SIMP_TAC (srw_ss()) []);
e (PAT_ASSUM ``!x:word32. X`` (fn thm => ASSUME_TAC (SPEC ``a:word32`` thm)));

blastLib.BBLAST_PROVE(``
(0xFFFFCw && ptb:bool[20] = 0xFFFFCw && w2w (a:bool[32] ⋙ 12))
==>
(ptb = w2w (a ⋙ 12))``);



r 1;
e  (FULL_SIMP_TAC (srw_ss()) [unmap_L2_pageTable_entry_def]);
e (Cases_on `ptb = (w2w (l2_base_add ⋙ 12):bool[20])`);

(* First case *)
e  (FULL_SIMP_TAC (srw_ss()) []) ;
(* Second case *)



e  (FULL_SIMP_TAC (srw_ss()) [unmap_L2_pageTable_entry_def]);
e  (RW_TAC (srw_ss()) []) ;
e  (Cases_on `pgtype (w2w (l2_base_add ⋙ (12 :num)) :word20) ≠ (2w :word2)`);
e  (FULL_SIMP_TAC (srw_ss()) []);
e  (Cases_on `~((l2_type = 2w) ∨ (l2_type = 3w))`);



e  (FULL_SIMP_TAC (srw_ss()) []);
e  (RW_TAC (srw_ss()) []);

e(Cases_on `a = entryAddrL2`);

e(`((w2w ((a :word32) ⋙ (12 :num)) :word20) <+
      (w2w ((l2_base_add :word32) ⋙ (12 :num)) :word20)) ==>
 (((w2w ((a :word32) ⋙ (12 :num)) :word20) <>
      (w2w ((l2_base_add :word32) ⋙ (12 :num)) :word20)) )` by blastLib.BBLAST_TAC);
e RES_TAC;
e(`(w2w(l2_base_add >>> 12):word20) = (w2w((0xFFFFF000w && l2_base_add) >>> 12):word20)` by blastLib.BBLAST_TAC);
e(`(a = entryAddrL2) ==> ((w2w(a >>> 12):word20) = (w2w (entryAddrL2 >>> 12):word20))` by blastLib.BBLAST_TAC);

e(`(w2w ((0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2) >>> 12):word20) = 
   (w2w((0xFFFFF000w && l2_base_add) >>> 12):word20)` by blastLib.BBLAST_TAC);

e(METIS_TAC []);



e ( Q.UNABBREV_TAC `entryAddrL2`);
e(`
(0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2)+3w >=+ 3w 
` by  (blastLib.BBLAST_TAC)); 

e(`(0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2) =
((0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2) && 0xFFFFFFFCw:word32)`
by blastLib.BBLAST_TAC);

e (Q.ABBREV_TAC `aAdd:bool[32] = (0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2)`);
e(`
 (((a + 3w) <+ aAdd) \/ (a >+ (aAdd + 3w)))`
by (FULL_SIMP_TAC (srw_ss())[mem_location_thm]));
e(`
read_mem32 (a,mem') = read_mem32 (a,mem)
` by METIS_TAC[write_read_unch_thm]);
e(METIS_TAC[]);
e(`
read_mem32 (a,mem') = read_mem32 (a,mem)
` by METIS_TAC[write_read_unch_thm]);
e(METIS_TAC[]);

(* =================================================================================================== *)

fun GUANCIO_SUM_4096_FN_EQ (asl, w) =
 let val (a,b) = dest_eq w
     val (_, f1) = dest_comb a
     val (_, f2) = dest_comb b in
     SUBGOAL_THEN ``(∀x. x < 4096 ⇒ (^f1 x = ^f2 x))`` (fn thm =>
       ASSUME_TAC thm
       THEN ASSUME_TAC ( 
         UNDISCH(
	 SPEC ``4096:num``(
	 GEN ``n:num`` (
	 SPECL [f1, f2] sum_numTheory.SUM_FUN_EQUAL)))
       )
       THEN (FULL_SIMP_TAC (arith_ss) [])
     ) (asl, w)
 end;


fun GUANCIO_SUM_1024_FN_EQ (asl, w) =
 let val (a,b) = dest_eq w
     val (_, f1) = dest_comb a
     val (_, f2) = dest_comb b in
     SUBGOAL_THEN ``(∀x. x < 1024 ⇒ (^f1 x = ^f2 x))`` (fn thm =>
       ASSUME_TAC thm
       THEN ASSUME_TAC ( 
         UNDISCH(
	 SPEC ``1024:num``(
	 GEN ``n:num`` (
	 SPECL [f1, f2] sum_numTheory.SUM_FUN_EQUAL)))
       )
       THEN (FULL_SIMP_TAC (arith_ss) [])
     ) (asl, w)
 end;


val ref_cnt_equality_thm =
    ``!(ptb:word20) (mem:word32->word8) mem'.

(!a:word32. (
((pgtype ptb = 0b10w) ==> (
     ((ptb = ((w2w (a >>> 12)):word20)) /\ (a = a && 0xFFFFFFFCw )) ==>
      (read_mem32(a,mem) = read_mem32(a,mem')))
) /\
(((pgtype ptb = 0b01w) /\ (ptb && 0xffffcw = ptb)) ==> (
     (((ptb && 0xffffcw) = ((w2w (a >>> 12)):word20 && 0xffffcw)) /\ (a = a && 0xFFFFFFFCw)) ==>
      (read_mem32(a,mem) = read_mem32(a,mem')))
)))
	  ==>
	  (
	   (count_pages_for_pt mem pgtype  ph_page ptb) =
	   (count_pages_for_pt mem' pgtype  ph_page ptb)
	  )``;

g`^ref_cnt_equality_thm`;
e(RW_TAC (srw_ss()) []);
e(FULL_SIMP_TAC(srw_ss()) [count_pages_for_pt_def]);
e(RW_TAC (srw_ss()) []);

(* case data: trivial *)
e(FULL_SIMP_TAC(srw_ss()) []);

(* case L1 *)
e GUANCIO_SUM_4096_FN_EQ;
e(RW_TAC (srw_ss()) []);
e(FULL_SIMP_TAC(srw_ss()) [l1_count_def]);
e(RW_TAC (srw_ss()) []);

e(`desc_add' = desc_add` by METIS_TAC []);
e(FULL_SIMP_TAC(srw_ss()) []);

e (PAT_ASSUM ``!x:word32. X`` (fn thm => ASSUME_TAC (SPEC ``desc_add:word32`` thm)));
e (FULL_SIMP_TAC(srw_ss()) []);

e (`(desc_add = 0xFFFFFFFCw && desc_add:bool[32])` by 
        ((Q.UNABBREV_TAC `desc_add`)
   THEN (Q.UNABBREV_TAC `global_page_add`)
   THEN blastLib.BBLAST_TAC)
);

e (FULL_SIMP_TAC(srw_ss()) []);

e (`(ptb = 0xFFFFCw && ptb) ==> (ptb = 0xFFFFCw && w2w (desc_add ⋙ 12))` by (
   (Q.UNABBREV_TAC `desc_add`)
   THEN (Q.UNABBREV_TAC `global_page_add`)
   THEN blastLib.BBLAST_TAC
));
e (FULL_SIMP_TAC(srw_ss()) []);
e (METIS_TAC []);

(* case L2 *)
e GUANCIO_SUM_1024_FN_EQ;
e(RW_TAC (srw_ss()) [l2_count_def]);
e(`desc_add' = desc_add` by METIS_TAC []);
e(FULL_SIMP_TAC(srw_ss()) []);
e (PAT_ASSUM ``!x:word32. X`` (fn thm => ASSUME_TAC (SPEC ``desc_add:word32`` thm)));

e (`(desc_add = 0xFFFFFFFCw && desc_add:bool[32])` by 
        ((Q.UNABBREV_TAC `desc_add`)
   THEN (Q.UNABBREV_TAC `global_page_add`)
   THEN blastLib.BBLAST_TAC)
);
e (FULL_SIMP_TAC(srw_ss()) []);

e (`ptb = w2w (desc_add ⋙ 12)` by 
        ((Q.UNABBREV_TAC `desc_add`)
   THEN (Q.UNABBREV_TAC `global_page_add`)
   THEN blastLib.BBLAST_TAC)
);
e (FULL_SIMP_TAC(srw_ss()) []);

e (METIS_TAC []);
val thm_1 = top_thm();




(* ===================================================================================================================== *)

val l2El_unmap_pgtype_thm = ``!l2_base_add c1:sctlrT.
           let (c1,c2',c3',mem',pgtype', pgrefs') = 
		(unmap_L2_pageTable_entry ((c1:sctlrT), (c2:bool[32]), (c3:bool[32]), mem, pgtype, pgrefs) (l2_base_add:bool[32]) (l2_index:bool[32])) in
	        (pgtype' = pgtype)
		
``;


g`^l2El_unmap_pgtype_thm`;
e (FULL_SIMP_TAC (srw_ss()) [unmap_L2_pageTable_entry_def]);
e BasicProvers.LET_ELIM_TAC;
e (Cases_on `pgtype (w2w (l2_base_add ⋙ 12)) ≠ 2w`);
e  (FULL_SIMP_TAC (srw_ss()) []) ;
e  (FULL_SIMP_TAC (srw_ss()) []) ;
e (Q.ABBREV_TAC `cnd = (l2_type = 2w) ∨ (l2_type = 3w)`);
e (Cases_on `cnd`);
e  (FULL_SIMP_TAC (srw_ss()) []) ;
e  (FULL_SIMP_TAC (srw_ss()) []) ;
val thm_3 = top_thm();

(* ===================================================================================================================== *)
val l2El_unmap_thm = ``!ptb:word20 (l2_base_add:bool[32]).
           let (c1,c2',c3',mem',pgtype', pgrefs') = 
		(unmap_L2_pageTable_entry (c1, c2, c3, mem, pgtype, pgrefs) (l2_base_add:bool[32]) (l2_index:bool[32])) in
	        
		((pgtype ptb = 1w) ⇒
				       (ptb = ptb && 0xFFFFCw) ⇒
				       (pgtype (ptb ‖ 1w) = 1w) ∧ (pgtype (ptb ‖ 2w) = 1w) ∧
				       (pgtype (ptb ‖ 3w) = 1w)) ==>

	       (
		 (ptb <+ w2w(l2_base_add >>> 12):word20) ==>
		 (!a:word32. (
		     ((pgtype ptb = 0b10w) ==> (
		      ((ptb = ((w2w (a >>> 12)):word20)) /\ (a = a && 0xFFFFFFFCw )) ==>
 		        (read_mem32(a,mem) = read_mem32(a,mem')))
		     ) /\
	        (((pgtype ptb = 0b01w) /\ (ptb && 0xffffcw = ptb)) ==> (
                 (((ptb && 0xffffcw) = ((w2w (a >>> 12)):word20 && 0xffffcw)) /\ (a = a && 0xFFFFFFFCw)) ==>
                   (read_mem32(a,mem) = read_mem32(a,mem')))
		 )))
		)
		
``;


g`^l2El_unmap_thm`;

e  (RW_TAC (srw_ss()) []) ;

(* FIRST CASE *)
e (ASSUME_TAC (SPECL [``w2w (a:bool[32] ⋙ 12):bool[20]``, ``l2_base_add:bool[32]``] thm_l2El_unmap_mem));
(* MAGIC SEQUENCE TO MANAGE HYPOTESYS WITH LET. WTIRE ON A STONE *)
e UNDISCH_ALL_TAC;
e (RW_TAC (let_ss) []);
e UNDISCH_ALL_TAC;
e (RW_TAC (srw_ss()) []);

e (RW_TAC (srw_ss()) [
blastLib.BBLAST_PROVE(``
  (w2w (a ⋙ 12):bool[20] <₊ w2w (l2_base_add ⋙ 12)) ==>
  (w2w (a:bool[32] ⋙ 12):bool[20] ≠ w2w (l2_base_add:bool[32] ⋙ 12):bool[20])
``)
]);

(* SECOND CASE *)
e (FULL_SIMP_TAC (srw_ss()) []);
e (ASSUME_TAC (SPECL [``0xFFFFCw &&(w2w (a:bool[32] ⋙ 12):word20)``, ``l2_base_add:bool[32]``] thm_l2El_unmap_mem));
(* MAGIC SEQUENCE TO MANAGE HYPOTESYS WITH LET. WTIRE ON A STONE *)
e UNDISCH_ALL_TAC;
e (RW_TAC (let_ss) []);
e UNDISCH_ALL_TAC;
e (RW_TAC (srw_ss()) []);

e (ASSUME_TAC (
blastLib.BBLAST_PROVE(``
  (((0xFFFFCw :word20) && (w2w ((a :word32) ⋙ (12 :num)) :word20)) <₊
      (w2w ((l2_base_add :word32) ⋙ (12 :num)) :word20)) ==>
  (((0xFFFFCw :word20) && (w2w ((a :word32) ⋙ (12 :num)) :word20)) <>
      (w2w ((l2_base_add :word32) ⋙ (12 :num)) :word20))
``)));
e (FULL_SIMP_TAC (srw_ss()) []);

e RES_TAC;
e (FULL_SIMP_TAC (srw_ss()) []);
e (RW_TAC (srw_ss()) []);

e (PAT_ASSUM ``!x:word32. X`` (fn thm => ASSUME_TAC (SPEC ``a:word32`` thm)));

e RES_TAC;
e (FULL_SIMP_TAC (srw_ss()) []);
e (RW_TAC (srw_ss()) []);
(* =================================================================================================== *)

val my_prop = ``
((w2w ((a :word32) ⋙ (12 :num)) :word20) =
(0xFFFFCw :word20) && (w2w ((a :word32) ⋙ (12 :num)) :word20) ‖
         (1w :word20)) \/
((w2w ((a :word32) ⋙ (12 :num)) :word20) =
(0xFFFFCw :word20) && (w2w ((a :word32) ⋙ (12 :num)) :word20) ‖
         (2w :word20)) \/
((w2w ((a :word32) ⋙ (12 :num)) :word20) =
(0xFFFFCw :word20) && (w2w ((a :word32) ⋙ (12 :num)) :word20) ‖
         (3w :word20)) \/
((w2w ((a :word32) ⋙ (12 :num)) :word20) =
(0xFFFFCw :word20) && (w2w ((a :word32) ⋙ (12 :num)) :word20))``;


val aAddEq =blastLib.BBLAST_PROVE(my_prop);
e (ASSUME_TAC aAddEq);

e(`
(^my_prop) ==> 
((pgtype (w2w ((a :word32) ⋙ (12 :num)) :word20)) = 1w)` by METIS_TAC[]);

e (Q.ABBREV_TAC ` eqR:bool = (^my_prop)`);
e RES_TAC;

e (Cases_on `(a :word32) = (l2_base_add:word32)`);
e(`((a :word32) = (l2_base_add:word32)) ==> 
    ((pgtype (w2w ((a :word32) ⋙ (12 :num)) :word20) ) = (pgtype (w2w ((l2_base_add :word32) ⋙ (12 :num)) :word20)))` 
      by METIS_TAC []);
e RES_TAC;



e  (FULL_SIMP_TAC (srw_ss()) [unmap_L2_pageTable_entry_def]);

(* Second Case *)
e  (FULL_SIMP_TAC (srw_ss()) [unmap_L2_pageTable_entry_def]);

e  (Cases_on `pgtype (w2w (l2_base_add ⋙ (12 :num)) :word20) ≠ (2w :word2)`);
e  (FULL_SIMP_TAC (srw_ss()) []);



e  (FULL_SIMP_TAC (srw_ss()) []);

e (Q.ABBREV_TAC `entryAddrL2:word32 =
             0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2`);
e  (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF]);
e (Q.ABBREV_TAC `page_desc:word32 = read_mem32 (entryAddrL2,mem)`);

e  (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF]);

e  (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF]);

e(Q.ABBREV_TAC`pgdOR:bool = (3w && page_desc = 2w) ∨ (3w && page_desc = 3w)`);
e  (Cases_on `~pgdOR`);

e  (FULL_SIMP_TAC (srw_ss()) []);


e  (FULL_SIMP_TAC (srw_ss()) []);
e  (FULL_SIMP_TAC (srw_ss()) [ LET_DEF]);
e (RW_TAC (srw_ss()) []);

e( Q.ABBREV_TAC ` (mem':word32->word8) =
 write_mem32
     ((entryAddrL2 :word32),mem,
      (0xFFFFFFFCw :word32) && (page_desc :word32))`);


(* ================ *)

e(Cases_on `a = entryAddrL2`);

e(`(w2w(l2_base_add >>> 12):word20) = (w2w((0xFFFFF000w && l2_base_add) >>> 12):word20)` by blastLib.BBLAST_TAC);
e(`(a = entryAddrL2) ==> ((w2w(a >>> 12):word20) = (w2w (entryAddrL2 >>> 12):word20))` by blastLib.BBLAST_TAC);

e ( Q.UNABBREV_TAC `entryAddrL2`);
e(`(w2w ((0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2) >>> 12):word20) = 
   (w2w((0xFFFFF000w && l2_base_add) >>> 12):word20)` by blastLib.BBLAST_TAC);

e  (FULL_SIMP_TAC (srw_ss()) []);




e ( Q.UNABBREV_TAC `entryAddrL2`);
e(`
(0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2)+3w >=+ 3w 
` by  (blastLib.BBLAST_TAC)); 

e(`(0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2) =
((0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2) && 0xFFFFFFFCw:word32)`
by blastLib.BBLAST_TAC);

e (Q.ABBREV_TAC `aAdd:bool[32] = (0xFFFFF000w && l2_base_add ‖ 4092w && l2_index ≪ 2)`);

e(`(a:word32 = 0xFFFFFFFCw && a) ==> ((a +3w) >=+ 3w)` by blastLib.BBLAST_TAC);
e RES_TAC;

e(`
 (((a + 3w) <+ aAdd) \/ (a >+ (aAdd + 3w)))`
by (FULL_SIMP_TAC (srw_ss())[mem_location_thm]));

e(`
read_mem32 (a,mem') = read_mem32 (a,mem)
` by METIS_TAC[write_read_unch_thm]);
e(METIS_TAC[]);
e(`
read_mem32 (a,mem') = read_mem32 (a,mem)
` by METIS_TAC[write_read_unch_thm]);
e(METIS_TAC[]);

val thm_2 = top_thm();








val mem_location_thm = Q.prove (
       `! a:word32 b:word32. ((a <> b) /\ (a = a && 0xFFFFFFFCw) /\  (b = b && 0xFFFFFFFCw))
	       ==>  (((a + 3w) <+ b) \/ (a >+ (b + 3w)))`,
  blastLib.BBLAST_TAC);

val helperump_1_thm = Q.prove(
    `!a:word32 va:word32 idx:word2. (a = (a && 0xFFFFC000w)) ==> 
	 ((w2w((a !! ((va >>> 20)<< 2)) >>> 12)):word20
	       = 
 	         (w2w((a !! (((va ) >>> 30)<< 12)) >>> 12)):word20)`,

  (blastLib.BBLAST_TAC));
val thm_tmp1 = blastLib.BBLAST_PROVE (``  ((va:bool[32] ⋙ 30) = 0w) \/
    ((va:bool[32] ⋙ 30) = 1w) \/
    ((va:bool[32] ⋙ 30) = 2w) \/
    ((va:bool[32] ⋙ 30) = 3w)``);

val l2Unmap = ``
  ! c1 va:word32. 
    (c1 = rec'sctlrT 1w ) ==>
    ((invariant_page_type c1 c2 c3 mem pgtype pgrefs) ==>
       (let (c1,c2',c3',mem',pgtype', pgrefs') = 
       (unmap_L2_pageTable_entry (c1, c2, c3, mem, pgtype, pgrefs) va) in
       (invariant_page_type c1 c2' c3' mem' pgtype' pgrefs')))
``;
(* val l2Unmap = `` *)
(*   ! c1 va:word32.  *)
(*     (c1 = rec'sctlrT 1w ) ==> *)
(*     ((invariant_ref_cnt c1 c2 c3 mem pgtype pgrefs) ==> *)
(*        (let (c1,c2',c3',mem',pgtype', pgrefs') =  *)
(*        (unmap_L2_pageTable_entry (c1, c2, c3, mem, pgtype, pgrefs) va) in *)
(*        (invariant_ref_cnt c1 c2' c3' mem' pgtype' pgrefs'))) *)
(* ``; *)
g `^l2Unmap`;

e (FULL_SIMP_TAC (srw_ss()) [unmap_L2_pageTable_entry_def]);
e (RW_TAC (srw_ss()) []);


e(Cases_on `pgtype (w2w (entryAddrL1 ⋙ 12)) ≠ 1w`);
e (FULL_SIMP_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) [COND_EXPAND_IMP]);

e(Cases_on ` mmu_l1_decode_type descL1 ≠ 1w`);
e (FULL_SIMP_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) [COND_EXPAND_IMP]);

e(Cases_on `pgtype (w2w (entryAddrL2 ⋙ 12)) ≠ 2w`);
e (FULL_SIMP_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) [COND_EXPAND_IMP]);

e(Cases_on `((l2_type = 2w) ∨ (l2_type = 3w))`);
r 1;
e (FULL_SIMP_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) [COND_EXPAND_IMP]);

(* // we have two sub goal for l1_type=2 \/ l1_type_2 *)
e(`entryAddrL2 <> entryAddrL1` by SIMP_TAC (srw_ss()) []);
e(STRIP_TAC);
e(FULL_SIMP_TAC (srw_ss()) []);


e(`
entryAddrL1+3w >=+ 3w 
` by (bossLib.UNABBREV_ALL_TAC) THEN (blastLib.BBLAST_TAC)); 


e(`
entryAddrL2+3w >=+ 3w
` by (bossLib.UNABBREV_ALL_TAC) THEN (blastLib.BBLAST_TAC)); 

(* //it generates two cases *)
e(`entryAddrL1 = (entryAddrL1 && 0xFFFFFFFCw)` by ( Q.UNABBREV_TAC `entryAddrL1`) THEN blastLib.BBLAST_TAC);
e(`entryAddrL2 = (entryAddrL2 && 0xFFFFFFFCw)` by ( Q.UNABBREV_TAC `entryAddrL2`) THEN blastLib.BBLAST_TAC);

e(`
 (((entryAddrL1 + 3w) <+ entryAddrL2) \/ (entryAddrL1 >+ (entryAddrL2 + 3w)))`
by (FULL_SIMP_TAC(srw_ss())[mem_location_thm]));

(* // From this point the proof should be repeated for the next subgoal *)
e(`
read_mem32 (entryAddrL1,mem) = read_mem32 (entryAddrL1,mem')
` by METIS_TAC[write_read_unch_thm]);



e (FULL_SIMP_TAC (srw_ss()) [invariant_page_type_def]);
e (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF]);

e (PAT_ASSUM ``!x:word20. X`` (fn thm => ASSUME_TAC (SPEC ``(w2w((c2 && 0xFFFFC000w:word32) ⋙ 12)):word20`` thm)
					 THEN (ASSUME_TAC thm)
  ));

e RES_TAC;
e (FULL_SIMP_TAC (srw_ss()) [mmu_tbl_base_addr_def]);
e(`((w2w ((0xFFFFCw :word32) && (c2' :word32) ⋙ (12 :num)) :word20) =
        (0xFFFFCw :word20) &&
        (w2w ((0xFFFFCw :word32) && c2' ⋙ (12 :num)) :word20))` by blastLib.BBLAST_TAC);

e RES_TAC;

e (PAT_ASSUM `` invariant_page_type_l1_def c1 c2' c3' mem pgtype' pgrefs
         (w2w (0xFFFFCw && c2' ⋙ 12))`` (
   fn thm => ASSUME_TAC (
	     SIMP_RULE (srw_ss()) [invariant_page_type_l1_def] ( thm))
  ));


e (PAT_ASSUM ``!x:word12. X`` (fn thm => ASSUME_TAC (SPEC `` (w2w (va:word32 >>> 20)):word12`` thm)));


e (FULL_SIMP_TAC (srw_ss()) [ LET_DEF(* ,mmu_tbl_base_addr_def *),ref_inv_l1_thm]);


e(FULL_SIMP_TAC (srw_ss()) [Once LET_DEF]);

e(`
(w2w
                ((0xFFFFCw :word20) &&
                 (w2w
                    ((0xFFFFCw :word32) && (c2' :word32) ⋙ (12 :num)) :
                    word20)) :word32) ≪ (12 :num)
=
 0xFFFFC000w && c2' ` by blastLib.BBLAST_TAC);


e(`
(w2w (w2w ((va :word32) ⋙ (20 :num)) :word12) :word32)
=
va ⋙ 20
` by blastLib.BBLAST_TAC);
e(FULL_SIMP_TAC (srw_ss()) []);

e(RW_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) [ LET_DEF]);

e( Q.UNABBREV_TAC `descL1`);
e( Q.UNABBREV_TAC `entryAddrL1`);

e(`(read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem) =
       read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem')) ==>
(mmu_l1_decode_type (read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem)) =
mmu_l1_decode_type(read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem')))      
` by METIS_TAC []);
e RES_TAC;
e(FULL_SIMP_TAC (srw_ss()) []);
(* // next *)

e (FULL_SIMP_TAC (srw_ss()) [ LET_DEF]);

e( Q.UNABBREV_TAC `descL1`);
e( Q.UNABBREV_TAC `entryAddrL1`);

e(`(read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem) =
       read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem')) ==>
(mmu_l1_decode_type (read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem)) =
mmu_l1_decode_type(read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem')))      
` by METIS_TAC []);
e RES_TAC;
e(FULL_SIMP_TAC (srw_ss()) []);

(* --------------------------------------------------------------------------- *)
(* /section part *)

e(RW_TAC (srw_ss()) []);
e( Q.UNABBREV_TAC `descL1`);
e( Q.UNABBREV_TAC `entryAddrL1`);


e(`
(((w2w
                ((0xFFFFCw :word20) &&
                 (w2w ((0xFFFFCw :word32) && (c2 :word32) ⋙ (12 :num)) :
                    word20)) :word32) ≪ (12 :num) ‖
             (w2w (w2w ((va :word32) ⋙ (20 :num)) :word12) :word32) ≪
             (2 :num),(mem' :word32 -> word8))
=
((0xFFFFC000w :word32) && (c2 :word32) ‖
             (va :word32) ⋙ (20 :num) ≪ (2 :num),
             (mem' :word32 -> word8)))`
by blastLib.BBLAST_TAC);

e(FULL_SIMP_TAC(srw_ss())[]);

e(`(read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem) =
       read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem')) ==>
(mmu_l1_decode_type (read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem)) =
mmu_l1_decode_type(read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem')))
` by METIS_TAC []);
e RES_TAC;
e(FULL_SIMP_TAC (srw_ss()) []);

(* // next section *)
e( Q.UNABBREV_TAC `descL1`);
e( Q.UNABBREV_TAC `entryAddrL1`);


e(`
(((w2w
                ((0xFFFFCw :word20) &&
                 (w2w ((0xFFFFCw :word32) && (c2 :word32) ⋙ (12 :num)) :
                    word20)) :word32) ≪ (12 :num) ‖
             (w2w (w2w ((va :word32) ⋙ (20 :num)) :word12) :word32) ≪
             (2 :num),(mem' :word32 -> word8))
=
((0xFFFFC000w :word32) && (c2 :word32) ‖
             (va :word32) ⋙ (20 :num) ≪ (2 :num),
             (mem' :word32 -> word8)))`
by blastLib.BBLAST_TAC);

e(FULL_SIMP_TAC(srw_ss())[]);

e(`(read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem) =
       read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem')) ==>
(mmu_l1_decode_type (read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem)) =
mmu_l1_decode_type(read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem')))
` by METIS_TAC []);
e RES_TAC;
e(FULL_SIMP_TAC (srw_ss()) []);
(* --------------------------------------------------------------------------- *)
(* //next sub goal *)

e(RW_TAC (srw_ss()) []);
e(FULL_SIMP_TAC (srw_ss()) [invariant_page_type_l2_def]);

e(RW_TAC (srw_ss()) []);

e(`(w2w (w2w ((va :word32) ⋙ (20 :num)) :word12) :
                       word32) =
((va :word32) ⋙ (20 :num))` by blastLib.BBLAST_TAC);
e(FULL_SIMP_TAC (srw_ss()) []);

e(`
(w2w
                  ((0xFFFFCw :word20) &&
                   (w2w
                      ((0xFFFFCw :word32) && (c2 :word32) ⋙ (12 :num)) :
                      word20)) :word32)≪ (12 :num)
=
(0xFFFFC000w :word32) && (c2 :word32)
` by blastLib.BBLAST_TAC);
e(FULL_SIMP_TAC (srw_ss()) []);

e (Q.ABBREV_TAC `l2_base:bool[22] = (rec'l1PTT
                (read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem))).addr`);



e (PAT_ASSUM ``!x:word20. X`` (fn thm => ASSUME_TAC (SPEC `` w2w((l2_base:bool[22]) >>> 2):word20`` thm) THEN (ASSUME_TAC thm) ));


(* e( Q.UNABBREV_TAC `ptEntry`); *)
(* e( Q.UNABBREV_TAC `descL1`); *)
e( Q.UNABBREV_TAC `entryAddrL1`);


(* e(` (read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem) = *)
(*        read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem')) ==> *)
(* ((w2w (((w2w *)
(*             (rec'l1PTT *)
(*                (read_mem32 *)
(*                   ((0xFFFFC000w :word32) && (c2 :word32) ‖ *)
(*                    (va :word32) ⋙ (20 :num) ≪ (2 :num), *)
(*                    (mem' :word32 -> word8)))).addr :word32) ≪ (10 : *)
(*          num) ‖ (1020w :word32) && va ⋙ (10 :num)) >>> 12 )):word20 *)

(* =  *)
(* (w2w ((rec'l1PTT *)
(*              (read_mem32 *)
(*                 ((0xFFFFC000w :word32) && (c2 :word32) ‖ *)
(*                  (va :word32) ⋙ (20 :num) ≪ (2 :num), *)
(*                  (mem :word32 -> word8)))).addr >>> 2)):word20) *)
(* ` by blastLib.BBLAST_TAC); *)
e(RW_TAC (srw_ss())[]);

e (PAT_ASSUM``
 (pgtype (w2w (l2_base ⋙ 2)) = 2w) ⇒
       ∀l2_idx.
         (let l2_desc =
                read_mem32
                  (w2w (w2w (l2_base ⋙ 2)) ≪ 12 ‖ w2w l2_idx ≪ 2,mem)
          in
            (3w && l2_desc = 0w) ∨
            ((3w && l2_desc = 2w) ∨ (3w && l2_desc = 3w)) ∧
            if pgtype (rec'l2SmallT l2_desc).addr ≠ 0w then
              (rec'l2SmallT l2_desc).ap = 2w
            else
              ((rec'l2SmallT l2_desc).ap = 2w) ∨
              ((rec'l2SmallT l2_desc).ap = 3w))`` (fn thm => IMP_RES_TAC thm));


e (PAT_ASSUM ``!l2_idx:bool[10].p`` (fn thm => 
  ASSUME_TAC (SPEC ``(w2w((l2_base:bool[22] && 3w) << 8):bool[10]) !! (w2w((va:bool[32] && 0xFF000w) ⋙ 12):bool[10])`` thm)
));


e(`
(read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem) =
        read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem')) ==>
((rec'l1PTT(read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem))).addr
=
(rec'l1PTT (read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem'))). addr)
` by blastLib.BBLAST_TAC);


e (PAT_ASSUM``(read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem) =
        read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem')) ⇒
       ((rec'l1PTT (read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem))).
        addr =
        (rec'l1PTT (read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem'))).
        addr)
`` (fn thm => IMP_RES_TAC thm));
e(FULL_SIMP_TAC(srw_ss())[]);
e(`
(rec'l1PTT (read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem'))). addr
= 
l2_base` by ( Q.UNABBREV_TAC `l2_base`) THEN FULL_SIMP_TAC(srw_ss())[]);
e(FULL_SIMP_TAC(srw_ss())[]);

e(`

((w2w (w2w ((l2_base :22 word) ⋙ (2 :num)) :word20) :
                   word32) ≪ (12 :num) ‖
                (w2w
                   ((w2w ((255w :word32) && (va :word32) ⋙ (12 :num)) :
                       word10) ‖
                    (w2w ((768w :22 word) && l2_base ≪ (8 :num)) :
                       word10)) :word32) ≪ (2 :num))
=
((w2w (l2_base :22 word) :word32) ≪ (10 :num) ‖
         (1020w :word32) && (va :word32) ⋙ (10 :num))

` by blastLib.BBLAST_TAC);

e(FULL_SIMP_TAC(srw_ss())[]);

e (FULL_SIMP_TAC (srw_ss()) [Once LET_DEF]);

e( Q.UNABBREV_TAC `page_desc`);
e( Q.UNABBREV_TAC `entryAddrL2`);

e( ` ptEntry.addr = l2_base` by METIS_TAC[]);
e(FULL_SIMP_TAC(srw_ss())[]);

e(bossLib.UNABBREV_ALL_TAC);
e(FULL_SIMP_TAC(srw_ss())[]);

(* (* r 1; *) the difference between two branchs of proof *)
e( Q.UNABBREV_TAC `page_desc`);
e( Q.UNABBREV_TAC `entryAddrL2`);

e( ` ptEntry.addr = l2_base` by METIS_TAC[]);
e(FULL_SIMP_TAC(srw_ss())[]);

e(bossLib.UNABBREV_ALL_TAC);
e(FULL_SIMP_TAC(srw_ss())[]);

(* e( Q.UNABBREV_TAC `page_desc`); *)
(* e( Q.UNABBREV_TAC `entryAddrL2`); *)


(* e(` *)
(* ((2w:word32) = *)
(*          (3w :word32) && *)
(*          read_mem32 *)
(*            ((w2w (l2_base :22 word) :word32) ≪ (10 :num) ‖ *)
(*             (1020w :word32) && (va :word32) ⋙ (10 :num), *)
(*             (mem :word32 -> word8))) *)
(* ==> *)
(* (((3w :word32) && *)
(*        read_mem32 *)
(*          ((w2w (l2_base :22 word) :word32) ≪ (10 :num) ‖ *)
(*           (1020w :word32) && (va :word32) ⋙ (10 :num), *)
(*           (mem :word32 -> word8))) <> *)
(*        (3w:word32)) *)
(* ` by blastLib.BBLAST_TAC); *)
(* e(bossLib.UNABBREV_ALL_TAC); *)
(* e RES_TAC; *)
(* e(FULL_SIMP_TAC(srw_ss())[]); *)

e( ` ptEntry.addr = l2_base` by METIS_TAC[]);
e(FULL_SIMP_TAC(srw_ss())[]);

e(Cases_on `(w2w ph_page ≪ 12 ‖ w2w l2_idx ≪ 2) = (w2w l2_base ≪ 10 ‖ 1020w && va ⋙ 10) `);

e(FULL_SIMP_TAC(srw_ss())[]);




e (ASSUME_TAC ( (
SPECL [``
(entryAddrL2:word32)
``,
``
(page_desc':word32)
``,
``mem:bool[32]->bool[8]``,
``mem':bool[32]->bool[8]``] write_read_thm
)));
e( Q.UNABBREV_TAC `entryAddrL2`);
e(FULL_SIMP_TAC(srw_ss())[]);

(* e (PAT_ASSUM``(mem' = write_mem32 (entryAddrL2,mem,page_desc')) ⇒ *)
(*        (read_mem32 *)
(*           (entryAddrL2,write_mem32 (entryAddrL2,mem,page_desc')) = *)
(*         page_desc')`` (fn thm => IMP_RES_TAC thm)); *)
(* e RES_TAC; *)
e( Q.UNABBREV_TAC `mem'`);
e(FULL_SIMP_TAC(srw_ss())[]);
e( Q.UNABBREV_TAC `page_desc'`);
e(FULL_SIMP_TAC(srw_ss())[]);
(* // next sub goal *)

e(`
(w2w l2_base ≪ 10 ‖ 1020w && va ⋙ 10)+3w >=+ 3w 
` by  (blastLib.BBLAST_TAC)); 
e(`
((w2w (ph_page:word20):word32) ≪ (12 :num) ‖
           (w2w (l2_idx:word10) :word32) ≪ (2 :num))+ 3w >=+ 3w` by blastLib.BBLAST_TAC);

e(`((w2w (ph_page :word20) :word32) ≪ (12 :num) ‖
        (w2w (l2_idx :word10) :word32) ≪ (2 :num)) =
(((w2w (ph_page :word20) :word32) ≪ (12 :num) ‖
        (w2w (l2_idx :word10) :word32) ≪ (2 :num)) && (0xFFFFFFFCw:word32))` 
by blastLib.BBLAST_TAC);
e(`((w2w (l2_base :22 word) :word32) ≪ (10 :num) ‖
        (1020w :word32) && (va :word32) ⋙ (10 :num)) =
(((w2w (l2_base :22 word) :word32) ≪ (10 :num) ‖
        (1020w :word32) && (va :word32) ⋙ (10 :num)) && 0xFFFFFFFCw:word32)`
by blastLib.BBLAST_TAC);


e (Q.ABBREV_TAC `gAdd:bool[32] = ((w2w (ph_page :word20) :word32) ≪ (12 :num) ‖
        (w2w (l2_idx :word10) :word32) ≪ (2 :num)) `);
e (Q.ABBREV_TAC `aAdd:bool[32] = ((w2w (l2_base :22 word) :word32) ≪ (10 :num) ‖
        (1020w :word32) && (va :word32) ⋙ (10 :num))`);


e(`
 (((gAdd + 3w) <+ aAdd) \/ (gAdd >+ (aAdd + 3w)))`
by (FULL_SIMP_TAC(srw_ss())[mem_location_thm]));

e(`
read_mem32 ((w2w ph_page ≪ 12 ‖ w2w l2_idx ≪ 2),mem') = read_mem32 ((w2w ph_page ≪ 12 ‖ w2w l2_idx ≪ 2),mem)
` by METIS_TAC[write_read_unch_thm]);
e( Q.UNABBREV_TAC `gAdd`);
e( Q.UNABBREV_TAC `aAdd`);

e(FULL_SIMP_TAC(srw_ss())[]);


e(`
read_mem32 ((w2w ph_page ≪ 12 ‖ w2w l2_idx ≪ 2),mem') = read_mem32 ((w2w ph_page ≪ 12 ‖ w2w l2_idx ≪ 2),mem)
` by METIS_TAC[write_read_unch_thm]);

e( Q.UNABBREV_TAC `gAdd`);
e( Q.UNABBREV_TAC `aAdd`);

e(FULL_SIMP_TAC(srw_ss())[]);

(* ----------------------------------------- *)




e(RW_TAC (srw_ss()) []);
e(FULL_SIMP_TAC (srw_ss()) [invariant_page_type_l1_def]);

e(RW_TAC (srw_ss()) []);


e (PAT_ASSUM ``!x:word12. X`` (fn thm => ASSUME_TAC (SPEC ``(w2w(va:word32 ⋙ 20)):word12`` thm) ));
e(FULL_SIMP_TAC(srw_ss())[]);



e(`
(w2w
                  ((0xFFFFCw :word20) &&
                   (w2w
                      ((0xFFFFCw :word32) && (c2 :word32) ⋙ (12 :num)) :
                      word20)) :word32)≪ (12 :num)
=
(0xFFFFC000w :word32) && (c2 :word32)
` by blastLib.BBLAST_TAC);
e(FULL_SIMP_TAC (srw_ss()) []);

e(`(w2w (w2w ((va :word32) ⋙ (20 :num)) :word12) :
                       word32) =
((va :word32) ⋙ (20 :num))` by blastLib.BBLAST_TAC);
e(FULL_SIMP_TAC (srw_ss()) []);

e (FULL_SIMP_TAC (srw_ss()) [ LET_DEF]);

e(FULL_SIMP_TAC (srw_ss()) []);
e(FULL_SIMP_TAC (srw_ss()) []);


e(Cases_on `(w2w (0xFFFFCw && ph_page) ≪ 12 ‖ w2w pg_idx ≪ 2) = (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2)`);
e(FULL_SIMP_TAC (srw_ss()) []);


e (Q.ABBREV_TAC `l2_base:bool[22] = (rec'l1PTT
                (read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem))).addr`);
e( `l2_base = ptEntry.addr` by METIS_TAC[]);

e(FULL_SIMP_TAC (srw_ss()) []);

e( Q.UNABBREV_TAC `entryAddrL1`);
e( Q.UNABBREV_TAC `descL1`);
e(`
rec'l1PTT (read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem'))
=
rec'l1PTT (read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem))` by METIS_TAC []);

e (METIS_TAC []);


e (Q.ABBREV_TAC `l2_base:bool[22] = (rec'l1PTT
                (read_mem32 (0xFFFFC000w && c2 ‖ va ⋙ 20 ≪ 2,mem))).addr`);
e(FULL_SIMP_TAC (srw_ss()) []);
e( `ptEntry.addr= l2_base` by METIS_TAC[]);

e(FULL_SIMP_TAC (srw_ss()) []);


e(Cases_on `(w2w (0xFFFFCw && ph_page) ≪ 12 ‖ w2w pg_idx ≪ 2) = (w2w l2_base ≪ 10 ‖ 1020w && va ⋙ 10) `);

e (ASSUME_TAC ( (
SPECL [``
(entryAddrL2:word32)
``,
``
(page_desc':word32)
``,
``mem:bool[32]->bool[8]``,
``mem':bool[32]->bool[8]``] write_read_thm
)));
e( Q.UNABBREV_TAC `entryAddrL2`);
e(FULL_SIMP_TAC(srw_ss())[]);

(* e (PAT_ASSUM``(mem' = write_mem32 (entryAddrL2,mem,page_desc')) ⇒ *)
(*        (read_mem32 *)
(*           (entryAddrL2,write_mem32 (entryAddrL2,mem,page_desc')) = *)
(*         page_desc')`` (fn thm => IMP_RES_TAC thm)); *)
(* e RES_TAC; *)
e( Q.UNABBREV_TAC `mem'`);
e(FULL_SIMP_TAC(srw_ss())[]);
e( Q.UNABBREV_TAC `page_desc'`);
e(FULL_SIMP_TAC(srw_ss())[]);


e(`mmu_l1_decode_type (0xFFFFFFFCw && page_desc) = 0w` by FULL_SIMP_TAC(srw_ss())[mmu_l1_decode_type_def]);
e(FULL_SIMP_TAC (srw_ss()) []);



(* ------------------------------------------------ *)

e(`
(w2w l2_base ≪ 10 ‖ 1020w && va ⋙ 10)+3w >=+ 3w 
` by  (blastLib.BBLAST_TAC)); 
e(`((w2w ((0xFFFFCw :word20) && (ph_page :word20)) :word32) ≪ (12 :
        num) ‖ (w2w (pg_idx :word12) :word32) ≪ (2 :num))+ 3w >=+ 3w` by blastLib.BBLAST_TAC);

e(`((w2w ((0xFFFFCw :word20) && (ph_page :word20)) :word32) ≪ (12 :
        num) ‖ (w2w (pg_idx :word12) :word32) ≪ (2 :num)) =
( ((w2w ((0xFFFFCw :word20) && (ph_page :word20)) :word32) ≪ (12 :
        num) ‖ (w2w (pg_idx :word12) :word32) ≪ (2 :num))&& (0xFFFFFFFCw:word32))` 
by blastLib.BBLAST_TAC);
e(`((w2w (l2_base :22 word) :word32) ≪ (10 :num) ‖
        (1020w :word32) && (va :word32) ⋙ (10 :num)) =
(((w2w (l2_base :22 word) :word32) ≪ (10 :num) ‖
        (1020w :word32) && (va :word32) ⋙ (10 :num)) && 0xFFFFFFFCw:word32)`
by blastLib.BBLAST_TAC);


e (Q.ABBREV_TAC `gAdd:bool[32] =  ((w2w ((0xFFFFCw :word20) && (ph_page :word20)) :word32) ≪ (12 :
        num) ‖ (w2w (pg_idx :word12) :word32) ≪ (2 :num))`);
e (Q.ABBREV_TAC `aAdd:bool[32] = ((w2w (l2_base :22 word) :word32) ≪ (10 :num) ‖
        (1020w :word32) && (va :word32) ⋙ (10 :num))`);


e(`
 (((gAdd + 3w) <+ aAdd) \/ (gAdd >+ (aAdd + 3w)))`
by (FULL_SIMP_TAC(srw_ss())[mem_location_thm]));



e(`
read_mem32 ((w2w (0xFFFFCw && ph_page) ≪ 12 ‖ w2w pg_idx ≪ 2),mem') = read_mem32 ((w2w (0xFFFFCw && ph_page) ≪ 12 ‖ w2w pg_idx ≪ 2),mem)
` by METIS_TAC[write_read_unch_thm]);
e( Q.UNABBREV_TAC `gAdd`);
e( Q.UNABBREV_TAC `aAdd`);

e(FULL_SIMP_TAC(srw_ss())[]);


e(`
read_mem32 ((w2w (0xFFFFCw && ph_page) ≪ 12 ‖ w2w pg_idx ≪ 2),mem') = read_mem32 ((w2w (0xFFFFCw && ph_page) ≪ 12 ‖ w2w pg_idx ≪ 2),mem)
` by METIS_TAC[write_read_unch_thm]);

e( Q.UNABBREV_TAC `gAdd`);
e( Q.UNABBREV_TAC `aAdd`);

e(FULL_SIMP_TAC(srw_ss())[]);
